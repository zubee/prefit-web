<?php

use App\Tag;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class BasicElements extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'SuperAdmin',
            'description' => 'SuperAdmin',
        ]);
        Role::create([
            'name' => 'admin',
            'description' => 'admin',
        ]);
        Role::create([
            'name' => 'trainer',
            'description' => 'trainer',
            ]);
            
        Role::create([
            'name' => 'dietitian',
            'description' => 'dietitian',
        ]);
     
        Role::create([
            'name' => 'user',
            'description' => 'user',
        ]);

        User::create([
            'role_id' => 1,
            'name' => 'admin',
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'password' => \Hash::make(123456),
        ]);

        User::create([
            'role_id' => 6,
            'name' => 'admin-support',
            'username' => 'support',
            'email' => 'support@ftble.com',
            'password' => \Hash::make('onetwothreefourfivesix'),
        ]);

        Tag::create([
            'name' => 'tag1',
        ]);
        Tag::create([
            'name' => 'tag2',
        ]);
    }
}
