<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_days', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('client_id');
            $table->unsignedInteger('coach_id');
            $table->unsignedInteger('package_id');
            $table->unsignedInteger('plan_id');
            $table->unsignedInteger('day_number');
            $table->string('date')->nullable();

            $table->longText('workouts')->nullable();
            $table->longText('workout_ids')->nullable();
            $table->longText('workout_steps')->nullable();
            $table->longText('workout_reps')->nullable();
            $table->longText('workout_status')->nullable();
            
            $table->longText('measurements')->nullable();

            $table->longText('meals')->nullable();
            $table->longText('meal_images')->nullable();
            $table->longText('meal_title')->nullable();
            $table->longText('meal_summary')->nullable();
            $table->longText('meal_needs')->nullable();
            $table->longText('meal_how_to_do')->nullable();
            
            $table->longText('supplement_timings')->nullable();
            $table->longText('supplement_ids')->nullable();
            $table->longText('supplement_prescription')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_days');
    }
}
