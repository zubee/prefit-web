<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewPlanDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_plan_days', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('client_id');
            $table->unsignedInteger('coach_id');
            $table->unsignedInteger('package_id');
            $table->unsignedInteger('plan_id');
            $table->unsignedInteger('day_number');
            $table->string('date')->nullable();

            $table->longText('workouts')->nullable();  
                      
            $table->longText('measurements')->nullable();

            $table->longText('meals')->nullable();

            $table->longText('supplements')->nullable();
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('new_plan_days');
    }
}
