<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedInteger('sender_id');
            $table->unsignedInteger('reciever_id');
            $table->boolean('status')->default(1);
            $table->boolean('read_status')->default(0);
            $table->text('message')->nullable();
            
            $table->string('filename')->nullable();
            $table->string('realname')->nullable();
            $table->string('mime')->nullable();
            $table->string('file')->nullable();
            
            $table->boolean('is_questionnaire')->default(0);
            $table->unsignedInteger('questionnaire_id')->nullable();
            
            $table->timestamps();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chats');
    }
}
