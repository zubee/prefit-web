<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('role_id');
            $table->string('name')->nullable();
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('status')->default(1);
            $table->rememberToken();
            $table->string('source')->nullable();
            $table->string('photo')->nullable();

            $table->string('fcm_token')->nullable();
            
            $table->timestamp('email_verified_at')->nullable();


            $table->boolean('confirmed')->default(0);
            $table->boolean('first_login')->default(0);
            $table->string('confirmation_code')->nullable();
            $table->string('confirmation_time')->nullable();            
            
            $table->timestamps();
        });
    }

    




    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
