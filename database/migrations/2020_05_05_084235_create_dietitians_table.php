<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDietitiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dietitians', function (Blueprint $table) {
            $table->id();
            // $table->string('name')->nullable();
            // $table->string('email')->nullable();
            $table->unsignedInteger('user_id');
            $table->string('phone')->nullable();
            $table->string('percentage')->nullable();
            $table->string('photo')->nullable();
            $table->string('slug')->nullable();
            $table->text('portfolio')->nullable();
            $table->text('tags')->nullable();
            // $table->text('password')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dietitians');
    }
}
