<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkoutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workouts', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('Exercise_Id')->nullable();
            $table->text('description')->nullable();
            $table->string('tags')->nullable();
            $table->longText('media')->nullable();
            $table->longText('videos')->nullable();
            $table->timestamps();
        });
        DB::update('ALTER TABLE workouts AUTO_INCREMENT = 10000');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workouts');
    }
}
