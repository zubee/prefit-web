<?php

namespace App;

use stdClass;
use Illuminate\Database\Eloquent\Model;

class Plan_Day extends Model
{
        // protected $table = 'new_plan_days';
        protected $table = 'plan_days';
        protected $guarded = ['id'];
        protected $casts = [
                // 'workout_status' => 'Object'
                'workout_status' => 'Array'
        ];

        public function plan(){
                return $this->belongsTo(Client_Plan::class , 'plan_id');
        }

        public function normalise_data1(){
                 
                if($this->workouts && is_array(json_decode($this->workouts))){
                        
                        $workouts = json_decode($this->workouts);
                        $exrx_ids = [];

                        foreach($workouts as $k => $workout){
                                if(is_string($workout->Exercise_Id)){
                                        $workouts[$k]->workout = Workout::where('Exercise_Id' , $workouts[$k]->Exercise_Id)->first();
                                }else{
                                        $workouts[$k]->workout = Exercise::where('Exercise_Id' , $workouts[$k]->Exercise_Id)->first();
                                        $exrx_ids[] = $workout->Exercise_Id;

                                }

                        }
                        $this->workouts = $workouts;
                        $this->exrx_workout_ids = $exrx_ids;
                        
                }
                
                
                if($this->meals && is_array(json_decode($this->meals))){
                        $this->meals = json_decode($this->meals);
                        
                        foreach($this->meals as $meal){
                                $meal_names[] = $meal->meal;
                        }
                        $this->all_meals = $meal_names;
                }
                if($this->supplements && is_array(json_decode($this->supplements))){
                        $this->supplements = json_decode($this->supplements);
                        
                        $supplements = $this->supplements;
                        foreach($supplements as $k => $supplement){
                                $supplement_times[] = $supplement->supplement_time;

                                // dd($supplements[$k]->supplements);
                                foreach($supplements[$k]->supplements as $i => $curr_sup){
                                        $supplements[$k]->supplements[$i]->supplement = Supplement::find($supplements[$k]->supplements[$i]->supplement);
                                }
                        }
                        $this->supplement_times = $supplement_times;
                }
                
        }







        public function normalise_data(){
                // dd($this);
                
                // workouts 
                // $this->workouts = Workout::whereIn('id' , json_decode($this->workout_ids))->get();
                // if($this->workouts->count() > 0){

                //         foreach($this->workouts as $workout){
                                
                //                 $workout->description_cleaned = $workout->description;

                //                 $workout->media = json_decode($workout->media);
                //                 $workout->videos = json_decode($workout->videos);

                //                 $workout->description_cleaned = str_replace('<br>' , " \n " , $workout->description_cleaned );
                //                 $workout->description_cleaned = str_replace('<\li>' , " \n " , $workout->description_cleaned );
                                
                //                 $workout->description_cleaned = strip_tags( $workout->description_cleaned);
                //         }

                // }
                $this->exrx_workout_ids = [];

                if($this->workout_ids && is_array(json_decode($this->workout_ids)) ){

                        $local_workout_ids = [];
                        $exrx_workout_ids = [];
                        $workout_ids = json_decode($this->workout_ids);

                        foreach($workout_ids as $id){
                                if($id < 2500){
                                        $exrx_workout_ids[] = $id;
                                }else{
                                        $local_workout_ids[] = $id;
                                }
                        }
                        $local_workouts = Workout::whereIn('Exercise_Id' , $local_workout_ids )->get()->toArray();
                        foreach($local_workouts as $k => $workout){
                                // dd($workout->media);
                                $local_workouts[$k]['media'] = json_decode($workout['media']);
                        }
                        $exrx_workouts = Exercise::whereIn('Exercise_Id', $exrx_workout_ids )->get()->toArray();

                        $workouts = array_merge($local_workouts ,$exrx_workouts );
                        $this->workouts = $workouts;
                        $this->exrx_workout_ids = $exrx_workout_ids;
                        // dd($workouts);

                        // $this->workouts = Exercise::fetchExercises($this->workout_ids);
                        $this->workout_ids = json_decode($this->workout_ids);

                }else{
                        $this->workouts = [];
                        $this->workout_ids = [];
                        $this->workout_status = [];
                }

                // dd($this);
                // $this->workouts = EXRX::findExercises($this->workout_ids);
                
                
                // $this->workout_steps = json_decode($this->workout_steps);
                if($this->workout_steps && is_array(json_decode($this->workout_steps))){
                        $this->workout_steps = json_decode($this->workout_steps , true) ;
                        $new_array = array();
                        foreach($this->workout_steps as $k => $step){
                                // if(is_string($this->workout_ids[$k])){
                                //         $new_array[(string)$this->workout_ids[$k]] = $step;
                                // }else{
                                //         $new_array[$this->workout_ids[$k]] = $step;
                                // }
                                $new_array[$this->workout_ids[$k]] = $step;

                        }
                        $this->workout_steps = $new_array;
                        
                        // ->values()
                }else{
                        $this->workout_steps = [];
                }



                if($this->workout_reps && is_array(json_decode($this->workout_reps))){

                        $new_array = array();
                        $this->workout_reps = json_decode($this->workout_reps);

                        foreach($this->workout_reps as $k => $reps){
                                

                                // if(is_string($this->workout_ids[$k])){
                                //         $new_array[(string)$this->workout_ids[$k]] = $reps;
                                // }else{
                                //         $new_array[$this->workout_ids[$k]] = $reps;
                                // }
                                $new_array[$this->workout_ids[$k]] = $reps;
                                // $new_array[$k+10] = $reps;
                                
                        }
                        $this->workout_reps = $new_array;
                }else{
                        $this->workout_reps = [];
                }

                // dd($this);

                // dd($this->workout_ids);
                
                // dd($this->workout_steps  , is_array($this->workout_steps) );
                // if(is_object($this->workout_steps)){
                //         $this->workout_steps = (array) $this->workout_steps;
                // }
                // $this->workout_steps = array_values($this->workout_steps);

                
                
                // dd('asd' , $this->workout_steps , $this->workout_reps);


                // meals



                $this->meals = json_decode($this->meals);
                $this->meal_images = json_decode($this->meal_images);
                $this->meal_title = json_decode($this->meal_title);
                $this->meal_summary = json_decode($this->meal_summary);
                $this->meal_needs = json_decode($this->meal_needs);
                $this->meal_how_to_do = json_decode($this->meal_how_to_do);
                
                $this->meal_needs_cleaned = null;
                $this->meal_how_to_do_cleaned = null;       
                
                if(count($this->meals) > 0){
                        $needs = null;
                        $how_tos = null;

                        foreach($this->meals as $k => $meal){
                                
                                $need = null;
                                $how_to = null;
                                // $this->meal_needs_cleaned[] = $this->meal_needs[$k];
                                // $this->meal_how_to_do_cleaned[] = $this->meal_how_to_do[$k];
                                
                                if(isset($this->meal_needs[$k])){
                                        $need = $this->meal_needs[$k];
                                        $need = str_replace('<br>' , " \n " , $need );
                                        $need = str_replace('<\li>' , " \n " , $need );
                                        $need = strip_tags( $need);
                                        $needs[] = $need;

                                }
                                if(isset($this->meal_how_to_do[$k])){
                                        $how_to = $this->meal_how_to_do[$k];
                                        $how_to = str_replace('<br>' , " \n " , $how_to );
                                        $how_to = str_replace('<\li>' , " \n " , $how_to );
                                        $how_to = strip_tags( $how_to);
                                        $how_tos[] = $how_to;
                                        
                                }
                                
                        }
                        $this->meal_needs_cleaned = $needs;
                        $this->meal_how_to_do_cleaned = $how_tos;
                }

                $this->all_meals = null;
                if(count($this->meals) > 0){
                        $all_meals = [];
                        foreach($this->meals as $k => $meal){

                                $need = null;
                                $how_to = null;

                                $obj = new stdClass();
                                $obj->name = $meal;
                                $obj->image = $this->meal_images[$k] ?? "";
                                $obj->title = $this->meal_title[$k] ?? "";
                                $obj->summary = $this->meal_summary[$k] ?? "";
                                // $obj->need = $this->meal_needs[$k];
                                // $obj->how_to_do = $this->meal_how_to_do[$k];

                                $need = $this->meal_needs[$k] ?? "";
                                $need = str_replace('<br>' , " \n " , $need );
                                $need = str_replace('<\li>' , " \n " , $need );
                                $need = strip_tags( $need);
                                $obj->need = $need;
                                
                                $how_to = $this->meal_how_to_do[$k] ?? "";
                                $how_to = str_replace('<br>' , " \n " , $how_to );
                                $how_to = str_replace('<\li>' , " \n " , $how_to );
                                $how_to = strip_tags( $how_to);
                                $obj->how_to_do = $how_to;

                                $all_meals[] = $obj;
                        }
                        $this->all_meals = $all_meals;
                }


                // dd($this);
                // if(count($this->meals) > 0){
                //         $this->meal_needs = json_decode($this->meal_needs);
                //         $this->meal_how_to_do = json_decode($this->meal_how_to_do);        
                // }
                // suuplements
                if(is_array(json_decode($this->supplement_ids))){

                        $this->total_supplements =  count(json_decode($this->supplement_ids));
                        
                        foreach(json_decode($this->supplement_ids) as $supplement_ids){
                                if(isset($supplement_ids)){
                                        $array[] = Supplement::whereIn('id' , $supplement_ids)->get();
                                }
                        }
                        
                        $this->supplements = $array;
                }

                $this->supplement_timings = json_decode($this->supplement_timings);
                $this->supplement_prescription = json_decode($this->supplement_prescription);
                
                // dump(is_array($this->supplements));
                if(is_array($this->supplement_timings)){
                        foreach($this->supplement_timings as $k => $supplement_time){
                                foreach($this->supplements[$k] as $j => $supplement){
                                // dump($supplement);
                                        $supplement->addLineBreaks();
                                }
                        }
                        // dd('sad');
                }


                if($this->measurements){
                        $this->measurements = json_decode($this->measurements);
                    }
                return $this;
        }










        // public function normalise_data(){
        //         // dd($this);
                
        //         // workouts 
        //         // $this->workouts = Workout::whereIn('id' , json_decode($this->workout_ids))->get();
        //         // if($this->workouts->count() > 0){

        //         //         foreach($this->workouts as $workout){
                                
        //         //                 $workout->description_cleaned = $workout->description;

        //         //                 $workout->media = json_decode($workout->media);
        //         //                 $workout->videos = json_decode($workout->videos);

        //         //                 $workout->description_cleaned = str_replace('<br>' , " \n " , $workout->description_cleaned );
        //         //                 $workout->description_cleaned = str_replace('<\li>' , " \n " , $workout->description_cleaned );
                                
        //         //                 $workout->description_cleaned = strip_tags( $workout->description_cleaned);
        //         //         }
        //         // }
                
        //         if(is_array(json_decode($this->workout_ids))){
        //                 $exrx_ids = [];
        //                 $local_ids = [];
        //                 // dump(json_decode($this->workout_ids));
        //                 foreach(json_decode($this->workout_ids) as $id){
                                
        //                         if(is_string($id)){
        //                                 $local_ids[] = $id;
        //                         }else{
        //                                 $exrx_ids[] = $id;
        //                         } 
        //                 }
        //                 // strpos($myString, 'Bob') !== false;
        //                 // dd($exrx_ids);
        //                 $exrx_workouts = Exercise::fetchExercises(json_encode($exrx_ids));
        //                 $local_workouts = Workout::whereIn('Exercise_Id' , $local_ids )->get()->toArray();
                        
        //                 // dd($local_workouts);
        //                 // $workouts[] = $local_workouts;
        //                 // $workouts[] = $exrx_workouts;
        //                 $this->exrx_workout_ids = $exrx_ids;
        //                 $this->workouts = array_merge($local_workouts , $exrx_workouts);
        
        //                 // dd($this->exrx_workout_ids);
        //                 // dd($exrx_workouts , $local_workouts );
        
        //                 $this->workout_ids = json_decode($this->workout_ids);
        
        //                 // $this->workout_steps = json_decode($this->workout_steps);
        
                        
        //         }else{
        //                 $this->workout_ids = [];
        //                 $this->workouts = [];
        //         }

        //         if(!$this->workout_status){
        //                 $this->workout_status = [];
        //         }


                
        //         // dd($this->workout_steps  , is_array($this->workout_steps) );
        //         // if(is_object($this->workout_steps)){
        //         //         $this->workout_steps = (array) $this->workout_steps;
        //         // }
        //         // $this->workout_steps = array_values($this->workout_steps);
                

        //         if(is_array(json_decode($this->workout_steps))){
        //                 $new_array = array();
        //                 // dd($this->workout_steps);
        //                 foreach(json_decode($this->workout_steps) as $k => $step){
        //                         $new_array[$this->workout_ids[$k]] = $step;
        //                 }
        //                 $this->workout_steps = json_decode($this->workout_steps , true) ;
        
        //                 $this->workout_steps = $new_array;
        //         }else{
        //                 $this->workout_steps = [];
        //         }

                
        //         if(is_array(json_decode($this->workout_reps))){
        //                 $new_array = array();
        //                 // ->values()                       
                        
        //                 foreach(json_decode($this->workout_reps) as $k => $reps){
        //                         $new_array[$this->workout_ids[$k]] = $reps;
        //                         // $new_array[$k+10] = $reps;
                                
        //                 }
        //                 $this->workout_reps = $new_array;
        //                 // dd('asd' , $this->workout_steps , $this->workout_reps);
        //         }else{
        //                 $this->workout_reps = [];
        //         }



        //         // meals



        //         $this->meals = json_decode($this->meals);
        //         $this->meal_images = json_decode($this->meal_images);
        //         $this->meal_title = json_decode($this->meal_title);
        //         $this->meal_summary = json_decode($this->meal_summary);
        //         $this->meal_needs = json_decode($this->meal_needs);
        //         $this->meal_how_to_do = json_decode($this->meal_how_to_do);
                
        //         $this->meal_needs_cleaned = null;
        //         $this->meal_how_to_do_cleaned = null;       
                
        //         if(count($this->meals) > 0){
        //                 $needs = null;
        //                 $how_tos = null;

        //                 foreach($this->meals as $k => $meal){
                                
        //                         $need = null;
        //                         $how_to = null;
        //                         // $this->meal_needs_cleaned[] = $this->meal_needs[$k];
        //                         // $this->meal_how_to_do_cleaned[] = $this->meal_how_to_do[$k];
                                
        //                         if(isset($this->meal_needs[$k])){
        //                                 $need = $this->meal_needs[$k];
        //                                 $need = str_replace('<br>' , " \n " , $need );
        //                                 $need = str_replace('<\li>' , " \n " , $need );
        //                                 $need = strip_tags( $need);
        //                                 $needs[] = $need;

        //                         }
        //                         if(isset($this->meal_how_to_do[$k])){
        //                                 $how_to = $this->meal_how_to_do[$k];
        //                                 $how_to = str_replace('<br>' , " \n " , $how_to );
        //                                 $how_to = str_replace('<\li>' , " \n " , $how_to );
        //                                 $how_to = strip_tags( $how_to);
        //                                 $how_tos[] = $how_to;
                                        
        //                         }
                                
        //                 }
        //                 $this->meal_needs_cleaned = $needs;
        //                 $this->meal_how_to_do_cleaned = $how_tos;
        //         }

        //         $this->all_meals = null;
        //         if(count($this->meals) > 0){
        //                 $all_meals = [];
        //                 foreach($this->meals as $k => $meal){

        //                         $need = null;
        //                         $how_to = null;

        //                         $obj = new stdClass();
        //                         $obj->name = $meal;
        //                         $obj->image = $this->meal_images[$k] ?? "";
        //                         $obj->title = $this->meal_title[$k] ?? "";
        //                         $obj->summary = $this->meal_summary[$k] ?? "";
        //                         // $obj->need = $this->meal_needs[$k];
        //                         // $obj->how_to_do = $this->meal_how_to_do[$k];

        //                         $need = $this->meal_needs[$k] ?? "";
        //                         $need = str_replace('<br>' , " \n " , $need );
        //                         $need = str_replace('<\li>' , " \n " , $need );
        //                         $need = strip_tags( $need);
        //                         $obj->need = $need;
                                
        //                         $how_to = $this->meal_how_to_do[$k] ?? "";
        //                         $how_to = str_replace('<br>' , " \n " , $how_to );
        //                         $how_to = str_replace('<\li>' , " \n " , $how_to );
        //                         $how_to = strip_tags( $how_to);
        //                         $obj->how_to_do = $how_to;

        //                         $all_meals[] = $obj;
        //                 }
        //                 $this->all_meals = $all_meals;
        //         }


        //         // dd($this);
        //         // if(count($this->meals) > 0){
        //         //         $this->meal_needs = json_decode($this->meal_needs);
        //         //         $this->meal_how_to_do = json_decode($this->meal_how_to_do);        
        //         // }
        //         // suuplements
        //         if(is_array(json_decode($this->supplement_ids))){

        //                 $this->total_supplements =  count(json_decode($this->supplement_ids));
                        
        //                 foreach(json_decode($this->supplement_ids) as $supplement_ids){
        //                         if(isset($supplement_ids)){
        //                                 $array[] = Supplement::whereIn('id' , $supplement_ids)->get();
        //                         }
        //                 }
                        
        //                 $this->supplements = $array;
        //         }

        //         $this->supplement_timings = json_decode($this->supplement_timings);
        //         $this->supplement_prescription = json_decode($this->supplement_prescription);
                
        //         // dump(is_array($this->supplements));
        //         if(is_array($this->supplement_timings)){
        //                 foreach($this->supplement_timings as $k => $supplement_time){
        //                         foreach($this->supplements[$k] as $j => $supplement){
        //                         // dump($supplement);
        //                                 $supplement->addLineBreaks();
        //                         }
        //                 }
        //                 // dd('sad');
        //         }


        //         if($this->measurements){
        //                 $this->measurements = json_decode($this->measurements);
        //             }
        //         return $this;
        // }
}
