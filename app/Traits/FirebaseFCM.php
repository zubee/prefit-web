<?php

namespace App\Traits;



use FCM;
use Push;

use stdClass;
use LaravelFCM\Message\Topics;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;


trait FirebaseFCM 
{

    
    public function PublishBroadcast($reciever , $sender , $data , $title = 'Empty' , $description = '' , $notification_type = 'notification' ){

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder( $title );
        $notificationBuilder->setBody($description)
                            // ->setClickAction($notification_type)
                            ->setSound('default');
                            // ->setBadge('badge');
                // ->setClickAction('http://localhost:8000/myaccount/messages/');

        $dataBuilder = new PayloadDataBuilder();

        $_data = new stdClass();
        $_sender = new stdClass();
        $_data->notif_type = $data->notif_type;

        $_sender->name = $sender->name ?? "";
        $_sender->email = $sender->email ?? "";
        $_sender->username = $sender->username ?? "";
        // dd($data , $_data);
        $dataBuilder->addData([
            'sender_name' => $reciever->name,
            'data' => $_data,
            'sender' => $_sender,
            // 'data' => $data,
            ]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        
        $token = $reciever->fcm_token;
        
        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
        // dd($data , $downstreamResponse->tokensWithError());
        return $downstreamResponse->numberSuccess();

        // \LaravelFCM\Message\PayloadNotificationBuilder LaravelFCM\Message\PayloadNotificationBuilder::setClickAction(String $action)

    }


}


// $notificationBuilder = new PayloadNotificationBuilder('my title');
// $notificationBuilder->setBody('Hello world')
// 				    ->setSound('default');

// $notification = $notificationBuilder->build();

// $topic = new Topics();
// $topic->topic('news');

// $topicResponse = FCM::sendToTopic($topic, null, $notification, null);

// $topicResponse->isSuccess();
// $topicResponse->shouldRetry();
// $topicResponse->error();


// public function notifyUser($user , $message){
//     // dd($user);
//     $optionBuilder = new OptionsBuilder();
//     $optionBuilder->setTimeToLive(60*20);

//     $notificationBuilder = new PayloadNotificationBuilder('You have A new message : ' .$user->name);
//     $notificationBuilder->setBody('New Message broadcast')
//                         ->setSound('default');
//                         // ->setClickAction('http://localhost:8000/myaccount/messages/');

//     $dataBuilder = new PayloadDataBuilder();
//     $dataBuilder->addData([
//         'sender_name' => $user->name,
//         'message' => $message,
//         ]);

//     $option = $optionBuilder->build();
//     $notification = $notificationBuilder->build();
//     $data = $dataBuilder->build();

//     $token = $user->fcm_token;

//     $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
    
//     return $downstreamResponse->numberSuccess();

//     // \LaravelFCM\Message\PayloadNotificationBuilder LaravelFCM\Message\PayloadNotificationBuilder::setClickAction(String $action)

// }
