<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client_Payment extends Model
{
        protected $guarded = ['id'];
        protected $table = 'client_payments';

    
        
        public function client()
        {
            return $this->belongsTo(User::class , 'client_id');
        }

        public function coach()
        {
            return $this->belongsTo(Coach::class , 'coach_id');
        }
        
        public function plan()
        {
            return $this->belongsTo(CLient_Plan::class , 'plan_id');
        }
        
}
