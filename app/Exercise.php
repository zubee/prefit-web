<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exercise extends Model
{
        protected $guarded = ['id'];
        protected $casts = [
                'exercise' => 'object',
                ];



        // public function scopeOfMainExercise($query){
        //         dd($query);
        // }
        
        public static function fetchExercises($ids){
                // dd($ids);
                $all = self::whereIn('exercise_id' , json_decode($ids ))->get();
                // dd($all );
                $exercises = [];
                foreach($all as $single ){
                        // self::getEXRCWrokout($single);
                        $exercises[] = $single->exercise;
                }
                // dd($exercises , 'ss');
                return $exercises;
        }


        public static function getEXRCWrokout($exercise){
                return $exercise->exercise;
        }

}
