<?php

namespace App;

use Carbon\Carbon;
use App\Jobs\SendEmail;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
        protected $guarded = ['id'];


        public function checkIfNew(){

                $todays_chat = Chat::where('sender_id' ,  $this->reciever_id )->whereDate('created_at' , Carbon::now()) ->get();

                if($todays_chat->count() == 0){
                        
                        $users = User::where('id' , $this->reciever_id)->get();
                        $subject = 'You have a new message';
                        
                        if($this->reciever->role_id == 6){
                                $body = 'Hi Admin <br>'. $this->sender->name . ' sent you a new message'  ;
                        }else{
                                $body = 'Hi '.$this->reciever->name.' <br>'. $this->sender->name . ' sent you a new message for you'  ;
        
                        }
                        dispatch(new SendEmail($users , $subject , $body ));
                }

        }

        public function sender(){
                return $this->belongsTo(User::class , 'sender_id');
        }

        public function reciever(){
                return $this->belongsTo(User::class , 'reciever_id');
        }


        public function questionnaire(){
                return $this->belongsTo(Client_Questionnaire::class , 'questionnaire_id');
        }

        public function getProperTimeAttribute($value){
                return $this->created_at->diffForHumans();
        }

}
