<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coach_Tag extends Model
{
        protected $table = 'coach_tags';
        protected $guarded = ['id'];


        public function coach(){
                return $this->belongsTo(Coach::class , 'coach_id');
        }

        public function tag(){
                return $this->belongsTo(Tag::class , 'tag_id');
        }

}
