<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */

    public $users;
    public $subject;
    public $body;

    public function __construct($users , $subject , $body )
    {
        $this->users = $users; 
        $this->subject = $subject; 
        $this->body = $body; 
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data['msg'] = $this->body ;
        
        foreach($this->users as $user){
            
            $send['email'] = $user->email;
            $send['name'] = $user->name;
            
            // dd($user , $data);
            \Mail::send('mails.general', $data, function ($message) use ($send) {
                // $message->from('dawami@dawami.com', 'Dawami');
                // $message->sender('dawami@dawami.com', 'Dawami');
                $message->to( $send['email'] , $send['name']);
                $message->subject($this->subject);
            });   
    }
    }
}
