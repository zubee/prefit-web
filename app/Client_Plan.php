<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Client_Plan extends Model
{
    
        protected $appends = ['plan_progress' , 'end_date' , 'is_completed'];
        protected $table = 'client_plans';
        protected $guarded = ['id'];

        protected static function boot()
        {
            static::retrieved(function($plan){

                // dump($plan->plan_progress);

                $payment = Client_Payment::where('package_id' , $plan->package_id )->
                where('coach_id' , $plan->coach_id )->
                where('client_id' , $plan->client_id )->
                first();

                if($payment){
                    $payment->plan_id = $plan->id;
                    $payment->save();
                }

                // $coach->average_ratings = $coach->average_ratings ;
            });

            parent::boot();
        }

        public function days(){
                return $this->hasMany(Plan_Day::class , 'plan_id');
        }

        public function getPlanProgressAttribute(){

            $total_days = $this->duration;
            $progress = "1";
            $progressed_days = Carbon::parse($this->start_date)->diffInDays(Carbon::now() );
            $progress = (string)ceil($progressed_days/$total_days * 100);
            
            if($progressed_days > $total_days){
                $progress = "100";
            }
            // $progressed_days = Carbon::now()->diffInDays(Carbon::parse($this->start_date) );
            // dump($progressed_days , $total_days );
            // dump($progress );
            $this->plan_progress = $progress; 
            return $progress;
            // dd($this, $total_days, $progressed_days ,  );
        }

        public function getEndDateAttribute(){
            if($this->start_status == 1){
                return $this->end_date = Carbon::parse($this->start_date)->addDays($this->duration)->toDateString();
            }else{
                return $this->end_date = null;
            }
        }

        public function getIsCompletedAttribute(){
            if($this->plan_progress == '100'){
                return true;
            }
            return false;
        }
        // public function getWeeksAttribute(){
        //         return ceil($this->duration/7);
        //     }

        public function getTotalWeeks(){
                if($this->start_status == 1){

                        $plan_start_date = Carbon::parse($this->start_date);

                        $counter = 1;
                        $all_weeks = [];
                            for($i = 1 ; $counter <= $this->duration  ; $i++ ){

                                for($j = 1; $j <= 7; $j++){

                                    if(array_sum(array_map("count", $all_weeks)) < $this->duration){
                
                                        $all_weeks[$i][$j]['date'] = Carbon::parse($this->start_date)->addDays($counter)->toDateString();
                                        $all_weeks[$i][$j]['day'] = Carbon::parse($this->start_date)->addDays($counter)->format('l') ;
                                        $all_weeks[$i][$j]['day_number'] = $counter ;

                                        $counter ++;
                                        
                                        if(Carbon::parse($this->start_date)->addDays($counter)->format('l') == 'Sunday'){
                                            break;
                                        }
                                    }
                                }
                            }

                        $this->total_weeks = $all_weeks;
                    }else{
                        $counter = 1;
                        $default_weeks = [];
                        for($i = 1 ; $counter <= $this->duration  ; $i++ ){
                            for($j = 1; $j <= 7; $j++){
                                if(array_sum(array_map("count", $default_weeks)) < $this->duration){
            
                                    $default_weeks[$i][$j] = $counter;
                                    $counter ++;
                                }
                            }
                        }
                        $this->total_weeks = $default_weeks;
            
                    }

                return $this;
        }



        public function getTotalWeeksAPI(){
                if($this->start_status == 1){
                        $counter = 1;
            
                        $all_weeks = [];
                            for($i = 1 ; $counter <= $this->duration  ; $i++ ){
                            // for($i = 1 ; $i <= $weeks ; $i++ ){
                                
                                // foreach()
                                // $all_weeks[] = Carbon::now()->addDays()
                                for($j = 1; $j <= 7; $j++){
                                    // dump($i * $j );
                                    if(array_sum(array_map("count", $all_weeks)) < $this->duration){
                
                                        $all_weeks[$i][$j]['date'] = Carbon::now()->addDays($counter)->toDateString();
                                        $all_weeks[$i][$j]['day'] = Carbon::now()->addDays($counter)->format('l') ;
                                        // dump($counter);
                                        $counter ++;
                                        
                                        if(Carbon::now()->addDays($counter)->format('l') == 'Sunday'){
                                            break;
                                        }
                                    }
                                }
                            }
                        $this->total_weeks = $all_weeks;
                    }

                return $this;
        }




        public function package(){
                return $this->belongsTo(Package::class , 'package_id');
        }



        public function coach(){
                return $this->belongsTo(Coach::class , 'coach_id');
        }



        public function client(){
                return $this->belongsTo(User::class , 'client_id');
        }


        public function payment(){
            return $this->hasOne(Client_Payment::class , 'plan_id');
        }


        public function review(){
            return $this->hasOne(Review::class , 'plan_id');
        }

        public function reviews(){
            return $this->hasMany(Review::class , 'plan_id');
        }

}
















// public function getTotalWeeks(){
//     if($this->start_status == 1){

//             $plan_start_date = Carbon::parse($this->start_date);
//             // dd($plan_start_date , $this->start_date);
//             $counter = 1;
//             $all_weeks = [];
//                 for($i = 1 ; $counter <= $this->duration  ; $i++ ){
//                 // for($i = 1 ; $i <= $weeks ; $i++ ){
                    
//                     // foreach()
//                     // $all_weeks[] = Carbon::now()->addDays()
//                     for($j = 1; $j <= 7; $j++){
//                         // dump($i * $j );
//                         if(array_sum(array_map("count", $all_weeks)) < $this->duration){
    
//                             // $all_weeks[$i][Carbon::now()->addDays($counter)->toDateString()] = Carbon::now()->addDays($counter)->format('l') ;

//                             $all_weeks[$i][$j]['date'] = Carbon::parse($this->start_date)->addDays($counter)->toDateString();
//                             $all_weeks[$i][$j]['day'] = Carbon::parse($this->start_date)->addDays($counter)->format('l') ;

//                             // dump($all_weeks , $counter , Carbon::parse($this->start_date)->addDays($counter)->toDateString() );
//                             // dump($counter);
//                             $counter ++;
                            
//                             if(Carbon::parse($this->start_date)->addDays($counter)->format('l') == 'Sunday'){
//                                 break;
//                             }
//                         }
//                     }

//                     // dd($all_weeks);
//                 }
//                 // dd('asd');
//             $this->total_weeks = $all_weeks;
//         }else{
//             $counter = 1;
//             $default_weeks = [];
//             for($i = 1 ; $counter <= $this->duration  ; $i++ ){
//                 for($j = 1; $j <= 7; $j++){
//                     if(array_sum(array_map("count", $default_weeks)) < $this->duration){

//                         $default_weeks[$i][$j] = $counter;
//                         $counter ++;
//                     }
//                 }
//             }
//             // $this->duration = $this->package->duration;
//             // $this->weeks = json_encode($default_weeks);
//             // $this->save();

//             $this->total_weeks = $default_weeks;

//         }

//     return $this;
// }



