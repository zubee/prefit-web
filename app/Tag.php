<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    protected $fillable = ['name'];


    public function users(){
        return $this->belongsToMany('App\Tag' , 'tags_users' , 'tag_id' , 'user_id');
    }

    public function coaches(){
        return $this->belongsToMany(Coach::class , 'coach_tags' , 'tag_id' , 'coach_id')->withPivot('tag_id' , 'coach_id');
    }


    public function supplements(){
        return $this->belongsToMany('App\Supplement' , 'supplements_tags');
    }

    public function workouts(){
        return $this->belongsToMany('App\Workout' , 'workouts_tags');
    }

}
