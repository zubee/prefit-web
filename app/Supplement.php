<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplement extends Model
{
    //
    protected $fillable = ['name', 'description', 'image','tag'];

    public function tags(){
        return $this->belongsToMany('App\Tag' , 'supplements_tags');
    }

    public function addLineBreaks()
    {

        $this->description_cleaned = $this->description;
        $this->description_cleaned = str_replace('<br>' , " \n " , $this->description_cleaned );
        $this->description_cleaned = str_replace('<\li>' , " \n " , $this->description_cleaned );

        $this->description_cleaned = strip_tags( $this->description_cleaned);

        return $this;
    }

}
