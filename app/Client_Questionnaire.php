<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client_Questionnaire extends Model
{
        protected $guarded = ['id'];
        protected $table = 'client_questionnaires';
        protected $appends = ['created_date'];


        public function getCreatedDateAttribute(){
                return $this->created_at->toDateString();
        }

        public function scopeRelated($query){
                $query->where('coach_id' , auth()->user()->coach->id );
        }


        public function scopeActive($query){
                $query->where('active' , true );
        }

        public function questions(){
                return $this->hasMany(Client_Question::class , 'questionnaire_id' , 'id');
        }


        public function coach(){
                return $this->belongsTo(Coach::class);
        }


        public function users(){
                return $this->belongsToMany(User::class , 'assigned_clients_questionaires' , 'questionaire_id' , 'user_id')->withPivot('created_at')->withTimestamps() ;
        }
        

        public function answersOfClient($user_id){
                
                foreach($this->questions as $question){
                        $question->answer = $question->answer($user_id);
                        if($question->type ==  'checkbox' && $question->answer){
                                $question->answer = json_decode($question->answer->answer);
                        }
                }
        }

}
