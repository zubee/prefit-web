<?php

namespace App\Http\Controllers;

use App\Tag;
use App\User;
use App\Package;
use App\Perfit_Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages = Package::all();


        return view('admin.packages.index', compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::whereIn('role_id',[3,4])->get();
        
        $tags = Tag::all();

        return view('admin.packages.create',compact('users' , 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());


        $request->validate([
            'price' => 'numeric|digits_between:0,4',
        ]);

        $package = Package::create([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
            'highlights' => $request->highlights,
            'duration' => $request->duration,
            'paid' => $request->paid ?? 0,
        ]);

        $package->coaches()->attach($request->assigned);
        
        $package->tags()->attach($request->tags);

        if($request->has('image')){

            $file = Perfit_Image::imageResize($request->image);
            $ext = $request->image->getClientOriginalExtension();
                            
            $file->save(public_path().'/images/packages/' . $package->id .'.'. $ext , 90);
            $package->image = $package->id .'.'. $ext;
            $package->save();
        }
        

        if($package)
        return redirect('/admin/packages')->with('success' , 'Package created successfully');
        else
        return redirect()->back()->with('error' , 'Some problem occured');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function show(Package $package)
    {
        // dd($package);

        return view('admin.packages.show' , compact('package'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package)
    {
        $users = User::whereIn('role_id',[3,4])->get();
        
        $tags = Tag::all();
        
        return view('admin.packages.edit' , compact('package','users' , 'tags'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Package $package)
    {

        $request->validate([
            'price' => 'numeric|digits_between:1,4',
        ]);

        $package->name = $request->name;
        $package->price = $request->price;
        $package->description = $request->description;
        $package->highlights = $request->highlights;
        $package->duration = $request->duration;
        $package->paid = $request->paid ?? 0;
        $package->save();

        $package->coaches()->sync($request->assigned);

        $package->tags()->sync($request->tags);


        if($request->has('image')){
            $path = public_path().'/images/packages/'.$package->image;

            if(file_exists($path)){
                    File::delete($path);
                }

            $file = Perfit_Image::imageResize($request->image);
            $ext = $request->image->getClientOriginalExtension();

            $file->save(public_path().'/images/packages/' . $package->id .'.'. $ext , 90);
    
            // $request->image->move(public_path().'/images/packages/' , $package->id .'.'. $ext);
            $package->image = $package->id .'.'. $ext;

        }


        if($package->save())
        return redirect('/admin/packages')->with('success' , 'Package updated successfully');
        else
        return redirect()->back()->with('error' , 'Some problem occured');
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        $package->delete();
        return redirect()->back()->with('success' , 'Package deleted successfully');

    }

    public function assignedto($id){


        $package = Package::find($id);
        return view('admin.packages.assignedto', compact('package'));
    }
}
