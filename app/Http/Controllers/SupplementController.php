<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Supplement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class SupplementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $supplements = Supplement::all();

        return view('admin.supplements.index',compact('supplements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tags = Tag::all();
        return view('admin.supplements.create' , compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required',
        ]);

        $supplement = Supplement::create([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'tag' => '',
        ]);

        $supplement->tags()->attach($request->tags);

        if($request->has('image')){
            $ext = $request->image->getClientOriginalExtension();
            $request->image->move(public_path().'/images/supplements/' , $supplement->name.$supplement->id .'.'. $ext);
            $supplement->image =$supplement->name.$supplement->id .'.'. $ext;
            $supplement->save();
        }

        if($supplement)
            return redirect()->route('admin.supplements.index')->with('success','Supplement Added Successfully');
        else
            return redirect()->back()->with('error','There are some Problem, Try Again');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tags = Tag::all();

        $supplement = Supplement::find($id);
        return view('admin.supplements.edit',compact('supplement' , 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'name' => 'required',
        ]);

        $supplement = Supplement::find($id);
        $supplement->name = $request->get('name');
        $supplement->description = $request->get('description');
        // $supplement->tag = $request->get('tag');

        $supplement->tags()->sync($request->tags);


        if($request->has('image')){
            $path = public_path().'/images/supplements/'.$supplement->image;

            if(file_exists($path)){
                File::delete($path);
            }
            $ext = $request->image->getClientOriginalExtension();
            $request->image->move(public_path().'/images/supplements/' , $supplement->name.$supplement->id .'.'. $ext);

            $supplement->image = $supplement->name.$supplement->id .'.'. $ext;

        }
        $supplement->save();

        if($supplement)
            return redirect()->route('admin.supplements.index')->with('success','Supplement Updated Successfully');
        else
            return redirect()->back()->with('error','There is Some Problem, Try Again');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $supplement = Supplement::find($id);

        $path = public_path().'/images/supplements/'.$supplement->image;

        if(file_exists($path)){
            File::delete($path);
        }

        $supplement->delete();

        return redirect()->route('admin.supplements.index')->with('success','Supplements Deleted Successfully');
    }
}
