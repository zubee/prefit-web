<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Client_Payment;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PaymentExport;

class FinancialReportsController extends Controller
{
    public function getReportForAdmin(){
        
        $payments = Client_Payment::latest()->get();

        return view('admin.payments.index' , compact('payments'));
    }

    public function payToCoach($payment_id){

        $payment = Client_Payment::find($payment_id);

        $payment->status = 1;
        $payment->payment_clear_date = Carbon::now()->toDateString();
        $payment->save();

        return redirect()->back()->with('success' , 'Payment Made successfully');

    }
    public function removePayToCoach($payment_id){

        $payment = Client_Payment::find($payment_id);

        $payment->status = 0;
        $payment->payment_clear_date = null;
        $payment->save();

        return redirect()->back()->with('success' , 'Payment remove successfully');

    }


    
    public function filterPayments(Request $request){
        
        // dd($request->all());
        
        $filters = $request->all();
        
        // dd($filters);

        $filterd_payments = Client_Payment::all();



        if($filters['lower_date'] && !$filters['upper_date'] ){
           
            $filterd_payments = Client_Payment::whereDate('created_at', '>=',  $filters['lower_date'])->get();

        }elseif(!$filters['lower_date'] && $filters['upper_date'] ){
           
            $filterd_payments = Client_Payment::whereDate('created_at', '<=',  $filters['upper_date'])->get();

        }elseif($filters['lower_date'] && $filters['upper_date'] ){
           
            $filterd_payments = Client_Payment::where('created_at', '>=', $filters['lower_date'] )->whereDate('created_at' ,'<=', $filters['upper_date'] )->get();

        }else{
            $filterd_payments = Client_Payment::all();
        }


       
        if(isset($filters['status'])){
            $filterd_payments = $filterd_payments->where('status' , $filters['status'] );
        }
        

        if($filters['client_name']){
            $all_words = explode(' ' , $request->client_name );
            
            $filterd_payments = Client_Payment::with('client')->whereIn('id' , $filterd_payments->pluck('id')->toArray())->whereHas( 'client' , function($payment) use ($all_words) {
                foreach($all_words as $word){
                    $payment->where('name','like', '%' . $word . '%');
                }
            } )->get();
        }
        
        
        if($filters['coach_name']){
            $all_words = explode(' ' , $request->coach_name );
            
            $filterd_payments = Client_Payment::with('client')->whereIn('id' , $filterd_payments->pluck('id')->toArray())->whereHas( 'coach.user' , function($payment) use ($all_words) {
                foreach($all_words as $word){
                    $payment->where('name','like', '%' . $word . '%');
                }
            } )->get();
        }
        
        
    
        
        return view('admin.payments.index', compact('filterd_payments' , 'filters'));


    }

    public function getReportForCoach(){
        
        $payments = Client_Payment::where('coach_id' , auth()->user()->coach->id )->latest()->get();

        return view('coach.payments.index' , compact('payments'));
    }


    public function filterPaymentsCoach(Request $request){
        
        // dd($request->all());
        
        $filters = $request->all();
        
        // dd($filters);

        $filterd_payments = Client_Payment::where('coach_id' , auth()->user()->coach->id )->get();



        if($filters['lower_date'] && !$filters['upper_date'] ){
           
            $filterd_payments = Client_Payment::where('coach_id' , auth()->user()->coach->id )->whereDate('created_at', '>=',  $filters['lower_date'])->get();

        }elseif(!$filters['lower_date'] && $filters['upper_date'] ){
           
            $filterd_payments = Client_Payment::where('coach_id' , auth()->user()->coach->id )->whereDate('created_at', '<=',  $filters['upper_date'])->get();

        }elseif($filters['lower_date'] && $filters['upper_date'] ){
           
            $filterd_payments = Client_Payment::where('coach_id' , auth()->user()->coach->id )->where('created_at', '>=', $filters['lower_date'] )->whereDate('created_at' ,'<=', $filters['upper_date'] )->get();

        }else{
            $filterd_payments = Client_Payment::where('coach_id' , auth()->user()->coach->id )->get();
        }


       
        if(isset($filters['status'])){
            $filterd_payments = $filterd_payments->where('status' , $filters['status'] );
        }
        

        if($filters['client_name']){
            $all_words = explode(' ' , $request->client_name );
            
            $filterd_payments = Client_Payment::with('client')->where('coach_id' , auth()->user()->coach->id )->whereIn('id' , $filterd_payments->pluck('id')->toArray())->whereHas( 'client' , function($payment) use ($all_words) {
                foreach($all_words as $word){
                    $payment->where('name','like', '%' . $word . '%');
                }
            } )->get();
        }
        
        return view('coach.payments.index', compact('filterd_payments' , 'filters'));


    }



    public function exportPayments($payment_ids){
        
        $payments = Client_Payment::latest()->whereIn('id' , json_decode($payment_ids))->get();
        // dd($payments);
        return Excel::download(new PaymentExport($payments) , 'finantial-reports.xlsx' );
        // return Excel::download(new ExportClients($clients), 'clients.xlsx');

    }


}
