<?php

namespace App\Http\Controllers;
use App\EXRX;


use App\Exercise;
use Illuminate\Http\Request;

class EXRXController extends Controller
{
    public function search($query)
    {

        // dd($query);

        $response = EXRX::searchExercises($query);
        
        if(isset($response) && is_object($response) && $response->exercises){
            
            $exercises = $response->exercises;

            $this->storeExcersies($exercises);

            return response()->json(['success' => true  , 'exercises' => $exercises]);

        }else{
            $error = $response;
            return response()->json(['success' => false , 'error' => $error] , 422);

        }
        
        // $exercises = Exercise::all();

    //  else{
    //         return response()->json(['success' => false , 'error' => $exercises] , 422);
    //     }
    }


    public function findExercises($ids){

        $response = EXRX::findExercises($ids);
        // $exercises = EXRX::findExercises(json_decode($ids));
        if(isset($response) && is_object($response) && $response->exercises){
            
            $exercises = $response->exercises;
            
            $this->storeExcersies($exercises);
            // if(is_array($exercises)){
            //     foreach($exercises as $exercise){
            //         Exercise::updateOrCreate(
            //             [ 'Exercise_Id' => $exercise->Exercise_Id],
            //             [
            //                 'Exercise_Id' => $exercise->Exercise_Id,
            //                 'exercise' => $exercise
            //             ]);
            //     }
            // }

            
            return response()->json(['success' => true  , 'exercises' => $exercises]);
        
        }
        else{
            $error = $response;
            return response()->json(['success' => false , 'error' => $error] , 422);
        }

    }

    public function storeExcersies($exercises){
        
        foreach($exercises as $exercise){
            if($exercise->Exercise_Id){
            Exercise::updateOrCreate(
                [ 'Exercise_Id' => $exercise->Exercise_Id],
                [
                    'Exercise_Id' => $exercise->Exercise_Id,
                    'exercise' => $exercise
                ]);
            }
        }
    }
}
