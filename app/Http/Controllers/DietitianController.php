<?php

namespace App\Http\Controllers;

use App\Tag;
use App\User;
use App\Dietitian;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class DietitianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dietitians = Dietitian::with('user')->get();
        return view('admin.dietitians.index' , compact('dietitians'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
        return view('admin.dietitians.create',compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'username' => 'required|unique:users,username',
            'email' => 'required|email|unique:users,email',
            'password' => 'required ',
            'percentage' => 'numeric|min:0|max:100',
            ]);
        
            
            
        // dd($request->all());

        if($request->password != $request->conf_password ){
            return redirect()->back()->with('error' , 'Password does not match');
        }

        $user = User::create([
            'role_id' => 4,
            'name' => $request->name,
            'username' => $request->get('username'),
            'email' => $request->email,
            'password' => \Hash::make($request->password),

        ]);


        $user->tags()->attach($request->tags);

        if($user){
        $dietitian = Dietitian::create([
            'user_id' => $user->id,
            'phone' => $request->phone,
            'portfolio' => $request->portfolio,
            'percentage' => $request->percentage,
            // 'tags' => $request->tags,
            ]);

        if($dietitian){
            $data['msg'] = 'Hi '.$user->name . '<br>Welcome To FTBLE. <br> You have been registered as a Dietitian. <br> <b>Your account credentials are</b> <br> <b>Email:</b>'.$user->email.'<br> <b>Password:</b>'.$request->password ;
            
            \Mail::send('mails.general', $data, function ($message) use ($user) {
                $message->from('Info@FTBLE.com', 'FTBLE');
                // $message->sender('john@johndoe.com', 'John Doe');
                $message->to($user->email, $user->name);
            
                $message->subject('Welcome To FTBLE');
            
                // $message->priority(3);            
                // $message->attach('pathToFile');
            });            
        }



        if($request->has('image')){

            $ext = $request->image->getClientOriginalExtension();
            
            $request->image->move(public_path().'/images/profile_images/' , $user->id .'.'. $ext);
            $user->photo = $user->id .'.'. $ext;
            
            $user->save();
        }
        }



        if($user)
            return redirect('/admin/dietitian')->with('success' , 'Dietitian created successfully');
        else
            return redirect()->back()->with('error' , 'Some problem occured');
        
    }   

    /**
     * Display the specified resource.
     *
     * @param  \App\Dietitian  $dietitian
     * @return \Illuminate\Http\Response
     */
    public function show(Dietitian $dietitian)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dietitian  $dietitian
     * @return \Illuminate\Http\Response
     */
    public function edit(Dietitian $dietitian)
    {
        // $dietitian = Dietitian::with('user')->get();
        $tags = Tag::all();

        return view('admin.dietitians.edit' , compact('dietitian' , 'tags'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dietitian  $dietitian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dietitian $dietitian)
    {
        // dd($request->all());

        $request->validate([
            'name' => 'required',
            'username' => 'sometimes',
            'email' => 'required|email',
            'password' => 'sometimes ',
            'percentage' => 'numeric|min:0|max:100',

        ]);
            // dd($request->all());

        if( isset($request->password) && $request->password != $request->conf_password ){
            return redirect()->back()->with('error' , 'Password does not match');
        }

        $user = User::findOrFail($dietitian->user_id);
            $user->name = $request->name;
            $user->username = $request->get('username');
            $user->email = $request->email;
            if($request->password){
                $user->password = \Hash::make($request->password);
            }
            $user->save();

            $user->tags()->sync($request->tags);
            
            $dietitian->user_id = $user->id;
            $dietitian->phone = $request->phone;
            $dietitian->portfolio = $request->portfolio;
            $dietitian->percentage = $request->percentage;
            // $dietitian->tags = $request->tags;

            
            if($request->has('image')){
                $path = public_path().'/images/profile_images/'.$user->photo;

                if(file_exists($path)){
                        File::delete($path);
                    }
                    $ext = $request->image->getClientOriginalExtension();
            
                    $request->image->move(public_path().'/images/profile_images/' , $user->id .'.'. $ext);
                    $user->photo = $user->id .'.'. $ext;
                    
                    $user->save();

                }

            $dietitian->save();


        if($user)
            return redirect('/admin/dietitian')->with('success' , 'Dietitian updated successfully');
        else
            return redirect()->back()->with('error' , 'Some problem occured');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dietitian  $dietitian
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dietitian $dietitian)
    {
        $user = User::find($dietitian->user_id);
        
        $path = public_path().'/images/dietitians/'.$dietitian->photo;
        if(file_exists($path)){
            File::delete($path);
        }

        $dietitian->delete();
        $user->delete();

        return redirect()->back()->with('success'  , 'Dietitian deleted successfully');
    }
}
