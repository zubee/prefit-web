<?php

namespace App\Http\Controllers;

use App\Tag;
use App\User;
use App\Coach;
use App\Package;
use App\Client_Plan;
use App\Perfit_Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

class CoachController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coaches = Coach::with('user')->get();
        return view('admin.coaches.index' , compact('coaches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
        return view('admin.coaches.create',compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // if($request->has('image')){

        //     $file = Image::make($request->image);
        //     $ext = $request->image->getClientOriginalExtension();
        //     // $file->save('files/' . $name , 90);    
        //     // $image = $manager->make('public/foo.jpg')->resize(300, 200);

        //     $file->resize(500, null, function ($constraint) {
        //         $constraint->aspectRatio();
        //     });
            
        //     $file->save(public_path().'/images/profile_images/' . time() .'.'. $ext , 90);
        //     // $request->image->move(public_path().'/images/profile_images/' , $user->id .'.'. $ext);
        // }


        // dd($request->all());


        $request->validate([
            'name' => 'required',
            'username' => 'required|unique:users,username',
            'email' => 'required|email|unique:users,email',
            'password' => 'required | same:conf_password',
            'percentage' => 'numeric|min:0|max:100',
            'conf_password' => 'required',
            ]);
        
            
            
        // dd($request->all());

        // if($request->password != $request->call_user_func ){
        //     return redirect()->back()->with('error' , 'Password does not match');
        // }

        $user = User::create([
            'role_id' => $request->role,
            'name' => $request->name,
            'username' => $request->get('username'),
            'email' => $request->email,
            'password' => \Hash::make($request->password),

        ]);


        // $user->tags()->attach($request->tags);

        if($user){
        $coach = Coach::create([
            'user_id' => $user->id,
            'role' => $request->role,
            'phone' => $request->phone,
            'portfolio' => $request->portfolio,
            'percentage' => $request->percentage,
            // 'tags' => $request->tags,
            ]);

            $coach->tags()->attach($request->tags , ['user_id' => $user->id]);


        if($coach){
              
            if($request->has('image')){

                // $file = Image::make($request->image);

                $file = Perfit_Image::imageResize($request->image);
                
                $ext = $request->image->getClientOriginalExtension();
                
                // $file->move(public_path().'/images/profile_images/' , $user->id .'.'. $ext);
                // $request->image->move(public_path().'/images/profile_images/' , $user->id .'.'. $ext);

                $file->save(public_path().'/images/profile_images/' . $user->id .'.'. $ext , 90);

                $user->photo = $user->id .'.'. $ext;
                $user->save();
            }

            $role = '';

            if($request->role == 3){
                $role = 'Trainer';
            }else{
                $role = 'Dietitian';
            }
            
            $data['msg'] = 'Hi '.$user->name . '<br>Welcome To FTBLE. <br> You have been registered as a '. $role . '. <br> <b>Your account credentials are</b> <br> <b>Username:</b>'.$user->username.'<br> <b>Password:</b>'.$request->password ;
            
            \Mail::send('mails.general', $data, function ($message) use ($user) {
                $message->to($user->email, $user->name);
                $message->subject('Welcome To FTBLE');
            
                // $message->priority(3);            
                // $message->attach('pathToFile');
            });            

          

            
        }



        }



        if($user)
            return redirect('/admin/coach')->with('success' , 'Coach created successfully');
        else
            return redirect()->back()->with('error' , 'Some problem occured');
        
    }   

    /**
     * Display the specified resource.
     *
     * @param  \App\Coach  $coach
     * @return \Illuminate\Http\Response
     */
    public function show(Coach $coach)
    {
        return view('admin.coaches.view' , compact('coach'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Coach  $coach
     * @return \Illuminate\Http\Response
     */
    public function edit(Coach $coach)
    {
        // $coach = Coach::with('user')->get();
        $tags = Tag::all();

        return view('admin.coaches.edit' , compact('coach' , 'tags'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Coach  $coach
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coach $coach)
    {
        // dd($request->all());

        $request->validate([
            'name' => 'required',
            'username' => 'sometimes',
            'email' => 'required|email',
            'password' => 'sometimes | required_with:conf_password|same:conf_password',
            
            'percentage' => 'numeric|min:0|max:100',

        ]);
            // dd($request->all());

        if( isset($request->password) && $request->password != $request->conf_password ){
            return redirect()->back()->with('error' , 'Password does not match');
        }

        $user = User::findOrFail($coach->user_id);
            $user->name = $request->name;
            $user->username = $request->get('username');
            $user->email = $request->email;
            if($request->password){
                $user->password = \Hash::make($request->password);
            }
            $user->save();

            // $user->tags()->sync($request->tags);
            
            $coach->user_id = $user->id;
            $coach->phone = $request->phone;
            $coach->portfolio = $request->portfolio;
            $coach->percentage = $request->percentage;
            // $coach->tags = $request->tags;

            $coach->tags()->detach();
            $coach->tags()->attach($request->tags , ['user_id' => $user->id]);

            
            // if($request->has('image')){
            //     $path = public_path().'/images/profile_images/'.$user->photo;

            //     if(file_exists($path)){
            //             File::delete($path);
            //         }
            //         $ext = $request->image->getClientOriginalExtension();
            
            //         $request->image->move(public_path().'/images/profile_images/' , $user->id .'.'. $ext);
            //         $user->photo = $user->id .'.'. $ext;
                    
            //         $user->save();

            //     }

                if($request->has('image')){
                $path = public_path().'/images/profile_images/'.$user->photo;

                if(file_exists($path)){
                        File::delete($path);
                    }
    
                    $file = Perfit_Image::imageResize($request->image);
                    
                    $ext = $request->image->getClientOriginalExtension();    
                    $file->save(public_path().'/images/profile_images/' . $user->id .'.'. $ext , 90);
    
                    $user->photo = $user->id .'.'. $ext;
                    $user->save();
                }
    

            $coach->save();


        if($user)
            return redirect('/admin/coach')->with('success' , 'Coach updated successfully');
        else
            return redirect()->back()->with('error' , 'Some problem occured');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Coach  $coach
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coach $coach)
    {
        
        $user = User::find($coach->user_id);
        // $user->delete();
        
        $path = public_path().'/images/coaches/'.$coach->photo;
        if(file_exists($path)){
            File::delete($path);
        }

        $coach->delete();
        $user->delete();

        return redirect()->back()->with('success'  , 'Coach deleted successfully');
    }



    public function viewClients($coach_id){

    $coach = Coach::findOrFail($coach_id);

    $clients = $coach->clients->unique();
    $clients->values()->all();

    return view('admin.coaches.clients' , compact('clients' , 'coach'));

    }

    public function viewClientPackages($coach_id , $client_id ){

        $client = User::findOrFail($client_id);
        $coach = Coach::findOrFail($coach_id);

        // dd($user->subscribed_packages->pivot);

        $packages = $client->subscribed_packages->where('pivot.coach_id' , $coach->id) ;

        return view('admin.coaches.client_packages' , compact( 'client','packages' , 'coach'));        

    }
    
    public function viewClientPackagePlan($coach_id , $client_id , $package_id ){

        $client = User::findOrFail($client_id);
        $coach = Coach::findOrFail($coach_id);
        $package = Package::findOrFail($package_id);

        // dd($user->subscribed_packages->pivot);
        $plan = Client_Plan::with('days')->where('package_id'  , $package_id )->where('coach_id'  , $coach_id )->where('client_id'  , $client_id )->first();

        $total_weeks = [];
        $plan->total_weeks = [];
        $plan->getTotalWeeks();
        $total_weeks = $plan->total_weeks;
            
        $packages = $client->subscribed_packages;


        return view('admin.coaches.show_plan' , compact( 'client','packages' , 'total_weeks', 'package' , 'coach' , 'plan'));        

    }

}
