<?php

namespace App\Http\Controllers;

use App\Chat;
use App\User;
use stdClass;
use App\Client_Request;
use App\Jobs\SendEmail;
use App\Client_Question;
use App\Traits\FirebaseFCM;
use Illuminate\Http\Request;
use App\Client_Questionnaire;
use App\Client_Question_Choice;


class ClientQuestionnaireController extends Controller
{
    use FirebaseFCM;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $questionnaires = Client_Questionnaire::with( 'users' , 'questions' , 'questions.choices')-> related()->active()->get();

        // $clients = auth()->user()->coach->clients;
        $clients = Client_Request:: with('client') -> whereIn('status' , ['pending' , 'accepted'])->get();
        
        $clients = User::whereIn('id', $clients->pluck('client_id')->toArray() )->get();
        // dd($all_clients , $clients);

        // $clients = collect();
        // foreach($all_clients as $client){
        //     $clients->push($client->client); 
        // }

        foreach($questionnaires as $questionnaire){
            $questionnaire->not_assigned_users = User::whereIn('id', $clients->pluck('client_id')->toArray() ) ->whereNotIn('id' , $questionnaire->users->pluck('id')->toArray() ) ->get();
        }
        
        // dd($questionnaires);

        return view('coach.clinet-questionnaires.index' , compact('questionnaires' , 'clients'));
    }

    public function getQuestionnaires(){

        // $questionnaires = Client_Questionnaire::with( 'users' , 'questions' , 'questions.choices')-> related()->active()->get();

        // // foreach($questionnaires as $questionnaire){
        //     // $questionnaire->unselected_users = User::
        // // }

        // // $clients = auth()->user()->coach->clients;
        // $all_clients = Client_Request:: with('client') ->where('coach_id' , auth()->user()->coach->id  ) ->  whereIn('status' , ['pending' , 'accepted'])->get();
        // $clients = collect();
        // foreach($all_clients as $client){
        //     $clients->push($client->client); 
        // }
        
        // $clients = $clients->unique(); 


        $questionnaires = Client_Questionnaire::with( 'users' , 'questions' , 'questions.choices')-> related()->active()->get();

        $clients = Client_Request:: with('client')->where('coach_id' , auth()->user()->coach->id ) -> whereIn('status' , ['pending' , 'accepted'])->get();
        
        $clients = User::whereIn('id', $clients->pluck('client_id')->toArray() )->get();

        foreach($questionnaires as $questionnaire){
            $questionnaire->not_assigned_users = User::whereIn('id', $clients->pluck('id')->toArray() ) ->whereNotIn('id' , $questionnaire->users->pluck('id')->toArray() ) ->get();
        }


        return response()->json(['questionnaires' => $questionnaires , 'clients' => $clients ]);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('coach.clinet-questionnaires.create' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client_Questionnaire  $client_Questionnaire
     * @return \Illuminate\Http\Response
     */
    public function show(Client_Questionnaire $client_questionnaire)
    {
        return view('coach.clinet-questionnaires.show' , compact('client_questionnaire') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client_Questionnaire  $client_Questionnaire
     * @return \Illuminate\Http\Response
     */
    public function edit(Client_Questionnaire $client_questionnaire)
    {
        $questionnaire = Client_Questionnaire::with('questions' , 'questions.choices')->find($client_questionnaire->id);

        // dd($questionnaire);

        return view('coach.clinet-questionnaires.create' , compact('questionnaire') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client_Questionnaire  $client_Questionnaire
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id = 0)
    {
        // dd($request->all());

        if(isset($request->questionnaire['id'] ) ){
            $questionnaire = Client_Questionnaire::find($request->questionnaire['id'] );
            $questionnaire->name = $request->questionnaire['name'];
            $questionnaire->save();            
        }else{
            $questionnaire = Client_Questionnaire::create([
                'name' => $request->questionnaire['name'],
                'coach_id' => $request->coach_id
            ]);
        }

        // $questionnaire->users()->sync($request->assigned_users);

        $all_questions = $questionnaire->questions->pluck('id')->toArray();
        $deleted_questions= [];
        // dd($all_questions);
        
        if(count($request->questions) > 0){
        foreach($request->questions as $question){
            
            if(isset($question['id']) && in_array($question['id'] , $all_questions)){

                    unset($all_questions[array_search($question['id'],$all_questions)]);
            
            }
            // dd($deleted_questions , 'deleted');
            // dd($all_questions);

            // dd($question);
            if($question['id']){
                $q = Client_Question::find($question['id']);
                $q->question = $question['question'];
                $q->type = $question['type'];
                $q->save();
            }else{
                $q = Client_Question::create([
                    'question' => $question['question'],
                    'type' => $question['type'],
                    'questionnaire_id' => $questionnaire->id 
                ]);
            }
            $q->choices()->delete();
            foreach($question['choices'] as $choice){
                Client_Question_Choice::create([
                    'question_id' => $q->id,
                    'name' => $choice['name'],
                ]);

            }

        }
        if($all_questions){
            foreach($all_questions as $q){
                Client_Question::find($q)->delete();
            }
        }
        }else{
            Client_Questionnaire::find($questionnaire->id )->questions()->delete();
        }


        return response()->json(['questionnaire' => $questionnaire ]);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client_Questionnaire  $client_Questionnaire
     * @return \Illuminate\Http\Response
     */
    public function showAnswer($client_id , $questionnaire_id  )
    {   

        $questionnaire = Client_Questionnaire::find($questionnaire_id);
        $client = User::find($client_id);

        // dd($questionnaire , $client);
        return view('coach.clinet-questionnaires.showAnswer' , compact('questionnaire' , 'client') );

    }


    public function destroy($questionnaire_id)
    {   
        
        $questionnaire = Client_Questionnaire::find($questionnaire_id);
        $questionnaire->active = 0;
        $questionnaire->save();
        return redirect()->back()->with('success' , 'Questionnaire removed');

    }

    public function clients($id)
    {   
        $questionnaire = Client_Questionnaire::find($id);
        return view('coach.clinet-questionnaires.assignedto' , compact('questionnaire') );
    }

    public function sendQuestionnaireToClient( Request $request)
    {   
        $questionnaire = Client_Questionnaire::find($request->questionnaire_id);

        if(!$questionnaire->users()->find($request->client_id)){
            $questionnaire->users()->attach($request->client_id);


        $message = Chat::create([
            'sender_id' => auth()->user()->id ,
            'file' => 0,
            'reciever_id' => $request->client_id,
            'is_questionnaire' => 1,
            'questionnaire_id' => $questionnaire->id,
        ]);
       
        $coach_user = User::findOrFail(auth()->user()->id);
        $client = User::findOrFail($request->client_id);



        if($client->fcm_token){

            $notification = new stdClass();
            
            $notification->text = $coach_user->name." sent you a Questionnaire";
            $notification->description = $coach->user->name. " sent you a Questionnaire" ;
            $notification->notif_type = "notif-subscribe";

            $this->PublishBroadcast($client , $coach_user , $notification , $coach_user->name , $notification->text , "");
        }


        $users = User::where('id' , $request->client_id)->get();
        $subject = $coach_user->name.' sent you a Questionnaire';
        $body = 'Hi '.$client->name.' 
        <br>Our ' . $coach_user->role->name .  ' has sent you a Questionnaire. Please to answer from the App
        <br>
        Stay Fit,
        <br>';
        
        dispatch(new SendEmail($users , $subject , $body ));                

        // if($reciever->fcm_token){
        //     $this->PublishBroadcast($reciever , $sender , $message , $sender->name , '' , "");
        // }
        
        return response()->json(['success' => true]);
        
    }else{
        return response()->json(['success' => false, 'message' => 'Already assinged'] , 422);

    }

    }
}
