<?php

namespace App\Http\Controllers;

use App\Tag;
use App\User;
use App\Package;
use App\Questionnaire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class DietitianDashboardController extends Controller
{
    public function index(){
        return view('dietitian.index');
    }

    public function showProfile(){
        return view('dietitian.profile.index');
    }
    
    public function editProfile(){
        $tags = Tag::all();
        return view('dietitian.profile.edit' , compact('tags'));
    }

    public function updateProfile($id , Request $request){
        
        // dd($request->all());

        $request->validate([
            'name' => 'required',
            'username' => 'sometimes',
            'email' => 'required|email',
            'password' => 'sometimes ',
            'percentage' => 'numeric|min:0|max:100',

        ]);
            // dd($request->all());

        if( isset($request->password) && $request->password != $request->conf_password ){
            return redirect()->back()->with('error' , 'Password does not match');
        }

        $user = User::findOrFail(auth()->user()->id);
            $user->name = $request->name;
            $user->username = $request->get('username');
            $user->email = $request->email;
            if($request->password){
                $user->password = \Hash::make($request->password);
            }
            $user->save();

            $user->tags()->sync($request->tags);
            
            $dietitian = auth()->user()->dietitian;
            
            $dietitian->user_id = $user->id;
            $dietitian->phone = $request->phone;
            $dietitian->portfolio = $request->portfolio;
            $dietitian->percentage = $request->percentage;
            // $dietitian->tags = $request->tags;

            
            if($request->has('image')){
                $path = public_path().'/images/dietitians/'.$dietitian->photo;

                if(file_exists($path)){
                        File::delete($path);
                    }
                $ext = $request->image->getClientOriginalExtension();
                $request->image->move(public_path().'/images/dietitians/' , $dietitian->id .'.'. $ext);

                $dietitian->photo = $dietitian->id .'.'. $ext;
    
            }

            $dietitian->save();


        if($user->save())
            return redirect('/dietitian/profile')->with('success' , 'Profile updated successfully');
        else
            return redirect()->back()->with('error' , 'Some problem occured');


    }

    
    public function login(){
        return view('auth.dietitian_login');
        
    }


    public function showPackages(){
        // dd(auth()->user()->packages);
        return view('dietitian.packages.index');

    }

    public function viewPackage($id){
        $package = Package::findOrFail($id);
        // dd(auth()->user()->packages);
        if( in_array($package->id , auth()->user()->packages->pluck('id')->toArray()) ){
            return view('dietitian.packages.show' , compact('package'));

        }else{
            return redirect()->back()->with('error' , 'That package does not belongs to you' );
        }

    }

    public function showQuestionnaires(){
        return view('dietitian.questionnaires.index');

    }
    public function showQuestionnaire($id){
        $questionnaire = Questionnaire::findOrFail($id);

        if( in_array($questionnaire->id , auth()->user()->questionnaires->pluck('id')->toArray()) ){
            return view('dietitian.questionnaires.show' , compact('questionnaire'));

        }else{
            return redirect()->back()->with('error' , 'That questionnaire does not belongs to you' );
        }

    }
}
