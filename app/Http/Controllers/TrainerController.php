<?php

namespace App\Http\Controllers;

use App\Tag;
use App\User;
use App\Trainer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class TrainerController extends Controller
{
    public function index()
    {
        $trainers = Trainer::with('user')->get();
        return view('admin.trainers.index' , compact('trainers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
        return view('admin.trainers.create' , compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'username' => 'required|unique:users',
            'email' => 'required|email|unique:users,email',
            'password' => 'required ',
            'percentage' => 'numeric|min:0|max:100',

        ]);
            // dd($request->all());

        if($request->password != $request->conf_password ){
            return redirect()->back()->with('error' , 'Password does not match');
        }

        $user = User::create([
            'role_id' => 3,
            'name' => $request->name,
            'username' => $request->get('username'),
            'email' => $request->email,
            'password' => \Hash::make($request->password),
        ]);

        $user->tags()->attach($request->tags);

        $trainer = Trainer::create([
            'user_id' => $user->id,
            'phone' => $request->phone,
            'portfolio' => $request->portfolio,
            'percentage' => $request->percentage,
            // 'tags' => $request->tags,

        ]);

        if($request->has('image')){

            $ext = $request->image->getClientOriginalExtension();

            // $request->image->move(public_path().'/images/trainers/' , $trainer->id .'.'. $ext);
            $request->image->move(public_path().'/images/profile_images/' , $user->id .'.'. $ext);
            
            // $trainer->photo = $trainer->id .'.'. $ext;
            $user->photo = $user->id .'.'. $ext;

            $user->save();
            // $trainer->save();
        }

        if($user)
            return redirect('/admin/trainer')->with('success' , 'Trainer created successfully');
        else
            return redirect()->back()->with('error' , 'Some problem occured');
        
    }   

    /**
     * Display the specified resource.
     *
     * @param  \App\Trainer  $trainer
     * @return \Illuminate\Http\Response
     */
    public function show(Trainer $trainer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Trainer  $trainer
     * @return \Illuminate\Http\Response
     */
    public function edit(Trainer $trainer)
    {
        // $trainer = Trainer::with('user')->get();
        $tags = Tag::all();

        return view('admin.trainers.edit' , compact('trainer' , 'tags'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Trainer  $trainer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trainer $trainer)
    {
        // dd($request->all());

        $request->validate([
            'name' => 'required',
            'username' => 'sometimes',
            'email' => 'required|email',
            'password' => 'sometimes ',
            'percentage' => 'numeric|min:0|max:100',

        ]);
            // dd($request->all());

        if( isset($request->password) && $request->password != $request->conf_password ){
            return redirect()->back()->with('error' , 'Password does not match');
        }

        $user = User::findOrFail($trainer->user_id);
            $user->name = $request->name;
            $user->username = $request->get('username');
            $user->email = $request->email;
            $user->password = \Hash::make($request->password);
            $user->save();

            $user->tags()->sync($request->tags);


            $trainer->user_id = $user->id;
            $trainer->phone = $request->phone;
            $trainer->portfolio = $request->portfolio;
            $trainer->percentage = $request->percentage;
            $trainer->tags = $request->tags;

            if($request->has('image')){
                $path = public_path().'/images/profile_images/'.$user->photo;
    
                if(file_exists($path)){
                        File::delete($path);
                    }
                    $ext = $request->image->getClientOriginalExtension();

                    // $request->image->move(public_path().'/images/trainers/' , $trainer->id .'.'. $ext);
                    $request->image->move(public_path().'/images/profile_images/' , $user->id .'.'. $ext);
                    
                    // $trainer->photo = $trainer->id .'.'. $ext;
                    $user->photo = $user->id .'.'. $ext;
        
                    $user->save();
                    // $trainer->save();
            
            }

            $trainer->save();

        if($user)
            return redirect('/admin/trainer')->with('success' , 'Trainer updated successfully');
        else
            return redirect()->back()->with('error' , 'Some problem occured');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Trainer  $trainer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trainer $trainer)
    {
        $user = User::find($trainer->user_id);

        $path = public_path().'/images/trainers/'.$trainer->photo;
        if(file_exists($path)){
            File::delete($path);
        }

        $trainer->delete();
        $user->delete();

        return redirect()->back()->with('success'  , 'Trainer deleted successfully');
    }
}
