<?php

namespace App\Http\Controllers;

use App\Chat;
use App\User;
use stdClass;
use App\Coach;
use App\Notification;
use App\Traits\FirebaseFCM;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Exports\ExportClients;
use Maatwebsite\Excel\Facades\Excel;

class HomeController extends Controller
{
    use FirebaseFCM;

    /**    
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('homepage');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function homepage(){
        // if(auth()->user()){
        //     if(auth()->user()->role_id == 1 || auth()->user()->role_id == 2){
        //     return redirect('/admin/dashboard');
        //     }
        //     elseif(auth()->user()->role_id == 3){
        //         return redirect('/trainer/dashboard');
        //     }
        //     elseif(auth()->user()->role_id == 3){
        //         return redirect('/dietitian/dashboard');
        //     }
        // }else{
        //     return redirect('/');
        // }
    }

    public function getNotifications($id){

        $user = User::find($id);

        $notifications = Notification::where('user_id' , $user->id)->where('status' , 0 )->latest()->get();
        
        foreach($notifications as $notification){
            $notification->time = \Carbon\Carbon::parse($notification->created_at)->diffForHumans();
        }

        // dd($notifications);
        
        $messages  = collect();

        if($user->role_id == 1 || $user->role_id == 2){

            $support = User::where('role_id' , 6)->first();

            $unread_messages = Chat::latest()->where('reciever_id' , $support->id)->where('read_status' , 0 )->get();

        }else{
            $unread_messages = Chat::latest()->where('reciever_id' , $user->id)->where('read_status' , 0 )->get();

        }
        // dd($unread_messages);

        foreach($unread_messages as $message){
            $messages->push($message->sender);
        }

        $messages = $messages->unique();
        // dd($messages);
        foreach($messages as $message){
            // dump($message);
            
            if($message){
                $message->last_message = $message->chat->last();
                $message->last_message->message = Str::limit($message->last_message->message , 200);
                $message->last_message->time = \Carbon\Carbon::parse($message->last_message->created_at)->diffForHumans();
            }else{
                $messages->pop($message);
            }
        }
        // dd($messages);


        return ['notifications' => $notifications , 'messages' => $messages->values() ];
    }


    public function exportClients($ids , $coach_id ){

        $coach = Coach::find($coach_id );
        $clients = User::whereIn('id' , json_decode($ids))->get();

        return Excel::download(new ExportClients($clients , $coach ), 'clients.xlsx');

    }


}
