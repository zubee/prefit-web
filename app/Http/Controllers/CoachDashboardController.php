<?php

namespace App\Http\Controllers;

use App\Chat;
use App\User;
use stdClass;
use App\Coach;
use App\Review;
use App\Package;
use App\Workout;
use Carbon\Carbon;
// use Maatwebsite\Excel\Excel;
use App\Supplement;
use App\Client_Plan;
use App\Client_Pivot;
use App\Questionnaire;
use App\Client_Request;
use App\Jobs\SendEmail;
use App\Traits\FirebaseFCM;
use Illuminate\Http\Request;
use App\Exports\ExportClients;
use Maatwebsite\Excel\Facades\Excel;

class CoachDashboardController extends Controller
{

    use FirebaseFCM;

    public function login(){
        return view('auth.coach_login');
        
    }
    public function index(){

        // dd(auth()->user());
        return view('coach.index');
    }


    public function showProfile(){
        return view('coach.profile.index');
    }
    
    public function editProfile(){
        $tags = Tag::all();
        return view('coach.profile.edit' , compact('tags'));
    }

    public function updateProfile($id , Request $request){
        
        // dd($request->all());

        $request->validate([
            'name' => 'required',
            'username' => 'sometimes',
            'email' => 'required|email',
            'password' => 'sometimes ',
            'percentage' => 'numeric|min:0|max:100',

        ]);
            // dd($request->all());

        if( isset($request->password) && $request->password != $request->conf_password ){
            return redirect()->back()->with('error' , 'Password does not match');
        }

        $user = User::findOrFail(auth()->user()->id);
            $user->name = $request->name;
            $user->username = $request->get('username');
            $user->email = $request->email;
            if($request->password){
                $user->password = \Hash::make($request->password);
            }
            $user->save();

            $user->tags()->sync($request->tags);
            
            $trainer = auth()->user()->trainer;
            
            $trainer->user_id = $user->id;
            $trainer->phone = $request->phone;
            $trainer->portfolio = $request->portfolio;
            $trainer->percentage = $request->percentage;
            // $dietitian->tags = $request->tags;

            
            if($request->has('image')){
                $path = public_path().'/images/trainers/'.$trainer->photo;

                if(file_exists($path)){
                        File::delete($path);
                    }
                $ext = $request->image->getClientOriginalExtension();
                $request->image->move(public_path().'/images/trainers/' , $trainer->id .'.'. $ext);

                $trainer->photo = $trainer->id .'.'. $ext;
    
            }

            $trainer->save();


        if($user->save())
            return redirect('/coach/profile')->with('success' , 'Profile updated successfully');
        else
            return redirect()->back()->with('error' , 'Some problem occured');


    }

    public function showPackages(){
        // dd(auth()->user()->packages);
        return view('coach.packages.index');

    }

    public function viewPackage($id){

        $package = Package::with('reviews' , 'reviews.user')->findOrFail($id);
        
        $package->reviews = $package->specificReviews(auth()->user()->coach->id);
        // dd($package->reviews->pluck('coach_id'));

        if( in_array($package->id , auth()->user()->coach->packages->pluck('id')->toArray()) ){
            return view('coach.packages.show' , compact('package'));
        }else{
            return redirect()->back()->with('error' , 'That package does not belongs to you' );
        }

    }

    
    public function showQuestionnaires(){
        return view('coach.questionnaires.index');

    }
    public function showQuestionnaire($id){
        $questionnaire = Questionnaire::findOrFail($id);

        if( in_array($questionnaire->id , auth()->user()->questionnaires->pluck('id')->toArray()) ){
            return view('coach.questionnaires.show' , compact('questionnaire'));

        }else{
            return redirect()->back()->with('error' , 'That questionnaire does not belongs to you' );
        }

    }
    
    
    public function showClients(){

        $clients = auth()->user()->coach->clients->unique();

        $clients->values()->all();
        
        $requesting_clients = auth()->user()->coach->new_requests->where('pivot.status' , 'pending');
        
        foreach($requesting_clients as $client){
            if(Package::find($client->pivot->package_id)){
                $client->pivot->package = Package::find($client->pivot->package_id);
            }else{
                $client->pivot->package = null;
            }
        }
        // dd($packages);
        
        return view('coach.clients.index' , compact('clients' , 'requesting_clients'));
        
    }
    
    public function showClientPackages($id){

        $client = User::findOrFail($id);

        // dd($user->subscribed_packages->pivot);

        $packages = $client->subscribed_packages->where('pivot.coach_id' , auth()->user()->coach->id) ;

        // dd($packages);

        $check = Client_Pivot::where('user_id' , $id )->whereIn('package_id' , auth()->user()->coach->packages->pluck('id')->toArray() )->where('coach_id' , auth()->user()->coach->id )->first();
        
        // dd($check );
        
        // dd(Client_Pivot::all());
        // dd($check);

        if($check ){
        return view('coach.clients.packages' , compact( 'client','packages'));
        }else{
            return redirect()->back()->with('error' , 'Record does not exists');
        }
        
        
        
    }    
    
    
    public function handleClientRequest($id , Request $request){

        // dd($request->all());
        $client = User::findOrFail($id);

        if($request->status == 'accepted' || $request->status == 'cancelled'){
            $client_request = Client_Request::find($request->request_id);
            $client_request->status = $request->status;
            $client_request->responded_at = Carbon::now();
            $client_request->save();
        }

        if($request->status == 'accepted'){
            
            return redirect('/coach/messages/'.$id);
        }

        if($request->status == 'cancelled'){
            return redirect()->back()->with('success' , 'Request Status Updated');
        }

        return redirect()->back()->with('error' , 'Request Was not able to be processed');

        
        
        
    }
    
    
    public function showClientDetails($id){

        $client = User::findOrFail($id);

        // dd($user->subscribed_packages->pivot);

        $packages = $client->subscribed_packages->where('pivot.coach_id' , auth()->user()->coach->id) ;

        // dd($packages);

        $check = Client_Pivot::where('user_id' , $id )->whereIn('package_id' , auth()->user()->coach->packages->pluck('id')->toArray() )->where('coach_id' , auth()->user()->coach->id )->first();
        
        // dd($check );
        
        // dd(Client_Pivot::all());
        // dd($check);

        if($check ){
        return view('coach.clients.view_client' , compact( 'client','packages'));
        }else{
            return redirect()->back()->with('error' , 'Record does not exists');
        }
        
        
        
    }
    

    public function createClientPlan($client_id , $package_id ){

        $client = User::findOrFail($client_id);
        $package = Package::findOrFail($package_id);

        $check = Client_Pivot::where('user_id' , $client_id )->where('coach_id' , auth()->user()->coach->id )->where('package_id' , $package_id )->first();

        if($check ){


        $plan = Client_Plan::firstOrCreate([
            'client_id' => $client_id,
            'coach_id' => auth()->user()->coach->id,
            'package_id' => $package_id,
            'duration' => $package->duration,
        ]);

        $total_weeks = [];
        $plan->total_weeks = [];
        $plan->getTotalWeeks();
        $total_weeks = $plan->total_weeks;

        $workouts = Workout::all();        
        $supplements = Supplement::all();
    
        $packages = $client->subscribed_packages;
            // dd($default_weeks);
            // return view('coach.clients.plans.create_new' , compact( 'client','packages' , 'package' , 'plan' , 'total_weeks' , 'workouts' , 'supplements'));
            return view('coach.clients.plans.create' , compact( 'client','packages' , 'package' , 'plan' , 'total_weeks' , 'workouts'));
            // return view('coach.clients.plans.create_old' , compact( 'client','packages' , 'package' , 'plan' , 'total_weeks' , 'workouts'));
        }else{
                return redirect()->back()->with('error' , 'Record does not exists');
            }
                
    }
    
    public function startClientPlan($client_id , $package_id ){
        // dd('start' , $client_id , $package_id );
        $client = User::findOrFail($client_id);
        $package = Package::findOrFail($package_id);

        // dd($user->subscribed_packages->pivot);
        $check = Client_Pivot::where('user_id' , $client_id )->where('coach_id' , auth()->user()->coach->id )->where('package_id' , $package_id )->first();

        $plan = Client_Plan::where('client_id' , $client_id ) -> where('package_id' , $package_id ) -> where('coach_id' , auth()->user()->coach->id )  ->first();


        $today_date = Carbon::now()->toDateString();
        $today_day = Carbon::now()->format('l') ;


        $counter = 1;

        $all_weeks = [];
            for($i = 1 ; $counter <= $plan->duration  ; $i++ ){
            // for($i = 1 ; $i <= $weeks ; $i++ ){
                
                // foreach()
                // $all_weeks[] = Carbon::now()->addDays()
                for($j = 1; $j <= 7; $j++){
                    // dump($i * $j );
                    if(array_sum(array_map("count", $all_weeks)) < $plan->duration){

                        $all_weeks[$i][Carbon::now()->addDays($counter)->toDateString()] = Carbon::now()->addDays($counter)->format('l') ;
                        // dump($counter);
                        $counter ++;
                        
                        if(Carbon::now()->addDays($counter)->format('l') == 'Sunday'){
                            break;
                        }
                    }
                }
            }

        // dd($all_weeks);

        $plan->weeks = json_encode($all_weeks);
        $plan->start_status = 1;
        $plan->start_date = Carbon::today()->toDateString() ;
        $plan->save();

        foreach($plan->days as $day){
            $day->date = Carbon::parse($plan->start_date)->addDays($day->day_number)->toDateString();
            $day->save();
        }
        

        $packages = $client->subscribed_packages;

        if($client->fcm_token){
            // dd($client->fcm_token);
            $notification = new stdClass();
            $notification->text = "Your plan from coach ". auth()->user()->name . " has started ";
            $notification->description = "Your plan from coach ". auth()->user()->name . " has started";
            $notification->notif_type = "notif-plan";

            $this->PublishBroadcast($client , auth()->user() , $notification , auth()->user()->name , $notification->text , " ");
        }

        $clients = User::where('id' , $client->id )->get();

        $subject = 'Your fitness plan is ready';
        $body = 'Hi '.$client->name.',
        <br>
        Your plan is ready and started by your '.auth()->user()->role->name.' “'.auth()->user()->name.'”. You can always contact the '.auth()->user()->role->name.' for more information 
        <br>
        Enjoy!
        <br>';

        dispatch(new SendEmail($clients , $subject , $body ));


        if($plan ){
            return redirect()->back();
            // return view('coach.clients.plans.create' , compact( 'client','packages' , 'package'));
        }else{
                return redirect()->back()->with('error' , 'Record does not exists');
            }
                
    }

    public function showMessages($reciever_id = 0){

        
        $coach_user = auth()->user();
        $support = User::where('role_id' , 6)->first();
        // $users->prepend($support);
        $support->getContactDetails($coach_user->id);

        $contact_ids = array_unique(Client_Plan::where('coach_id' , $coach_user->coach->id)->get()->pluck('client_id')->toArray());
        // array_push($contact_ids , $support->id );
        // array_unshift($contact_ids , $support->id );

        $contacts = User::whereIn('id' , $contact_ids )->get();
        
        
        // dd($users);
        $request_ids = array_unique(Chat::where('reciever_id' , $coach_user->id)->whereNotIn('sender_id' , $contact_ids)->where('sender_id', '!=' , $coach_user->id)->get()->pluck('sender_id')->toArray());
        
        $user_requests = User::where('role_id' , 5 )->whereIn('id' , $request_ids )->get();
        // foreach($user_requests as $user){
        //     if($user->chat->last()->read_status == 0){
        //         $user->have_unread_message = true;
        //     }else{
        //         $user->have_unread_message = false;
        //     }
        // }
        $total_unread_requests = $user_requests->where('have_unread_message' , true)->count();
        // dd($contacts , $user_requests);
        
        foreach($contacts as $user){
            // $user->contact_details();
            // $user->unread_count = $user->chat->where('reciever_id' , auth()->user()->id)->where('read_status' , 0)->count();
            $user->getContactDetails($coach_user->id);

            
        }
        foreach($user_requests as $user){
            // $user->unread_count = $user->chat->where('reciever_id' , auth()->user()->id)->where('read_status' , 0)->count();
            $user->getContactDetails($coach_user->id);

        }

        $user_requests = $user_requests->sortBy('last_message.created_at');
        $user_requests = $user_requests->reverse()->values();

        $contacts = $contacts->sortBy('last_message.created_at');
        $contacts = $contacts->reverse()->values();

        // $support = User::where('role_id' , 6)->first();
        // $users = auth()->user()->chatable_clients;
        // $users->prepend($support);
        $contacts->prepend($support);

        // dd($contacts , $user_requests );

        // foreach($users as $user){
        //     $user->unread_count = $user->chat->where('reciever_id' , auth()->user()->id)->where('read_status' , 0)->count();
        // }
        $reciever = null;
        $reciever_type = null;

        if($reciever_id != 0){
            $reciever = User::find($reciever_id);
            if(in_array($reciever->id , $request_ids)){
                $reciever_type = 'new request';
            }
        }

        // dd($users);

        return view('coach.chat.index' , compact('contacts' , 'user_requests' , 'reciever_id' , 'reciever' , 'reciever_type' , 'total_unread_requests'));
    }

    // public function showMessages($reciever_id = 0){

    //     // $users = User::whereIn('role_id' , 5)->get();
    //     $support = User::where('role_id' , 6)->first();
    //     $users = auth()->user()->chatable_clients;
    //     $users->prepend($support);


    //     foreach($users as $user){
    //         $user->unread_count = $user->chat->where('reciever_id' , auth()->user()->id)->where('read_status' , 0)->count();
    //     }
    //     $reciever = null;
    //     if($reciever_id != 0){
    //         $reciever = User::find($reciever_id);
    //     }

    //     // dd($users);

    //     return view('coach.chat.index' , compact('users' , 'reciever_id' , 'reciever'));
    // }


   

    public function showWorkout($id){
        $workout = Workout::findOrFail($id);

        if($workout){
            return view('coach.clients.plans.show_workout' , compact('workout'));
        }
    }




    public function filterClients(Request $request){
        
        // dd($request->all());
        
        $filters = $request->all();
        
        // dd($filters);

        $clients = auth()->user()->coach->clients->unique();

        $clients->values()->all();

        $requesting_clients = auth()->user()->coach->new_requests->where('pivot.status' , 'pending');


        // if($filters['lower_date'] && !$filters['upper_date'] ){
           
        //     $filterd_payments = Client_Payment::where('coach_id' , auth()->user()->coach->id )->whereDate('created_at', '>=',  $filters['lower_date'])->get();

        // }elseif(!$filters['lower_date'] && $filte rs['upper_date'] ){
           
        //     $filterd_payments = Client_Payment::where('coach_id' , auth()->user()->coach->id )->whereDate('created_at', '<=',  $filters['upper_date'])->get();

        // }elseif($filters['lower_date'] && $filters['upper_date'] ){
           
        //     $filterd_payments = Client_Payment::where('coach_id' , auth()->user()->coach->id )->where('created_at', '>=', $filters['lower_date'] )->whereDate('created_at' ,'<=', $filters['upper_date'] )->get();

        // }else{
        //     $filterd_payments = Client_Payment::where('coach_id' , auth()->user()->coach->id )->get();
        // }


       
        // if(isset($filters['status'])){
        //     $filterd_payments = $filterd_payments->where('status' , $filters['status'] );
        // }
        

        if($filters['package']){
        
            // dd($request->package ,  $clients);

            foreach($clients as $client){
                if($client->subscribed_packages->where('id' , $filters['package'])->count() == 0 ){
                    $clients->pop($client);
                }
            }
            
            foreach($requesting_clients as $client){
                if($client->subscribed_packages->where('id' , $filters['package'])->count() == 0 ){
                    $clients->pop($client);
                }
            }
            // 
       
        }

        if($filters['client_name']){
            $all_words = explode(' ' , $request->client_name );
            
            $clients = User::whereIn('id' , $clients->pluck('id')->toArray())->where(function($user) use ($all_words) {
                foreach($all_words as $word){
                    $user->where('name','like', '%' . $word . '%');
                }
            } )->get();
            
            
        // $requesting_clients = auth()->user()->coach->new_requests->where('pivot.status' , 'pending');

            $requesting_clients = User::whereIn('id' , $requesting_clients->pluck('id')->toArray())->where(function($user) use ($all_words) {
                foreach($all_words as $word){
                    $user->where('name','like', '%' . $word . '%');
                }
            } )->get();
        }

        $requesting_clients = auth()->user()->coach->new_requests->whereIn('id' , $requesting_clients->pluck('id')->where('pivot.status' , 'pending')->toArray() );

        // $requesting_clients = auth()->user()->coach->new_requests->where('pivot.status' , 'pending');

        foreach($requesting_clients as $client){
            if(Package::find($client->pivot->package_id)){
                $client->pivot->package = Package::find($client->pivot->package_id);
            }else{
                $client->pivot->package = null;
                
            }
        }
        
        // dd($requesting_clients);
        
        // if($filters['package']){
        //     $all_words = explode(' ' , $request->client_name );
            
        //     $filterd_payments = User::with('client')->where('coach_id' , auth()->user()->coach->id )->whereIn('id' , $filterd_payments->pluck('id')->toArray())->whereHas( 'subscribed_packages' , function($payment) use ($all_words) {
        //         foreach($all_words as $word){
        //             $payment->where('name','like', '%' . $word . '%');
        //             $payment->wherePivot('coach_id' , auth()->user()->coach->id );
        //         }
        //     } )->get();
        // }
        $packages = auth()->user()->coach->packages;

        
        return view('coach.clients.index', compact('clients' , 'filters' , 'requesting_clients' , 'packages'));



    }


    public function exportClients($ids){

        $clients = User::whereIn('id' , json_decode($ids))->get();

        return Excel::download(new ExportClients($clients), 'clients.xlsx');

    }
    
    public function exportClients1($ids , $coach_id ){

        $coach = Coach::find($coach_id );
        $clients = User::whereIn('id' , json_decode($ids))->get();

        return Excel::download(new ExportClients($clients , $coach ), 'clients.xlsx');

    }




    public function showReviews(){
        
        $reviews = Review::where('coach_id' , auth()->user()->coach->id ) -> latest() ->paginate(20);
        return view('coach.reviews.index' , compact('reviews'));
    }

}
