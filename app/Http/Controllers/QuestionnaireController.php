<?php

namespace App\Http\Controllers;

use App\User;
use App\Coach;
use App\Answer;
use App\Question;
use App\Questionnaire;
use App\Question_Choices;
use Illuminate\Http\Request;

class QuestionnaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Questionnaire::all();
        return view('admin.questionnaires.index' , compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::whereIn('role_id' , [3,4] )->get();
        return view('admin.questionnaires.create_new' , compact('users'));
        // return view('admin.questionnaires.create' , compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $question = Questionnaire::create([
            'name' => $request->questionnaire,
        ]);

        $question->users()->attach($request->assigned);

        if($request->question){

            for($i=0; $i<count($request->question); $i++){
                
            $questins = Question::create([
                'question' => $request->question[$i]["name"],
                'type' => $request->question[$i]["type"],
                'questionnaires_id' => $question->id,

                ]);
                if(isset($request->question[$i]["choices"])){
                    for($j=0; $j<count($request->question[$i]["choices"]); $j++){

                        Question_Choices::create([
                            'question_id' => $questins->id,
                            'name' => $request->question[$i]["choices"][$j],
                            ]);
                        }
                    }
                    
                }
            }

        /*if($question->type == 'checkbox' || $question->type == 'radiobutton'){
            if(isset($request->choices)){
                foreach($request->choices as $choice){
                    Question_Choices::create([
                        'question_id' => $question->id,
                        'name' => $choice,
                    ]);
                }
            }
        }*/

        if($question)
        return redirect('/admin/questionnaires')->with('success' , 'Question created successfully');
        else
        return redirect()->back()->with('error' , 'Some problem occured');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $questionnaire = Questionnaire::findOrFail($id);
        return view('admin.questionnaires.show' , compact('questionnaire'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $questionnaire = Questionnaire::with('questions' , 'questions.choices')->findOrFail($id);
        $users = User::whereIn('role_id' , [3,4] )->get();
        $choice = Question_Choices::all()->count();
        return view('admin.questionnaires.create_new' , compact('users' , 'questionnaire','choice'));
        // return view('admin.questionnaires.edit' , compact('users' , 'question','choice'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());


        $question = Questionnaire::findOrFail($id);
        $question->name = $request->questionnaire;
        $question->save();

        $question->users()->sync($request->assigned);
        $all_questions = $request->questions;

        // dd($all_questions);
        // dd($request->all());
        // $deleteQuestion = Question::where('questionnaires_id',$question->id)->delete();

        if($request->question){

        for($i=0; $i<count($request->question); $i++){

            if(isset($request->questions[$i])){
                $key = array_search( $request->questions[$i], $request->questions );
                unset($all_questions[$key]);
                
                // dd($all_questions);
                // dd('asd');
            }
            // dd($request->all());

            if(isset($request->questions[$i])){
                $q = Question::find($request->questions[$i]);
                $q->question = $request->question[$i]["name"];
                $q->type = $request->question[$i]["type"];
                $q->save();
                
            }else{
                $q = Question::create([
                    'question' => $request->question[$i]["name"],
                    'type' => $request->question[$i]["type"],
                    'questionnaires_id' => $question->id,
                ]);
            }
            
            $q->choices()->delete();

            if(isset($request->question[$i]["choices"])){
                for($j=0; $j<count($request->question[$i]["choices"]); $j++){

                    Question_Choices::create([
                        'question_id' => $q->id,
                        'name' => $request->question[$i]["choices"][$j],
                    ]);
                }
            }

        }
        // $deleted_questions = array_diff($request->questions , $all_questions);
        // dd($deleted_questions , $request->questions , $all_questions);
        if($all_questions){
            foreach($all_questions as $q){
                Question::find($q)->delete();
            }
        }
        }else{
            Questionnaire::find($question->id)->questions()->delete();
        }



        /*
                    for($i=1; $i<=count($request->question); $i++) {

                        if(isset($request->question[$i])){

                        $questins = Question::updateOrCreate([
                            'question' => $request->question[$i]["name"],
                            'type' => $request->question[$i]["type"],
                            'questionnaires_id' => $question->id,

                        ],
                            [
                                'question' => $request->question[$i]["name"],
                                'type' => $request->question[$i]["type"],
                                'questionnaires_id' => $question->id,


                            ]);

                        $current_questions[] = $questins->id;

                        }
                        if(isset($request->question[$i]["choices"])){
                        for ($j = 0; $j<count($request->question[$i]["choices"]); $j++) {


                            if(isset($request->question[$i]["choices"][$j])){

                                $save_choice = Question_Choices::updateOrCreate([
                                    'question_id' => $questins->id,
                                    'name' => $request->question[$i]["choices"][$j],
                                ], [
                                        'question_id' => $questins->id,
                                        'name' => $request->question[$i]["choices"][$j],
                                    ]
                                );

                                $current_choices[] = $save_choice;
                            }
                        }
                        }


                    }






                   if(isset($current_choices)){

                        $all_choices = Question_Choices::where('question_id', $questins->id)->pluck('id')->toArray();

                        $deleted = array_merge(array_diff($all_choices, $current_choices), array_diff($current_choices, $all_choices));
                        // dd($deleted , $all_choices);
                        foreach ($deleted as $delete) {
                            Question_Choices::findOrFail($delete)->delete();
                        }
                   }

                   if(isset($current_questions)){

                    $all_questions = Question::where('questionnaires_id', $question->id)->pluck('id')->toArray();

                    $deletedquestion = array_merge(array_diff($all_questions, $current_questions), array_diff($current_questions, $all_questions));
                    // dd($deleted , $all_choices);
                    foreach ($deletedquestion as $delete) {
                        Question::findOrFail($delete)->delete();
                    }
                   }
                   */




            if($question->save())
        return redirect('/admin/questionnaires')->with('success' , 'Question Update successfully');
        else
        return redirect()->back()->with('error' , 'Some problem occured');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function storeAnswer($id , Request $request){
        
        
        // dump($request->all());
        foreach($request->questions as $question){
            if($request->{'question-'.$question}){
                
                // dd($request->{'question-'.$question} , $question);
                
                $answer = Answer::updateOrCreate(
                    ['question_id' => $question ],
                [
                    'question_id' => $question,
                    'coach_id' => auth()->user()->coach->id,
                    'answer' =>  is_array($request->{'question-'.$question}) ? json_encode($request->{'question-'.$question}) : $request->{'question-'.$question},
                    
                    ]
                );
            }
        }

        return redirect()->back();
    }


    public function destroy($id)
    {
        $question = Questionnaire::findOrFail($id);
        $question->users()->sync([]);
        $question->delete();

        return redirect()->back()->with('success' , 'Question deleted successfully');

    }

    public function assignedto($id){

        $question = Questionnaire::find($id);
        return view('admin.questionnaires.assignedto',compact('question'));
    }
    
    public function showCoachAnswers($questionnaire_id , $coach_id ){

        $questionnaire = Questionnaire::find($questionnaire_id);
        $coach = Coach::find($coach_id);
        return view('admin.questionnaires.showAnswer',compact('questionnaire' , 'coach'));
    }
}
