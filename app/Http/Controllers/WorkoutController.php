<?php

namespace App\Http\Controllers;

use App\Tag;
use App\Workout;
use App\Perfit_Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class WorkoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $workouts = Workout::all();

        // dd(Workout::where('id' , '.')->get());

        return view('admin.workouts.index' , compact('workouts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $tags = Tag::all();
        return view('admin.workouts.create' , compact('tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());


        $request->validate([
            'name'=>'required',
        ]);
        $workout = Workout::create([
            'name' => $request->get('name'),
            'description' => $request->get('description'),
            'tags' => '',
        ]);

        $workout->Exercise_Id =$workout->id; 
        
        $workout->tag()->attach($request->tags);
        
        if($request->has('images')){
            $all_images = array();

            foreach($request->images as $image){
                $ext = $image->getClientOriginalExtension();
                $name = $image->getClientOriginalName();

                $file = Perfit_Image::imageResize($image);
                $file->save(public_path().'/workouts/images/' . $name.time().'.'.$ext , 90);
                // $image->move(public_path().'/workouts/images/',$name.time().'.'.$ext);

                $all_images[] = $name.time().'.'.$ext;
            }

            $workout->media = isset($all_images) ? json_encode($all_images) : null ;
        }
        
        if($request->has('videos')){
            $all_videos = array();

            foreach($request->videos as $video){
                $ext = $video->getClientOriginalExtension();
                $name = $video->getClientOriginalName();
                $video->move(public_path().'/workouts/videos/',$name.time().'.'.$ext);
                $all_videos[] = $name.time().'.'.$ext;
            }

            $workout->videos = isset($all_videos) ? json_encode($all_videos) : null ;
        }

               
        // if($request->has('video')){
            
        //     $ext = $request->video->getClientOriginalExtension();
        //     $name = $request->video->getClientOriginalName();
        
        //     $request->video->move(public_path().'/workouts/videos/',$name.time().'.'.$ext);
        
        //     $workout->video = $name.time().'.'.$ext;
        //     }

        

        $workout->save();
        
        if($workout)
            return redirect()->route('admin.workouts.index')->with('success','workout added susscessfully');
        else
            return redirect()->back()->with('error','Some problem try again!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $workout = Workout::find($id);
        $tags = Tag::all();

        return view('admin.workouts.edit',compact('workout','tags'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        // dd($request->all());


        $request->validate([
            'name'=> 'required',

        ]);
        $workout = Workout::find($id);
        $workout->name = $request->get('name');
        $workout->description = $request->get('description');
        // $workout->tags = $request->get('tags');
        $workout->save();

        $workout->tag()->sync($request->tags);

        // removing deleted images
            if($request->deleted_images){
                $current_images = json_decode($workout->media);
                foreach($request->deleted_images as $deleted){
                $del = public_path().'/workouts/images/'.$current_images[$deleted];

                    if(file_exists($del)){
                        File::delete($del);
                    }
                    unset($current_images[$deleted]);

                }
                $current_images = array_values($current_images);
                // dd($current_images);

                $workout->media  = json_encode($current_images);
            }
        
            // uploading images
        if($request->has('images')){
            $all_images = (isset($workout->media) && is_array(json_decode($workout->media)) ) ?  json_decode($workout->media) : array() ;

            foreach($request->images as $image){
                $ext = $image->getClientOriginalExtension();
                $name = $image->getClientOriginalName();

                $file = Perfit_Image::imageResize($image);
                $file->save(public_path().'/workouts/images/' . $name.time().'.'.$ext , 90);
                // $image->move(public_path().'/workouts/images/',$name.time().'.'.$ext);
                $all_images[] = $name.time().'.'.$ext;
            }

            $workout->media = isset($all_images) ? $all_images : null ;
            // $workout->media = isset($all_images) ? json_encode($all_images) : null ;
        }
        
         // removing deleted videos
         if($request->deleted_videos){
            $current_videos = json_decode($workout->videos);
            foreach($request->deleted_videos as $deleted){
            $del = public_path().'/workouts/videos/'.$current_videos[$deleted];

                if(file_exists($del)){
                    File::delete($del);
                }
                unset($current_videos[$deleted]);

            }
            $current_videos = array_values($current_videos);
            // dd($current_videos);

            $workout->videos  = json_encode($current_videos);
        }
        // dd($request->all( ));
        // uploading videos
        if($request->has('videos')){

            $all_videos = (isset($workout->videos) && is_array(json_decode($workout->videos)) ) ?  json_decode($workout->videos) : array() ;

            foreach($request->videos as $video){
                $ext = $video->getClientOriginalExtension();
                $name = $video->getClientOriginalName();
                $video->move(public_path().'/workouts/videos/',$name.time().'.'.$ext);
                $all_videos[] = $name.time().'.'.$ext;
            }
            $workout->videos = isset($all_videos) ? json_encode($all_videos) : null ;
        }

        // $workout->Exercise_Id = '000'. $workout->id; 

        $workout->save();
        


        if($workout)
            return redirect('/admin/workouts')->with('success','workout updated successfully');
            // return redirect()->route('admin.workouts.index')->with('success','workout updated successfully');
        else
            return redirect()->back()->with('error','Some problem, try again!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $workout = Workout::find($id);
        
        if($workout->media && is_array(json_decode($workout->media)) ){
            $all_images = json_decode($workout->media);
            foreach($all_images as $image){
                $old = public_path().'/workouts/images/'.$image;
                if(file_exists($old)){
                    File::delete($old);
                }
            }
        }

        $workout->delete();

        return redirect()->route('admin.workouts.index')->with('success','workout deleted successfully');
    }
}
