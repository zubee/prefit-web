<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers{
        login as baseLogin;

    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    /*protected $redirectTo = RouteServiceProvider::HOME;*/
    
    public function login(Request $request)
    {
        $user = User::where('username' , $request->username)->first();
        // dd($user);
        if($user){
        if($request->role == 1 && !in_array($user->role_id , [1,2])){
            // dd('admin login but not admin');
            return redirect()->back()->with('unauthorized' , 'You are not an Admin' );
        }elseif($request->role == 3 && !in_array($user->role_id , [3,4])){
            return redirect()->back()->with('unauthorized' , 'You are not an A Coach' );
            // dd('trainer but not trainer');
        }else{
            
            // dd('Parent');
            if(in_array($user->role_id , [3,4]) && $user->status == 0 ){
                return redirect()->back()->with('deactivated' , 'Your Account is deactivated' );
            }
        }

        
    }
    return $this->baseLogin($request);
    }

    public function redirectTo(){
        $role = Auth::user()->role_id;

        // dd(auth()->user());
        // switch ($role){
        //     case '1':
        //         return route('admin.index');
        //         break;
        //     case '2':
        //         return route('admin.index');
        //         break;
        //     case '3':
        //         return route('trainer.dashboard');
        //         break;
        //     case '4':
        //         return route('dietitian.dashboard');
        //         break;
    
        //     default :
        //         return route('/');
        // }

        if(auth()->user()){
            if(auth()->user()->role_id == 1 || auth()->user()->role_id == 2){
            return route('admin.index');
            // return redirect('/admin/dashboard');
            }
            elseif(auth()->user()->role_id == 3 || auth()->user()->role_id == 4){
                // return redirect('/dietitian');
                return route('coach.index');
            }
        }else{
            return redirect('/');
        }

        
    }


    

    // if(auth()->user()){
    //     if(auth()->user()->role_id == 1 || auth()->user()->role_id == 2){
    //     return redirect('/admin/dashboard');
    //     }
    //     elseif(auth()->user()->role_id == 3){
    //         return redirect('/trainer/dashboard');
    //     }
    //     elseif(auth()->user()->role_id == 3){
    //         return redirect('/dietitian/dashboard');
    //     }
    // }else{
    //     return redirect('/');
    // }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function username()
    {
        return 'username';
    }
    protected function credentials(Request $request)
    {
       /* $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
            ? $this->username()
            : 'username';
        return [
            $field => $request->get($this->username()),
            'password' => $request->password,
        ];*/



        return $request->only($this->username(),'password');
    }

}
