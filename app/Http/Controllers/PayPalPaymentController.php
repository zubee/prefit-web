<?php 

namespace App\Http\Controllers;

use stdClass;
use Paypalpayment;
use PayPal\Api\Payer;
use Payum\Core\Payum;
use PayPal\Api\Amount;
use PayPal\Api\Address;
// use Payum\Core\Model\CreditCard;
use PayPal\Api\CreditCard;
use PayPal\Api\Transaction;
use Illuminate\Http\Request;

use Payum\Core\PayumBuilder;
use Payum\Core\Model\Payment;
use PayPal\Api\FundingInstrument;
use Payum\Paypal\Rest\Model\PaymentDetails;




class PaypalPaymentController {

    public function braintreePayment(Request $request)
    {
        $gateway = new \Braintree\Gateway([
            'environment' => env('BRAINTREE_MODE'),
            'merchantId' => env('BRAINTREE_MERCHANT_ID'),
            'publicKey' => env('BRAINTREE_PUBLIC_KEY'),
            'privateKey' => env('BRAINTREE_PRIVATE_KEY')
        ]);
      
        // $clientToken = $gateway->clientToken()->generate([
        //     "customerId" => "1234"
        // ]);
        // $customer = $gateway->customer()->create([
        //     'firstName' => 'Mike',
        //     'lastName' => 'Jones',
        //     'company' => 'Jones Co.',
        //     'email' => 'mike.jones@example.com',
        //     'phone' => '281.330.8004',
        //     'fax' => '419.555.1235',
        //     'website' => 'http://example.com'
        // ]);
        // dd($customer->customer->id);

        $customer = $gateway->customer()->create([
            // 'paymentMethodNonce' => nonceFromTheClient,
            'firstName' => 'Mike',
            // 'lastName' => 'Jones',
            // 'company' => 'Jones Co.',
            'email' => 'mike.jones@example.com',
            // 'phone' => '281.330.8004',
            // 'fax' => '419.555.1235',
            // 'website' => 'http://example.com',
            'creditCard' => [
                'number' => '4111111111111111',
                'expirationDate' => '06/22',
                'cvv' => '100',
                'billingAddress' => [
                    'firstName' => 'Jen',
                    // 'lastName' => 'Smith',
                    // 'company' => 'Braintree',
                    'streetAddress' => '123 Address',
                    // 'locality' => 'City',
                    // 'region' => 'State',
                    // 'postalCode' => '12345'
                ]
            ]
        ]);

        // dd($customer , $customer->customer->creditCards[0]->token);
        
        // $card = $gateway->creditCard()->create([
        //     'customerId' => $customer->customer->id ,
        //     'number' => '4111111111111111',
        //     'expirationDate' => '06/22',
        //     'cvv' => '100'
        // ]);

        // dd($customer->paymentMethods['token']);

        // $payment_method = $gateway->paymentMethod()->create([
        //     'customerId' => $customer->customer->id,
        // ]);

        // dd($payment_method);

        $nonce = $gateway->paymentMethodNonce()->create($customer->customer->creditCards[0]->token);
        // dd($nonce->paymentMethodNonce->nonce);
        

        // $payload = $request->input("payload", false);
        // $status = \Braintree\Braintree_Transaction::sale([
        $status = $gateway->transaction()->sale([
                            "amount" => "10.00",
                            "paymentMethodNonce" => $nonce->paymentMethodNonce->nonce,
                            // "options" => [
                            //            "submitForSettlement" => True
                            // ],
                        ]);

        dd($status);
        // $customer = $gateway->customer()->create([
        //     'firstName' => 'Mike',
        //     'lastName' => 'Jones',
        //     'company' => 'Jones Co.',
        //     'email' => 'mike.jones@example.com',
        //     'phone' => '281.330.8004',
        //     'fax' => '419.555.1235',
        //     // 'creditCard' => '4111111111111111',
        //     'website' => 'http://example.com',
        //     'paymentMethodNonce' => $clientToken

        // ]);


        
        // $result->success;

        dd($result);

        // $result = $gateway->paymentMethodNonce()->create('A_PAYMENT_METHOD_TOKEN');
        // $nonce = $result->paymentMethodNonce->nonce;



        
        // $payload = $request->input("payload", false);
        // $nonce = $payload["nonce"];
        // // $status = \Braintree\Braintree_Transaction::sale([
        // $status = $gateway->transaction()->sale([
        //                         "amount" => "10.00",
        //                         "paymentMethodNonce" => $nonce,
        //                         "options" => [
        //                                    "submitForSettlement" => True
        //                         ],
        //           ]);
        return response()->json($status);



    }



    public function payment(Request $request)
    {
        $request_params = array (
          	'METHOD' => 'Paypal',
          	// 'METHOD' => 'DoDirectPayment',
          	'USER' => 'sb-47awg63006468_api1.business.example.com',
          	'PWD' => 'K26JBM34PTVMWPA2',
          	'SIGNATURE' => 'ASaquCljUE-24WrQoCBhrQnLya8SAVpqzmwWoqVR8-sQ0Z1oeXhG3o0i',
          	'VERSION' => '85.0',
          	'PAYMENTACTION' => 'Sale',
          	'IPADDRESS' => '127.0.0.1',
          	'CREDITCARDTYPE' => 'Visa',
          	'ACCT' => '4032035715036612',
          	'EXPDATE' => '082023',
          	'CVV2' => '764',
          	'FIRSTNAME' => 'Yang',
          	'LASTNAME' => 'Ling',
          	'STREET' => '1 Main St',
          	'CITY' => 'San Jose',
          	'STATE' => 'CA',
          	'COUNTRYCODE' => 'US',
          	'ZIP' => '95131',
          	'AMT' => '100.00',
          	'CURRENCYCODE' => 'USD',
          	'DESC' => 'Testing Payments Pro'
       );     
     
       $nvp_string = '';     
       foreach($request_params as $var=>$val)
       {
          $nvp_string .= '&'.$var.'='.urlencode($val);
       }
     
       	$curl = curl_init();     
       	curl_setopt($curl, CURLOPT_VERBOSE, 0);     
       	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);     
       	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);     
       	curl_setopt($curl, CURLOPT_TIMEOUT, 30);     
       	curl_setopt($curl, CURLOPT_URL, 'https://api-3t.sandbox.paypal.com/nvp');     
       	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);     
       	curl_setopt($curl, CURLOPT_POSTFIELDS, $nvp_string); 

       	$result = curl_exec($curl);     
       	curl_close($curl);   

       	$data = $this->NVPToArray($result);

        dd($data);
       	if($data['ACK'] == 'Success') {
       		# Database integration...
       		echo "Your payment was processed success.";
       	} if ($data['ACK'] == 'Failure') {
       		# Database integration...
       		echo "Your payment was declined/fail.";
       	} else {
       		echo "Something went wront please try again letter.";
       	}
    }
    public function  NVPToArray($NVPString)
    {
       $proArray = array();
       while(strlen($NVPString)) {
            // key
            $keypos= strpos($NVPString,'=');
            $keyval = substr($NVPString,0,$keypos);
            //value
            $valuepos = strpos($NVPString,'&') ? strpos($NVPString,'&'): strlen($NVPString);
            $valval = substr($NVPString,$keypos+1,$valuepos-$keypos-1);

            $proArray[$keyval] = urldecode($valval);
            $NVPString = substr($NVPString,$valuepos+1,strlen($NVPString));
        }
        return $proArray;
    }




    /** @var \Payum\Core\Payum $payum */
    

    public function PayumPayment(){
        
        $payum = (new PayumBuilder())
        ->addDefaultStorages()
        ->addGateway('gatewayName', [
            'factory' => 'paypal_rest',
            'client_id' => env('PERFIT_PAYPAL_CLIENT_ID'),
            'client_secret' => env('PERFIT_PAYPAL_CLIENT_SECRET'),
            // 'config_path' => 'REPLACE IT',
            'config_path' => '%kernel.root_dir%/config/sdk_config.ini'

        ])->getPayum();
    
        dd($payum);

        // $paypalRestPaymentDetailsClass = new PaymentDetails();

        // $storage = $payum->getStorage($paypalRestPaymentDetailsClass);

        // $payment = $storage->create();
        // $storage->update($payment);

        // dd($storage);

        // $storage = $this->getPayum()->getStorage('Payum\Paypal\Rest\Model\PaymentDetails');
        // $payment = $storage->create();
    
        // $storage = $payum->getStorage($paypalRestPaymentDetailsClass);

        // $payment = $storage->create();
        // $storage->update($payment);
        
        // $storage = $payum->getStorage($paypalRestPaymentDetailsClass);

        // $payment = $storage->create();
        // $storage->update($payment);
        
        $address = new Address();
        $address->line1 = "3909 Witmer Road";
        // $address->line2 = "Niagara Falls";
        $address->city = "Doha";
        $address->state = "Doha";
        $address->postal_code = "14305";
        $address->country_code = "QA";
        $address->phone = "716-298-1822";
        
        // dd($address);

        $card = new CreditCard();
        $card->type = "visa";
        $card->number = "4417119669820331";
        $card->expire_month = "11";
        $card->expire_year = "2019";
        $card->cvv2 = "012";
        $card->first_name = "Joe";
        $card->last_name = "Shopper";
        $card->billing_address = $address;
        

        // dd($card);

        $fi = new FundingInstrument();
        $fi->credit_card = $card;
        
        $payer = new Payer();
        $payer->payment_method = "credit_card";
        $payer->funding_instruments = array($fi);
        
        $amount = new Amount();
        $amount->currency = "USD";
        $amount->total = "1.00";
        
        $transaction = new Transaction();
        $transaction->amount = $amount;
        $transaction->description = "This is the payment description.";
        
        // dd($transaction);

        // $payment = new stdClass();
        $payment->intent = "sale";
        $payment->payer = $payer;
        $payment->transactions = array($transaction);
        
        // dd($transaction);

        $captureToken = $payum->getTokenFactory()->createCaptureToken('paypalRest', $payment, 'create_recurring_payment.php');

        dd($captureToken);
        
    }




    
    /*
    * Process payment using credit card
    */
    public function paywithCreditCard()
    {
        // ### Address
        // Base Address object used as shipping or billing
        // address in a payment. [Optional]
        $shippingAddress = Paypalpayment::shippingAddress();
        $shippingAddress->setLine1("3909 Witmer Road")
            ->setLine2("Niagara Falls")
            ->setCity("Niagara Falls")
            ->setState("NY")
            ->setPostalCode("14305")
            ->setCountryCode("US")
            ->setPhone("716-298-1822")
            ->setRecipientName("Jhone");

        // ### CreditCard
        $card = Paypalpayment::creditCard();
        $card->setType("visa")
            ->setNumber("4758411877817150")
            ->setExpireMonth("05")
            ->setExpireYear("2019")
            ->setCvv2("456")
            ->setFirstName("Joe")
            ->setLastName("Shopper");

        // ### FundingInstrument
        // A resource representing a Payer's funding instrument.
        // Use a Payer ID (A unique identifier of the payer generated
        // and provided by the facilitator. This is required when
        // creating or using a tokenized funding instrument)
        // and the `CreditCardDetails`
        $fi = Paypalpayment::fundingInstrument();
        $fi->setCreditCard($card);

        // ### Payer
        // A resource representing a Payer that funds a payment
        // Use the List of `FundingInstrument` and the Payment Method
        // as 'credit_card'
        $payer = Paypalpayment::payer();
        $payer->setPaymentMethod("credit_card")
            ->setFundingInstruments([$fi]);

        $item1 = Paypalpayment::item();
        $item1->setName('Ground Coffee 40 oz')
                ->setDescription('Ground Coffee 40 oz')
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setTax(0.3)
                ->setPrice(7.50);

        $item2 = Paypalpayment::item();
        $item2->setName('Granola bars')
                ->setDescription('Granola Bars with Peanuts')
                ->setCurrency('USD')
                ->setQuantity(5)
                ->setTax(0.2)
                ->setPrice(2);


        $itemList = Paypalpayment::itemList();
        $itemList->setItems([$item1,$item2])
            ->setShippingAddress($shippingAddress);


        $details = Paypalpayment::details();
        $details->setShipping("1.2")
                ->setTax("1.3")
                //total of items prices
                ->setSubtotal("17.5");

        //Payment Amount
        $amount = Paypalpayment::amount();
        $amount->setCurrency("USD")
                // the total is $17.8 = (16 + 0.6) * 1 ( of quantity) + 1.2 ( of Shipping).
                ->setTotal("20")
                ->setDetails($details);

        // ### Transaction
        // A transaction defines the contract of a
        // payment - what is the payment for and who
        // is fulfilling it. Transaction is created with
        // a `Payee` and `Amount` types

        $transaction = Paypalpayment::transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());

        // ### Payment
        // A Payment Resource; create one using
        // the above types and intent as 'sale'

        $payment = Paypalpayment::payment();

        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setTransactions([$transaction]);

        try {
            // ### Create Payment
            // Create a payment by posting to the APIService
            // using a valid ApiContext
            // The return object contains the status;
            $payment->create(Paypalpayment::apiContext());
        } catch (\PPConnectionException $ex) {
            return response()->json(["error" => $ex->getMessage()], 400);
        }

        return response()->json([$payment->toArray()], 200);
    }




     
}
