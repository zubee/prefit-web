<?php

namespace App\Http\Controllers;

use App\User;
use stdClass;
use App\Package;
use App\Workout;
use Carbon\Carbon;
use App\Perfit_Image;
use App\Client_Payment;
use Illuminate\Http\Request;
use App\Exports\ExportClients;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

class DashboardController extends Controller
{
       public function index(){

        $counter['users'] = User::where('role_id' , 5 )->get()->count();
          
        $counter['coaches'] = User::whereIn('role_id' , [3 , 4] )->get()->count();  

        $counter['packages'] = Package::all()->count();  

        $counter['workouts'] = Workout::all()->count();  


        $payments = Client_Payment::get()->groupBy(function($date) {
            //return Carbon::parse($date->created_at)->format('Y'); // grouping by years
            return Carbon::parse($date->created_at)->format('d'); // grouping by months
        });

        // $payments = Client_Payment::whereRaw('DATE(created_at) = DATE_SUB(CURDATE(), INTERVAL 7 DAY)')->get();
            

        // $payments = $payments->values();

        for($i = 1 ; $i < 8; $i++ ){
            $dates[] = Carbon::now()->subDays($i); 
        }

        // for($i = 0 ; $i < 7; $i++ ){
        //     $months[] = Carbon::now()->subMonth($i); 
        // }

        foreach($dates as $k => $date){
            // dump($months[$k]->month());
            
            $week_payments[$date->format('d')] = Client_Payment::whereDate('created_at', Carbon::now()->subDays($k+1))->get()->count();
            // $month_payments[$months[$k]->format('m')] = Client_Payment::whereMonth('created_at', Carbon::now()->subMonth($k))->get()->count();

        }
        // dd($month_payments , $dates );

        $week_payments = array_values($week_payments);
        // $month_payment_names = array_keys($month_payments);
        // $month_payments = array_values($month_payments);


        $data = [];
        
        
        foreach($dates as $k => $date){
          $obj = new stdClass();
          $all_dates[] = $date->toDateString();
          $all_payments[] = $week_payments[$k];
          $obj->date = $date;
          $obj->units =  $week_payments[$k] ;
          $data[] = $obj;
        }     
        
        // $month_data = [];
        // foreach($months as $k => $month){

        //   $obj = new stdClass();
        //   $obj->month = $month->format('F Y');
        //   $obj->amount =  $month_payments[$k] ;
        //   $month_data[] = $obj;
        // }

        // dd($month_data);


        return view('admin.index' , compact('counter' , 'dates' , 'payments' , 'data' , 'all_dates', 'all_payments'));
    }
    


    public function showMessages($reciever_id = 0){

        // $users = User::whereIn('role_id' , 5)->get();
        $support = User::where('role_id' , 6)->first();

        $users = User::whereNotIn('role_id' , [1 , 2 , 6] )->get();

        $reciever = null;
        if($reciever_id != 0){
            $reciever = User::find($reciever_id);
        }

        foreach($users as $user){
            $user->getContactDetails($support->id);
            // $user->unread_count = $user->chat->where('reciever_id' , $support->id)->where('read_status' , 0)->count();
        }
        $users = $users->sortBy('last_message.created_at');
        $users = $users->reverse()->values();

        // $users = $users->sortBy('last_unread_message.created_at');
        // $users = $users->reverse()->values();

        // $usersDesc = $users->sortBy('sorting_factor');
        // $users = $users->reverse()->values();

        // $users = $users->sortByDesc('sorting_factor');

        // dump($users->pluck('sorting_factor'));
        // dump($usersDesc->pluck('sorting_factor'));
        // dump($users->pluck('id'));
        // dump($users);

        // $users = $users->sortBy('sorting_factor');
        // $users = $users->reverse()->values();
        
        // dump($users->pluck('id'));
        // dd($users->pluck('sorting_factor'));

        return view('admin.chat.index' , compact('users' , 'support' , 'reciever'));
    }

    public function editContactUS(){
        
        $support = User::where('role_id' , 6)->first();
        return view('admin.contact.edit' , compact('support'));
    }
    

    public function updateContactUS(Request $request){
        

        $support = User::where('role_id' , 6)->first();

        if($request->has('image')){
            $path = public_path().'/images/profile_images/'.$support->photo;

            if(file_exists($path)){
                    File::delete($path);
                }
                $ext = $request->image->getClientOriginalExtension();
                
                $file = Perfit_Image::imageResize($request->image);
                $file->save(public_path().'/images/profile_images/' . $support->id .'.'. $ext , 90);
                // $request->image->move(public_path().'/images/profile_images/' , $support->id .'.'. $ext);

                $support->photo = $support->id .'.'. $ext;
            }


        $support->name = $request->name;
        $support->email = $request->email;
        $support->save();

        return redirect()->back()->with('success' , 'Contact Information Updated');
    }
    

    public function exportClients($ids){

        $clients = User::whereIn('id' , json_decode($ids))->get();

        return Excel::download(new ExportClients($clients), 'clients.xlsx');

    }

}
