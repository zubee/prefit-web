<?php

namespace App\Http\Controllers\Api;

use App\Tag;
use App\User;
use App\Coach;
use App\Package;
use App\Trainer;
use App\Workout;
use App\Coach_Tag;
use App\Dietitian;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DataController extends Controller
{
   
    
    
    public function getTrainers($user_id = 0){

        $user = User::find($user_id);
        // $user_id = 0;
        // dd('sad');
        if($user_id > 0 && $user && $user->profile && $user->profile->goals && is_array($user->profile->goals) ){

            $trainers = Coach::fetchSortedCoaches($user , 3);

        }elseif($user_id == 0){
            $trainers = Coach::with('user' , 'user.tags' , 'tags'  , 'packages' , 'reviews' )->fetchTrainers()->get();
            $trainers = $trainers->sortByDesc('total_review_count');
            // dd($trainers);
        }
        else{
            $trainers = Coach::where('role' , 3)->with('user' , 'tags'  , 'user.tags' , 'packages' , 'reviews' )->get();
            $trainers = $trainers->sortByDesc('total_review_count');

            // $trainers = Coach::where('role' , 3)->with('user' , 'tags' , 'packages' , 'reviews' , 'reviews.user' )->get();

        }

        // dump($trainers);


        if($trainers){  
            foreach($trainers as $trainer){
                // $trainer->portfolio = strip_tags( $trainer->portfolio);


                foreach($trainer->packages as $package){

                    // $package->description = strip_tags($package->description);
                    $package->addLineBreaks();

                    $package->makeHighlightsArray();
                    
                }
                $trainer->addLineBreaks();

            }

            // dd($trainers);

            return response()->json( ['success' => true , 'user' => $user ?? NULL , 'trainers' => $trainers->values()->toArray() ]); 
            // return response()->json( ['success' => true , 'user' => $user ?? NULL , 'trainers' => $trainers->reverse()->values() ]); 
        }else{
            return response()->json (['success' => false ,  'error' => 'Some Problem Occoured']); 
        }

    }

    

    public function viewTrainer($id){

        // $trainer = Coach::with('user')->find($id);

        // dd($trainer);
        $trainer = Coach::with('user' , 'tags' , 'reviews' , 'reviews.user')->find($id);



        if($trainer && $trainer->role == 3){

            // $trainer->portfolio = strip_tags( $trainer->portfolio);


            if($trainer->packages){

                $trainer->packages = $trainer->packages;
                
                if($trainer->packages){
                foreach($trainer->packages as $package){
                    
                    // $package->description = strip_tags($package->description);
                    $package->addLineBreaks();

                    $package->makeHighlightsArray();
                    }
                }            
            }

            $trainer->addLineBreaks();

            return response()->json( ['success' => true ,  'trainer' => $trainer]); 

        }else{
            return response()->json (['success' => false ,  'error' => 'Trainer Not Found']); 
        }

    }



    public function getDietitians($user_id = 0){

        // $dietitians = Coach::with('user' , 'user.tags')->where('role' , 4)->get();

        $user = User::find($user_id);
        if($user_id > 0 && $user && $user->profile && $user->profile->goals && is_array($user->profile->goals)){

            $dietitians = Coach::fetchSortedCoaches($user , 4);

        }elseif($user_id == 0){
            $dietitians = Coach::with('user' , 'user.tags' , 'tags'  , 'packages' , 'reviews')->fetchDietitians()->get();
            $dietitians = $dietitians->sortByDesc('total_review_count');
            // dd($trainers);
        }
        else{
            $dietitians = Coach::where('role' , 4)->with('user' ,'tags'  , 'packages' , 'reviews' )->get();
            $dietitians = $dietitians->sortByDesc('total_review_count');

        }


        if($dietitians){

            foreach($dietitians as $dietitian){
                // $dietitian->portfolio = strip_tags( $dietitian->dietitian);
                
                $dietitian->packages = $dietitian->packages;
                
                foreach($dietitian->packages as $package){

                    // $package->description = strip_tags($package->description);
                    $package->addLineBreaks();

                    $package->makeHighlightsArray();
                }

                $dietitian->addLineBreaks();

            }



            return response()->json( ['success' => true , 'user' => $user ?? NULL , 'dietitians' => $dietitians->values()->toArray() ]); 
            // return response()->json( ['success' => true ,  'user' => $user ?? NULL , 'dietitians' => $dietitians->reverse()->values() ]); 
        }else{
            return response()->json (['success' => false ,  'error' => 'Some Problem Occoured']); 
        }

    }

    

    public function viewDietitian($id){

        $dietitian = Coach::with('user' , 'tags', 'reviews' , 'reviews.user' )->find($id);

        if($dietitian && $dietitian->role == 4){
            
            // $dietitian->portfolio = strip_tags( $dietitian->portfolio);

            $dietitian->packages = $dietitian->packages;

            foreach($dietitian->packages as $package){
                // $package->description = strip_tags($package->description);

                $package->addLineBreaks();
                $package->makeHighlightsArray();

                // if(strpos($package->highlights , 'ul')){
                //     $package->highlights = explode( '<li>' , $package->highlights);
                    
                //     foreach($package->highlights as $highlight){
                //         $array[] = strip_tags($highlight);
                // }
                
                // unset($array[0]);
                // // $package->highlights = $array;
                
                // }else{
                //     $array[] = strip_tags( $package->highlights);
                    
                // }
                // $package->highlights = $array;
            }

            $dietitian->addLineBreaks();

            return response()->json( ['success' => true ,  'dietitian' => $dietitian]); 
        }else{
            return response()->json (['success' => false ,  'error' => 'Dietitian Not Found']); 
        }

    }



      public function showPackages(){

        $packages = Package::with('coaches' , 'reviews' ,  'reviews.user' )->get();

        if($packages){

            foreach($packages as $package){
                $package->addLineBreaks();
                $package->makeHighlightsArray();

            }
  
            return response()->json( ['success' => true ,  'packages' => $packages]); 
        }else{
            return response()->json (['success' => false ,  'error' => 'Some Problem Occoured']); 
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function viewPackage($id , $user_id = 0)
    {

        $package = Package::with('coaches' , 'tags' ,  'reviews' , 'reviews.user' )->find($id);

        if($package){

            // $package->stripHTMLTags();
            $user = User::find($user_id);
            if( $user_id != 0 && $user && $user->subscribers->find($package->id)){
                $package->is_subscribed = true;
            }else{
                $package->is_subscribed = false;
            }

            if($user_id != 0){
                $package->getRequestStatus($user_id);
            }

            
            $package->addLineBreaks();
            $package->makeHighlightsArray();

            
            
            return response()->json( ['success' => true ,  'package' => $package , 'user' => $user]); 
        }else{
            return response()->json (['success' => false ,  'error' => 'Package Not Found']); 
        }
    }      
    
    public function viewPackagePost( Request $request)
    {

        $package = Package::with('coaches' , 'tags' ,  'reviews' , 'reviews.user' )->find($request->package_id);

        if($package){

            // $package->stripHTMLTags();
            $user = User::find($request->user_id);
            if( $request->user_id != 0 && $user && $user->subscribers->find($package->id)){
                $package->is_subscribed = true;
            }else{
                $package->is_subscribed = false;
            }

            if($request->user_id != 0){
                $package->getRequestStatus($request->user_id , $request->coach_id);
            }

            
            $package->addLineBreaks();
            $package->makeHighlightsArray();

            
            
            return response()->json( ['success' => true ,  'package' => $package , 'user' => $user]); 
        }else{
            return response()->json (['success' => false ,  'error' => 'Package Not Found']); 
        }
    }      
    
    public function showWorkouts()
    {   
        
        $workouts = Workout::all();

        foreach($workouts as $workout){
            // $workout->description = strip_tags( $workout->description);
            $workout->addLineBreaks();
        }


        if($workouts){
            return response()->json( ['success' => true ,  'workouts' => $workouts]); 
        }else{
            return response()->json (['success' => false ,  'error' => 'Some Problem Occoured']); 
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function viewWorkout($id)
    {
        $workout = Workout::find($id);

        if($workout){

            if($workout->media){
                $workout->media = json_decode($workout->media);
            }
    
            if($workout->videos){
                $workout->videos = json_decode($workout->videos);
            }

            $workout->addLineBreaks();

            // $workout->description = strip_tags( $workout->description);

            return response()->json( ['success' => true ,  'workout' => $workout]); 
        }else{
            return response()->json (['success' => false ,  'error' => 'Workout Not Found']); 
        }
    }



    public function showTags(){
        
        $tags = Tag::all();
        
        if($tags){
            return response()->json( ['success' => true ,  'tags' => $tags]); 
        }else{
            return response()->json (['success' => false ,  'error' => 'Tags Not Found']); 
        }

    }

    public function showTagCoach(Request $request){
        
        // $tag_ids = [3];
        $tag_ids = $request->tags;
        $all_coaches_ids = Coach_Tag::whereIn('tag_id' , $tag_ids)->get()->pluck('coach_id')->toArray();

        // dd($all_coaches_ids);
        $coaches = Coach:: with('user'  , 'tags'  , 'packages' , 'reviews' , 'reviews.user' ) -> whereIn('id' , $all_coaches_ids)->get();

        if(isset($request->type) && $request->type == 'trainer'){
            $coaches = $coaches->where('role' , 3 );
        }
        
        if(isset($request->type) && $request->type == 'dietitian'){
            $coaches = $coaches->where('role' , 4 );
        }

        if($coaches){
            return response()->json( ['success' => true ,  'coaches' => $coaches->values()]); 
        }else{
            return response()->json (['success' => false ,  'error' => 'Tags Not Found']); 
        }

    }


}
