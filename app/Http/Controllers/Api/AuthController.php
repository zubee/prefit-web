<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Jobs\SendEmail;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    public function login(Request $request){
      
        $errors = null;
        // $validator = \Validator::make($request->all(), [
        //     'email' => ['required','email', 'max:255'],
        //     'password' => ['required'],
        // ] );

        // if(!($request->has('email') && $request->has('password') ) ){
        //     return  response()->json(['success' => false ,  'error' => 'Missing required fields']); 
        // }

        if($request->role == 'coach'){
            $user = User::where('username' , $request->username)->first();
        }else{
            $user = User::where('email' , $request->email)->first();
        }
        
        if($user->status == 0){
            return response()->json( [ 'success' => false , 'message' => 'User Disabled' ]) ;
        }

        if($user && ( ($request->role == 'user' && $user->role_id == 5) || ($request->role == 'coach' && $user->role_id == 3 || $user->role_id == 4) ) ){
        // if($user->status == 0){
        //     return response()->json([ 'success' => false , 'error' => 'User is deactivated' ] );
        // }
        // if($user->confirmed == 0){
        //     return response()->json([ 'success' => false , 'error' => 'User is not confirmed' ]) ;
        // }

        if($user && Hash::check($request->password, $user->password )){
            \Auth::login($user);
            if($request->role == 'coach'  ){
                $user->coach;
            }
            
            return response()->json( [ 'success' => true , 'user' => $user ]) ;

        }else{
            return response()->json( [ 'success' => false , 'error' => 'Credentials does not match']);
        }
        }else{
            return response()->json( [ 'success' => false , 'error' => 'User roles not specified']);

        }
            
    }

    public function coachLogin(Request $request){
      
        $errors = null;
        if(!($request->has('username') && $request->has('password') ) ){
            return  response()->json(['success' => false ,  'error' => 'Missing required fields']); 
        }

        $user = User::where('username' , $request->username)->first();
        
        if($user && Hash::check($request->password, $user->password )){
            \Auth::login($user);
            $user->coach;
            return response()->json( [ 'success' => true , 'user' => $user ]) ;
        }else{
            return response()->json( [ 'success' => false , 'error' => 'Credentials does not match']);
        }
    }
 

    public function register(Request $request){


        // $validator = \Validator::make($request->all(), [
        //         'name' => ['required', 'string', 'max:255'],
        //         'username' => ['required', 'string', 'max:255', 'unique:users'],
        //         'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        //         'password' => ['required', 'string', 'min:8'],
        //     ] );

        // if ($validator->fails()) {
        // return  [ 'success' => false , 'errors' => response()->json($validator->errors(), 422)];
        // }

        $errors = null;
        
        // dd($request->all());

        if(!($request->has('name') && $request->has('email') && $request->has('username') && $request->has('password') ) ){
            return  response()->json(['success' => false ,  'error' => 'Missing required fields']); 

        }

        $email = preg_replace('/\s+/', '', $request->email);
        $username = preg_replace('/\s+/', '', $request->username);

        $email_check = User::where('email' , $email)->get()->count();

        $username_check = User::where('username' , $username)->get()->count();

        if($email_check > 0){
            return response()->json( ['success' => false ,  'error' => 'Email Already Exists']); 
        }
        
        if($username_check > 0){
            return response()->json (['success' => false ,  'error' => 'Username Already Exists']); 
        }
        // elseif($request->password != $request->conf_password){
        //     return response()->json( ['success' => false ,  'error' => 'Password does not match']); 
        // }
        
        // $token = md5(Str::random());
        $code = mt_rand(100000, 999999);

        $user = User::create([
            'name' => $request->name,
            'username' => $username,
            'email' => $email,
            'password' => \Hash::make($request->password),
            'role_id' => 5,
            'confirmed' => 0,
            // 'status' => 1,
            'first_login' => 0,
            'confirmation_code' => $code,
            'confirmation_time' => \Carbon\Carbon::now() ,

        ]);
        if($user){

            
            $user->confirmation_code = $code;
            $user->confirmation_time = null;
            $user->save();


            // $data['msg'] = 'Dear User. <br> Your verification code <h1>'.$code.'</h1>    <br> ';

            // \Mail::send('mails.mobile_verification', $data, function ($message) use ($user) {
            //     $message->to($user->email, $user->name);
            //     $message->subject('FTBLE Email Verification Code');
            // });


            

            // Verification email
            $users = User::where('id' , $user->id )->get();

            $subject = 'FTBLE Email Verification Code';
            $body = 'Dear User. <br> Your verification code is: <h1>'.$code.'</h1><br>';
    
            dispatch(new SendEmail($users , $subject , $body ));
    

            // $user->sendWeclomeEmail();

        }

        if($user){
            return response()->json(['success' => true , 'user' => $user ]);
        }else{
            return response()->json(['success' => false ,  'message' => 'Some problem occured']);
        }

    }


    public function sendPasswordLink(Request $request){

        $user = User::where('email' , $request->email)->first();

        if($user){
        $code = mt_rand(100000, 999999);
        
        $user->confirmation_code = $code;
        $user->confirmation_time = null;
        $user->save();

        // $data['msg'] = 'Dear User. <br> Your verification code <h1>'.$code.'</h1>    <br> ';
        
        // \Mail::send('mails.mobile_verification', $data, function ($message) use ($user) {
        //     $message->to($user->email, $user->name);
        //     $message->subject('FTBLE Email Verification Code');
        // });
        $users = User::where('id' , $user->id )->get();

        $subject = 'FTBLE Email Verification Code';
        $body = 'Dear User. <br> Your verification code <h1>'.$code.'</h1><br> ';

        dispatch(new SendEmail($users , $subject , $body ));

        // \Mail::send('mails.mobile_verification', $data, function ($message) use ($user) {
        //     $message->from('dawami@dawami.com', 'Dawami');
        //     $message->sender('dawami@dawami.com', 'Dawami');
        //     $message->to($user->email, $user->name);
        //     $message->subject('Dawami Email Verification Code');
        // });

        return response()->json(['success' => true , 'user'=> $user,  'message' => 'Email sent']);

        }else{
            return response()->json(['success' => false ,  'message' => 'No user found for this email']);

        }
    }
    public function resetPassword(Request $request){

        $user = User::where('email' , $request->email )->first();
        
        if(empty($request->password)){
            return response()->json(['success' => false ,  'message' => 'Password not found']);

        }

        if($user){
            $user->password = \Hash::make($request->password);
            $user->save();

            return response()->json(['success' => true , 'user'=> $user,  'message' => 'Password updated successfully' , 'user' => $user ]);

        }else{
            return response()->json(['success' => false ,  'message' => 'No user found for this email']);

        }
    }

    public function checkUsernameAndEmail(Request $request){

        $email = User::where('email' , $request->email)->get();

        $username = User::where('username' , $request->username)->get();

        if($username->count() > 0){
            return response()->json(['success' => false ,  'message' => 'Username Taken']);
        }

        if($email->count() > 0){
            return response()->json(['success' => false ,  'message' => 'Email already Exists']);
        }

        if($username->count() == 0 && $email->count() == 0 ){
            return response()->json(['success' => true , 'message' => 'Email And Username availabled' ]);
        }


    }
    




    public function verifyUserToken(Request $request){
    
        $user = User::where('email' , $request->email)->first();
        
        if($user){
            if($user->confirmation_code == $request->token){
                $user->confirmation_time = \Carbon\Carbon::now();
                $user->confirmed = 1;
                $user->save();

                $user->sendWeclomeEmail();





                return response()->json(['success' => true , 'user'=> $user,  'message' => 'User verification successful']);
            
            }else{
                return response()->json(['success' => false ,  'message' => 'User token not matched']);
    
            }
        }else{
            return response()->json(['success' => false ,  'message' => 'No user found for this email']);

        }
    }

    public function handleGoogleLogin(Request $request){
        
        // $users = User::where('username' , trim($user->name) )->get();
        $username =  $request->name .  \Carbon\Carbon::now()->timestamp  ;

        $exists = User::where('email' , $request->email )->first();

        $user = User::firstOrCreate([
            'email' => $request->email,
        ] ,[
            'email' => $request->email,
            'role_id' => 5,
            'name' => $request->name,
            'username' => $username,
            'source' => 'google',
            'status' => 1,
            'password' => \Hash::make(Str::random(24)),
            // 'confirmed' => 1,
        ]);
        
        \Auth::login($user);

        if($user && !$exists){
            $user->sendWeclomeEmail();
        }
        
        if($user){
            return response()->json( [ 'success' => true , 'user' => $user , 'message' => "Login successful" ]) ;
        }else{
            return response()->json( [ 'success' => false , 'message' => "some problem occoured" ]) ;

        }

    }

    public function handleFacebookLogin(Request $request){
        
        // $users = User::where('username' , trim($user->name) )->get();
        $username =  $request->name .  \Carbon\Carbon::now()->timestamp  ;

        $exists = User::where('email' , $request->email )->first();

        $current_username = User::where('username' , $username)->get();
        if($current_username->count() > 0){
    
            $username =  $request->name .  \Carbon\Carbon::now()->timestamp . Str::random(4)  ;
        }

        $user = User::firstOrCreate([
            'email' => $request->email,
        ] ,[
            'email' => $request->email,
            'role_id' => 5,
            'name' => $request->name,
            'username' => $username,
            'source' => 'facebook',
            'status' => 1,
            'password' => \Hash::make(Str::random(24)),
            // 'confirmed' => 1,
        ]);

        \Auth::login($user);
        
        if($user && !$exists){
            $user->sendWeclomeEmail();
        }
        
        if($user){
            return response()->json( [ 'success' => true , 'user' => $user , 'message' => "Login successful" ]) ;
        }else{
            return response()->json( [ 'success' => false , 'message' => "some problem occoured" ]) ;

        }
    }


    public function checkUsername($name){
        $username = User::where('username' , $name)->get();

        if($username->count() > 0){
            return response()->json( [ 'success' => false , 'message' => "Username Already Taken" ]) ;
            
        }else{
            return response()->json( [ 'success' => true , 'message' => "Username is available" ]) ;

        }
    }   
    
    public function checkEmail($email){
        $username = User::where('email' , $email)->get();

        if($username->count() > 0){
            return response()->json( [ 'success' => false , 'message' => "email Already Taken" ]) ;
            
        }else{
            return response()->json( [ 'success' => true , 'message' => "email is available" ]) ;

        }
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sendEmail(){

        $code = mt_rand(100000, 999999);
        $data['msg'] = 'Dear User. <br> Your verification code <h1>'.$code.'</h1>    <br> ';

        \Mail::send('mails.mobile_verification', $data, function ($message) {
            // $message->from('dawami@dawami.com', 'Dawami');
            // $message->sender('dawami@dawami.com', 'Dawami');
            $message->to('saadanjum047@gmail.com', 'Saad');
            $message->subject('FTBLE Email Verification Code');
        });

        echo 'Done!';
    }

    
}
