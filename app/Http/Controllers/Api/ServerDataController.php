<?php

namespace App\Http\Controllers\Api;

use App\Workout;
use App\Supplement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServerDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }




    public function getAllDataForPlan()
    {
        // $exercises = Ex 

        $workouts = Workout::all();
        foreach($workouts as $k => $workout){
            $workouts[$k]->media = json_decode($workouts[$k]->media);
        }

        $supplements = Supplement::all();
        return response()->json(['success' => true , 'workouts' => $workouts , 'supplements' => $supplements ]);
    }


}
