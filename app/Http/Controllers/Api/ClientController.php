<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Coach;
use App\Review;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getSubscriptions($user_id){
    $user = User::find($user_id);
    if($user){

        $packages = $user->subscribed_packages;
        $coaches = $user->coaches->unique();
        $trainer_packages = collect();
        $dietittian_packages = collect();
        
        $all_packages_ids = $user->subscribed_packages->pluck('id')->toArray();
        $all_packages_ids = array_unique($all_packages_ids);

        $trainers = $coaches->where('role' , 3);
        foreach($trainers as $trainer ){

            $trainer_packages = $trainer->packages->whereIn('id' ,$all_packages_ids );
            // $trainer->addLineBreaks();
            foreach($trainer_packages as $package){
                
                $package->reviews = $package->reviews->where('coach_id' , $trainer->id );

                $package->addLineBreaks();
                $package->makeHighlightsArray();

                $plan = $package->plans->where('client_id' , $user_id )->first();
                $package->plan = (isset($plan) && $plan->start_status == 1) ? $plan : null;
                unset($package->plans );
                if($package->plan){

                    $package->plan->end_date = Carbon::parse($plan->start_date)->addDays($package->duration)->toDateString() ;

                }
    
            }
        }
        
        $dietittians = $coaches->where('role' , 4) ;
        foreach($dietittians as $dietittian ){
            $dietittian_packages = $dietittian->packages->whereIn('id' ,$all_packages_ids );
            // $dietittian->addLineBreaks();
            $dietittian->reviews;

            foreach($dietittian_packages as $package){
                $package->reviews = $package->reviews->where('coach_id' , $dietittian->id );

                $package->addLineBreaks();
                $package->makeHighlightsArray();
                
                $plan = $package->plans->where('client_id' , $user_id )->first();
                $package->plan = (isset($plan) && $plan->start_status == 1) ? $plan : null;
                unset($package->plans );
                if($package->plan){
                    $package->plan->end_date = Carbon::parse($plan->start_date)->addDays($package->duration)->toDateString() ;

                }
            }

        }

        return response()->json( ['success' => true ,  'dietittians' => $dietittian_packages->values() , 
        'trainers' => $trainer_packages->values() ]);
    }else{
        return response()->json (['success' => false ,  'error' => 'User Not Found']); 
    }    
    }
}
