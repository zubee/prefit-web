<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Coach;
use App\Package;
use Carbon\Carbon;
use App\Client_Plan;
use App\Client_Pivot;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class CoachController extends Controller
{



    public function getCoachData($id){
        
        $coach = Coach::with( 'user' , 'reviews' , 'reviews.user')->find($id);
        $coach->review_count = $coach->reviews->count();

        return response()->json(['success' => true , 'coach' => $coach]);

    }
    
    public function getCoachDetails($id){
        
        $coach = Coach::with( 'user' , 'subscribers')->find($id);
        
        $all_client_ids = $coach->subscribers->pluck('id')->toArray();
        
        $coach_plans = Client_Plan::where('coach_id' , $coach->id)->get();
        // dd($coach_plans->where('plan_progress'  ,'100' ));

        // $completed_plans = $coach->subscribers->where('plans.plan_progress' ,'==', '100' );
        // $in_progress = $coach->subscribers->where('plans.plan_progress' ,  '<' ,'100' );
        
        $completed_plans = count(array_unique($coach_plans->where('start_status' , 1)->where('plan_progress' , '100' )->pluck('client_id')->toArray()));
        
        $in_progress = count(array_unique($coach_plans->where('start_status' , 1)->where('plan_progress' ,'<', '100' )->pluck('client_id')->toArray()));
        
        $new_members = count(array_unique($coach_plans->where('start_status' , 0)->pluck('client_id')->toArray()));
        
        // $completed_plans = count(array_unique($coach_plans->where('plan_progress' , '100' )->pluck('user_id')->toArray()));
        
        // $in_progress = count(array_unique($coach_plans->where('plan_progress' ,'<', '100' )->pluck('user_id')->toArray()));
        
        // $new_members = $coach->subscribers->where('start_status' , 0 )->count();
        // dd($completed_plans->pluck('plans')->pluck('plan_progress') , $in_progress->pluck('plans')->pluck('plan_progress'));
        
        // $completed_plans = User::with('plans')->whereIn('id' , $all_client_ids)->whereHas('plans' , function($plans){ 
        //     // dd($plans);
        //     $plans->where('plans.plan_progress' , '100' );          
        // })->get();


        // $in_progress = User::with('plans')->whereIn('id' , $all_client_ids)->whereHas('plans' , function($plans){ 
        //     $plans->where('plan_progress' ,  '<' ,'100' );
        // })->get();
        
        // dd($completed_plans, $in_progress );


        // $new_members = User::whereIn('id' , $all_client_ids)->whereHas('plans' , function($plan) use ($coach) {         
        //     $plan->where('coach_id' , $coach->id);
        //     $plan->where('start_status' , 0);
        // })->get()->count();

        // dd($new_members);
        // $new_members = Client_Pivot::where('coach_id' , $id)->whereDate('created_at', '>', Carbon::now()->subDays(30))->count();
        // $new_members = 10;
        

        
        $all_payments = array_sum($coach->plans->pluck('price')->toArray());
        $recieved_payments = array_sum($coach->payments->where('status' , 1 ) ->pluck('amount')->toArray());
        $pending_payments = $all_payments - $recieved_payments;
        // dd($coach);
        return response()->json([
            'success' => true , 
            'coach' => $coach,
            'completed_plans' => $completed_plans,
            'in_progress' => $in_progress,
            'new_members' => $new_members,
            'all_payments' => $all_payments,
            'recieved_payments' => $recieved_payments,
            'pending_payments' => $pending_payments,
            ]);

    }


    public function getCoachSubscribers($id){
        
        $coach = Coach::with( 'user' , 'clients', 'clients.profile' )->find($id);
      
        $plans = Client_Plan::with('client' , 'client.profile')->where('coach_id' , $coach->id)->get();
        
        foreach($plans as $plan){
            if($plan->start_status == 0){
                $plan->client_type = 'New Member';
            }elseif($plan->start_status == 1 && $plan->plan_progress < '100'){
                $plan->client_type = 'In Progress';
            }elseif($plan->start_status == 1 && $plan->plan_progress == '100'){
                $plan->client_type = 'Completed';
            }

            if($plan->payment){
                $plan->payment_status = true;

            }elseif( $plan && $plan->package && $plan->package->paid == 0 ){
                
                $plan->payment_status = "FREE";
            }else{
                
                $plan->payment_status = false;
            }
            if($plan && $plan->client && $plan->client->profile){
                $plan->client->profile->normalize();
            }
        }

        $requesting_clients = $coach->new_requests->where('pivot.status' , 'pending')->values();

        foreach($requesting_clients as $client){
            if(Package::find($client->pivot->package_id)){
                $client->pivot->package = Package::find($client->pivot->package_id);
            }else{
                $client->pivot->package = null;
            }
        }

        // foreach($coach->subscribers as $subscriber){
        //     $subscriber->all_plans = $subscriber->plans->where('coach_id' , $id);
        //     // dump($subscriber->plans);
        //     // $subscriber->plans = $subscriber->plans->where('coach_id' , $id);
        //     foreach($subscriber->all_plans as $plan){
        //         $plan->end_date = Carbon::parse($plan->start_date)->addDays($plan->duration)->toDateString();
        //         // $plan->plan_progress = $plan->plan_progress;
        //         // dump($plan->plan_progress);
        //     }
        //     if(isset($subscriber->plans)){
        //         unset($subscriber->plans);
        //     }
        //     if(isset($subscriber->pivot)){
        //         unset($subscriber->pivot);
        //     }
        // }

        return response()->json(['success' => true , 'coach' => $coach , 'plans' => $plans , 'requesting_clients' => $requesting_clients ]);

    }
    
    

    public function getCoachClients($id){
        
        $coach = Coach::with( 'user' , 'clients', 'clients.profile' )->find($id);
      
        $plans = Client_Plan::with('client' , 'client.profile')->where('coach_id' , $coach->id)->get();
        
        foreach($plans as $plan){
            if($plan->start_status == 0){
                $plan->client_type = 'New Member';
            }elseif($plan->start_status == 1 && $plan->plan_progress < '100'){
                $plan->client_type = 'In Progress';
            }elseif($plan->start_status == 1 && $plan->plan_progress == '100'){
                $plan->client_type = 'Completed';
            }

            if($plan->payment){
                $plan->payment_status = true;

            }elseif( $plan && $plan->package && $plan->package->paid == 0 ){
                
                $plan->payment_status = "FREE";
            }else{
                
                $plan->payment_status = false;
            }
            if($plan && $plan->client && $plan->client->profile){
                $plan->client->profile->normalize();
            }
        }

        return response()->json(['success' => true , 'coach' => $coach , 'plans' => $plans  ]);

    }

    public function getCoachNewRequests($id){

        $coach = Coach::with( 'user' , 'clients', 'clients.profile' )->find($id);
      
        $requesting_clients = $coach->new_requests->where('pivot.status' , 'pending')->values();

        foreach($requesting_clients as $client){
            if(Package::find($client->pivot->package_id)){
                $client->pivot->package = Package::find($client->pivot->package_id);
            }else{
                $client->pivot->package = null;
            }
        }

        return response()->json(['success' => true , 'coach' => $coach , 'requesting_clients' => $requesting_clients ]);

    }
    
    

    public function updateProfile( $coach_id , Request $request ){
        
        $coach = Coach::find($coach_id);

        // if($request->image_file){


        //     if(file_exists($path)){
        //             File::delete($path);
        //         }
                
                
        //         $ext = $request->extension;
        //         $mime = $request->mime;
        //         $path = $coach->user->id .'.'. $ext;
                
        //         $file_data = $request->image_file;
                
        //         if ($file_data != "") { 
        //             $file = base64_decode($request->image_file);
        //         $success = file_put_contents(public_path().'/messages/files/'.$path, $file);
        //         if($success){
        //             $coach->user->save();

        //             return response()->json(['success' => true , 'message' => 'Profile Image Updated Successfully' , 'coach' => $coach]);
        //         }
        //     }
        // }

    if($request->has('image')){
        $path = public_path().'/images/profile_images/'.$coach->user->photo;

        if(file_exists($path)){
                File::delete($path);
            }
            $ext = $request->image->getClientOriginalExtension();
    
            $request->image->move(public_path().'/images/profile_images/' , $coach->user->id .'.'. $ext);
            $coach->user->photo = $coach->user->id .'.'. $ext;
            
            $coach->user->save();
            
            return response()->json(['success' => true , 'message' => 'Profile Image Updated Successfully' , 'coach' => $coach]);

        }

        return response()->json(['success' => false , 'message' => 'Profile Image Upload Failed']);
    
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
