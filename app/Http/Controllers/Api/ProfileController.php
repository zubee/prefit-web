<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Profile;
use App\Perfit_Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( $user_id , Request $request)
    {
        // dd($request->all());

        $profile = Profile::updateOrCreate(
            ['user_id' => $user_id],
            [
            'user_id' => $user_id,
            'gender' => $request->gender,
            'weight' => $request->weight,
            'height' => $request->height,
            'date_of_birth' => $request->date_of_birth,

            'goals' => $request->goals,
            'workout' => $request->workout,
            // 'goals' => isset($request->goals) ? json_encode($request->goals) : null,
            // 'workout' => isset($request->workout) ? json_encode($request->workout) : null,
            'exercise_level' => $request->exercise_level ,
        ]);

        if($profile){
            return response()->json(['success' => true , 'profile' => $profile ]);
        }else{
            return response()->json(['success' => false ,  'message' => 'Some problem occured']);
        }


    }


    public function getProfile($id){
        $profile = Profile::with('user')->where( 'user_id' , $id)->first();

        if($profile){

            $profile->normalize();
            // $profile->goals = json_decode($profile->goals);
            // $profile->workout = json_decode($profile->workout);

            return response()->json(['success' => true , 'profile' => $profile ]);
        }else{
            return response()->json(['success' => false ,  'message' => 'Profile Not Found']);
        }
    }
    
    public function updateClientImage($id , Request $request){


        // dd($request->all());
        $user = User::find($id);

        if($user){

            if($request->has('image')){
                $path = public_path().'/images/profile_images/'.$user->photo;
    
            if(file_exists($path)){
                File::delete($path);
                }
                $ext = $request->image->getClientOriginalExtension();
                
                $file = Perfit_Image::imageResize($request->image);
                $file->save(public_path().'/images/profile_images/' . $user->id .'.'. $ext , 90);
                // $request->image->move(public_path().'/images/profile_images/' , $user->id .'.'. $ext);

                $user->photo = $user->id .'.'. $ext;
                
                $user->save();
                
                return response()->json(['success' => true , 'message' => 'Profile Image Updated Successfully' , 'user' => $user]);
                
            }
        }else{
            return response()->json(['success' => false , 'message' => 'User Not Found']);
        }
    
            return response()->json(['success' => false , 'message' => 'Profile Image Upload Failed']);
    
    }

    public function updateClientImage64($id , Request $request){


        // dd($request->all());
        $user = User::find($id);

        if($user){

            if($request->has('image')){
                $path = public_path().'/images/profile_images/'.$user->photo;
    
            if(file_exists($path)){
                File::delete($path);
                }
                $ext = $request->extension;
                
                //$request->image->move(public_path().'/images/profile_images/' , $user->id .'.'. $ext);
                $file = base64_decode($request->image);
                $success = file_put_contents(public_path().'/images/profile_images/'.$user->id .'.'. $ext, $file);
                $user->photo = $user->id .'.'. $ext;
                
                $user->save();
                
                return response()->json(['success' => true , 'message' => 'Profile Image Updated Successfully' , 'user' => $user]);
                
            }
        }else{
            return response()->json(['success' => false , 'message' => 'User Not Found']);
        }
    
            return response()->json(['success' => false , 'message' => 'Profile Image Upload Failed']);
    
    }
    
    public function getClientProfile($id){

        $profile = Profile::with('user')->where( 'user_id' , $id)->first();

        if($profile){
            $profile->normalize();

            // $profile->goals = json_decode($profile->goals);
            // $profile->workout = json_decode($profile->workout);

            return response()->json(['success' => true , 'profile' => $profile ]);
        }else{
            return response()->json(['success' => false ,  'message' => 'Profile Not Found']);
        }
    }
    
    
    public function storeProfileDetails( $user_id , Request $request)
    {
        // dd($request->all());

        $profile = Profile::updateOrCreate(
            ['user_id' => $user_id],
            [
            'user_id' => $user_id,
            'gender' => $request->gender,
            // 'weight' => $request->weight,
            'starting_weight' => $request->starting_weight,
            'target_weight' => $request->target_weight,
            'height' => $request->height,
            'date_of_birth' => $request->date_of_birth,
            // 'goals' => isset($request->goals) ? json_encode($request->goals) : null,
            // 'workout' => isset($request->workout) ? json_encode($request->workout) : null,
            // 'exercise_level' => $request->exercise_level ,
        ]);

        if($profile){
            return response()->json(['success' => true , 'profile' => $profile ]);
        }else{
            return response()->json(['success' => false ,  'message' => 'Some problem occured']);
        }
    }


    public function storeProfileGoals( $user_id , Request $request)
    {
        // dd($request->all());

        // $goals = !is_array($request->goals) ? json_decode($request->goals) : $request->goals;
        // $workout = !is_array($request->workout) ? json_decode($request->workout) : $request->workout;

        // $goals = $request->goals;
        $goals = explode( ',' , $request->goals);
        
        $workout = explode( ',' , $request->workout);

        // dd($goals);
        // dd($goals);
        $profile = Profile::updateOrCreate(
            ['user_id' => $user_id],
            [
            // 'user_id' => $user_id,
            // 'gender' => $request->gender,
            // 'weight' => $request->weight,
            // 'height' => $request->height,
            // 'date_of_birth' => $request->date_of_birth,
            // 'goals' => isset($request->goals) ? json_encode($request->goals) : null,
            // 'workout' => isset($request->workout) ? json_encode($request->workout) : null,
            'goals' => $goals,
            'workout' => $workout,
            'exercise_level' => $request->exercise_level ,
        ]);

        if($profile){
            
            // $profile->normalize();

            return response()->json(['success' => true , 'profile' => $profile ]);
        }else{
            return response()->json(['success' => false ,  'message' => 'Some problem occured']);
        }
    }
    
    public function storeProfileMeasurements( $user_id , Request $request)
    {
        // dd($request->all());

        $measurements = array(
    
            'head' => $request->head,

            'shoulder' => $request->shoulder,
            'chest' => $request->chest,
            'left_bicep' => $request->left_bicep,
            'left_forearm' => $request->left_forearm,
            'waist' => $request->waist,
            'left_thigh' => $request->left_thigh,
            'left_calf' => $request->left_calf,
            
            'hips' => $request->hips,
            
            'right_bicep' => $request->right_bicep,
            'right_forearm' => $request->right_forearm,
            'right_thigh' => $request->right_thigh,
            'right_calf' => $request->right_calf,
        );

        $profile = Profile::updateOrCreate(
            ['user_id' => $user_id],
            [
            'measurements' => isset($measurements) ? json_encode($measurements) : null ,
            'current_weight' => $request->current_weight,
            ]);

        if($profile){
            
            $profile->normalize();

            return response()->json(['success' => true , 'profile' => $profile ]);
        }else{
            return response()->json(['success' => false ,  'message' => 'Some problem occured']);
        }
    }
    
    public function storeProfileWeight( $user_id , Request $request)
    {
        // dd($request->all());
        $profile = Profile::updateOrCreate(
            ['user_id' => $user_id],
            [
            'current_weight' => $request->current_weight,
            'starting_weight' => $request->starting_weight,
            'target_weight' => $request->target_weight,
            ]);

        if($profile){
            
            $profile->normalize();

            return response()->json(['success' => true , 'profile' => $profile ]);
        }else{
            return response()->json(['success' => false ,  'message' => 'Some problem occured']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
