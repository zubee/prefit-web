<?php

namespace App\Http\Controllers\Api;

use App\Client_Plan;
use App\Client_Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientPaymentController extends Controller
{
   
    public function makePayment(Request $request , $client_id , $coach_id , $package_id ){
        

        $payment = Client_Payment::where('package_id' , $package_id )->
        where('coach_id' , $coach_id )->
        where('client_id' , $client_id )->
        first();



        $plan = Client_Plan::where('package_id' , $package_id )->
        where('coach_id' , $coach_id )->
        where('client_id' , $client_id )->
        first();


        // dd($request->all() , $payment , $plan);

        if($payment){
            return response()->json([ 'success' => false , 'message' => 'Payment is already done' ]);
        }

        
        if(!$payment){
            // dd('new payment');
            $payment = Client_Payment::create([
                'plan_id' => $plan->id ?? null,
                'coach_id' => $coach_id,
                'client_id' => $client_id,
                'package_id' => $package_id,
                'amount' => $request->amount,
                'transaction_id' => $request->transaction_id,
                'body' => $request->body,
            ]);

            if($payment){
                return response()->json([ 'success' => true , 'message' => 'Payment completed successfully' , 'payment' => $payment]);
            }
        }

        return redirect()->back()->with('error' , 'Payment failed' );
    }   
    
    public function makePaymentPost($plan_id , Request $request){
        
        $plan = Client_Plan::find($plan_id);

        if($plan && !$plan->payment ){
            $payment = Client_Payment::create([
                'plan_id' => $plan_id,
                'coach_id' => $plan->coach_id,
                'client_id' => $plan->client_id,
                'amount' => $request->amount,
                // 'amount' => $plan->package->price,
                'transaction_id' => $request->transaction_id,
                // other useful feilds to come
            ]);

            if($payment){
                return response()->json([ 'success' => true , 'message' => 'Payment Made successfully' ,  'payment' => $payment]);
            }
        }else{
            return response()->json([ 'success' => false , 'message' => 'Payment Already done' ]);
        }


        return response()->json([ 'success' => false , 'message' => 'Payment Failed' ]);
    }   
    
    public function removePayment($payment_id){
      
        
        $payment = Client_Payment::find($payment_id);
        
        if($payment){
            $payment->delete();
            return response()->json([ 'success' => true , 'message' => 'Payment removed' ]);
        }else{
            return response()->json([ 'success' => false , 'message' => 'Payment Not Found' ]);
        }
    }
}
