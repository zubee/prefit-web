<?php

namespace App\Http\Controllers\Api;

use App\User;
use stdClass;
use App\Coach;
use App\Client_Answer;
use App\Jobs\SendEmail;
use App\Client_Question;
use App\Traits\FirebaseFCM;
use Illuminate\Http\Request;
use App\Client_Questionnaire;
use App\Client_Question_Choice;
use App\Http\Controllers\Controller;

class ApiClientQuestionnaireController extends Controller
{

    use FirebaseFCM;

    public function storeAnswer(Request $request){

        
        // dump($request->all());
        

        // dd($request->{'question-'.$question} , $question);
        $questionnaire = Client_Questionnaire::find($request->questionnaire_id);

        foreach($questionnaire->questions as $question){

            if(isset($request->answers['answer_'.$question->id])){
                $answer = Client_Answer::updateOrCreate(
                    ['question_id' => $question->id , 'client_id' => $request->client_id ],
                    [
                        'question_id' => $question->id,
                        'client_id' => $request->client_id,
                        'answer' =>  is_array($request->answers['answer_'.$question->id]) ? json_encode($request->answers['answer_'.$question->id]) : $request->answers['answer_'.$question->id],
                        ]
                    );
                }
            }


                 
        $coach = $questionnaire->coach;
        $client = User::findOrFail($request->client_id);



        if($coach->fcm_token){

            $notification = new stdClass();
            
            $notification->text = $client->name." answered a Questionnaire";
            $notification->description = $client->name." answered the Questionnaire: ". $questionnaire->name ;
            $notification->notif_type = "notif-subscribe";

            $this->PublishBroadcast($coach->user , $client , $notification , $client->name , $notification->text , "");
        }


        $users = User::where('id' , $request->client_id)->get();
        $subject = $client->name.' answered a Questionnaire';
        $body = 'Hi '.$coach->user->name.' 
        <br> Your client ' . $client->name . ' has anwered to your Questionnaire: ' . $questionnaire->name . ' 
        <br>
        Please click <a href="'. route('coach.client.questionnaires.show' , [$request->client_id , $questionnaire->id ]) .'">here</a> to view
        <br>
        Stay Fit,
        <br>';
        
        dispatch(new SendEmail($users , $subject , $body ));                


        
        return response()->json(['success' => true, 'message' => 'Answer is stored successfully']);
    }

    public function getQuestionnaire( $user_id , $questionnaire_id){

        $questionnaire = Client_Questionnaire::with('questions' , 'questions.choices' )->find($questionnaire_id);

        $questionnaire->answersOfClient($user_id);
        
        return response()->json(['success' => true , 'questionnaire' => $questionnaire ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id = 0)
    {
        // dd($request->all());

        if(isset($request->questionnaire['id'] ) ){
            $questionnaire = Client_Questionnaire::find($request->questionnaire['id'] );
            $questionnaire->name = $request->questionnaire['name'];
            $questionnaire->save();            
        }else{
            $questionnaire = Client_Questionnaire::create([
                'name' => $request->questionnaire['name'],
                'coach_id' => $request->coach_id
            ]);
        }

        // $questionnaire->users()->sync($request->assigned_users);

        $all_questions = $questionnaire->questions->pluck('id')->toArray();
        $deleted_questions= [];
        // dd($all_questions);
        
        if(count($request->questions) > 0){
        foreach($request->questions as $question){
            
            if(isset($question['id']) && in_array($question['id'] , $all_questions)){

                    unset($all_questions[array_search($question['id'],$all_questions)]);
            
            }
            // dd($deleted_questions , 'deleted');
            // dd($all_questions);

            // dd($question);
            if($question['id']){
                $q = Client_Question::find($question['id']);
                $q->question = $question['question'];
                $q->type = $question['type'];
                $q->save();
            }else{
                $q = Client_Question::create([
                    'question' => $question['question'],
                    'type' => $question['type'],
                    'questionnaire_id' => $questionnaire->id 
                ]);
            }
            $q->choices()->delete();
            foreach($question['choices'] as $choice){
                Client_Question_Choice::create([
                    'question_id' => $q->id,
                    'name' => $choice['name'],
                ]);

            }

        }
        if($all_questions){
            foreach($all_questions as $q){
                Client_Question::find($q)->delete();
            }
        }
        }else{
            Client_Questionnaire::find($questionnaire->id )->questions()->delete();
        }


        return response()->json(['questionnaire' => $questionnaire ]);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
