<?php

namespace App\Http\Controllers\API;

use App\Question;
use App\Questionnaire;
use App\Question_Choices;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiQuestionnaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

     
    public function update(Request $request, $id = 0)
    {
        // dd($request->all());

        if(isset($request->questionnaire['id'] ) ){
            $questionnaire = Questionnaire::find($request->questionnaire['id'] );
            $questionnaire->name = $request->questionnaire['name'];
            $questionnaire->save();            
        }else{
            $questionnaire = Questionnaire::create([
                'name' => $request->questionnaire['name'],
            ]);
        }

        $questionnaire->users()->sync($request->assigned_users);

        $all_questions = $questionnaire->questions->pluck('id')->toArray();
        $deleted_questions= [];
        // dd($all_questions);
        
        if(count($request->questions) > 0){
        foreach($request->questions as $question){
            
            if(isset($question['id']) && in_array($question['id'] , $all_questions)){

                    unset($all_questions[array_search($question['id'],$all_questions)]);
            
            }
            // dd($deleted_questions , 'deleted');
            // dd($all_questions);

            // dd($question);
            if($question['id']){
                $q = Question::find($question['id']);
                $q->question = $question['question'];
                $q->type = $question['type'];
                $q->save();
            }else{
                $q = Question::create([
                    'question' => $question['question'],
                    'type' => $question['type'],
                    'questionnaires_id' => $questionnaire->id 
                ]);
            }
            $q->choices()->delete();
            foreach($question['choices'] as $choice){
                Question_Choices::create([
                    'question_id' => $q->id,
                    'name' => $choice['name'],
                ]);

            }

        }
        if($all_questions){
            foreach($all_questions as $q){
                Question::find($q)->delete();
            }
        }
        }else{
            Questionnaire::find($questionnaire->id )->questions()->delete();
        }


        return response()->json(['questionnaire' => $questionnaire ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
