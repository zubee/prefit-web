<?php

namespace App\Http\Controllers\Api;

use App\Coach;
use App\Review;
use App\Client_Plan;
use App\Notification;
use App\Jobs\SendEmail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\User;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $check = Review::where('plan_id' , $request->plan_id)->first();
        // dd($request->all());

        $plan = Client_Plan::where('coach_id' , $request->coach_id ) -> where('client_id' , $request->user_id) -> where('package_id' , $request->package_id)  -> first();

        $coach = Coach::find($request->coach_id);
        $client = User::find($request->user_id);


        $is_subscribed = $coach->subscribers->find($client);

        // dd($check , $coach , $client);
        if($is_subscribed){

            if(!$check && $coach && $client ){
                
                $review = Review::create([
                    'coach_id' => $request->coach_id,
                    'user_id' => $request->user_id,
                    'plan_id' => isset($plan) ? $plan->id : NULL,
                    'package_id' => $request->package_id,
                    'rating' => $request->rating,
                    'description' => $request->description,
            ]);
        
            
            if($review){
                $notification = Notification::create([
                    'user_id' => $coach->user_id,
                    'title' => 'Your client '.$client->name.' left a review ',
                    'link' => '/reviews',
            ]);
        }


        
        $coaches = User::where( 'id' , $coach->user->id)->get();
        // $coaches = Coach::where('id' , $request->coach_id)->get();

        $subject = 'Your client left a review';
        $body = 'Hi “'.$coach->user->name.'”,
        <br>
        There is a new review for you by “'.$client->name.'”
        <br>
        '. $request->description .'
        <br>';
        
        dispatch(new SendEmail($coaches , $subject , $body ));
        
        
        $admins = User::whereIn( 'role_id' , [1,2])->get();
        
        $subject = 'Client reviewed a '.$coach->user->role->name;
        $body = 'Hi Admin,,
        <br>
        There is a new review by “'.$client->name.'” for “'.$coach->user->name.'”
        <br>' ;
        
        dispatch(new SendEmail($admins , $subject , $body ));
        
        
        
        if($review){
            return response()->json(['success' => true ,  'review' => $review ] );
        }
        else{
            return response()->json(['success' => false ,  'message' => "Some Problem Occurred" ] );
        }
    }
    else{
        return response()->json(['success' => false ,  'message' => "You already reviewed the plan" , 'review' => $check ] );
    }
    }else{
        return response()->json(['success' => false ,  'message' => "You are not subsribed to the Coach"  ] );
    }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
