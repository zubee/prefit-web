<?php

namespace App\Http\Controllers\Api;

use App\User;
use stdClass;
use App\Coach;
use Exception;
use App\Package;

use App\Plan_Day;

use Carbon\Carbon;

use Stripe\Charge;
use Stripe\Stripe;

use App\Client_Plan;
use Omnipay\Omnipay;
// use PayPal\Api\Item;
use App\Error_Logger;
// use PayPal\Api\Payer;
// use PayPal\Api\Amount;
use App\Client_Payment;
// use PayPal\Api\Details;
// use PayPal\Api\Payment;
// use PayPal\Api\ItemList;
use App\Traits\FirebaseFCM;
use Illuminate\Support\Str;

// use PayPal\Api\Transaction;
use PayPal\Rest\ApiContext;
use PayPalHttp\Environment;
use Illuminate\Http\Request;
// use PayPal\Api\RedirectUrls;
use Intervention\Image\Image;
// use Omnipay\Common\CreditCard;

use PHPUnit\TextUI\ResultPrinter;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use PayPal\Auth\OAuthTokenCredential;
use Illuminate\Support\Facades\Validator;

use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;


use PayPal\Api\Address;
use PayPal\Api\Amount;
use PayPal\Api\CreditCard;
use PayPal\Api\Payer;
use PayPal\Api\FundingInstrument;
use PayPal\Api\Transaction;


class ApiPlanController extends Controller
{
    use FirebaseFCM;    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // public function __construct()
    // {
    // /** PayPal api context **/
    //         $paypal_conf = \Config::get('paypal');
    //         $this->_api_context = new ApiContext(new OAuthTokenCredential(
    //             $paypal_conf['client_id'],
    //             $paypal_conf['secret'])
    //         );
    //         $this->_api_context->setConfig($paypal_conf['settings']);
    // }


    public $gateway;
 
    public function __construct()
    {
        $this->gateway = Omnipay::create('PayPal_Rest');
        $this->gateway->setClientId(env('PERFIT_PAYPAL_CLIENT_ID'));
        $this->gateway->setSecret(env('PERFIT_PAYPAL_CLIENT_SECRET'));
        $this->gateway->setTestMode(true); //set it to 'false' when go live

        // $this->gateway = Omnipay::create('PayPal_Rest');
        // $this->gateway->setClientId('AVxP-jU0lumxBdqgWJGwc-GydC3vPpRx2X3SrYY_j_b7gsSRy0DCflw6a7w7YA-7YTgxJMpJqAx68H5R');
        // $this->gateway->setSecret('EP0YEH1RwnV1qIw8Vgn21K9vaSEIk7gh1Ybp54kayQwUygeJQoA4-SqWCGIIm3f4B9mrNHVr0ZH1Q28v');
        // $this->gateway->setTestMode(true); //set it to 'false' when go live

    }



    public function paypalWeb(){
        try {
            
            $response = $this->gateway->purchase(array(
                'amount' =>  '100',
                'currency' => 'USD' ,
                'returnUrl' => url('paypal_return'),
                'cancelUrl' => url('paypal_return'),
            ))->send();
      
            if ($response->isRedirect()) {
                $response->redirect(); // this will automatically forward the customer
            } else {
                // not successful

                return $response->getMessage();
            }
        } catch(Exception $e) {
            
            return $e->getMessage();
        }

    }

    public function paypalReturn(Request $req){
        dd($req  , $req->all());
    }



    public function payForPlan(Request $request){

        $apiContext = new ApiContext(new OAuthTokenCredential(env('PAYPAL_CLIENT_ID') , env('PAYPAL_SECRET') ));



        $payer = new Payer();
        dd($payer);
        $payer->setPaymentMethod("paypal");


        // $item1 = new Item();
        // $item1->setName('Ground Coffee 40 oz')
        //     ->setCurrency('USD')
        //     ->setQuantity(1)
        //     ->setSku("123123") // Similar to `item_number` in Classic API
        //     ->setPrice(7.5);
        // $item2 = new Item();
        // $item2->setName('Granola bars')
        //     ->setCurrency('USD')
        //     ->setQuantity(5)
        //     ->setSku("321321") // Similar to `item_number` in Classic API
        //     ->setPrice(2);

        // $itemList = new ItemList();
        // $itemList->setItems(array($item1, $item2));

        $details = new Details();
        $details->setShipping(1.2)
            ->setTax(1.3)
            ->setSubtotal(17.50);


        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal(20)
            ->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            // ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(url('success'))
            ->setCancelUrl(url('fail'));

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

            $request = clone $payment;

        try {
            $payment->create($apiContext);
        } catch (Exception $ex) {
            // ResultPrinter::printError("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", null, $request, $ex);
            echo('Error');
            exit(1);
        }

        dd($request);
        $approvalUrl = $payment->getApprovalLink();

        // ResultPrinter::printResult("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", "<a href='$approvalUrl' >$approvalUrl</a>", $request, $payment);


        // return $payment;
                
        $approvalUrl = $payment->getApprovalLink();

        return $approvalUrl;
    }

    public function PayForPlan1(){
        ini_set('error_reporting', E_ALL); // or error_reporting(E_ALL);
        ini_set('display_errors', '1');
        ini_set('display_startup_errors', '1');

        // $environment = new Environment(env('PAYPAL_CLIENT_ID') , env('PAYPAL_SECRET'));
        $environment = new SandboxEnvironment(env('PAYPAL_CLIENT_ID') , env('PAYPAL_SECRET'));
        $client = new PayPalHttpClient($environment);

        $request = new OrdersCreateRequest();
        $request->prefer('return=representation');
        $request->body = [
                        "intent" => "CAPTURE",
                        "purchase_units" => [[
                            "reference_id" => "test_ref_id1",
                            "amount" => [
                                "value" => "100.00",
                                "currency_code" => "USD"
                            ]
                        ]],
                        "application_context" => [
                            "cancel_url" => url('success'),
                            "return_url" => url('fail')
                        ] 
                    ];
        
        try {
            // Call API with your client and get a response for your call
            $response = $client->execute($request);
            
            // If call returns body in response, you can get the deserialized version from the result attribute of the response

            print_r($response);
        }catch (HttpException $ex) {
            echo $ex->statusCode;
            print_r($ex->getMessage());
        }

        dd($response , $client);

        
    }



    // public function PayForPlan2(Request $request){
        
    public function PayPalPayment(Request $request){

        // dd($request->all());
        $validator =  \Validator::make($request->all(),[
            // 'name' => 'required|string|max:255',
            // 'email' => 'required|string|email|max:255|unique:users',
            // 'password' => 'required|string|min:6|confirmed',
            'package_id' => 'required',
            'coach_id' => 'required',
            'client_id' => 'required',
            'number' => 'required| min:16 | max:16',
            'month' => 'required | min:2 | max:2',
            'year' => 'required | min:4 | max:4',
            'cvv' => 'required | min:3 | max:3',
        ]);

        if($validator->fails()){
            return response()->json([
                'success' => false,
                "error" => 'validation_error',
                "message" => $validator->errors(),
            ], 422);
        }
        
        // dd($request->all());

        $payment = Client_Payment::where('package_id' , $request->package_id )->
        where('coach_id' , $request->coach_id )->
        where('client_id' , $request->client_id )->
        first();

        $package = Package::find($request->package_id);
        $client = User::find($request->client_id);

        if($payment){
            return response()->json([ 'success' => false , 'message' => 'Payment is already done' , 'payment' => $payment ]);
        }


        $plan = Client_Plan::where('package_id' , $request->package_id )->
        where('coach_id' , $request->coach_id )->
        where('client_id' , $request->client_id )->
        first();


        // dd($request->all() , $payment , $plan);

        // new payum method
        /** @var \Payum\Core\Storage\StorageInterface $storage */

        // $paypalRestPaymentDetailsClass;

        /** @var \Payum\Core\Payum $payum */
        // $storage = $payum->getStorage($paypalRestPaymentDetailsClass);


        // paypal payment
        
        if(!$payment){

            $gateway = Omnipay::create('PayPal_REST');

            // $gateway->setClientId(env('PERFIT_PAYPAL_CLIENT_ID'));
            // $gateway->setSecret(env('PERFIT_PAYPAL_CLIENT_SECRET'));
            // $gateway->setToken(env('PERFIT_PAYPAL_TOKEN'));
            // $gateway->setTestMode(true);
            $gateway->setClientId(env('PERFIT_PAYPAL_CLIENT_ID_LIVE'));
            $gateway->setSecret(env('PERFIT_PAYPAL_CLIENT_SECRET_LIVE'));
            $gateway->setToken(env('PERFIT_PAYPAL_TOKEN_LIVE'));
            $gateway->setTestMode(false);

            
            // dd($gateway->getDefaultParameters());

            $card = new CreditCard(array(
                'firstName' => $client->name,
                'lastName' => $client->name,
                'number' => $request->number,
                'expiryMonth'           => $request->month,
                'expiryYear'            => $request->year,
                'cvv'                   => $request->cvv,
                // 'number' => '5110924080573469',
                // 'expiryMonth'           => '08',
                // 'expiryYear'            => '2023',
                // 'cvv'                   => '764',
                'billingAddress1'       => '1 Scrubby Creek Road',
                'billingCountry'        => 'QA',
                'billingCity'           => 'Scrubby Creek',
                'billingPostcode'       => '4999',
                'billingState'          => 'QA',
    
            ));

            // dd($package->price);


            // Do a purchase transaction on the gateway
            $transaction = $gateway->purchase(array(
                'amount'        => '1.00',
                'currency'      => 'USD',
                'description'   => 'A Basic payment for the package of FTBLE of 1 USD',
                'card'          => $card,
                // 'cancelUrl ' => url('fail'),
                // 'returnUrl' => url('success'),
            ));


            $response = $transaction->send();
            $responseData = $response->getData();
            // dd($responseData , $responseData['id']);
            if($response->isSuccessful()){

                $payment = Client_Payment::create([
                    'plan_id' => $plan->id ?? null,
                    'coach_id' => $request->coach_id,
                    'client_id' => $request->client_id,
                    'package_id' => $request->package_id,
                    'amount' => $responseData['transactions'][0]['amount']['total'],
                    'transaction_id' => $responseData['id'],
                    'body' => json_encode($responseData),
                ]);

                
                return response()->json(['success' => true , 'message' => 'Payment was successful' , 'data' => $responseData , 'payment' => $payment ]);
            }else{
                Error_Logger::create(['error' => json_encode($response->getMessage())]);
                
                return response()->json(['success' => false , 'message' => 'Payment failed' , 'error' => $response->getMessage()  ]);
            }
        }





        // $gateway = Omnipay::create('PayPal_REST');
        // $gateway = Omnipay::create('PayPal_Pro');
        // $gateway = Omnipay::create('PayPal_Express');
        // $gateway->setUsername("sb-8qfzb2576719_api1.business.example.com");
        // $gateway->setPassword("GLGV8E3NCVJS8TAF");
        // $gateway->setSignature(SIGNATURE);
        // $gateway->setSellerPaypalAccountId(SELLERPAYPALACCOUNTID);


        // $gateway->setClientId(env('PERFIT_PAYPAL_CLIENT_ID'));
        // $gateway->setSecret(env('PERFIT_PAYPAL_CLIENT_SECRET'));
        // $gateway->setToken(env('PERFIT_PAYPAL_TOKEN'));

        // $gateway->setClientId(env('PAYPAL_CLIENT_ID_1'));
        // $gateway->setSecret(env('PAYPAL_SECRET_1'));
        // $gateway->setToken(env('PAYPAL_TOKEN_1'));
        // $gateway->setTestMode(true);
        
        // dd($gateway->getDefaultParameters() , $gateway );
        // $gateway->initialize(array(
        //     'clientId' => 'AbDupx54ohER8p435e7BkK8DUdQPgRqWYApTyws_F6bLc6P0as-IAUDnQLqTAypEReM7w1-NrDh7TmdN',
        //     'secret'   => 'EMf9T9oIa8fA4BfjzSDcDKHbzA3rXOkC9hDNA5SjZCICCAxniwDWLTCFTeQiRbQl9608XRwncLtvTiGE',
        //     'testMode' => true, // Or false when you are ready for live transactions
        // ));
        // $gateway->setUsername('sb-8qfzb2576719_api1.business.example.com');
        // $gateway->setPassword('GLGV8E3NCVJS8TAF');

        // dd($gateway);

        
            $card = new CreditCard(array(
            
                // 5000.00 USD
                // 6065.17 USD

                'firstName' => 'Muhammad',
                'lastName' => 'Saad',
                // 'number' => $request->number,
                // 'expiryMonth'           => $request->month,
                // 'expiryYear'            => $request->year,
                // 'cvv'                   => $request->cvv,
                'number' => '5110924080573469',
                'expiryMonth'           => '08',
                'expiryYear'            => '2023',
                'cvv'                   => '764',
                'billingAddress1'       => '1 Scrubby Creek Road',
                'billingCountry'        => 'US',
                'billingCity'           => 'Scrubby Creek',
                'billingPostcode'       => '4999',
                'billingState'          => 'QLD',
    
            ));
            // Do a purchase transaction on the gateway
            $transaction = $gateway->purchase(array(
                'amount'        => '100.00',
                'currency'      => 'USD',
                'description'   => 'This is a test purchase transaction.',
                'card'          => $card,
                // 'cancelUrl ' => url('fail'),
                // 'returnUrl' => url('success'),
            ));

            // dd($transaction);

            $response = $transaction->send();

            // dd( $response  , $response->getData() , $response->getMessage() );
            // dump($response , $response->getTransactionReference() );

            if($response->isSuccessful()){
                return response()->json(['success' => true , 'message' => 'Payment was successful' , 'data' => $response->getData()]);
            }else{
                return response()->json(['success' => false , 'message' => 'Payment failed']);
            }

            if ($response->isSuccessful()) {
                echo "Purchase transaction was successful!\n";
                $sale_id = $response->getTransactionReference();
                echo "Transaction reference = " . $sale_id . "\n";
            }

            // dd($response , $response->transactions[0]->related_resources[0]->sale['id'] );

        // 4032032496350817
        // 09/2025
        // 4960.00 USD
        // 6037.24 USD

        // $params = [
        //     'amount' => $order->amount,
        //     'issuer' => $issuerId,
        //     'description' => $order->description,
        //     'returnUrl' => url('success'),
        // ];
        
        
        // $response = Omnipay::purchase($params)->send();
        
        // if ($response->isSuccessful()) {
        //     // payment was successful: update database
        //     print_r($response);
        // } elseif ($response->isRedirect()) {
        //     // redirect to offsite payment gateway
        //     return $response->getRedirectResponse();
        // } else {
        //     // payment failed: display message to customer
        //     echo $response->getMessage();
        // }


    }


    public function PayForPlan3(){

            $request_params = array (
                  'METHOD' => 'DoDirectPayment',
                  'USER' => 'laracode101_api1.gmail.com',
                  'PWD' => 'BQJBSMHVP8WCEETS',
                  'SIGNATURE' => 'AyVuUcpetZUWCXV.EHq.qRhxEb6.AChQnxfoM9w6UJXXRBh2cH1GUawh',
                  'VERSION' => '85.0',
                  'PAYMENTACTION' => 'Sale',
                  'IPADDRESS' => '127.0.0.1',
                  'CREDITCARDTYPE' => 'Visa',
                  'ACCT' => '4032032851107356',
                  'EXPDATE' => '072023',
                  'CVV2' => '123',
                  'FIRSTNAME' => 'Yang',
                  'LASTNAME' => 'Ling',
                  'STREET' => '1 Main St',
                  'CITY' => 'San Jose',
                  'STATE' => 'CA',
                  'COUNTRYCODE' => 'US',
                  'ZIP' => '95131',
                  'AMT' => '100.00',
                  'CURRENCYCODE' => 'USD',
                  'DESC' => 'Testing Payments Pro'
           );     
         
           $nvp_string = '';     
           foreach($request_params as $var=>$val)
           {
              $nvp_string .= '&'.$var.'='.urlencode($val);
           }
         
               $curl = curl_init();     
               curl_setopt($curl, CURLOPT_VERBOSE, 0);     
               curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);     
               curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);     
               curl_setopt($curl, CURLOPT_TIMEOUT, 30);     
               curl_setopt($curl, CURLOPT_URL, 'https://api-3t.sandbox.paypal.com/nvp');     
               curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);     
               curl_setopt($curl, CURLOPT_POSTFIELDS, $nvp_string); 
    
               $result = curl_exec($curl);     
               curl_close($curl);   
    
               dd($result);
               $data = $this->NVPToArray($result);
    
               
               if($data['ACK'] == 'Success') {
                   # Database integration...
                   echo "Your payment was processed success.";
               } if ($data['ACK'] == 'Failure') {
                   # Database integration...
                   echo "Your payment was declined/fail.";
               } else {
                   echo "Something went wront please try again letter.";
               }
        
    }




    public function PayForPlan4(){
        try{
 
            // Setup payment Gateway
            $pay = Omnipay::create('Stripe');
            $pay->setApiKey('YOUR API KEY');
            // Send purchase request
            $response = $pay->purchase(
                [
                    'amount' => $value['amount'],
                    'currency' => $value['currency'],
                    'card' => "4032032851107356",
                ]
            )->send();
 
            // Process response
            dd($response);

            if ($response->isSuccessful()) {
 
                return "Thankyou for your payment";
 
            } elseif ($response->isRedirect()) {
 
                // Redirect to offsite payment gateway
                return $response->getMessage();
 
            } else {
               // Payment failed
               return $response->getMessage();
            }
        }
        catch(\Exception $ex){
            return $ex->getMessage();
        }
    }
        


    public function payWithpaypal(Request $request)
    {
        $payer = new Payer();
                $payer->setPaymentMethod('paypal');

        $item_1 = new Item();
        $item_1->setName('Item 1') /** item name **/
                    ->setCurrency('USD')
                    ->setQuantity(1)
                    ->setPrice($request->get('amount')); /** unit price **/

        $item_list = new ItemList();
                $item_list->setItems(array($item_1));

        $amount = new Amount();
                $amount->setCurrency('USD')
                    ->setTotal($request->get('amount'));
        
        $transaction = new Transaction();
                $transaction->setAmount($amount)
                    ->setItemList($item_list)
                    ->setDescription('Your transaction description');
        
        
        $redirect_urls = new RedirectUrls();
                $redirect_urls->setReturnUrl(URL::route('status')) /** Specify return URL **/
                    ->setCancelUrl(URL::route('status'));
        
        
        $payment = new Payment();
                $payment->setIntent('Sale')
                    ->setPayer($payer)
                    ->setRedirectUrls($redirect_urls)
                    ->setTransactions(array($transaction));
                /** dd($payment->create($this->_api_context));exit; **/
                try {
        $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
        if (\Config::get('app.debug')) {
        \Session::put('error', 'Connection timeout');
                        return Redirect::route('paywithpaypal');
        } else {
        \Session::put('error', 'Some error occur, sorry for inconvenient');
                        return Redirect::route('paywithpaypal');
        }
        }
        foreach ($payment->getLinks() as $link) {
        if ($link->getRel() == 'approval_url') {
        $redirect_url = $link->getHref();
                        break;
        }
        }
        /** add payment ID to session **/
                Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
        /** redirect to paypal **/
                    return Redirect::away($redirect_url);
        }
        \Session::put('error', 'Unknown error occurred');
                return Redirect::route('paywithpaypal');
        }


        public function PayWithSadad(Request $request){
            
            // if ($_SERVER['REQUEST_METHOD'] == 'GET' && realpath(__FILE__) ==
            // realpath($_SERVER['SCRIPT_FILENAME'])) {
            // echo 'access denied';
            //     die;
            // }

            $secretkey = '5rQRZSC4t7Ze42Cb'; // Secret key of the merchant
            $sadadid = 7648426; // Sadad id of the merchant
            $registered_domain = 'www.vquick.qa'; // Registered domain of the merchant
            $type = 'sandbox'; // 'live' - For live SDK,'sandbox' - For Sandbox


            // include 'config.php';
            if (isset($type) && $type == 'live') {
                $wspath = 'https://api.sadadqatar.com/api-v4/';
            } else if (isset($type) && $type == 'sandbox') {
                $wspath = 'https://sandboxapi.sadadqa.com/api/';
            } else {
                echo 'Please insert valid type.';
            }


            $requestdata = array();
            $requestip = $_SERVER['REMOTE_ADDR']; // Ip of app sdk user

            $requestdata = '{
                                "sadadId": "7648426",
                                "secretKey": "5rQRZSC4t7Ze42Cb",
                                "domain": "www.vquick.qa" 
                            }';

            $ch = curl_init('https://sandboxapi.sadadqa.com/api/' . 'userbusinesses/getsdktoken');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, @$requestdata);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 
            'Content-Length: ' . strlen(@$requestdata)));
            $returndata = curl_exec($ch) . "\n";
            curl_close($ch);

            dd($returndata);

        }




        public function make(Request $request)
        {

            // dd($request->all());

            $gateway = new \Braintree\Gateway([
                'environment' => 'sandbox',
                'merchantId' => env('BRAINTREE_MERCHANT_ID'),
                'publicKey' => env('BRAINTREE_PUBLIC_KEY'),
                'privateKey' => env('BRAINTREE_PRIVATE_KEY')
            ]);

          

            
            $customer = $gateway->customer()->create([
                'firstName' => 'Mike',
                'lastName' => 'Jones',
                'company' => 'Jones Co.',
                'email' => 'mike.jones@example.com',
                'phone' => '281.330.8004',
                'fax' => '419.555.1235',
                // 'creditCard' => '4111111111111111',
                'website' => 'http://example.com',
                'paymentMethodNonce' => nonceFromTheClient

            ]);

            $result = $gateway->creditCard()->create([
                'customerId' => $customer->customer->id,
                'number' => '4111111111111111',
                'expirationDate' => '06/22',
                'cvv' => '100'
            ]);

            
            // $result->success;

            dd($result);

            $result = $gateway->paymentMethodNonce()->create('A_PAYMENT_METHOD_TOKEN');
            $nonce = $result->paymentMethodNonce->nonce;



            
            $payload = $request->input("payload", false);
            $nonce = $payload["nonce"];
            // $status = \Braintree\Braintree_Transaction::sale([
            $status = $gateway->transaction()->sale([
                                    "amount" => "10.00",
                                    "paymentMethodNonce" => $nonce,
                                    "options" => [
                                               "submitForSettlement" => True
                                    ],
                      ]);
            return response()->json($status);
        }
        



        public function payForPlan5(){
            // $data = '{
            //     "intent":"sale",
            //     "redirect_urls":{
            //       "return_url":"http://<return URL here>",
            //       "cancel_url":"http://<cancel URL here>"
            //     },
            //     "payer":{
            //       "payment_method":"paypal"
            //     },
            //     "transactions":[
            //       {
            //         "amount":{
            //           "total":"7.47",
            //           "currency":"USD"
            //         },
            //         "description":"This is the payment transaction description."
            //       }
            //     ]
            //   }';
              

        $ch = curl_init();
        $clientId = env("PAYPAL_CLIENT_ID");
        $secret = env("PAYPAL_CLIENT_SECRET");

        curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/oauth2/token");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

        $result = curl_exec($ch);

        if(empty($result))die("Error: No response.");
        else
        {
                $json = json_decode($result);
                    //print_r($json->access_token);
                        $data = '{
                            "intent":"sale",
                            "redirect_urls":{
                            "return_url":"http://returnurl.com",
                            "cancel_url":"http://returnurl.com"
                            },
                            "payer":{
                            "payment_method":"paypal"
                            },
                            "transactions":[
                            {
                            "amount":{
                            "total":"' . preg_replace("/[^0-9,.]/", "", 100) .'",
                            "currency":"' . 'USD' .'"
                            },
                            "description":"' . ' Logo Purchase. ' . '"
                            }
                            ]
                            }';

                            curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/payments/payment");
                            curl_setopt($ch, CURLOPT_POST, true);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type:application/json',
                            "Authorization: Bearer A21AAGcbzmjhcB59lbhYH5kTtYOkXIZB68ydtiHNEHaRdU4QkQy24S-LhCahVEnt6DxIgo015dXySUFSHdJPyqDKEQKdN2iWQ", 
                            "Content-length: ".strlen($data))
                            );
                            print_r($data);
                            $result = curl_exec($ch);

                            if(empty($result))die("Error: No response.");
                            else {
                                $json_output = json_decode($result);
                                print_r($result);

                                if(isset($json_output->{'links'}[2]->{'href'}))
                                    $_SESSION['execute_url'] = $json_output->{'links'}[2]->{'href'};
                                if(isset($json_output->{'links'}[1]->{'href'}))
                                    header('Location:' .$json_output->{'links'}[1]->{'href'} );
                            }

                    

                }

        curl_close($ch);



              
            //   $result = curl_exec($ch);

            //   dd($result);
              
        }


























































    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function get_plan($client_id , $coach_id , $package_id){


        // dd($client_id , $coach_id , $package_id);
        $plan = Client_Plan::where('client_id' , $client_id)->where('coach_id' , $coach_id)->where('package_id' , $package_id)->first();

        // dd('stop');
        // dd($plan , $plan->days );

        if($plan){

            foreach($plan->days as $day){
                $day->normalise_data();
               
            }

            return response()->json(['success' => true , 'plan' => $plan , 'plan_days_ids' => $plan->days->pluck('day_number')->toArray() ]);
        }else{
           return response()->json(['success' => false , 'message' => 'Plan Not Found' ]);
        }

    }


    public function getPlanDetails($plan_id  ){

        $plan = Client_Plan::with('payment')->find($plan_id);

 

        // dd($total_weeks);

        if($plan){

            if($plan->start_status == 1){

                foreach($plan->days as $day){
                    $day->normalise_data();
                    
                }

                $total_weeks = [];
                $plan->total_weeks = [];
                $plan->getTotalWeeks();
                $total_weeks = $plan->total_weeks;
        
                foreach($total_weeks as $i => $week){
                    $total_weeks[$i] = array_values($total_weeks[$i]);
                    // dump($week);
                }

                // dd($total_weeks);

                return response()->json(['success' => true , 'plan' => $plan , 'total_weeks' => array_values($total_weeks)  ]);

            }else
            return response()->json(['success' => false , 'message' => 'Plan Not Started' ]);

        }else{
           return response()->json(['success' => false , 'message' => 'Plan Not Found' ]);
        }


    }


    public function getASingleDay($plan_id , $date ){

        $plan = Client_Plan::find($plan_id);

        // dd($plan , $day , $date , Carbon::parse($date));
        
        if($plan){
            $day = $plan->days->where('date' , $date )->first();
            
        if($day){
            $day->normalise_data();

            return response()->json(['success' => true , 'day' => $day ]);

        }else{
            return response()->json(['success' => false , 'message' => 'Day Not Found' ]);
        }
        }
        else{
           return response()->json(['success' => false , 'message' => 'Plan Not Found' ]);
        }
    }
    
    public function completeExerciseInDay($exercise_id , $day_id ){

        $day = Plan_Day::find($day_id);
        
        if($day){
            
        $status = new stdClass();
        $status->is_completed = true;
        $status->completed_at = Carbon::now()->format('d-m-Y h:m A');

        $workouts_status = [];

        foreach(json_decode($day->workout_ids) as $key => $workout){
        // dd($day->workout_status->{$workout});
            // dd($day->workout_status[$workout]);
            // if(isset($day->workout_status) && isset($day->workout_status->{$workout} ) ){
            if(isset($day->workout_status) && isset($day->workout_status[$workout] ) ){
                // dump(isset($day->workout_status[$workout] ));
                $workouts_status[$workout] = $day->workout_status[$workout];
            }else{
                $workouts_status[$workout] = null;
            }
        }

        // dd($workouts_status);

        

        $workouts_status[$exercise_id] = $status;
        $day->workout_status = $workouts_status;
        $day->save();

        
        if($day->save()){
            $day->normalise_data();
            return response()->json(['success' => true , 'day' => $day ]);
        }else{
            return response()->json(['success' => false , 'message' => 'Day Not Found' ] , 422 );
        }}else{
            return response()->json(['success' => false , 'message' => 'Day Not Found' ] , 422 );
        }
   


    //     if($day->workouts && is_array(json_decode($day->workouts))){

    //         $workouts = json_decode($day->workouts);
            
    //         foreach($workouts as $k => $workout){
    //             if($workout->Exercise_Id == $exercise_id){
    //             $status->is_completed = true;
    //             $status->completed_at = Carbon::now()->format('d-m-Y h:m A');
    //             $workouts[$k]->status = $status;
    //             // dd($workouts , $workouts[$k] );
    //             $day->workouts = json_encode($workouts);
    //             $day->save();
    //             break;
    //         }
    //     }
    // }


    }


        public function PayForPlanStripe(Request $request){
            
            // dd($request->all());

            Stripe::setApiKey("sk_test_51HHaPcDqMeCNJddkfOxXc7u2QaTcZTCVep19UKtwT5Pv6YVRVv3DGs4AkH6kcEvihoN2axafd2OMf7K4DNI0GHnE00G9D7Gk1C");    

            $token = $request->stripeToken;
            // dd($token);
            $plan = Client_Plan::find($request->plan_id);

            if($plan){

            $charge = \Stripe\Charge::create([
            'amount' => $plan->package->price * 100,
            'currency' => 'usd',
            'description' => 'FTBLE package ' . $plan->package->name . ' charges.',
            'source' => $token,

            ]);

            $coach = Coach::find($request->coach_id);

                $payment = Client_Payment::create([
                    'plan_id' => $plan->id,
                    'coach_id' => $plan->coach_id,
                    'client_id' => $plan->client_id,
                    'amount' => $plan->package->price,
                    'coach_percentage' => $coach->percentage,
                ]);
    
                if($payment){
                    return redirect()->back()->with('success' , 'Payment Made successfully' );
                }
            }

        }





        public function storeDayData(Request $request)
        {
    
    
    
            // sleep(10);
            // dd($main_array);
            // dd('Stop');
            // dump($request->all());
    
    
            // $a = $request->selected_workouts_reps;
            // $b = $request->selected_workouts_steps;


    
    
            $new_array = array();
            $selected_workouts = $request->selected_workouts;
            sort($selected_workouts);
            
            $workout_steps = [];
            $workout_reps = [];
            $workout_status = [];
            $i = 0;
            foreach($selected_workouts as $k => $workout){
                    // dd($request->selected_workouts_reps[(int)$workout]);
                    $workout_steps[$i] = $request->selected_workouts_steps[(int)$workout];
                    $workout_reps[$i] = $request->selected_workouts_reps[(int)$workout];
                    if(isset($request->selected_workouts_status[$workout])){
                        $workout_status[(int)$workout] = $request->selected_workouts_status[(int)$workout];
                    }else{
                        $workout_status[(int)$workout] = null;
                    }
                    $i++;
            }
            
            // dd($workout_status);
            // dump($request->all());
            // dd($selected_workouts , $workout_steps , $workout_reps );

    
            // $new_array = array();
    
            // $i = 0;
            // foreach($request->selected_workouts_steps as $k => $step){
            //     if(isset($step)){
            //         $new_array[$i] = $step;
            //         $i++;
            //     }
            // }
            // $workout_steps = $new_array;
            // // $workout_steps = $request->selected_workouts_steps;
    
            // $new_array1 = array();
            // $j = 0;
            // foreach($request->selected_workouts_reps as $k => $rep){
            //     if(isset($rep)){
            //         $new_array1[$j] = $rep;
            //         $j++;
            //     }
            // }
            
            // $workout_reps = $new_array1;
            // // $workout_reps = $request->selected_workouts_reps;
            
            // dump($request->all());
            
            // dd($workout_steps , $workout_reps , $selected_workouts);
            
            
            $plan = Client_Plan::where( 'client_id' , $request->client_id)->where('coach_id' , $request->coach_id)->where('package_id' , $request->package_id)->first();
    
            
    
            // dd(Carbon::parse($plan->start_date)->addDays($request->day_number));
            
    
            // foreach($request->meal_description_images as $k => $image){
            foreach($request->meal_description_title as $k => $title){
            
                if( isset($request->meal_images_upload_indication[$k]) && $request->meal_images_upload_indication[$k] == 1 && isset($request->meal_description_images[$k])){
                
                $randome_string = Str::random(12);
                $name = $randome_string.".".explode('/',explode(':',substr($request->meal_description_images[$k], 0, strpos($request->meal_description_images[$k],';')))[1])[1];
                // dump(\Image::make($image));
                $image_upload = \Image::make($request->meal_description_images[$k])->save(public_path().'/images/plan_images/'.$name);
               
                // dump($image_upload);
                if($image_upload){
                    if( isset($request->current_images[$k]) && file_exists(public_path().'/images/plan_images/'.$request->current_images[$k] )){
                        
                        File::delete(public_path().'/images/plan_images/'.$request->current_images[$k]);
    
                        // dump('deletion');
                        
                    }    
                }
    
                $image_names[$k] = $name;
                
            }elseif( isset($request->current_images[$k])){
            // }elseif( isset($request->current_images[$k]) && !isset($request->meal_description_images[$k]) && !isset($request->meal_images_upload_indication[$k])){
                $image_names[$k] = $request->current_images[$k];
                
            }else{
                $image_names[$k] = null;
    
            }
    
            }
    
            // dd($request->all());
    
            
            // retriving selected supplemets' arrays for each time
    
            if(isset($request->selected_supplements) && is_array($request->selected_supplements)){
    
                // foreach($request->selected_supplements as $i => $selected_supplements){
                for($i = 0; $i < 3; $i++){
                    if(isset($request->selected_supplements[$i])){
                        $array = array();
                        foreach($request->selected_supplements[$i] as $k => $supplement){
                            // dump($supplement['id']);
                            $array[] = $supplement['id'];
                        }
                        $all_selected_supplements[] = $array;
                        $array = null; 
                    }else{
                        $all_selected_supplements[] = [];
                    }
    
                    if(isset($request->supplement_prescription[$i])){
                        $prescriptions[] = $request->supplement_prescription[$i];
                    }else{
                        $prescriptions[] = [];
                    }
                }
            }
            // dd($prescriptions);
    
            $day = Plan_Day::updateOrCreate(
                [
                    'client_id' => $request->client_id,
                    'coach_id' => $request->coach_id,
                    'package_id' => $request->package_id,
                    'plan_id' => $plan->id,
                    'day_number' => $request->day_number,
                ],[
                    'client_id' => $request->client_id,
                    'coach_id' => $request->coach_id,
                    'package_id' => $request->package_id,
                    'plan_id' => $plan->id,
                    'day_number' => $request->day_number,
                    'date' => isset($plan->start_date) ? (Carbon::parse($plan->start_date)->addDays($request->day_number))->toDateString() : null ,
    
                    'workout_ids' => isset($selected_workouts) && is_array($selected_workouts) ? json_encode($selected_workouts) : null ,
                    'workout_steps' => isset($request->selected_workouts) ? json_encode($workout_steps ) : null ,
                    'workout_reps' => isset($request->selected_workouts) ? json_encode($workout_reps  ) : null ,
                    'workout_status' => isset($request->selected_workouts) ? $workout_status : null ,
    
    
                    'meals' => json_encode($request->selected_meals),
                    'meal_images' => isset($image_names) && is_array($image_names) ?  json_encode($image_names) : null,
                    'meal_title' => json_encode($request->meal_description_title),
                    'meal_summary' => json_encode($request->meal_description_summary),
                    'meal_needs' => json_encode($request->meal_description_needs),
                    'meal_how_to_do' => json_encode($request->meal_description_how_to_do),
    
    
                    'supplement_timings' => json_encode($request->selected_supplements_timings),
                    'supplement_ids' => isset($all_selected_supplements) ? json_encode($all_selected_supplements) : null ,
                    'supplement_prescription' => isset($prescriptions) ? json_encode($prescriptions) : null ,
                    // 'supplement_prescription' => json_encode($request->supplement_prescription),
                ]
            );
    
    
            if($day){
                return response()->json(['success' => true , 'plan' => $plan , 'day' => $day ]);
            }else
            return response()->json(['success' => false , 'message' => 'Some Problem Occurred' ] , 500);
        }
    
    

        



    
    public function storeDayData1(Request $request)
    {


        // dump($request->all());

        // sleep(10);
        // dd($main_array);
        // dd('Stop');
        // dd($request->all());


        // $a = $request->selected_workouts_reps;
        // $b = $request->selected_workouts_steps;
        // dump($request->all());


        $new_array = array();

        // foreach($request->selected_workouts as $k => $workout_id){
            //     if(isset($workout_id)){
                //         $selected_workouts[] = (int)$workout_id;
                //     }
                // }
                
        $selected_workouts = $request->selected_workouts;
        // sort($selected_workouts);
        


        // $new_array = array();

        // $i = 0;
        // foreach($request->selected_workouts_steps as $k => $step){
        //     if(isset($step)){
        //         $new_array[$i] = $step;
        //         $i++;
        //     }
        // }
        // $workout_steps = $new_array;

        // $new_array1 = array();
        // $j = 0;
        // foreach($request->selected_workouts_reps as $k => $rep){
        //     if(isset($rep)){
        //         $new_array1[$j] = $rep;
        //         $j++;
        //     }
        // }
        
        // $workout_reps = $new_array1;
        

        $workout_steps = array_values($request->selected_workouts_steps);

        $workout_reps = array_values($request->selected_workouts_reps);
        // dd($workout_reps , $workout_steps , $selected_workouts);
        
        
        $plan = Client_Plan::where( 'client_id' , $request->client_id)->where('coach_id' , $request->coach_id)->where('package_id' , $request->package_id)->first();

        

        // dd(Carbon::parse($plan->start_date)->addDays($request->day_number));
        

        // foreach($request->meal_description_images as $k => $image){
        foreach($request->meal_description_title as $k => $title){
        
            if( isset($request->meal_images_upload_indication[$k]) && $request->meal_images_upload_indication[$k] == 1 && isset($request->meal_description_images[$k])){
            
            $randome_string = Str::random(12);
            $name = $randome_string.".".explode('/',explode(':',substr($request->meal_description_images[$k], 0, strpos($request->meal_description_images[$k],';')))[1])[1];
            // dump(\Image::make($image));
            $image_upload = \Image::make($request->meal_description_images[$k])->save(public_path().'/images/plan_images/'.$name);
           
            // dump($image_upload);
            if($image_upload){
                if( isset($request->current_images[$k]) && file_exists(public_path().'/images/plan_images/'.$request->current_images[$k] )){
                    
                    File::delete(public_path().'/images/plan_images/'.$request->current_images[$k]);

                    // dump('deletion');
                    
                }    
            }

            $image_names[$k] = $name;
            
        }elseif( isset($request->current_images[$k])){
        // }elseif( isset($request->current_images[$k]) && !isset($request->meal_description_images[$k]) && !isset($request->meal_images_upload_indication[$k])){
            $image_names[$k] = $request->current_images[$k];
            
        }else{
            $image_names[$k] = null;

        }

        }

        // dd($request->all());

        
        // retriving selected supplemets' arrays for each time

        if(isset($request->selected_supplements) && is_array($request->selected_supplements)){

            // foreach($request->selected_supplements as $i => $selected_supplements){
            for($i = 0; $i < 3; $i++){
                if(isset($request->selected_supplements[$i])){
                    $array = array();
                    foreach($request->selected_supplements[$i] as $k => $supplement){
                        // dump($supplement['id']);
                        $array[] = $supplement['id'];
                    }
                    $all_selected_supplements[] = $array;
                    $array = null; 
                }else{
                    $all_selected_supplements[] = [];
                }

                if(isset($request->supplement_prescription[$i])){
                    $prescriptions[] = $request->supplement_prescription[$i];
                }else{
                    $prescriptions[] = [];
                }
            }
        }
        // dd($prescriptions);

        $day = Plan_Day::updateOrCreate(
            [
                'client_id' => $request->client_id,
                'coach_id' => $request->coach_id,
                'package_id' => $request->package_id,
                'plan_id' => $plan->id,
                'day_number' => $request->day_number,
            ],[
                'client_id' => $request->client_id,
                'coach_id' => $request->coach_id,
                'package_id' => $request->package_id,
                'plan_id' => $plan->id,
                'day_number' => $request->day_number,
                'date' => isset($plan->start_date) ? (Carbon::parse($plan->start_date)->addDays($request->day_number))->toDateString() : null ,

                'workout_ids' => isset($selected_workouts) && is_array($selected_workouts) ? json_encode($selected_workouts) : null ,
                'workout_steps' => isset($request->selected_workouts) ? json_encode($workout_steps ) : null ,
                'workout_reps' => isset($request->selected_workouts) ? json_encode($workout_reps  ) : null ,
                // 'workout_status' => isset($workouts_status) ? json_encode($workouts_status) : null ,


                'meals' => json_encode($request->selected_meals),
                'meal_images' => isset($image_names) && is_array($image_names) ?  json_encode($image_names) : null,
                'meal_title' => json_encode($request->meal_description_title),
                'meal_summary' => json_encode($request->meal_description_summary),
                'meal_needs' => json_encode($request->meal_description_needs),
                'meal_how_to_do' => json_encode($request->meal_description_how_to_do),


                'supplement_timings' => json_encode($request->selected_supplements_timings),
                'supplement_ids' => isset($all_selected_supplements) ? json_encode($all_selected_supplements) : null ,
                'supplement_prescription' => isset($prescriptions) ? json_encode($prescriptions) : null ,
                // 'supplement_prescription' => json_encode($request->supplement_prescription),
            ]
        );


        $day = Plan_Day::where('client_id' , $request->client_id)
            ->where('coach_id' , $request->coach_id)
            ->where('package_id' , $request->package_id)
            ->where('plan_id' , $plan->id)
            ->where('day_number' , $request->day_number)->first();

        if($day){
            $status = new stdClass();
            $workouts_status = [];
    
            foreach(json_decode($day->workout_ids) as $key => $workout){
            // dd($day->workout_status->{$workout});
            
                if(isset($day->workout_status->{$workout} ) ){
                    $workouts_status[$workout] = $day->workout_status->{$workout};
                }else{
                    $workouts_status[$workout] = null;
                }
            }

            $day->workout_status = $workouts_status;
            $day->save();
            
        }
        
        



        if($day){
            return response()->json(['success' => true , 'plan' => $plan , 'day' => $day ]);
        }else
        return response()->json(['success' => false , 'message' => 'Some Problem Occurred' ] , 500);
    }


    public function startPlanNow($plan_id , $coach_id)
    {
        $plan = Client_Plan::find($plan_id);
        $coach = Coach::find($coach_id);
        $client = User::find($plan->client_id);

        if($plan->coach_id != $coach_id){
            return response()->json(['message' => "Plan Does not belongs to coach" ] , 500);
        }

        if($plan && $client){
            if($plan->start_status == 0){
                $plan->start_status = 1;
                $plan->start_date = Carbon::today()->toDateString() ;
                $plan->save();

                foreach($plan->days as $day){
                    $day->date = Carbon::parse($plan->start_date)->addDays($day->day_number)->toDateString();
                    $day->save();
                }

                if($client->fcm_token){
                    // dd($client->fcm_token);
                    $notification = new stdClass();
                    $notification->text = "Your plan from coach ". $coach->user->name . " has started ";
                    $notification->description = "Your plan from coach ". $coach->user->name . " has started";
                    $notification->notif_type = "notif-plan";
        
                    $this->PublishBroadcast($client , $coach->user , $notification , $coach->user->name , $notification->text , " ");
                }
                
                if($coach->user->fcm_token){
                    // dd($client->fcm_token);
                    $notification = new stdClass();
                    $notification->text = "You have started a plan of the client: ". $client->name;
                    $notification->description = "You have started a plan of the client: ". $client->name;
                    $notification->notif_type = "notif-plan";
        
                    $this->PublishBroadcast($coach->user , $client , $notification , $client->name , $notification->text , " ");
                }
        
                
                return response()->json(['success' => true , 'message' => 'Plan Started Successfully' ] );
            }elseif($plan->start_status == 1)
            return response()->json([ 'success' => false , 'message' => "Plan Started Already" ] , 500);
        }
        else
        return response()->json(['message' => "Plan Not Found" ] , 500);
    }

    // public function startPlanNow($plan_id)
    // {
    //     $plan = Client_Plan::find($plan_id);
        
    //     if($plan){
    //         if($plan->start_status == 0){
    //             $plan->start_status = 1;
    //             $plan->start_date = \Carbon\Carbon::today()->toDateString() ;
    //             $plan->save();

    //             foreach($plan->days as $day){
    //                 $day->date = Carbon::parse($plan->start_date)->addDays($day->day_number)->toDateString();
    //                 $day->save();
    //             }
                
    //             return response()->json(['success' => true , 'message' => 'Plan Started Successfully' ] );
    //         }elseif($plan->start_status == 1)
    //         return response()->json(['message' => "Plan Started Already" ] , 500);
    //     }
    //     else
    //     return response()->json(['message' => "Plan Not Found" ] , 500);
    // }


    public function storeDayMeasurements(Request $request)
    {
        $plan = Client_Plan::where([
        ['client_id' ,  $request->client_id],
        ['coach_id' ,  $request->coach_id],
        ['package_id' , $request->package_id],
        ])->first();

        $user = User::find($request->client_id);

        
        if($plan){
            $day = $plan->days->find($request->day_id);
        }
        if($plan && $day){

            $measurements = array(
    
                'shoulder' => $request->shoulder,
                'chest' => $request->chest,
                'left_bicep' => $request->left_bicep,
                'left_forearm' => $request->left_forearm,
                'waist' => $request->waist,
                'left_thigh' => $request->left_thigh,
                'left_calf' => $request->left_calf,
                
                'hips' => $request->hips,
                'head' => $request->head,
                
                'right_bicep' => $request->right_bicep,
                'right_forearm' => $request->right_forearm,
                'right_thigh' => $request->right_thigh,
                'right_calf' => $request->right_calf,
            );

            $day->measurements = isset($measurements) ? json_encode($measurements) : null;
            $day->save();

            if($user){
                $user->profile->measurements = isset($measurements) ? json_encode($measurements) : null;
                $user->profile->save();
            }

            $day->measurements = json_decode($day->measurements);
            
            return response()->json(['success' => true , 'measurements' => $measurements , 'day' => $day ] );
        }else{
        return response()->json(['message' => "Plan or Day Not Found" ] , 422);
        }
    }






    public function getDayData($id){

        // dd($id);
        
        $day = Plan_Day::find($id);

        if($day){

            $day->normalise_data();

            return response()->json(['success' => true , 'day' => $day ] );
        }else{
        return response()->json(['message' => "Plan or Day Not Found" ] , 422);
        }
    }
    
    public function createDay(Request $request){

        // dd($request->all());
        $plan = Client_Plan::where([
            ['client_id' ,  $request->client_id],
            ['coach_id' ,  $request->coach_id],
            ['package_id' , $request->package_id],
            ])->first();
    
    
        $day = Plan_Day::create(
            [
                'client_id' => $request->client_id,
                'coach_id' => $request->coach_id,
                'package_id' => $request->package_id,
                'plan_id' => $request->plan_id,
                'day_number' => $request->day_number,
                'date' => isset($plan->start_date) ? (Carbon::parse($plan->start_date)->addDays($request->day_number))->toDateString() : null ,
            ]);

        if($day){
            return response()->json(['success' => true ] );
        }else{
        return response()->json(['message' => "Plan or Day Not Found" ] , 422);
        }
    }




    public function saveWorkouts(Request $request){

        // dd($request->all());
        $plan = Client_Plan::where([
            ['client_id' ,  $request->client_id],
            ['coach_id' ,  $request->coach_id],
            ['package_id' , $request->package_id],
            ])->first();
    
    
        $day = Plan_Day::updateOrCreate(
            [
                'client_id' => $request->client_id,
                'coach_id' => $request->coach_id,
                'package_id' => $request->package_id,
                'plan_id' => $request->plan_id,
                'day_number' => $request->day_number,
            ],[
                'date' => isset($plan->start_date) ? (Carbon::parse($plan->start_date)->addDays($request->day_number))->toDateString() : null ,
                'workouts' => (isset($request->workouts) && is_array($request->workouts)) ? json_encode($request->workouts) : '',
            ]);

        if($day){
            return response()->json(['success' => true ] );
        }else{
        return response()->json(['message' => "Plan or Day Not Found" ] , 422);
        }
    }


    public function saveMeals(Request $request){

        $plan = Client_Plan::where([
            ['client_id' ,  $request->client_id],
            ['coach_id' ,  $request->coach_id],
            ['package_id' , $request->package_id],
            ])->first();
    
            $meals = $request->meals;


            foreach($meals as $k => $meal){
                // dd($meal['image'] , 'asd');
        
                if( isset($meal['image'])){

                
                $randome_string = Str::random(12);
                $name = $randome_string.".".explode('/',explode(':',substr($meal['image'], 0, strpos($meal['image'],';')))[1])[1];
                $image_upload = \Image::make($meal['image'])->save(public_path().'/images/plan_images/'.$name);
               
                // dump($image_upload);
                if($image_upload){
                    if( isset($meal['image']) && file_exists(public_path().'/images/plan_images/'.$meal['image'] )){
                        
                        File::delete(public_path().$meal['image']);
                        
                    }    
                }
    
                // $meals[$k] = $name;
                $meals[$k]['image'] = '/images/plan_images/' . $name;
            }
        }

            // dd($meals);
                
            
            
        $day = Plan_Day::updateOrCreate(
            [
                'client_id' => $request->client_id,
                'coach_id' => $request->coach_id,
                'package_id' => $request->package_id,
                'plan_id' => $request->plan_id,
                'day_number' => $request->day_number,
            ],[
                'date' => isset($plan->start_date) ? (Carbon::parse($plan->start_date)->addDays($request->day_number))->toDateString() : null ,
                'meals' => (isset($request->meals) && is_array($request->meals)) ? json_encode($request->meals) : '',
            ]);



        if($day){
            return response()->json(['success' => true ] );
        }else{
        return response()->json(['message' => "Plan or Day Not Found" ] , 422);
        }
    }


    public function saveSupplements(Request $request){

        // dd($request->all());

        $plan = Client_Plan::where([
            ['client_id' ,  $request->client_id],
            ['coach_id' ,  $request->coach_id],
            ['package_id' , $request->package_id],
            ])->first();
    

        $supplements = $request->supplements;

        foreach($supplements as $k => $supplement ){
            foreach($supplements[$k]['supplements'] as $i =>  $supplement_detail ){
                $supplements[$k]['supplements'][$i]['supplement'] = $supplement_detail['supplement']['id'];
            }
        }

        // dd($supplements);

            
        $day = Plan_Day::updateOrCreate(
            [
                'client_id' => $request->client_id,
                'coach_id' => $request->coach_id,
                'package_id' => $request->package_id,
                'plan_id' => $request->plan_id,
                'day_number' => $request->day_number,
            ],[
                'date' => isset($plan->start_date) ? (Carbon::parse($plan->start_date)->addDays($request->day_number))->toDateString() : null ,
                'supplements' => (isset($supplements) && is_array($supplements)) ? json_encode($supplements) : '',
            ]);



        if($day){
            return response()->json(['success' => true ] );
        }else{
        return response()->json(['message' => "Plan or Day Not Found" ] , 422);
        }
    }

}
