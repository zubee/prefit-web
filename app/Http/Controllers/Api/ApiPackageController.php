<?php

namespace App\Http\Controllers\Api;

use App\User;
use stdClass;
use App\Coach;
use App\Package;
use Carbon\Carbon;
use App\Client_Plan;
use App\Client_Pivot;
use App\Notification;
use App\Client_Request;
use App\Jobs\SendEmail;
use App\Traits\FirebaseFCM;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class ApiPackageController extends Controller
{
    use FirebaseFCM;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    $packages = Package::all();

        if($packages){
            return response()->json( ['success' => true ,  'packages' => $packages]); 
        }else{
            return response()->json (['success' => false ,  'error' => 'Some Problem Occurred']); 
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $package = Package::find($id);

        if($package){
            return response()->json( ['success' => true ,  'package' => $package]); 
        }else{
            return response()->json (['success' => false ,  'error' => 'Package Not Found']); 
        }
    }






    public function subscribe($user_id ,  $coach_id ,  $package_id ){
        
   
        $user = User::find($user_id);
        $coach = Coach::find($coach_id);
        $package = Package::find($package_id);

        if(!$user){
            return response()->json ([ 'success' => false ,  'error' => 'User Not Found']); 
        }

        if(!$coach){
            return response()->json ([ 'success' => false ,  'error' => 'Coach Not Found']); 
        }

        if(!$package){
            return response()->json ([ 'success' => false ,  'error' => 'Package Not Found']); 
        }

        if($user->subscribers->find($package->id)){
            return response()->json ([ 'success' => false ,  'error' => 'Already Subscribed']); 
        }

        if(!$coach->packages->find($package) ){
            return response()->json ([ 'success' => false ,  'error' => 'Package Not Assigned To Coach']); 
        }

        // dd($user->subscribers->find($package->id));

        if( $user && $coach && $package && $user->role_id == 5 && !($user->subscribers->find($package->id)) && $coach->packages->find($package) ){

            
            
                $user->subscribe()->attach([$package_id] , [ 'coach_id' => $coach_id , 'created_at' => Carbon::now() ]);

                
                $plan = Client_Plan::create([
                    'client_id' => $user_id,
                    'coach_id' => $coach_id,
                    'package_id' => $package_id,
                    'duration' => $package->duration,
                    'price' => $package->price ?? 0,
                ]);

                if($user->fcm_token){
                    $notification = new stdClass();
                    $notification->text = "You have subscibed to the ". $package->name . " from the coach " . $coach->user->name;
                    
                    $notification->description = "You have subscibed to the ". $package->name . " from the coach " . $coach->user->name;

                    $notification->notif_type = "notif-subscribe";

                    $this->PublishBroadcast($user , $coach->user , $notification , $coach->user->name , $notification->text , " ");
                }
                if($coach->user->fcm_token){
                    $coach_notification = new stdClass();
                    $coach_notification->text = "A new client: " .  $user->name  . " has subscibed to the package: ". $package->name;
                    
                    $coach_notification->description = "A new client: " .  $user->name  . " has subscibed to the package: ". $package->name;

                    $coach_notification->notif_type = "notif-subscribe";

                    $this->PublishBroadcast($coach->user , $user , $coach_notification , $user->name , $coach_notification->text , " ");
                }

                $users = User::where('id' , $user_id)->get();
                $subject = 'FTBLE Package Subscription email';
                $body = 'Dear User <br>You have subscribed to the package of the coach: <b>'. $coach->user->name . '</b> package: <b>' . $package->name .'</b>' ;

                dispatch(new SendEmail($users , $subject , $body ));

                    // $notification = Notification::create([
                    //     'user_id' => $users->first()->id,
                    //     'title' => 'You have subscribed to the package: '.$package->name,
                    //     'link' => '/myaccount/messages',
                    //     'sender_id' => $request->sender,
                    // ]);
                
                
                $users = User::where('id' , $coach->user->id)->get();

                $subject = 'FTBLE Package Subscription email';
                $body = 'Dear Coach <br>Your package: <b>' .$package->name. '</b> have been subscribed by the client: <b>'. $user->name . '</b>' ;

                dispatch(new SendEmail($users , $subject , $body ));

                
                $users = User::whereIn('role_id' , [1 , 2] )->get();

                $subject = 'FTBLE Package Subscription email';
                $body = 'Dear Admin <br>Package: <b>' . $package->name . '</b> is subscribed by the client: <b>'. $user->name . '</b> of the trainer: <b>' . $coach->user->name.'</b>' ;

                dispatch(new SendEmail($users , $subject , $body ));

                return response()->json( ['success' => true ,  'message' => 'Package Subscribed' , 'plan' => $plan ]);
        }else{
            return response()->json (['success' => false ,  'error' => 'Some Problem Occurred']); 
        }    
    }



    public function makeRequest($user_id ,  $coach_id ,  $package_id ){
        
   
        $user = User::find($user_id);
        $coach = Coach::find($coach_id);
        $package = Package::find($package_id);

        $client_request = Client_Request::where([
            'client_id' => $user->id,
            'coach_id' => $coach->id,
            'package_id' => $package->id,
            'status' => 'pending',
        ])->first();


        if($client_request){
            return response()->json ([ 'success' => false ,  'error' => 'You have already requested for this package']); 
        }

        if(!$user){
            return response()->json ([ 'success' => false ,  'error' => 'User Not Found']); 
        }

        if(!$coach){
            return response()->json ([ 'success' => false ,  'error' => 'Coach Not Found']); 
        }

        if(!$package){
            return response()->json ([ 'success' => false ,  'error' => 'Package Not Found']); 
        }

        if($user->subscribers->find($package->id)){
            return response()->json ([ 'success' => false ,  'error' => 'Already Subscribed']); 
        }

        if(!$coach->packages->find($package) ){
            return response()->json ([ 'success' => false ,  'error' => 'Package Not Assigned To Coach']); 
        }

        // dd($user->subscribers->find($package->id));

        if( $user && $coach && $package && $user->role_id == 5 && !($user->subscribers->find($package->id)) && $coach->packages->find($package) ){


            // dd('passed checks');
                $client_request = Client_Request::create([
                    'client_id' => $user->id,
                    'coach_id' => $coach->id,
                    'package_id' => $package->id,
                ]);

                if($client_request){
                    $notification = Notification::create([
                        'user_id' => $coach->user_id,
                        'title' => 'You have a new package request',
                        'link' => '/manage-clients',
                    ]);
                }
        

                if($user->fcm_token){
                    $notification = new stdClass();
                    $notification->text = "You have requested for the package: ". $package->name . " to the coach " . $coach->user->name;
                    
                    $notification->description = "You have requested for the package ". $package->name . " to the coach " . $coach->user->name;

                    $notification->notif_type = "notif-subscribe";

                    $this->PublishBroadcast($user , $coach->user , $notification , $coach->user->name , $notification->text , " ");
                }
                if($coach->user->fcm_token){
                    $coach_notification = new stdClass();
                    $coach_notification->text = "A new client: " .  $user->name  . " request is pending for the package: ". $package->name;
                    
                    $coach_notification->description = "A new client: " .  $user->name  . " request is pending for the package: ". $package->name;

                    $coach_notification->notif_type = "notif-subscribe";

                    $this->PublishBroadcast($coach->user , $user , $coach_notification , $user->name , $coach_notification->text , " ");
                }


                $users = User::where('id' , $coach->user->id)->get();

                $subject = 'A New Client Request is Pending';
                $body = 'Dear Coach <br>Your package: <b>' .$package->name. '</b> has a new subscription request by the client: <b>'. $user->name . '</b>' ;

                dispatch(new SendEmail($users , $subject , $body ));

            
                return response()->json( ['success' => true ,  'message' => 'Request Submitted' ]);
        }else{
            return response()->json (['success' => false ,  'error' => 'Some Problem Occurred']); 
        }    
    }


    public function handleClientRequest($user_id ,  $coach_id ,  $package_id , Request $request){
        
   
        $user = User::find($user_id);
        $coach = Coach::find($coach_id);
        $package = Package::find($package_id);

        if(!$user){
            return response()->json ([ 'success' => false ,  'error' => 'User Not Found']); 
        }

        if(!$coach){
            return response()->json ([ 'success' => false ,  'error' => 'Coach Not Found']); 
        }

        if(!$package){
            return response()->json ([ 'success' => false ,  'error' => 'Package Not Found']); 
        }

        if($user->subscribers->find($package->id) && $request->status == 'cancelled'){

            $client_request = Client_Request::where( 'client_id' , $user->id)->
            where( 'coach_id', $coach->id)->
            where( 'package_id' , $package->id)->
            where( 'status' , 'pending')->first();

            $client_request->status = $request->status;
            $client_request->responded_at = Carbon::now();
            $client_request->save();

            return response()->json ([ 'success' => true ,  'error' => 'Request Canvelled']); 
        }


        if($user->subscribers->find($package->id)){
            return response()->json ([ 'success' => false ,  'error' => 'Already Subscribed']); 
        }


        if(!$coach->packages->find($package) ){
            return response()->json ([ 'success' => false ,  'error' => 'Package Not Assigned To Coach']); 
        }

        // dd($user->subscribers->find($package->id));

        // dd($request->all());
        if( $user && $coach && $package && $user->role_id == 5 && !($user->subscribers->find($package->id)) && $coach->packages->find($package) ){


                if($request->status == 'accepted' || $request->status == 'cancelled'){
                    $client_request = Client_Request::where( 'client_id' , $user->id)->
                                where( 'coach_id', $coach->id)->
                                where( 'package_id' , $package->id)->
                                where( 'status' , 'pending')->first();


                    if($client_request){
                        $client_request->status = $request->status;
                        $client_request->responded_at = Carbon::now();
                        $client_request->save();
                    }else{
                        
                        return response()->json( ['success' => false ,  'message' => 'No Client Request found for this package and coach']);

                    }
    

                if($client_request && $client_request->status == 'accepted'){

                $user->subscribe()->attach([$package_id] , [ 'coach_id' => $coach_id , 'created_at' => Carbon::now() ]);

                
                $plan = Client_Plan::create([
                    'client_id' => $user_id,
                    'coach_id' => $coach_id,
                    'package_id' => $package_id,
                    'duration' => $package->duration,
                    'price' => $package->price ?? 0,
                ]);

                if($user->fcm_token){

                    $notification = new stdClass();
                    
                    $notification->text = "Request Accepted";
                    $notification->description = "Your request to join the plan ". $package->name ." has been accepted by ".$coach->user->name;

                    $notification->notif_type = "notif-subscribe";

                    $this->PublishBroadcast($user , $coach->user , $notification , $coach->user->name , $notification->text , " ");
                }

                // if($coach->user->fcm_token){
                //     $coach_notification = new stdClass();
                //     $coach_notification->text = "A new client: " .  $user->name  . " has subscibed to the plan ". $package->name;
                    
                //     $coach_notification->description = "A new client: " .  $user->name  . " has subscibed to the package: ". $package->name;

                //     $coach_notification->notif_type = "notif-subscribe";

                //     $this->PublishBroadcast($coach->user , $user , $coach_notification , $user->name , $coach_notification->text , " ");
                // }

                

                // user / client notification
                $users = User::where('id' , $user_id)->get();
                $subject = 'FTBLE Package Subscription';
                $body = 'Hi '.$user->name.' 
                <br>Thank you for subscribing to “'.$package->name.'”. “'.$coach->user->name.'” is preparing a program that meets your goals and you will be notified when its ready to start the fun!
                <br>
                Stay Fit,
                <br>';
                
                dispatch(new SendEmail($users , $subject , $body ));                
                


                // Coach Notification
                // $users = User::where('id' , $coach->user->id)->get();

                // $subject = 'FTBLE Package Subscription';
                // // $body = 'Dear Coach <br>Your package: <b>' .$package->name. '</b> have been subscribed by the client: <b>'. $user->name . '</b>' ;
                // $body = 'Hi “'.$coach->user->name.'”,
                // <br>
                // “'.$user->name.'” Had subscribed to “'.$package->name.'”. Please click on
                // Start Plan after preparing the fitness program. Your client will be notified when you start the plan.
                // <br>' ;

                // dispatch(new SendEmail($users , $subject , $body ));

                

                // admin email notification
                $admins = User::whereIn('role_id' , [1 , 2] )->get();

                $subject = 'New package subscription';

                $body = 'Hi Admin,
                <br>
                “'.$user->name.'” subscribed to “'.$package->name.'” with the '.$coach->user->role->name.' “'.$coach->user->name.'”.' ;

                dispatch(new SendEmail($admins , $subject , $body ));

                return response()->json( ['success' => true ,  'message' => 'Package Subscribed' , 'plan' => $plan ]);

            }elseif($client_request && $client_request->status == 'cancelled'){
                
                
                if($user->fcm_token){
                    $notification = new stdClass();

                    $notification->text = "Request Cancelled";
                    $notification->description = "Your request to join the plan ". $package->name ." has been cancelled by ".$coach->user->name;

                    $notification->notif_type = "notif-subscribe";

                    $this->PublishBroadcast($user , $coach->user , $notification , $coach->user->name , $notification->text , " ");
                }


                // if($coach->user->fcm_token){
                //     $coach_notification = new stdClass();
                //     $coach_notification->text = "Request of the client: " .  $user->name  . " to the package: ". $package->name . " has been cancelled";
                    
                //     $coach_notification->description = "Request of the client: " .  $user->name  . " to the package: ". $package->name . " has been cancelled";

                //     $coach_notification->notif_type = "notif-subscribe";

                //     $this->PublishBroadcast($coach->user , $user , $coach_notification , $user->name , $coach_notification->text , " ");
                // }

                


                $users = User::where('id' , $user_id)->get();
                $subject = 'FTBLE Package Subscription Response';
                $body = 'Hi '.$user->name.' 
                <br>Your request to the package “'.$package->name.'” is cancelled by “'.$coach->user->name.'”.<br>Please contact your coach for further details!
                <br>
                Stay Fit,
                <br>';
                
                dispatch(new SendEmail($users , $subject , $body ));                

            }
        }
        return response()->json (['success' => true ,  'message' => 'Request Cancelled']); 

        }else{
            return response()->json (['success' => false ,  'error' => 'Some Problem Occurred']); 
        }    
    }



    public function checkSubscription($user_id ,  $coach_id ,  $package_id ){
        
   
        $user = User::find($user_id);
        $coach = Coach::find($coach_id);
        $package = Package::find($package_id);

        if(!$user){
            return response()->json ([ 'success' => false ,  'error' => 'User Not Found']); 
        }

        if(!$coach){
            return response()->json ([ 'success' => false ,  'error' => 'Coach Not Found']); 
        }

        if(!$package){
            return response()->json ([ 'success' => false ,  'error' => 'Package Not Found']); 
        }

        if($user->subscribers->find($package->id)){
            return response()->json ([ 'success' => true , 'error' => 'Already Subscribed']); 
        }else{
            return response()->json ([ 'success' => false , 'error' => 'Package not Subscribed']); 
        }

    }
   
    public function unsubscribe($user_id ,  $coach_id ,  $package_id ){
        
        $subscription = Client_Pivot::where('user_id' , $user_id)->where('coach_id' , $coach_id ) -> Where('package_id' , $package_id )->first();
        
        $plan = Client_Plan::where('client_id' , $user_id)->where('coach_id' , $coach_id ) -> Where('package_id' , $package_id )->first();
        
        if($subscription){

         

            $subscription->delete();
            $plan->delete();
            
            return response()->json( ['success' => true ,  'message' => 'Package unubscribed' ]);
        }else{
            return response()->json ([ 'success' => false ,  'error' => 'Subscription Not Found']); 
        }    



        
    }
}
