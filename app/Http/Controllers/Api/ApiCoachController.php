<?php

namespace App\Http\Controllers\Api;

use App\Trainer;
use App\Dietitian;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiCoachController extends Controller
{

    

    public function showTrainers(){

        $trainers = Trainer::with('user')->get();

        if($trainers){
            return response()->json( ['success' => true ,  'trainers' => $trainers]); 
        }else{
            return response()->json (['success' => false ,  'error' => 'Some Problem Occoured']); 
        }

    }

    

    public function viewTrainer($id){

        $trainer = Trainer::with('user')->find($id);

        if($trainer){
            return response()->json( ['success' => true ,  'trainer' => $trainer]); 
        }else{
            return response()->json (['success' => false ,  'error' => 'Trainer Not Found']); 
        }

    }



    public function showDietitians(){

        $dietitians = Dietitian::with('user')->get();

        if($dietitians){
            return response()->json( ['success' => true ,  'dietitians' => $dietitians]); 
        }else{
            return response()->json (['success' => false ,  'error' => 'Some Problem Occoured']); 
        }

    }

    

    public function viewDietitian($id){

        $dietitian = Dietitian::with('user')->find($id);

        if($dietitian){
            return response()->json( ['success' => true ,  'dietitian' => $dietitian]); 
        }else{
            return response()->json (['success' => false ,  'error' => 'Dietitian Not Found']); 
        }

    }


}
