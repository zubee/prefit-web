<?php

namespace App\Http\Controllers\Api;

use FCM;
use Push;
use App\Chat;
use App\User;
use App\Client_Plan;

use App\Client_Pivot;
use App\Notification;
use App\Jobs\SendEmail;
use App\Traits\FirebaseFCM;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use LaravelFCM\Message\Topics;

use App\Events\ChatMessageEvent;
use App\Http\Controllers\Controller;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;


class ChatController extends Controller
{
    use FirebaseFCM;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( $sender_id , $reciever_id)
    {
        // dd($sender_id , $reciever_id);
        $chats = Chat::with('questionnaire') -> whereIn('sender_id' , [$sender_id , $reciever_id] )->whereIn('reciever_id' , [$sender_id , $reciever_id])->orderBy('created_at' , 'asc')->get();

        foreach($chats as $chat){
            if($chat->reciever_id == $sender_id){
                $chat->read_status = 1;
                $chat->save();
            }
        }

        if($chats){
            return response()->json( [ 'success' => true , 'chats' => $chats ]) ;
        }else{
        return response()->json( [ 'success' => false , 'message' => 'Some problem occoured' ]) ;
        }

        // return $chats;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    //   $this->fcm_test();

        // dd($request->all());

        
        $message = Chat::create([
            'sender_id' => $request->sender,
            // 'reciever_id' => 1,
            'reciever_id' => $request->reciever,
            'message' => $request->message,
            'file' => 0,
        ]);
        // event(new ChatMessageEvent($message));
        $user = User::find($request->reciever);
       
        $sender = User::findOrFail($request->sender);
        $reciever = User::findOrFail($request->reciever);



        $support = User::supportUsers()->first();
        
        if(in_array($reciever->role_id , [3,4,6])){
            $message->checkIfNew();
        }
        if($support && $request->sender == $support->id){
            $message['notif_type'] = "notif-support";
        }else{
            $message['notif_type'] = "notif-chat";
        }
        
        $is_client = '';
            if(($reciever->role_id == 3 || $reciever->role_id == 4) && $sender->role_id == 5) {
                $is_client = Client_Pivot::where('user_id' , $sender->id )->first();
                if(!$is_client){
                    $message['notif_type'] = "notif-chat-req";
                }
            }

        if($reciever->fcm_token){
            $this->PublishBroadcast($reciever , $sender , $message , $sender->name , $request->message , "");
        }
        
        if($sender->role_id == 5){
            if(in_array( $reciever->role_id , [3,4] )){
                $reciever->chatable_clients()->syncWithoutDetaching($request->sender);
            }
        }

        if($message){
            $notification = Notification::create([
                'chat_id' => $message->id,
                'user_id' => $request->reciever,
                'title' => 'You have a new message from: '.$sender->name,
                'link' => '/messages',
                'sender_id' => $request->sender,
            ]);
        }

        if($message){
            return response()->json( [ 'success' => true , 'message' => $message ]) ;
        }else{
        return response()->json( [ 'success' => false , 'message' => 'Some problem occoured' ]) ;
        }

}

    public function storeFile(Request $request)
    {
        if($request->file){
            
        $file = $request->file;
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();

        $path = hash( 'sha256', time()).'.'.$extension;

        
    //    dd($request->all());

        if(  $file->move( public_path().'/messages/files/' ,$path) ) {

            // $path = $path;

        $message = Chat::create([
            'sender_id' => $request->sender,
            'reciever_id' => $request->reciever,
            'realname' => $filename,
            'filename' => $path,
            'mime' => $file->getClientMimeType(),
            'file' => 1,

        ]);
        $sender = User::findOrFail($request->sender);
        $reciever = User::findOrFail($request->reciever);

    
        
        
        $support = User::supportUsers()->first();
        
        if($support && $request->reciever == $support->id){
            $admins = User::adminUsers()->get();
            $subject = 'You have a new query';
            $body = 'Dear User <br>You have a new query from: <b>'. $sender->name . '</b>' ;
            dispatch(new SendEmail($admins , $subject , $body ));
        }

        if($support && $request->sender == $support->id){
            $message['notif_type'] = "notif-support";
        }else{
            $message['notif_type'] = "notif-chat";
        }
        
        $is_client = '';
        if(($reciever->role_id == 3 || $reciever->role_id == 4) && $sender->role_id == 5) {
            $is_client = Client_Pivot::where('user_id' , $sender->id )->first();
            if(!$is_client){
                $message['notif_type'] = "notif-chat-req";
            }
        }  
      

        if($message){
        $notification = Notification::create([
            'chat_id' => $message->id,
            'user_id' => $request->reciever,
            'title' => 'You have a new message from: '.$sender->name,
            'link' => '/messages',
            'sender_id' => $request->sender,
        ]);
        
    }

    if($sender->role_id == 5){
        if(in_array( $reciever->role_id , [3,4]  )){
            $reciever->chatable_clients()->syncWithoutDetaching($request->sender);
        }
        }

        if($reciever->fcm_token){
            $this->PublishBroadcast($reciever , $sender , $message , $sender->name , "" , "chat");
        }
        

    // if($reciever->role_id == 4){
    //     if($sender->role_id != 4 ){
    //         $sender->users()->syncWithoutDetaching($request->reciever);
    //     }}

    // if($reciever->role_id == 3){
    //     if( in_array ($sender->role_id , [1,2,5] ) ){
    //         $reciever->company_contacts()->syncWithoutDetaching($sender->id);
    //     }}

            
        // event(new ChatMessageEvent($message));
        
        return response()->json( [ 'success' => true , 'message' => $message ]) ;

        // return [
        //     'success' => true,
        //     'message' => $message
        // ];

    }else{

        return response()->json( [ 'success' => false , 'message' => 'Some problem occoured' ]) ;

        // return [
        //     'success' => false
        // ];
    }
 
    }else{

        return response()->json( [ 'success' => false , 'message' => 'No file attached' ]) ;
    }
    }


    public function storeFileBase64(Request $request)
    {
        if($request->image_file){
            
            $ext = $request->extension;
            $mime = $request->mime;
            $path = hash( 'sha256', Str::random(6)).'.'.$ext;
            
            $file_data = $request->image_file;
        
            if ($file_data != "") { 

            $file = base64_decode($request->image_file);

            // $image_upload = \Image::make($request->image_file )->save(public_path().'/messages/files/' .$path );
            // dd($image_upload);

            $success = file_put_contents(public_path().'/messages/files/'.$path, $file);
            // $success = $file->move( public_path().'/messages/files/' ,$path);
            
            $message = Chat::create([
                'sender_id' => $request->sender,
                'reciever_id' => $request->reciever,
                'realname' => 'image',
                'filename' => $path,
                'mime' => $mime,
                'file' => 1,
                ]);
            $sender = User::findOrFail($request->sender);
            $reciever = User::findOrFail($request->reciever);
    
                        
            $support = User::supportUsers()->first();
        
            if($support && $request->reciever == $support->id){
                $admins = User::adminUsers()->get();
                $subject = 'You have a new query';
                $body = 'Dear User <br>You have a new query from: <b>'. $sender->name . '</b>' ;
                dispatch(new SendEmail($admins , $subject , $body ));
            }
    
            if($support && $request->sender == $support->id){
                $message['notif_type'] = "notif-support";
            }else{
                $message['notif_type'] = "notif-chat";
            }

            $is_client = '';
            if(($reciever->role_id == 3 || $reciever->role_id == 4) && $sender->role_id == 5) {
                $is_client = Client_Pivot::where('user_id' , $sender->id )->first();
                if(!$is_client){
                    $message['notif_type'] = "notif-chat-req";
                }
            }  
            
            if($message){
            $notification = Notification::create([
                'chat_id' => $message->id,
                'user_id' => $request->reciever,
                'title' => 'You have a new message from: '.$sender->name,
                'link' => '/messages',
                'sender_id' => $request->sender,
            ]);
    
            // $message['notif_type'] = "notif-chat";

        }

    if($sender->role_id == 5){
        if(in_array( $reciever->role_id , [3,4]  )){
            $reciever->chatable_clients()->syncWithoutDetaching($request->sender);
        }
        }

        if($reciever->fcm_token){
            $this->PublishBroadcast($reciever , $sender , $message , $sender->name , $request->message , "chat");
        }

    // if($reciever->role_id == 4){
    //     if($sender->role_id != 4 ){
    //         $sender->users()->syncWithoutDetaching($request->reciever);
    //     }}

    // if($reciever->role_id == 3){
    //     if( in_array ($sender->role_id , [1,2,5] ) ){
    //         $reciever->company_contacts()->syncWithoutDetaching($sender->id);
    //     }}

            
        // event(new ChatMessageEvent($message));
        
        return response()->json( [ 'success' => true , 'message' => $message ]) ;

        // return [
        //     'success' => true,
        //     'message' => $message
        // ];

    }else{

        return response()->json( [ 'success' => false , 'message' => 'Some problem occoured' ]) ;

        // return [
        //     'success' => false
        // ];
    }
 
    }else{

        return response()->json( [ 'success' => false , 'message' => 'No file attached' ]) ;
    }
    }

  
    public function getActiveCoachChat($id){

        $coach_user = User::find($id);
        // $users = $coach_user->chatable_clients;

        if($coach_user && $coach_user->coach){

        // $support = User::where('role_id' , 6)->first();
        // $users->prepend($support);

        $contact_ids = array_unique(Client_Plan::where('coach_id' , $coach_user->coach->id)->get()->pluck('client_id')->toArray());
        // array_push($contact_ids , $support->id );
        // array_unshift($contact_ids , $support->id );

        $contacts = User::whereIn('id' , $contact_ids )->get();
        // $contacts->prepend($support);
        
        
        // dd($users);
        $request_ids = array_unique(Chat::where('reciever_id' , $coach_user->id)->whereNotIn('sender_id' , $contact_ids)->where('sender_id', '!=' , $coach_user->id)->get()->pluck('sender_id')->toArray());
        
        $user_requests = User::where('role_id' , 5 )->whereIn('id' , $request_ids )->get();
        
        // dd($contacts , $user_requests);
        
        foreach($contacts as $user){
            // $user->contact_details();
            $user->getContactDetails($coach_user->id);
        }
        foreach($user_requests as $user){
            $user->getContactDetails($coach_user->id);
        }
        
        // foreach($users as $user){
            
            //     $chats = Chat::whereIn('sender_id' , [$user->id , $coach_user->id] )->whereIn('reciever_id' , [$user->id , $coach_user->id])->orderBy('created_at' , 'asc')->get();
            
            //     // $chats = $user->chat->where('reciever_id' , $coach_user->id)->where('read_status' , 0);
            //     $user->unread_count = $chats->count();
            
            //     if($chats->last()){
            //         // dump($chats->last()->proper_time);
            //         $user->last_message_time = $chats->last()->proper_time;
            //     }else{
            //         $user->last_message_time = false;
            
            //     }
            // }
            
            if($contacts ){
                return response()->json(['success' => true , 'user_requests' => $user_requests , 'contacts' => $contacts ]);
            }else{
                return response()->json( [ 'success' => false , 'message' => 'Some problem occoured' ]) ;
            }
        }else{
            return response()->json( [ 'success' => false , 'message' => 'User Not Found' ]) ;
        }
    }
    
    // public function getActiveCoachChat($id){

    //     $coach_user = User::find($id);
    //     $users = $coach_user->chatable_clients;

    //     $support = User::where('role_id' , 6)->first();
    //     $users->prepend($support);


    //     foreach($users as $user){
            
    //         $chats = Chat::whereIn('sender_id' , [$user->id , $coach_user->id] )->whereIn('reciever_id' , [$user->id , $coach_user->id])->orderBy('created_at' , 'asc')->get();

    //         // $chats = $user->chat->where('reciever_id' , $coach_user->id)->where('read_status' , 0);
    //         $user->unread_count = $chats->count();

    //         if($chats->last()){
    //             // dump($chats->last()->proper_time);
    //             $user->last_message_time = $chats->last()->proper_time;
    //         }else{
    //             $user->last_message_time = false;

    //         }
    //     }
        
    //     if($users ){
    //         return response()->json(['success' => true , 'contacts' => $users ]);
    //     }else{
    //         return response()->json( [ 'success' => false , 'message' => 'Some problem occoured' ]) ;
    //     }
    // }



    
    public function getContacts(){

        $support = User::where('role_id' , 6)->first();
        $users = User::whereIn('role_id' , [ 3 , 4 ] )->get();
        $users->prepend($support);


        // foreach($users as $user){
        //     $user->unread_count = $user->chat->where('reciever_id' , auth()->user()->id)->where('read_status' , 0)->count();
        // }
        
        if($users ){
            return response()->json(['success' => true , 'contacts' => $users ]);
        }else{
            return response()->json( [ 'success' => false , 'message' => 'Some problem occoured' ]) ;
        }
    }
    

    
    public function downloadChatFile($id){
        $message = Chat::find($id);
        if($message->file == 1){
            if(file_exists(public_path() . "/messages/files/".$message->filename)){
                return response()->download(public_path() . "/messages/files/".$message->filename);
            }else{
                return response()->json( [ 'success' => false , 'message' => 'File Not Found' ]) ;
            }
        }else{
            return response()->json( [ 'success' => false , 'message' => 'File Not Found' ]) ;

        }
    }

    public function saveToken(Request $request){
        $user = User::findOrFail($request->id);
        $user->fcm_token = $request->token;
        $user->save();

        return $user;

    }

    public function searchUsers($name){
        
        $users = User::with('profile')->where('role_id' , 4 )->where('status' , 1 )->get();
        $matching = collect();

        foreach($users as $user){
            if(preg_match("/{$name}/i", $user->name) || preg_match("/{$name}/i", $user->profile->full_name)){
                $matching->push($user);
            }
        }

        if($users){
            return response()->json( [ 'success' => true , 'users' => $matching ]) ;
        }else{
            return response()->json( [ 'success' => false , 'message' => 'File Not Found' ]) ;

        }
    }

    // public function notifyUser($user , $message){
    //     // dd($user);
    //     $optionBuilder = new OptionsBuilder();
    //     $optionBuilder->setTimeToLive(60*20);

    //     $notificationBuilder = new PayloadNotificationBuilder('You have A new message : ' .$user->name);
    //     $notificationBuilder->setBody('New Message broadcast')
    //                         ->setSound('default');
    //                         // ->setClickAction('http://localhost:8000/myaccount/messages/');

    //     $dataBuilder = new PayloadDataBuilder();
    //     $dataBuilder->addData([
    //         'sender_name' => $user->name,
    //         'message' => $message,
    //         ]);

    //     $option = $optionBuilder->build();
    //     $notification = $notificationBuilder->build();
    //     $data = $dataBuilder->build();

    //     $token = $user->fcm_token;

    //     $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
        
    //     return $downstreamResponse->numberSuccess();

    //     // \LaravelFCM\Message\PayloadNotificationBuilder LaravelFCM\Message\PayloadNotificationBuilder::setClickAction(String $action)

    // }




    public function getActiveChat($id){
        
        $user = User::find($id);
        $support = User::where('role_id' , 6)->first();


        // $user_requests = $user_requests->sortBy('last_message.created_at');
        // $user_requests = $user_requests->reverse()->values();

        if($user->role_id == 3 || $user->role_id == 4){

            $contact_ids = array_unique(Client_Plan::where('coach_id' , $user->coach->id)->get()->pluck('client_id')->toArray());
    
            $contacts = User::whereIn('id' , $contact_ids )->get();
            // $contacts->prepend($support);
                        
            // dd($users);
            $request_ids = array_unique(Chat::where('reciever_id' , $user->id)->whereNotIn('sender_id' , $contact_ids)->where('sender_id', '!=' , $user->id)->get()->pluck('sender_id')->toArray());
            
            $user_requests = User::where('role_id' , 5 )->whereIn('id' , $request_ids )->get();
                        
            foreach($contacts as $contact){
                // $user->contact_details();
                // $user->unread_count = $user->chat->where('reciever_id' , $user->id)->where('read_status' , 0)->count();
                $contact->getContactDetails($user->id);

                unset($contact->chat);
            }
            foreach($user_requests as $contact){
                // $user->unread_count = $user->chat->where('reciever_id' , $user->id)->where('read_status' , 0)->count();
                $contact->getContactDetails($user->id);

                unset($contact->chat);
            }

            $user_requests = $user_requests->sortBy('last_message.created_at');
            $user_requests = $user_requests->reverse()->values();

            $contacts = $contacts->sortBy('last_message.created_at');
            $contacts = $contacts->reverse()->values();

    
            if($contacts && $user_requests ){
                return response()->json( [ 'success' => true , 'contacts' => $contacts , 'user_requests' => $user_requests ]) ;
    
            }
            
        }elseif($user->role_id == 5){
            // $contacts = User::whereIn('role_id' , [3,4])->get();
            
            $all_chats = Chat::where('sender_id' , $user->id )->orWhere('reciever_id' , $user->id )->get();
            
            $sent = $all_chats->pluck('sender_id')->toArray();
            $recieved = $all_chats->pluck('reciever_id')->toArray();
            $contact_ids = array_unique(array_merge($sent,$recieved ));
            // $all_chats = $all_chats->where();
            // $all_contact_ids = array_unique($user->chat->pluck('reciever_id')->toArray());

            $contacts = User::whereIn('id' , $contact_ids )->whereIn('role_id' , [3,4])->get();
            
            $contacts = $contacts->sortBy('last_message.created_at');
            $contacts = $contacts->reverse()->values();
            // dd($contacts );
            // $contacts->prepend($support);

            // dd($contacts);

            foreach($contacts as $contact){
                $contact->getContactDetails($user->id);

                // $chats = Chat::whereIn('sender_id' , [$user->id , $contact->id] )->whereIn('reciever_id' , [$user->id , $contact->id])->orderBy('created_at' , 'asc')->get();

                // $contact->unread_count = $chats->where('read_status' , 0)->where('sender_id' ,$contact->id )->count();
                // if($chats->last()){
                //     // dump($chats->last()->proper_time);
                //     $contact->last_message_time = $chats->last()->proper_time;
                // }else{
                //     $contact->last_message_time = false;
                // }
            }
        
        if($user){
            return response()->json( [ 'success' => true , 'contacts' => $contacts ]) ;
        }
        }else{
            return response()->json( [ 'success' => false , 'message' => 'User not found' ]) ;
        }
    }



    // public function getActiveChat($id){
        
    //     $user = User::find($id);

    //     if($user->role_id == 3 || $user->role_id == 4){
    //         $contacts = $user->chatable_clients;

            
    //     }else{
    //         $contacts = User::whereIn('role_id' , [3,4])->get();
    //     }
        
    //     $support = User::where('role_id' , 6)->first();
    //     $contacts->prepend($support);

    //     // dd($contacts);

    //     foreach($contacts as $contact){

    //         $chats = Chat::whereIn('sender_id' , [$user->id , $contact->id] )->whereIn('reciever_id' , [$user->id , $contact->id])->orderBy('created_at' , 'asc')->get();

    //         $contact->unread_count = $chats->where('read_status' , 0)->where('sender_id' ,$contact->id )->count();
    //         if($chats->last()){
    //             // dump($chats->last()->proper_time);
    //             $contact->last_message_time = $chats->last()->proper_time;
    //         }else{
    //             $contact->last_message_time = false;
    //         }
    //     }
        
    //     if($user){
    //         return response()->json( [ 'success' => true , 'contacts' => $contacts ]) ;
    //     }else{
    //         return response()->json( [ 'success' => false , 'message' => 'User not found' ]) ;
    //     }
    // }





    public function getUnreadMessages($id){
        // dd($id);
        $coach_user = User::find($id);
        $support = User::where('role_id' , 6)->first();
        // $users->prepend($support);
        $support->getContactDetails($coach_user->id);

        $contact_ids = array_unique(Client_Plan::where('coach_id' , $coach_user->coach->id)->get()->pluck('client_id')->toArray());
        // array_push($contact_ids , $support->id );
        // array_unshift($contact_ids , $support->id );

        $contacts = User::whereIn('id' , $contact_ids )->get();
        // $contacts->prepend($support);
        
        
        // dd($users);
        $request_ids = array_unique(Chat::where('reciever_id' , $coach_user->id)->whereNotIn('sender_id' , $contact_ids)->where('sender_id', '!=' , $coach_user->id)->get()->pluck('sender_id')->toArray());
        
        $user_requests = User::where('role_id' , 5 )->whereIn('id' , $request_ids )->get();
        
        // dd($contacts , $user_requests);
        
        foreach($contacts as $user){
            // $user->contact_details();
            // $user->unread_count = $user->chat->where('reciever_id' , $coach_user->id)->where('read_status' , 0)->count();
            $user->getContactDetails($coach_user->id);

        }
        foreach($user_requests as $user){
            // $user->unread_count = $user->chat->where('reciever_id' , $coach_user->id)->where('read_status' , 0)->count();
            $user->getContactDetails($coach_user->id);

        }

        $user_requests = $user_requests->sortBy('last_message.created_at');
        $user_requests = $user_requests->reverse()->values();

        $contacts = $contacts->sortBy('last_message.created_at');
        $contacts = $contacts->reverse()->values();

        $contacts->prepend($support);

        if($contacts && $user_requests ){
            return response()->json( [ 'success' => true , 'users' => $contacts , 'user_requests' => $user_requests ]) ;

        }else{
            return response()->json( [ 'success' => false , 'message' => 'User not found' ]) ;

        }

    }
    
    // public function getUnreadMessages($id){
    //     // dd($id);
    //     $support = User::where('role_id' , 6)->first();
    //     $curent_user = User::find($id);
    //     $users = $curent_user->chatable_clients;
    //     $users->prepend($support);


    //     foreach($users as $user){
    //         $user->unread_count = $user->chat->where('reciever_id' , $curent_user->id)->where('read_status' , 0)->count();
    //     }
    //     if($users){
    //         return response()->json( [ 'success' => true , 'users' => $users ]) ;

    //     }else{
    //         return response()->json( [ 'success' => false , 'message' => 'User not found' ]) ;

    //     }

    // }
    
    
    public function getUnreadMessagesAdmin(){
        // dd($id);
        $support = User::where('role_id' , 6)->first();

        $users = User::whereNotIn('role_id' , [1 , 2 , 6] )->get();

        // foreach($users as $user){
        //     $user->unread_count = $user->chat->where('reciever_id' , $support->id)->where('read_status' , 0)->count();
        // }

        foreach($users as $user){
            $user->getContactDetails($support->id);
            // $user->unread_count = $user->chat->where('reciever_id' , $support->id)->where('read_status' , 0)->count();
        }
        $users = $users->sortBy('last_message.created_at');
        $users = $users->reverse()->values();

        // $users = $users->sortBy('last_unread_message.created_at');
        // $users = $users->reverse()->values();
        // dd($users);
        if($users){
            return response()->json( [ 'success' => true , 'users' => $users ]) ;

        }else{
            return response()->json( [ 'success' => false , 'message' => 'User not found' ]) ;

        }

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
