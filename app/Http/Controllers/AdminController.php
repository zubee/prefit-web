<?php

namespace App\Http\Controllers;

use App\User;
use stdClass;
use App\Perfit_Image;
use App\Traits\FirebaseFCM;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class AdminController extends Controller
{
    use FirebaseFCM;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('role_id' , 2)->get();
        return view('admin.admins.index' , compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admins.create');
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'username' => 'required|unique:users,username',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'conf_password' => 'required',
        ]);

        // dd($request->all());

        if($request->password != $request->conf_password){
            return redirect()->back()->with('error' , 'Password does not match');

        }

        $user = User::create([
            'role_id' => 2,
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'password' => \Hash::make($request->password),
        ]);
        if($user){

            // if($request->has('image')){
            //     $ext = $request->image->getClientOriginalExtension();   
            //     $request->image->move(public_path().'/images/profile_images/' , $user->id .'.'. $ext);
            //     $user->photo = $user->id .'.'. $ext;
                
            //     $user->save();
            // }

            if($request->has('image')){
                
                $file = Perfit_Image::imageResize($request->image);
                $ext = $request->image->getClientOriginalExtension();
                                
                $file->save(public_path().'/images/profile_images/' . $user->id .'.'. $ext , 90);
                $user->photo = $user->id .'.'. $ext;
                $user->save();
            }


        }


        if($user){
            return redirect('/admin/admins')->with('success' , 'Admin created Successfully');
        }else{
            return redirect()->back()->with('error' , 'Some Problem occourd');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('admin.admins.edit' , compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            // 'username' => 'required|unique:users,username',
            'email' => 'required|email| unique:users,email,'.$id ,
        ]);

        if(isset($request->password) && $request->password != $request->conf_passowrd ){
            return redirect()->back()->with('error' , 'Password does not match');
        }

        // dd($request->all());

        $user = User::find($id);
        $user->name = $request->name;
        // $user->username = $request->username;
        $user->email = $request->email;
        $user->password = isset($request->password) ? \Hash::make($request->password) : $user->password;


        if($request->has('image')){
            $path = public_path().'/images/profile_images/'.$user->photo;

            if(file_exists($path)){
                    File::delete($path);
                }
                // $ext = $request->image->getClientOriginalExtension();
        
                // $request->image->move(public_path().'/images/profile_images/' , $user->id .'.'. $ext);
                // $user->photo = $user->id .'.'. $ext;
                
                // $user->save();

            }
                
            $file = Perfit_Image::imageResize($request->image);
            $ext = $request->image->getClientOriginalExtension();
                            
            $file->save(public_path().'/images/profile_images/' . $user->id .'.'. $ext , 90);
            $user->photo = $user->id .'.'. $ext;
            $user->save();

            
        $user->save();

            
        if($user->save()){
            return redirect('/admin/admins')->with('success' , 'User updated Successfully');
        }else{
            return redirect()->back()->with('error' , 'Some Problem occourd');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::FindOrFail($id);
        // dd($user);
        
        if($user->delete())
        return redirect()->back()->with('success' , 'User Deleted successfully');
        else
        return redirect()->back()->with('error' , 'Some problem occured');
    }


    public function changeStatus($id)
    {
        $user = User::FindOrFail($id);
        // dd($user);
        $user->status = !$user->status;
        $user->save();
        
        return $user->status;
    }



    public function notific(){

        $users = User::all();
        foreach($users as $user){
            if($user->fcm_token){
                $notification = new stdClass();
                
                $notification->text = "Testing Notification";
                $notification->description = "Testing Notification";

                $notification->notif_type = "notif-subscribe";
                $this->PublishBroadcast($user , $user , $notification , $user->name , $notification->text);
            }
        }
    }




}
