<?php

namespace App\Http\Controllers;

use App\Exports\UserExport;
use App\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('role_id' , 5)->paginate(50);
        return view('admin.users.index' , compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
            'conf_password' => 'required',
        ]);

        // dd($request->all());

        if($request->password != $request->conf_password){
            return redirect()->back()->with('error' , 'Password does not match');

        }

        $user = User::create([
            'role_id' => 5,
            'name' => $request->name,
            'email' => $request->email,
            'password' => \Hash::make($request->password),
        ]);

        if($user){
            return redirect('/admin/users')->with('success' , 'User created Successfully');
        }else{
            return redirect()->back()->with('error' , 'Some Problem occourd');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('admin.users.view' , compact('user'));
        // dd($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('admin.users.edit' , compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email| unique:users,email,'.$id ,
        ]);

        if(isset($request->password) && $request->password != $request->conf_passowrd ){
            return redirect()->back()->with('error' , 'Password does not match');
        }

        // dd($request->all());

        $user = User::find($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = isset($request->password) ? \Hash::make($request->password) : $user->password;

        $user->save();

            
        if($user->save()){
            return redirect('/admin/users')->with('success' , 'User updated Successfully');
        }else{
            return redirect()->back()->with('error' , 'Some Problem occourd');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::FindOrFail($id);
        $user->notifyCoaches();
        // dd($user);
        
        if($user->delete())
            return redirect()->back()->with('success' , 'User Deleted successfully');
        else
            return redirect()->back()->with('error' , 'Some problem occured');
        }


    public function changeStatus($id)
    {
        $user = User::FindOrFail($id);
        // dd($user);
        $user->status = !$user->status;
        $user->save();
        
        return $user->status;
    }

    public function verifyUserName(Request $request){

        $data = $request->get('username');

        $users = User::where('username' , $data )->get();
        if($users->count() > 0){
            return 1;
        }else{
            return 0;
        }
    }
    public function verifyEmail(Request $request){

        $data = $request->get('email');

        $users = User::where('email' , $data)->get();

        if($users->count() > 0){
            return 1;
        }else{
            return 0;
        }
    }

    public function userExport(){
        return Excel::download(new UserExport, 'users.xlsx');
    }



}
