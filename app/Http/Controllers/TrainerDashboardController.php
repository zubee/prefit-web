<?php

namespace App\Http\Controllers;

use App\Tag;
use App\User;
use App\Package;
use App\Questionnaire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class TrainerDashboardController extends Controller
{
    public function index(){

        // dd(auth()->user());
        return view('trainer.index');
    }


    public function showProfile(){
        return view('trainer.profile.index');
    }
    
    public function editProfile(){
        $tags = Tag::all();
        return view('trainer.profile.edit' , compact('tags'));
    }

    public function updateProfile($id , Request $request){
        
        // dd($request->all());

        $request->validate([
            'name' => 'required',
            'username' => 'sometimes',
            'email' => 'required|email',
            'password' => 'sometimes ',
            'percentage' => 'numeric|min:0|max:100',

        ]);
            // dd($request->all());

        if( isset($request->password) && $request->password != $request->conf_password ){
            return redirect()->back()->with('error' , 'Password does not match');
        }

        $user = User::findOrFail(auth()->user()->id);
            $user->name = $request->name;
            $user->username = $request->get('username');
            $user->email = $request->email;
            if($request->password){
                $user->password = \Hash::make($request->password);
            }
            $user->save();

            $user->tags()->sync($request->tags);
            
            $trainer = auth()->user()->trainer;
            
            $trainer->user_id = $user->id;
            $trainer->phone = $request->phone;
            $trainer->portfolio = $request->portfolio;
            $trainer->percentage = $request->percentage;
            // $dietitian->tags = $request->tags;

            
            if($request->has('image')){
                $path = public_path().'/images/trainers/'.$trainer->photo;

                if(file_exists($path)){
                        File::delete($path);
                    }
                $ext = $request->image->getClientOriginalExtension();
                $request->image->move(public_path().'/images/trainers/' , $trainer->id .'.'. $ext);

                $trainer->photo = $trainer->id .'.'. $ext;
    
            }

            $trainer->save();


        if($user->save())
            return redirect('/trainer/profile')->with('success' , 'Profile updated successfully');
        else
            return redirect()->back()->with('error' , 'Some problem occured');


    }


    public function login(){
        return view('auth.trainer_login');
        
    }
    
    public function pt_login(){
        return view('auth.pt_login');
        
    }

    public function showPackages(){
        // dd(auth()->user()->packages);
        return view('trainer.packages.index');

    }

    public function viewPackage($id){
        $package = Package::findOrFail($id);
        // dd(auth()->user()->packages);
        if( in_array($package->id , auth()->user()->packages->pluck('id')->toArray()) ){
            return view('trainer.packages.show' , compact('package'));

        }else{
            return redirect()->back()->with('error' , 'That package does not belongs to you' );
        }

    }

    
    public function showQuestionnaires(){
        return view('trainer.questionnaires.index');

    }
    public function showQuestionnaire($id){
        $questionnaire = Questionnaire::findOrFail($id);

        if( in_array($questionnaire->id , auth()->user()->questionnaires->pluck('id')->toArray()) ){
            return view('trainer.questionnaires.show' , compact('questionnaire'));

        }else{
            return redirect()->back()->with('error' , 'That questionnaire does not belongs to you' );
        }

    }
    
}
