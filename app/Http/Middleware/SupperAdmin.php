<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SupperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

            if (auth()->user()->role_id == 1) {
                return $next($request);
            }


        return redirect()->route('login')->with('error',"You Don't Have Admin Access");
    }
}
