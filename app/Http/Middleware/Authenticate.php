<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        // if (! $request->expectsJson()) {
        //     return route('login');
        // }

        if (! $request->expectsJson()) {

            if(strpos(url()->current() , '/admin')){
                return route('login');
        
                // }elseif(\Request::is('company')){
                }elseif(strpos(url()->current() , '/coach')){
                        return route('coach.login');
        
                }elseif(strpos(url()->current() , '/dietitian')){
                        return route('dietitian.login');
        
                }else{
                    return redirect('/');
                }
        
            // return route('login');
        }
        
    }
}
