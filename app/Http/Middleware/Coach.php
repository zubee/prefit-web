<?php

namespace App\Http\Middleware;

use Closure;

class Coach
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->role_id == 4 || auth()->user()->role_id == 3){
            return $next($request);
        }else{
            return redirect('/')->with('error',"You Are not Authorized");
        }
        // return $next($request);
    }
}
