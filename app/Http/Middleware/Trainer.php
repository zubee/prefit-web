<?php

namespace App\Http\Middleware;

use Closure;

class Trainer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(auth()->user()->role_id == 4){
            return $next($request);
        }else{
            return redirect()->route('login')->with('error',"You Are not Authorized");
        }
        // return $next($request);
    }
}
