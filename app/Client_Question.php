<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client_Question extends Model
{
        protected $guarded = ['id'];
        protected $table = 'client_questionnaire_questions';

        public function questionnaire(){
                return $this->belongsTo(Client_Questionnaire::class , 'questionnaire_id');
        }

        public function choices(){
                return $this->hasMany(Client_Question_Choice::class , 'question_id');
        }
        public function answers(){
                return $this->hasMany(Client_Answer::class , 'question_id' );
        }

        public function answer($user_id){
                return $this->hasMany(Client_Answer::class , 'question_id')->where('client_id' , $user_id)->first();
        }
}
