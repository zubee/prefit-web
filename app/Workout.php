<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workout extends Model
{
    //
    protected $fillable = ['name','description','tags','media'];
    // protected $casts = [ 'media' => "Array"];

    protected static function boot()
    {
        parent::boot();

        // static::created(function ($workout) {
        //     // dd($workout , 'created');
        //     $workout->id = '000'. $workout->id; 
        //     $workout->save(); 
        // });

        // // static::creating(function ($workout) {
        // //     dd($workout , 'creating');
        // // });

        // static::updated(function ($workout) {
        //     // dd($workout , 'updated');
        //     $workout->id = 'Local'. $workout->id; 
        //     $workout->save(); 
        // });
        
        // static::updating(function ($workout) {
        //     // dd($workout , 'updating');
        //     $workout->id = 'Local'. $workout->id; 
        //     $workout->save(); 
        // });

        // static::saving(function ($workout) {
        //     $workout->id = '000'.$workout->id; 
        //     $workout->save(); 
        //     // dd($workout , 'saving');
        // });

    }



    public function tag(){
        return $this->belongsToMany('App\Tag' , 'workouts_tags');
    }

    public function addLineBreaks()
    {

        $this->description = str_replace('<br>' , " \n " , $this->description );
        $this->description = str_replace('<\li>' , " \n " , $this->description );

        $this->description = strip_tags( $this->description);

        return $this;
    }

    
}
