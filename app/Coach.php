<?php
namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Coach extends Model
{
    
    protected $appends = ['average_ratings' , 'total_transformed' , 'total_review_count'];
    protected $guarded = ['id'];


    
    protected static function boot()
    {
        static::deleting(function ($coach) {
            // dd($user->profile);
        $coach->user->delete();
        
    });

        static::retrieved(function($coach){
            // $coach->average_ratings = $coach->average_ratings ;
        });
        parent::boot();
    }


    

    public function scopeGetActiveRequests($query)
    {
        // new_requests
        dd($this);
        return $this->new_requests->query->where('role', 3 );
    }

    public function scopeFetchTrainers($query)
    {
        
        return $query->where('role', 3 );
    }

    public function scopeFetchDietitians($query)
    {
        
        return $query->where('role', 4 );
    }


    public function scopeFetchSortedCoaches($query , $user , $type = 3)
    {

        
        $user->profile->normalize();
        
        $tags = Tag::all();

        // dd($tags);

        $sorted_tags = Tag::whereIn('name' , $user->profile->goals )->with('coaches' , 'coaches.tags' , 'coaches.user' , 'coaches.packages')->get();
        
        $sorted_coaches = collect();
        foreach($sorted_tags as $tag){

                $sorted_coaches = $sorted_coaches->merge($tag->coaches->where('role' , $type));

                $sorted_coaches = $sorted_coaches->sortByDesc('total_review_count');
            // foreach($tag->coaches->where('role' , $type) as $coach){
            //     $sorted_coaches->push($coach);
            // }
        }

        // dd($sorted_coaches);
        
        $unsorted_coaches = Coach::where('role' , $type )->with('user' , 'user.tags' , 'tags'  , 'packages' , 'reviews' )-> whereNotIn('id' , $sorted_coaches->pluck('id')->toArray() )->get();
        
        // dump($user->profile->goals , $tags , $sorted_trainers->pluck('id') , $unsorted_trainers->pluck('id'));

        $coaches = $sorted_coaches->merge($unsorted_coaches);

        // $coaches = $coaches->unique('id')->values();
        
        return $coaches;






        // // dd($user->profile->goals);
        // $coaches = collect();
        // $sorted_coaches_ids = [];

        // $tags = Tag::whereIn('name' , $user->profile->goals )->with('coaches' , 'coaches.tags' , 'coaches.user' , 'coaches.packages')->get();
        
        // foreach($tags as $tag){

        //     // dump($tag->coaches , $tag->coaches->pluck('id')->toArray());
        //     array_merge($sorted_coaches_ids , $tag->coaches->pluck('id')->toArray());

        //     // dump($coaches , $tag->coaches->pluck('id'));
        //     $coaches = $coaches->merge($tag->coaches);
        //     // dd($coaches);
        // }
        // dd($coaches->pluck('id'));

        // $unsorted_coaches = Coach::whereNotIn('id' , $coaches->pluck('id')->toArray() )->get();
        // // dd( $user->profile->tags->pluck('name') , $coaches->pluck('tags')->pluck('name'));

    }



    public function getTotalReviewCountAttribute(){
        return $this->reviews->count();
    }






    public function new_requests(){
        return $this->belongsToMany(User::class , 'client_requests' , 'coach_id' ,'client_id')->withPivot('status' ,  'responded_at' , 'package_id' , 'id' , 'created_at');
    }
    
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function payments(){
        return $this->hasMany(Client_Payment::class , 'coach_id');
    }
    
    public function plans(){
        return $this->hasMany(Client_Plan::class , 'coach_id');
    }
    
    public function tags(){
        return $this->belongsToMany(Tag::class , 'coach_tags' ,'coach_id' , 'tag_id' )->withPivot('tag_id' , 'coach_id');
    }

    public function packages(){
        return $this->belongsToMany(Package::class,'assigned_packages','coach_id','package_id');
    }

    public function subscribers(){
        return $this->belongsToMany(User::class,'package_subscribers','coach_id','user_id');
    }

    public function clients(){
        return $this->belongsToMany(User::class,'package_subscribers','coach_id','user_id');
    }

    public function addLineBreaks()
    {

        $this->portfolio = str_replace('<br>' , " \n " , $this->portfolio );
        $this->portfolio = str_replace('<\li>' , " \n " , $this->portfolio );

        $this->portfolio = strip_tags( $this->portfolio);

        return $this;
    }


    public function reviews($plan_id = null){
        return $this->hasMany(Review::class , 'coach_id') ;
        // return $this->hasMany(Review::class , 'coach_id')->where('plan_id' , null ) ;
    }


    public function getAverageRatingsAttribute(){
        if($this->reviews->count() > 0 ){

            $average_ratings = number_format((float) array_sum($this->reviews->pluck('rating')->toArray())/$this->reviews->count() ,  1, '.', '');
            
            $this->average_ratings = $average_ratings;
            return $average_ratings ;
            
            // return array_sum($this->reviews->pluck('rating')->toArray())/$this->reviews->count();
        }else{
            return "0";
        }
    }

    public function getTotalTransformedAttribute(){
        return $this->plans->where('is_completed' , 1)->count();
    }


    public function getStatisticsAttribute(){

        $completed_plans = $this->plans->where( 'start_status' , 1 ) -> where('end_date', '<', Carbon::now()->toDateString())->count();

        $new_plans = $this->plans->where( 'start_status' , 0 )->count() ;

        $total_reviews = $this->reviews->count();

        $total_packages = $this->packages->count();


        $clients = $this->clients->unique()->values()->unique();

        // dd($clients);


        $stats['total_clients'] = $clients->count();
        $stats['completed_plans'] = $completed_plans;
        $stats['new_plans'] = $new_plans;
        $stats['total_reviews'] = $total_reviews;
        $stats['average_ratings'] = $this->average_ratings;

        // dd($stats);
        // $stats['total_packages'] = $total_packages;

        return $stats;
        // average_ratings

        // dd($this->plans->where( 'start_status' , 0 ) );
        // dd($this->plans->pluck('plan_progress') );
        // dd($this);

    }


    
}


// namespace App;

// use Carbon\Carbon;
// use Illuminate\Database\Eloquent\Model;

// class Coach extends Model
// {
    
//     protected $appends = ['average_ratings' , 'total_transformed' , 'total_review_count'];
//     protected $guarded = ['id'];

    
//     protected static function boot()
//     {
//         static::deleting(function ($coach) {
//             // dd($user->profile);
//         $coach->user->delete();
        
//     });

//         static::retrieved(function($coach){
//             // $coach->average_ratings = $coach->average_ratings ;
//         });
//         parent::boot();
//     }


    

//     public function scopeGetActiveRequests($query)
//     {
//         // new_requests
//         dd($this);
//         return $this->new_requests->query->where('role', 3 );
//     }

//     public function scopeFetchTrainers($query)
//     {
        
//         return $query->where('role', 3 );
//     }

//     public function scopeFetchDietitians($query)
//     {
        
//         return $query->where('role', 4 );
//     }


//     public function scopeFetchSortedCoaches($query , $user , $type = 3)
//     {

        
//         $user->profile->normalize();
        
//         $tags = Tag::all();

//         // dd($tags);

//         $sorted_tags = Tag::whereIn('name' , $user->profile->goals )->with('coaches' , 'coaches.tags' , 'coaches.user' , 'coaches.packages')->get();
        
//         $sorted_coaches = collect();
//         foreach($sorted_tags as $tag){

//                 $sorted_coaches = $sorted_coaches->merge($tag->coaches->where('role' , $type));

//                 $sorted_coaches = $sorted_coaches->sortByDesc('total_review_count');
//             // foreach($tag->coaches->where('role' , $type) as $coach){
//             //     $sorted_coaches->push($coach);
//             // }
//         }

//         // dd($sorted_coaches);
        
//         $unsorted_coaches = Coach::where('role' , $type )->with('user' , 'user.tags' , 'tags'  , 'packages' , 'reviews'  )-> whereNotIn('id' , $sorted_coaches->pluck('id')->toArray() )->get();
        
//         // dump($user->profile->goals , $tags , $sorted_trainers->pluck('id') , $unsorted_trainers->pluck('id'));

//         $coaches = $sorted_coaches->merge($unsorted_coaches);

//         // $coaches = $coaches->unique('id')->values();
        
//         // dd($coaches->pluck('id') );
//         return $coaches;









//         // // dd($user->profile->goals);
//         // $coaches = collect();
//         // $sorted_coaches_ids = [];

//         // $tags = Tag::whereIn('name' , $user->profile->goals )->with('coaches' , 'coaches.tags' , 'coaches.user' , 'coaches.packages')->get();
        
//         // foreach($tags as $tag){

//         //     // dump($tag->coaches , $tag->coaches->pluck('id')->toArray());
//         //     array_merge($sorted_coaches_ids , $tag->coaches->pluck('id')->toArray());

//         //     // dump($coaches , $tag->coaches->pluck('id'));
//         //     $coaches = $coaches->merge($tag->coaches);
//         //     // dd($coaches);
//         // }
//         // dd($coaches->pluck('id'));

//         // $unsorted_coaches = Coach::whereNotIn('id' , $coaches->pluck('id')->toArray() )->get();
//         // // dd( $user->profile->tags->pluck('name') , $coaches->pluck('tags')->pluck('name'));

//     }



//     public function getTotalReviewCountAttribute(){
//         return $this->reviews->count();
//     }






//     public function new_requests(){
//         return $this->belongsToMany(User::class , 'client_requests' , 'coach_id' ,'client_id')->withPivot('status' ,  'responded_at' , 'package_id' , 'id' , 'created_at');
//     }
    
//     public function user(){
//         return $this->belongsTo(User::class);
//     }

//     public function payments(){
//         return $this->hasMany(Client_Payment::class , 'coach_id');
//     }
    
//     public function plans(){
//         return $this->hasMany(Client_Plan::class , 'coach_id');
//     }
    
//     public function tags(){
//         return $this->belongsToMany(Tag::class , 'coach_tags' ,'coach_id' , 'tag_id' )->withPivot('tag_id' , 'coach_id');
//     }

//     public function packages(){
//         return $this->belongsToMany(Package::class,'assigned_packages','coach_id','package_id');
//     }

//     public function subscribers(){
//         return $this->belongsToMany(User::class,'package_subscribers','coach_id','user_id');
//     }

//     public function clients(){
//         return $this->belongsToMany(User::class,'package_subscribers','coach_id','user_id');
//     }

//     public function addLineBreaks()
//     {

//         $this->portfolio = str_replace('<br>' , " \n " , $this->portfolio );
//         $this->portfolio = str_replace('<\li>' , " \n " , $this->portfolio );

//         $this->portfolio = strip_tags( $this->portfolio);

//         return $this;
//     }


//     public function reviews($plan_id = null){
//         return $this->hasMany(Review::class , 'coach_id') ;
//         // return $this->hasMany(Review::class , 'coach_id')->where('plan_id' , null ) ;
//     }


//     public function getAverageRatingsAttribute(){
//         if($this->reviews->count() > 0 ){

//             $average_ratings = number_format((float) array_sum($this->reviews->pluck('rating')->toArray())/$this->reviews->count() ,  1, '.', '');
            
//             $this->average_ratings = $average_ratings;
//             return $average_ratings ;
            
//             // return array_sum($this->reviews->pluck('rating')->toArray())/$this->reviews->count();
//         }else{
//             return "0";
//         }
//     }

//     public function getTotalTransformedAttribute(){
//         return $this->plans->where('is_completed' , 1)->count();
//     }


//     public function getStatisticsAttribute(){

//         $completed_plans = $this->plans->where( 'start_status' , 1 ) -> where('end_date', '<', Carbon::now()->toDateString())->count();

//         $new_plans = $this->plans->where( 'start_status' , 0 )->count() ;

//         $total_reviews = $this->reviews->count();

//         $total_packages = $this->packages->count();


//         $clients = $this->clients->unique()->values()->unique();

//         // dd($clients);


//         $stats['total_clients'] = $clients->count();
//         $stats['completed_plans'] = $completed_plans;
//         $stats['new_plans'] = $new_plans;
//         $stats['total_reviews'] = $total_reviews;
//         $stats['average_ratings'] = $this->average_ratings;

//         // dd($stats);
//         // $stats['total_packages'] = $total_packages;

//         return $stats;
//         // average_ratings

//         // dd($this->plans->where( 'start_status' , 0 ) );
//         // dd($this->plans->pluck('plan_progress') );
//         // dd($this);

//     }


    
// }
