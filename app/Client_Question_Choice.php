<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client_Question_Choice extends Model
{
        protected $guarded = ['id'];
        protected $table = 'client_questions_choices';

        public function question(){
                return $this->belongsTo(Client_Question::class , 'question_id');
            }
        
}
