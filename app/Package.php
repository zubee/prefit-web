<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $guarded = [];
    protected $appends = ['descriptive_duration'];
    // public function users(){
    //     return $this->belongsToMany(User::class,'assigned_packages','package_id','user_id');
    // }

    public function coaches(){
        return $this->belongsToMany(Coach::class,'assigned_packages','package_id','coach_id');
    }


    public function tags(){
        return $this->belongsToMany(Tag::class,'tags_packages','package_id','tag_id');
    }

    public function subscribers(){
        return $this->belongsToMany(User::class,'package_subscribers','package_id','user_id');
    }



    public function plans(){
        return $this->hasMany(Client_Plan::class , 'package_id');
    }


    public function reviews(){
        return $this->hasMany(Review::class , 'package_id')->latest();
    }

    public function requests(){
        return $this->hasMany(Client_Request::class)->latest();
    }


    public function specificReviews($id){
        return $this->reviews->where('coach_id' , $id);
    }

    public function getWeeksAttribute(){
        return ceil($this->duration/7);
    }

    public function getDescriptiveDurationAttribute(){
        $duration = '';
        if($this->duration < 7){
            $duration = $this->duration . ' days';
        }elseif($this->duration > 7 && $this->duration < 28){
            $duration = number_format($this->duration/7 , 1) . ' weeks';
            // $duration = $this->duration/7 . ' weeks';
            
        }elseif($this->duration > 28){
            $duration = number_format($this->duration / 30 , 1) . ' months';
            // $duration = $this->duration/30 . ' month';
        }else{
            $duration = $this->duration . ' days';
        }
        return $duration;
    }
    

    public function addLineBreaks()
    {

        $this->description = str_replace('<br>' , " \n " , $this->description );
        $this->description = str_replace('<\li>' , " \n " , $this->description );

        $this->description = strip_tags( $this->description);

        return $this;
    }
    
    public function makeHighlightsArray(){

        // dd(strpos($this->highlights , 'ul') , $this->highlights);
        
        // dump($this);
        if(!is_array($this->highlights)){
            
        if(strpos($this->highlights , 'ul') || strpos($this->highlights , 'ol')){
            
            $highlights = explode( '<li>' , $this->highlights);
            
            foreach($highlights as $k => $highlight){
                $array[] = strip_tags($highlight);
                // $all_highlights[] = strip_tags($highlight) ;
                // $array[] = strip_tags($highlight);
            }
        
            array_shift($array);
            // unset($array[0]);
            // unset($all_highlights[0]);
            // $package->highlights = $array;
            
        }else{
            // dd('asd');
            $array[] = strip_tags( $this->highlights );
            
        }
            $this->highlights = $array;

        }
        // dd($array);

        // $this->highlights_in_array = $all_highlights ?? '';
        // dd($this->highlights[1]);


        return $this;
    }

    public function stripHTMLTags(){

    $this->description = strip_tags( $this->description);

            // dd(strip_tags($package->highlights));
            if(strpos($this->highlights , 'ul') || strpos($this->highlights , 'ol')){
                
                $this->highlights = explode( '<li>' , $this->highlights);
                  
                foreach($this->highlights as $highlight){
                    $array[] = strip_tags($highlight);
                }

                    unset($array[0]);
                    // $package->highlights = $array;

            }else{
                $array[] = strip_tags( $this->highlights);

            }

            $this->highlights = $array;

        }


    public function getRequestStatus($user_id , $coach_id){
        // dd($user_id , $coach_id , $this->requests->where('client_id' , $user_id)->where('coach_id' , $coach_id)->count() > 0 );
        if($this->requests->count() > 0 && $this->requests->where('client_id' , $user_id)->where('coach_id' , $coach_id)->count() > 0 ){
            $this->request_status = ucwords($this->requests->where('client_id' , $user_id)->where('coach_id' , $coach_id)->first()->status);
        }else{
            $this->request_status = false;
        }
        // dd($this->request_status );
    }


}
