<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client_Answer extends Model
{
        protected $guarded = ['id'];
        protected $table = 'clients_answers';

        public function question(){
                return $this->belongsTo(Client_Question::class , 'question_id');
        }

}
