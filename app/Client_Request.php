<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client_Request extends Model
{
        protected $guarded = ['id'];

        protected $table = 'client_requests';

        public function scopeActive($query)
        {
                return $query->where('status' , 'pending');
        }

        public function client()
        {
                return $this->belongsTo(User::class , 'client_id');
        }

        public function coach()
        {
                return $this->belongsTo(Coach::class);
        }


        public function package()
        {
                return $this->belongsTo(Package::class , 'package_id');
        }

}
