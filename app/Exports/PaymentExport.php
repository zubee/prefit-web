<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithHeadings;

class PaymentExport implements FromView
{

    public $payments;
    public function __construct($payments = [], $headings = [])
    {
        $this->payments = $payments;
    }
    
    public function view(): view {
        return view('exports.payments', [
            'data' => $this->payments
        ]);
    }
}
