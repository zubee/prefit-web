<?php

namespace App\Exports;

use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ExportClients implements FromView
{

    public $users;
    public $coach;
    
    public function __construct($users = [],  $coach , $headings = [])
    {
        $this->users = $users;
        $this->coach = $coach;
    }
    
    public function view(): view {
        return view('exports.clients', [
            'data' => $this->users,
            'coach' => $this->coach
        ]);
    }
}
