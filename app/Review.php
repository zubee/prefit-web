<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
        protected $guarded = ['id'];
        protected $appends = ['proper_date'];
        protected $with = ['user'];

        public function getProperDateAttribute(){
                return $this->created_at->toDateString();
        }

        public function user(){
                return $this->belongsTo(User::class , 'user_id');
        }

        public function coach(){
                return $this->belongsTo(Coach::class , 'coach_id');
        }

        public function package(){
                return $this->belongsTo(Package::class , 'package_id');
        }

}
