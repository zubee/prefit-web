<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    //

    protected $guarded = [];

    public function users(){
        return $this->belongsToMany('App\User' , 'assigned_questionaires' , 'question_id' , 'user_id');
    }

    public function questions(){
        return $this->hasMany('App\Question','questionnaires_id','id')->orderBy('created_at' , 'asc');
    }
}
