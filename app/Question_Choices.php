<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question_Choices extends Model
{
    protected $guarded = [];
    protected $table = 'question_choices';

    public function question(){
        return $this->belongsTo('App\Question');
    }
    //
}
