<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Error_Logger extends Model
{
        protected $table = 'error_logger';
        protected $guarded = ['id'];
        
}
