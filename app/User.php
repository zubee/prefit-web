<?php

namespace App;

use stdClass;
use App\Jobs\SendEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Traits\FirebaseFCM;


class User extends Authenticatable
{
    use FirebaseFCM;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email', 'password',
    // ];

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $appends = [
        'tags',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot()
    {
        static::deleting(function ($user) {


            // dd($user->chat);

            if($user->role_id == 5 && $user->profile){
                $user->profile->delete();
            }
            
            $user->chat()->delete();
            $user->reivews()->delete();

            // if(($user->role_id == 3 || $user->role_id == 4 ) && $user->coach ){
            //     // dd($user->profile);
            //     $user->coach->delete();
            // }

        });

        static::created(function($user){
            $profile = Profile::create([
                'user_id' => $user->id,
            ]);
        });
        
        parent::boot();
    }



    // scopes

    public function scopeAdminUsers($query)
    {
        return $query->whereIn('role_id', [1,2]);
    }

    public function scopeCoachUsers($query)
    {
        return $query->whereIn('role_id', [4,3]);
    }

    public function scopeClientUsers($query)
    {
        return $query->where('role_id', 5);
    }

    public function scopeSupportUsers($query)
    {
        return $query->where('role_id', 6);
    }







    // relations 
    public function questions(){
        return $this->belongsToMany(User::class , 'assigned_questionaires' ,'user_id' , 'question_id');
    }

    public function questionnaires(){
        return $this->belongsToMany(Questionnaire::class , 'assigned_questionaires' ,'user_id' , 'question_id');
    }

    public function packages(){
        return $this->belongsToMany(Package::class,'assigned_packages','user_id','package_id');
    }

    public function role(){
        return $this->belongsTo(Role::class,'role_id','id');
    }

    public function tags(){
        return $this->belongsToMany(Tag::class , 'tags_users' , 'user_id' , 'tag_id');
    }

    public function coach(){
        return $this->hasOne(Coach::class , 'user_id');
    }
    
    // public function dietitian(){
    //     return $this->hasOne('App\Dietitian' , 'user_id');
    // }
    
    // public function trainer(){
    //     return $this->hasOne('App\Trainer' , 'user_id');
    // }


    public function profile(){
        return $this->hasOne(Profile::class);
    }


    public function notifications(){
        return $this->hasMany( Notification::class , 'user_id');
    }

    
    public function subscribe(){
        return $this->belongsToMany(Package::class,'package_subscribers','user_id' , 'package_id')->withTimestamps();
    }

    public function subscribers(){
        return $this->belongsToMany(Package::class,'package_subscribers','user_id' , 'package_id')->withTimestamps();
    }

    public function plans(){
        return $this->hasMany(Client_Plan::class , 'client_id')->orderBy('created_at' , 'desc') ;
    }

    public function subscribed_packages(){
        return $this->belongsToMany(Package::class,'package_subscribers','user_id' , 'package_id')->withPivot('coach_id')->withTimestamps() ;
    }

    public function coaches(){
        return $this->belongsToMany(Coach::class,'package_subscribers','user_id' , 'coach_id')->with('reviews')->withTimestamps();
    }


    public function chat(){
        return $this->hasMany(Chat::class , 'sender_id');
    }

    public function chatable_clients()
    {
        return $this->belongsToMany(User::class, 'chatable_clients' , 'coach_id' , 'client_id');
    }


    public function reivews(){
        return $this->hasMany(Review::class , 'user_id');
    }


    // attributes

    public function getContactDetails($reciever_id){
            
        $chats = Chat::whereIn('sender_id' , [ $this->id , $reciever_id] )->whereIn('reciever_id' , [$this->id , $reciever_id])->orderBy('created_at' , 'asc')->get();

        $this->unread_count = $chats->whereIn('sender_id' , $this->id)->where('read_status' , 0)->count();

        if($chats->last()){
            $this->last_message = $chats->last();
            $this->last_message_time = $chats->last()->proper_time;
            $this->last_message_created = $chats->last()->created_at;
        }else{
            $this->last_message_time = false;
            $this->last_message_created = 'NOT';
        }

        return $this;
    
    }

    public function notifyCoaches(){

        $coaches = $this->coaches->pluck('user')->unique();

        foreach($coaches as $coach){
            if($coach->fcm_token){

                $notification = new stdClass();
                $notification->text =  "Your client " . $this->name . " has been removed";
                $notification->description = "Your client " . $this->name . " has been removed";
                $notification->notif_type = "notif-subscribe";
        
                $this->PublishBroadcast($user , $coach->user , $notification , $coach->user->name , $notification->text , " ");

            }
        }

    }




    public function getTagsAttribute(){
        if($this->coach){
            $tags = $this->coach->tags;
        }else{
            $tags = [];
        }
        return $tags;
    }
    // public function getLastMessageAttribute(){
    //     if($this->chat)
    // }


    public function sendWeclomeEmail(){
        // Welcome email
        $users = User::where('id' , $this->id )->get();

        $subject = 'Welcome to FTBLE family';
        
        $body = '
        <div class="banner-image-box">
            <img src="'.asset('img/welcome.jpg').'" class="banner-image" />
        </div>
        <br>
        Hi “'.$this->name.'”,
        <br>
        Thank you for joining FTBLE and we hope you enjoy using the App. FTBLE App will help you find the best Personal Trainer or Dietitian who will prepare a customized fitness program that fits your goals for a healthier &amp; fit body.
        <br>
        Please check the list of Trainers &amp; Dietitians on the App and choose the best Trainer/Dietitian, chat with them for more details about their packages and discuss your fitness goals, subscribe to the package that suits you and start the fun!
        <br>
        Stay in touch! Follow our Instagram page to stay up to date and check our blog on ftble.com';

        dispatch(new SendEmail($users , $subject , $body ));
        

        // admins
        $admins = User::whereIn('role_id' , [1,2] )->get();
        
        $subject = 'New user sign up';
        $body = 'Hi Admin,
        <br>
        '.$this->name.' had signed up as a new user with this email '.$this->email.'.';
        dispatch(new SendEmail($admins , $subject , $body ));

    }


    public function sendFCMNotification(){
        
    }




}
