<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded = [];

    public function choices(){
        return $this->hasMany(Question_Choices::class);
    }

    public function questionnaire(){
        return $this->belongsTo('App\Questionnaire','questionnaires_id','id');
    }

    public function answers(){
        return $this->hasMany(Answer::class);
    }

    public function answer($coach_id){
        return $this->hasMany(Answer::class)->where('coach_id' , $coach_id)->first();
    }

}
