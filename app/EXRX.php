<?php

namespace App;

use GuzzleHttp\Psr7;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Exception\RequestException;


class EXRX
{

        private $token = '';


        public static function login(){
                $client = new Client();
                try {
                        $res = $client->request(
                                'POST',
                                'http://204.235.60.194/consumer/login',  [
                                'form_params' => [
                                'username' => 'perfitappexrx',
                                'password' => '4ErUHNvM',
                                ]]
                        );
                
                        // echo($res->getBody());
                        $body = $res->getBody();
                        $token = json_decode($body->getContents())->token;

                }catch (RequestException $e) {
                        // echo Psr7\Message::toString($e->getRequest());
                        if ($e->hasResponse()) {
                                // echo Psr7\Message::toString($e->getResponse());
                                $error = Psr7\Message::toString($e->getResponse());
                        }else{
                                $error = "The Server is not responding due to some connectivity error. Please try again";
                        }
                }

                if(isset($token)){
                        return $token;
                }else{
                        return $error ;
                }
        }




        public static function searchExercises($exercise_name = ""){
                
                try {
                        $client = new Client();
                        $res = $client->request(
                            'GET', 
                        //     'http://204.235.60.194/exrxapi/v1/allinclusive/exercises?secondarymusclegroup='.$exercise_name,
                            'http://204.235.60.194/exrxapi/v1/allinclusive/exercises?exercisename='.$exercise_name,

                            [ 'headers' => [
                                    'Authorization' => "Bearer ".self::login()
                            ]
                        ]
                        );
                        $response = json_decode($res->getBody()->getContents());
                        //  if(is_object($response) && is_object($response->exercises)){
                        //         $response = json_decode($res->getBody()->getContents());
                        //         $error = $response->exercises;
                        // }

                    }catch (RequestException $e) {
                        // echo Psr7\Message::toString($e->getRequest());
                        if ($e->hasResponse()) {
                                // echo Psr7\Message::toString($e->getResponse());
                                $error = Psr7\Message::toString($e->getResponse());
                        }else{
                                $error = "The Server is not responding due to some connectivity error. Please try again";
                        }
                }

                if(isset($response)){
                        return $response;
                }else{
                        return $error ;
                }
                    
                
        }





        public static function findExercises($exercise_ids = []){
                
                // dd($exercise_ids , 'ss');
                try {
                        $client = new Client();
                        $res = $client->request(
                            'GET', 
                            'http://204.235.60.194/exrxapi/v1/allinclusive/exercises?exerciseids='.$exercise_ids,

                            [ 'headers' => [
                                    'Authorization' => "Bearer ".self::login()
                            ]
                        ]
                        );
                        // $exercises = $res->getBody();
                        $response = json_decode($res->getBody()->getContents());

                    }catch (RequestException $e) {
                        // echo Psr7\Message::toString($e->getRequest());
                        if ($e->hasResponse()) {
                                // echo Psr7\Message::toString($e->getResponse());
                                $error = Psr7\Message::toString($e->getResponse());
                        }else{
                                $error = "The Server is not responding due to some connectivity error. Please try again";
                        }
                }

                if(isset($response)){
                        return $response;
                }else{
                        return $error ;
                }
                    
                
        }



        
        public static function exercises($exercise_name = "burpee"){
                
                try {
                        $client = new Client();
                        $res = $client->request(
                            'GET', 
                            'http://204.235.60.194/exrxapi/v1/allinclusive/exercises?exercisename=burp',

                        // 'http://204.235.60.194/exrxapi/v1/allinclusive/exercises?exerciseids=[1991]',  

                        // for category
                        // 'http://204.235.60.194/exrxapi/v1/allinclusive/exercises?subcategory=calisthenic',  
                        
                        // name with default values
                        //     'http://204.235.60.194/exrxapi/v1/allinclusive/exercises?exercisename='.$exercise_name,
                        
                        // for names
                        //     'http://204.235.60.194/exrxapi/v1/allinclusive/exercises?exercisename=Assisted',  

                        // for ids
                        // 'http://204.235.60.194/exrxapi/v1/allinclusive/exercises?exerciseids=[1,2]',  

                            [ 'headers' => [
                                    'Authorization' => "Bearer ".self::login()
                            ]
                        ]
                        );
                        // $exercises = $res->getBody();
                        $response = json_decode($res->getBody()->getContents());

                    }catch (RequestException $e) {
                        // echo Psr7\Message::toString($e->getRequest());
                        if ($e->hasResponse()) {
                                // echo Psr7\Message::toString($e->getResponse());
                                $error = Psr7\Message::toString($e->getResponse());
                        }else{
                                $error = "The Server is not responding due to some connectivity error. Please try again";
                        }
                }

                if(isset($response)){
                        return $response->exercises;
                }else{
                        return $error ;
                }
                    
                
        }


}
