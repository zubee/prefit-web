<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{

    protected $guarded = ['id'];
    protected $appends  = ['weight_loss_progress'];
    protected $casts = [
        'goals' => 'array',
        'workout' => 'array',
        'measurements' => 'object',
    ];
    public function normalize()
    {
    // if($this->goals){
    //     $this->goals = json_decode($this->goals);
    // }
    // if($this->workout){
    //     $this->workout = json_decode($this->workout);
    // }
    // if($this->measurements && gettype($this->measurements) == 'string' ){
    //     $this->measurements = json_decode($this->measurements);
    // }

    // if($this->measurements){
    //     $this->measurements = json_decode($this->measurements);
    // }

    // $this->weight_loss_progress = $this->weight_loss_progress;
    
    }


    public function user(){
        return $this->belongsTo(User::class);
    }

    public function getWeightLossProgressAttribute(){

        $progress = 1;
        
        if($this->current_weight && $this->starting_weight && $this->target_weight){
            $progress = ceil($this->current_weight/$this->starting_weight * $this->target_weight);
        }
        // $this->weight_loss_progress = $progress;
        return $progress;
    }


    
}
