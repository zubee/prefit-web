<?php

use App\EXRX;
use App\User;
use GuzzleHttp\Psr7;
use App\Jobs\SendEmail;
use App\Jobs\SendEmailTest;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Response;
use GuzzleHttp\Exception\RequestException;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('testing' , 'AdminController@notific');

Route::get('/', function () {
    // dd('sad');
    return view('welcome');
    // return redirect()->route('login');
})->name('/');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('get/notifications/{user_id}', 'HomeController@getNotifications')->name('get.notifications');


Route::get('/homepage', 'HomeController@homepage')->name('homepage');

Route::group(['prefix' => 'admin' , 'as' => 'admin.', 'middleware'=>['auth','admin']  ], function () {
    Route::get('/' , function(){
            return redirect('/admin/dashboard');
    } );
    Route::get('/dashboard' , 'DashboardController@index' )->name('index');
    Route::resource('admins', 'AdminController');
    Route::resource('users', 'UserController');
    Route::resource('dietitian', 'DietitianController');

    Route::resource('coach', 'CoachController');
    
    Route::get('coach/{coach_id}/clients/' , 'CoachController@viewClients' )->name('coach.clients') ;
    Route::get('coach/{coach_id}/client/{clinet_id}/all-packages' , 'CoachController@viewClientPackages' )->name('coach.client.packages') ;

    Route::get('coach/{coach_id}/client/{clinet_id}/package/{package_id}/plan/view' , 'CoachController@viewClientPackagePlan' )->name('coach.client.package.plan.view') ;

    Route::resource('trainer', 'TrainerController');
    Route::resource('questionnaires', 'QuestionnaireController');
    Route::get('questionnaires/{id}/assignedto','QuestionnaireController@assignedto')->name('question.assigned');
    Route::get('questionnaires/{id}/coach/{coach_id}/show','QuestionnaireController@showCoachAnswers')->name('question.coach.show');
    Route::resource('packages', 'PackageController');
    Route::get('packages/{id}/assignedto','PackageController@assignedto')->name('package.assigned');
    Route::resource('tags', 'TagController');
    Route::resource('supplements','SupplementController');
    Route::resource('workouts','WorkoutController');
    Route::get('user/export','UserController@userExport')->name('export.user');

    Route::get('/messages/{reciever?}' , 'DashboardController@showMessages')->name('messages');
    Route::get('/contact' , 'DashboardController@editContactUS')->name('contact');
    Route::post('/contact/update' , 'DashboardController@updateContactUS')->name('contact.update');
    
    
    Route::get('/financial-reports' , 'FinancialReportsController@getReportForAdmin')->name('financial-reports');
    Route::get('/financial-reports/filter' , 'FinancialReportsController@filterPayments')->name('financial-reports.filter');

    Route::get('/pay-to-coach/{payment_id}' , 'FinancialReportsController@payToCoach')->name('pay-to-coach');
    Route::get('/remove/pay-to-coach/{payment_id}' , 'FinancialReportsController@removePayToCoach')->name('remove.pay-to-coach');
    


    Route::get('/exports/payments/{client_ids}' , 'FinancialReportsController@exportPayments')->name('export.payments');


    Route::get('/clients/exports/{client_ids}' , 'DashboardController@exportClients')->name('clients.export');

    // Route::get('/dietitian' , 'DietitianController@index' )->name('dietitian');
});



Route::post('/verifyusername', 'UserController@verifyUserName')->name('verify.username');
Route::post('verifyemail','UserController@verifyEmail')->name('verify.email');
Route::get('tagnames','TagController@tagnames')->name('tagnames');

// ajax routes
Route::get('/change/user/status/{user_id}' , 'UserController@changeStatus' );
















Route::get('coach/login' , 'CoachDashboardController@login' )->name('coach.login');

Route::group(['prefix' => 'coach' , 'as' => 'coach.' , 'middleware'=>['auth' , 'coach']   ], function () {
    Route::get('/' , 'CoachDashboardController@index' )->name('index');
    Route::get('profile' , 'CoachDashboardController@showProfile' )->name('profile');
    // Route::get('profile/edit' , 'CoachDashboardController@editProfile' )->name('profile.edit');
    Route::post('profile/update/{id}' , 'CoachDashboardController@updateProfile' )->name('profile.update');

    Route::get('dashboard' , 'CoachDashboardController@index' )->name('dashboard');
    Route::get('packages' , 'CoachDashboardController@showPackages' )->name('packages');
    Route::get('packages/view/{id}' , 'CoachDashboardController@viewPackage' )->name('packages.view');

    Route::get('questionnaires' , 'CoachDashboardController@showQuestionnaires' )->name('questionnaires');
    Route::get('questionnaires/view/{id}' , 'CoachDashboardController@showQuestionnaire' )->name('questionnaires.view');
    Route::post('questionnaires/store-answer/{id}' , 'QuestionnaireController@storeAnswer' )->name('questionnaires.store-answer');
    

    Route::get('manage-clients' , 'CoachDashboardController@showClients' )->name('manage-clients');
    
    Route::get('manage-clients/filter' , 'CoachDashboardController@filterClients' )->name('manage-clients.filter');
    
    Route::get('manage-client/{client_id}/all-packages' , 'CoachDashboardController@showClientPackages' )->name('manage-client.packages');
    
    
    Route::post('manage-client/{client_id}/handle-client-request' , 'CoachDashboardController@handleClientRequest' )->name('manage-client.handle-client-request');


    Route::get('manage-client/{client_id}/details' , 'CoachDashboardController@showClientDetails' )->name('manage-client.details');

    Route::get('manage-client/{client_id}/package/{package}/create-plan' , 'CoachDashboardController@createClientPlan' )->name('manage-client.package.create-plan');
    
    
    Route::get('manage-client/{client_id}/package/{package}/start-plan' , 'CoachDashboardController@startClientPlan' )->name('manage-client.package.start-plan');
    
    Route::get('/messages/{reciever?}' , 'CoachDashboardController@showMessages')->name('messages');

    Route::get('/view/workout/{id}' , 'CoachDashboardController@showWorkout' )->name('view.workout');


    Route::get('/financial-reports' , 'FinancialReportsController@getReportForCoach')->name('financial-reports');

    Route::get('/financial-reports/filter' , 'FinancialReportsController@filterPaymentsCoach')->name('financial-reports.filter');

    Route::get('reviews' , 'CoachDashboardController@showReviews' )->name('reviews');

    Route::resource('client-questionnaires' , 'ClientQuestionnaireController' );
    Route::post('client-questionnaires-update' , 'ClientQuestionnaireController@update' );
    Route::get('client-questionnaires/{questionnaires_id}/assigned-clients' , 'ClientQuestionnaireController@clients' )->name('client-questionnaires.assigned-clients');
    Route::get('client-questionnaires-delete/{questionnaires_id}' , 'ClientQuestionnaireController@destroy');
    
    Route::post('send-questionnaires-client' , 'ClientQuestionnaireController@sendQuestionnaireToClient');
    
    Route::get('client/{client_id}/questionnaires/{questionnaires_id}/show' , 'ClientQuestionnaireController@showAnswer')->name('client.questionnaires.show') ;

    Route::get('get-questionnaires' , 'ClientQuestionnaireController@getQuestionnaires') ;

    // Route::get('manage-client/{client_id}/package/{package}/plan/{plan}' , 'CoachDashboardController@showClientPackages' )->name('manage-client.packages');


});


Route::get('/exports/clients/{client_ids}/{coach}' , 'HomeController@exportClients')->name('export.clients');











Route::get('local_payment/{plan_id}'  , 'Api\ClientPaymentController@makePayment' )->name('local_payment') ;
Route::get('remove_local_payment/{payment_id}'  , 'Api\ClientPaymentController@removePayment' )->name('remove_local_payment') ;



// Route::get('paypal' ,  'Api\ApiPlanController@PayForPlan2' );
Route::get('sadad' ,  'Api\ApiPlanController@PayWithSadad' );
Route::get('payment' ,  'PaypalPaymentController@braintreePayment' );
// Route::get('paypal' ,  'PaypalPaymentController@payment' );
// Route::get('paypal' ,  'PaypalPaymentController@paywithCreditCard' );
// Route::get('paypal' ,  'PaypalPaymentController@PayumPayment' );
// Route::post('stripe' ,  'Api\ApiPlanController@PayForPlanStripe' );
// Route::post('stripe-mobile' ,  'Api\ApiPlanController@PayForPlanStripe1' );
Route::get('brain' , 'Api\ApiPlanController@make' );


// Route::get('paypal' , 'Api\ApiPlanController@payForPlan2' );
Route::get('paypal_web' , 'Api\ApiPlanController@paypalWeb' );
Route::get('paypal_return' , 'Api\ApiPlanController@paypalReturn' );

// Route::get('paypal' , function(){


//     $gateway = new Braintree\Gateway([
//         'environment' => 'sandbox',
//         'merchantId' => env('BRAINTREE_MERCHANT_ID'),
//         'publicKey' => env('BRAINTREE_PUBLIC_KEY'),
//         'privateKey' => env('BRAINTREE_PRIVATE_KEY')
//     ]);

//     // $clientToken = $gateway->clientToken()->generate([
//     //     "customerId" => 11
//     // ]);
//     $clientToken = $gateway->clientToken()->generate();

//     // dd($clientToken);

//     return view('coach\clients\payment' , compact('clientToken'));
// } );

Route::get("/payment/make" , 'Api\ApiPlanController@make')->name('payment.make');


Route::get('stripe' , 'Api\ApiPlanController@PayForPlanStripe1' );

Route::get('success' , function(){
    dd('success');
});

Route::get('fail' , function(){
    dd('fail');
});





// Route::get('test-notification' , function(){
//     dd('asd');
// });







Route::group(['prefix' => 'trainer' , 'as' => 'trainer.' , 'middleware'=>['auth' , 'trainer']   ], function () {
    Route::get('/' , 'TrainerDashboardController@index' )->name('index');
    Route::get('profile' , 'TrainerDashboardController@showProfile' )->name('profile');
    Route::get('profile/edit' , 'TrainerDashboardController@editProfile' )->name('profile.edit');
    Route::post('profile/update/{id}' , 'TrainerDashboardController@updateProfile' )->name('profile.update');

    Route::get('dashboard' , 'TrainerDashboardController@index' )->name('dashboard');
    Route::get('packages' , 'TrainerDashboardController@showPackages' )->name('packages');
    Route::get('packages/view/{id}' , 'TrainerDashboardController@viewPackage' )->name('packages.view');

    Route::get('questionnaires' , 'TrainerDashboardController@showQuestionnaires' )->name('questionnaires');
    Route::get('questionnaires/view/{id}' , 'TrainerDashboardController@showQuestionnaire' )->name('questionnaires.view');

});
// trainer
// dietitian

Route::group(['prefix' => 'dietitian' , 'as' => 'dietitian.' , 'middleware'=>['auth' , 'dietitian']  ], function () {
    Route::get('/' , 'DietitianDashboardController@index' )->name('index');
    Route::get('profile' , 'DietitianDashboardController@showProfile' )->name('profile');
    Route::get('profile/edit' , 'DietitianDashboardController@editProfile' )->name('profile.edit');
    Route::post('profile/update/{id}' , 'DietitianDashboardController@updateProfile' )->name('profile.update');
    Route::get('dashboard' , 'DietitianDashboardController@index' )->name('dashboard');
    Route::get('packages' , 'DietitianDashboardController@showPackages' )->name('packages');
    Route::get('packages/view/{id}' , 'DietitianDashboardController@viewPackage' )->name('packages.view');
    Route::get('questionnaires' , 'DietitianDashboardController@showQuestionnaires' )->name('questionnaires');
    Route::get('questionnaires/view/{id}' , 'DietitianDashboardController@showQuestionnaire' )->name('questionnaires.view');

});



Route::get('/admin/pt/login' , 'TrainerDashboardController@pt_login' )->name('pt.login');
Route::get('/trainer/login' , 'TrainerDashboardController@login' )->name('trainer.login');
Route::get('/dietitian/login' , 'DietitianDashboardController@login' )->name('dietitian.login');





Route::get('file' , function(){
    // Installation-Guide.txt
    // return response()->file(public_path()."/files/scan0349.pdf");
    // $file = File::get(public_path()."/files/scan0349.pdf");
    // $response = response()->make($file, 200);
    // $response->header("Content-Type", '.pdf');
    // return $response;
    dd(File::get(public_path()."/files/recent2.docx"));


    // dd(User::supportUsers()->get()->pluck('role_id') );
});


// Route::get('admins' , function(){
//     dd(User::supportUsers()->get()->pluck('role_id') );
// });

Route::get('user' , function(){
    dd(auth()->user());
});
Route::get('migrate' , function(){
    Artisan::call('migrate');
});

Route::get('migrate-fresh-seed' , function(){
    Artisan::call('migrate:fresh --seed');
});

Route::get('seed' , function(){
    Artisan::call('db:seed');
});

Route::get('clear' , function(){
    Artisan::call('cache:clear');
    Artisan::call('view:clear');
    Artisan::call('config:clear');
    return 'clear';
} );

Route::get('composer-update' , function(){
    $output = shell_exec('ls -lart');
    echo "<pre>$output</pre>";
});


Route::get('start-queue' , function(){
    Artisan::call("queue:restart");
    Artisan::call("queue:work");    
});




Route::get('email-demo' , function(){
    $data['msg'] = '
    <div class="banner-image-box">
        <img src="'.asset('img/welcome.jpg').'" class="banner-image" />
    </div>
    
    <br>
    Hi “saad11”,
    <br>
    Thank you for joining Perfit and we hope you enjoy using the App. Perfit App will help you find the best Personal Trainer or Dietitian who will prepare a customized fitness program that fits your goals for a healthier & fit body.
    <br>
    Please check the list of Trainers & Dietitians on the App and choose the best Trainer/Dietitian, chat with them for more details about their packages and discuss your fitness goals, subscribe to the package that suits you and start the fun!
    <br>
    Stay in touch! Follow our Instagram page to stay up to date and check our blog on Perfitapp.com
    ';
    return view('mails.general' , $data);
    });
    
    Route::get('email-test' , function(){
    
        $users = User::all()->take(1);
        $subject = "Testing Queue and emails";
        $body = "Testing Queue and emails";
        dispatch(new SendEmailTest($users , $subject , $body ));

    });


Route::get('search-exercises/{query}' , 'EXRXController@search' );
Route::get('find-exercises/{ids}' , 'EXRXController@findExercises' );