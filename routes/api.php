<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('send-mail' , 'Api\AuthController@sendEmail' );

// auth
Route::post('login' , 'Api\AuthController@login');

Route::post('coach-login' , 'Api\AuthController@coachLogin');

Route::post('register' , 'Api\AuthController@register');


// Route::post('email-check/{email}' , 'Api\AuthController@checkEmail');
// Route::post('username-check/{username}' , 'Api\AuthController@checkUsername');

Route::post('check' , 'Api\AuthController@checkUsernameAndEmail');

Route::post('verify/token' , 'Api\AuthController@verifyUserToken');

Route::post('facebook-login' , 'Api\AuthController@handleFacebookLogin');
Route::post('google-login' , 'Api\AuthController@handleGoogleLogin');



// Password Reset
Route::post('reset/password/sendEmail' , 'Api\AuthController@sendPasswordLink');
Route::post('reset/password/verify' , 'Api\AuthController@verifyUserToken');
Route::post('reset/password' , 'Api\AuthController@resetPassword');






// general

Route::get('trainers/all/{user_id?}' , 'Api\DataController@getTrainers' );
Route::get('trainer/{trainer_id}/view' , 'Api\DataController@viewTrainer' );

Route::get('dietitians/all/{user_id?}' , 'Api\DataController@getDietitians' );
Route::get('dietitian/{dietitian_id}/view' , 'Api\DataController@viewDietitian' );

Route::get('packages/all' , 'Api\DataController@showPackages' );
Route::get('package/{package_id}/view/{user_id?}' , 'Api\DataController@viewPackage' );
Route::post('view-package' , 'Api\DataController@viewPackagePost' );

Route::get('workouts/all' , 'Api\DataController@showWorkouts' );
Route::get('workout/{workout_id}/view' , 'Api\DataController@viewWorkout' );

Route::get('tags/all' , 'Api\DataController@showTags' );
// Route::get('tags/coaches' , 'Api\DataController@showTagCoach' );
Route::post('tags/coaches' , 'Api\DataController@showTagCoach' );






// client app

Route::get('user/{user_id}/get/profile' , 'Api\ProfileController@getProfile');

Route::post('user/{user_id}/profile/image-update' , 'Api\ProfileController@updateClientImage');

Route::post('user/{user_id}/profile/image-update-base' , 'Api\ProfileController@updateClientImage64');

Route::get('user/{user_id}/get/client-profile' , 'Api\ProfileController@getClientProfile');

Route::post('user/{user_id}/profile/update' , 'Api\ProfileController@store');
 
Route::post('user/{user_id}/profile/details' , 'Api\ProfileController@storeProfileDetails' );

Route::post('user/{user_id}/profile/goals' , 'Api\ProfileController@storeProfileGoals' );

Route::post('user/{user_id}/profile/measurements' , 'Api\ProfileController@storeProfileMeasurements' );

Route::post('user/{user_id}/profile/weight' , 'Api\ProfileController@storeProfileWeight' );



Route::get('subscribe/{user}/coach/{coach}/package/{package}' , 'Api\ApiPackageController@subscribe' );


Route::post('handle-client-request/{user}/coach/{coach}/package/{package}' , 'Api\ApiPackageController@handleClientRequest' );

Route::get('make-request/{user}/coach/{coach}/package/{package}' , 'Api\ApiPackageController@makeRequest' );

Route::get('subscription-check/{user}/coach/{coach}/package/{package}' , 'Api\ApiPackageController@checkSubscription' );

Route::get('unsubscribe/{user}/coach/{coach}/package/{package}' , 'Api\ApiPackageController@unsubscribe' );




Route::post('review/create' , 'Api\ReviewController@store' );


Route::get('client/{client}/subscriptions' , 'Api\ClientController@getSubscriptions' );





Route::get('client/questionnaire' , 'Api\ApiClientQuestionnaireController@storeAnswer' );







// web routes
Route::get('get_all_data' , 'Api\ServerDataController@getAllDataForPlan' );

Route::get('start_plan/{plan_id}/coach/{coach_id}' , 'Api\ApiPlanController@startPlanNow' );
// Route::get('start_plan_now/plan/{plan_id}' , 'Api\ApiPlanController@startPlanNow' );

Route::get('get_plan/client/{client_id}/coach/{coach_id}/package/{package_id}' , 'Api\ApiPlanController@get_plan' );


Route::get('get_plan_details/{plan_id}' , 'Api\ApiPlanController@getPlanDetails' );

Route::get('get/plan/{plan_id}/day/{date}' , 'Api\ApiPlanController@getASingleDay' );

Route::get('complete-exercise/{exercise_id}/day/{day_id}' , 'Api\ApiPlanController@completeExerciseInDay' );

Route::post('payment/client/{client_id}/coach/{coach_id}/package/{package_id}' , 'Api\ClientPaymentController@makePayment' );

// payment using credit card paypal
Route::post('paypal-payment' , 'Api\ApiPlanController@PayPalPayment' );

Route::post('payment/{plan_id}' , 'Api\ClientPaymentController@makePaymentPost' );

Route::get('payment/remove/{payment_id}'  , 'Api\ClientPaymentController@removePayment' );


Route::get('payment-via-paypal/client/{client_id}/coach/{coach_id}/package/{package_id}' , 'Api\ApiPlanController@payForPlan' );

Route::post('store_day_data' , 'Api\ApiPlanController@storeDayData' );

Route::post('save_measurements' , 'Api\ApiPlanController@storeDayMeasurements' );


Route::post('payment_via_paypal' , 'Api\ApiPlanController@PayForPlan1' );
// Route::post('payment_via_paypal' , 'Api\ApiPlanController@payForPlan' );











// chat routes

Route::get('chat/{sender}/get/{reciever}' , 'Api\ChatController@index' );
Route::post('chat/store' , 'Api\ChatController@store' );
Route::post('chat/store/file' , 'Api\ChatController@storeFile' );
Route::post('chat/store/filebase64' , 'Api\ChatController@storeFileBase64' );

Route::get('chat/download/file/{id}' , 'Api\ChatController@downloadChatFile' );

Route::get('chat/contacts' , 'Api\ChatController@getContacts' );

Route::get('user/contacts/{user_id}' , 'Api\ChatController@getActiveChat');

Route::get('get/new-messages/{user_id}' , 'Api\ChatController@getUnreadMessages');

Route::get('get/new-messages-admin/' , 'Api\ChatController@getUnreadMessagesAdmin');

Route::post('save/token' , 'Api\ChatController@saveToken' );









Route::get('client/{user_id}/questionnaire/{q_id}/get' , 'Api\ApiClientQuestionnaireController@getQuestionnaire');
Route::post('questionnaire-save' , 'Api\ApiClientQuestionnaireController@storeAnswer');
Route::post('client-questionnaires-update' , 'Api\ApiClientQuestionnaireController@update');



// coach routes

Route::get('coach/{id}' , 'Api\CoachController@getCoachData');
Route::get('coach/{id}/details' , 'Api\CoachController@getCoachDetails');

Route::get('coach/{id}/subscribers' , 'Api\CoachController@getCoachSubscribers');

Route::get('coach/{id}/clients' , 'Api\CoachController@getCoachClients');
Route::get('coach/{id}/new-requests' , 'Api\CoachController@getCoachNewRequests');

Route::post('coach/{user_id}/profile/update' , 'Api\CoachController@updateProfile');

Route::get('coach/contacts/{user_id}' , 'Api\ChatController@getActiveCoachChat');




// 
Route::get('get/day-data/{id}', 'Api\ApiPlanController@getDayData' );
Route::post('create-day', 'Api\ApiPlanController@createDay' );
Route::post('save-workouts', 'Api\ApiPlanController@saveWorkouts' );
Route::post('save-meals', 'Api\ApiPlanController@saveMeals' );
Route::post('save-supplements', 'Api\ApiPlanController@saveSupplements' );