
@extends('layouts.dashboard')


@section('content')


<div class="row">

   @include('layouts.partials.swals')

<div class="col-md-12" style="padding:30px" id="vue" >
    <div class="card ">
      <div class="card-header">
        <h3 class="card-title">Manage Questionnaires</h3>
        {{--  <h3 class="card-title">Manage Questionnaires</h3>  --}}
        <a class="btn btn-outline-primary float-right" href="{{route('coach.client-questionnaires.create')}}"> <i class="fas fa-plus"></i> Create</a>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >

        <client-questionnaires :clients="{{$clients}}" :current_questionnaires="{{$questionnaires}}" ></client-questionnaires>

      {{--  <table class="table">
          <tr>
              <th>#</th>
              <th>Name</th>
              <th>Assign To</th>
              <th>Share</th>
              <th>Create Date</th>
              <th>Actions</th>
          </tr>
          @if(isset($questionnaires))
          @foreach($questionnaires as $k => $questionnaire)
          <tr>
              <td> {{$k+1}} </td>
              <td> {{$questionnaire->name}} </td>
              <td>
                  <a class="btn btn-sm btn-success" @if($questionnaire->users()->count() == 0 ) disabled @endif href="{{route('coach.client-questionnaires.assigned-clients', $questionnaire->id)}}"> View ({{$questionnaire->users()->count()}}) </a>
                </td>
              
                <td> 
                  <div class="btn-group">
                    <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-share-alt"></i> Share
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="#">Action</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                </td>

              <td> {{$questionnaire->created_at->format('m/d/Y') }} </td>
              <td>
                
                <a href="javascript:;" onclick="AskBeforeDelete({{$questionnaire->id}})" class="text-danger"  > <i class="fas fa-trash"></i> </a>

                <a href="{{route('coach.client-questionnaires.edit' , $questionnaire->id)}}" > <i class="fas fa-edit"></i> </a>

                <a href="{{route('coach.client-questionnaires.show' , $questionnaire->id)}}" class="btn btn-sm btn-primary" > <i class="fas fa-eye"></i> View</a>


              </td>

              <form id="delete-form-{{$questionnaire->id}}" action="{{route('coach.client-questionnaires.destroy' , $questionnaire->id)}}" method="POST" >
                  @csrf
                  @method('DELETE')
              </form>
          </tr>
          @endforeach
          @endif
      </table>  --}}
    </div>
    </div>
  
  </div>
</div>

@endsection

@section('scripts')
    <script>
        function AskBeforeDelete(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
              }).then((result) => {
                if (result.value) {
                  
                    $('#delete-form-'+id).submit();
                }
              })
        }
    </script>
@endsection