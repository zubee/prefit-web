@extends('layouts.dashboard')


@section('content')


    <div class="row">

        @include('layouts.partials.swals')

        <div class="col-md-12" style="padding:30px" >
            <div class="card ">
                <div class="card-header">
                    <h3 class="card-title"><b>{{$questionnaire->question}}</b> Questionnaire Assigned To</h3>
                </div>
                <!-- /.card-header -->
                <div style="padding:30px" >
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>User Name</th>
                            <th>Photo</th>
                            <th>Email</th>
                            <th>Assigned Date</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($questionnaire))
                            @foreach($questionnaire->users as $k=> $user)
                                <tr id="user-list">
                                    <td>{{$k+1}}</td>
                                    <td> {{$user->name}} </td>
                                    <td> <img class="profile-picture" src="@if($user->photo) {{asset('images/profile_images/'.$user->photo)}} @else {{asset('img/no-image-found.jpg')}} @endif"> </td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->pivot->created_at->toDateString()}}</td>
                                    <td>
                                        
                                        <a href="{{route('coach.client.questionnaires.show' , [$user->id , $questionnaire->id ])}}" class="btn btn-sm btn-primary">View</a>
                                    </td>
                                </tr>
                        </tbody>
                        @endforeach
                        @endif
                    </table>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
    <script>

    </script>
@endsection




