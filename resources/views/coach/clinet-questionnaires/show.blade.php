@extends('layouts.dashboard')


@section('content')


<div class="row">

   @include('layouts.partials.swals')

<div class="col-md-12" style="padding:30px" >
    <div class="card card-info ">
      <div class="card-header">
        <h3 class="card-title">View Questions</h3>
      </div>
      <!-- /.card-header -->

    <div style="padding:30px" >

      <h4>{{$client_questionnaire->name}}</h4>
        <div>
        @foreach($client_questionnaire->questions as $k => $question)
        <div class="ml-3">
        <label class="col-form-label">Q {{$k+1}} : {{$question->question}} </label>
        
        <div>
            Type
            <label class="col-form-label">{{$question->type}}</label>
        </div>
        @if($question->type != 'text')
        @foreach($question->choices as $choice)

        <input class="ml-5" @if($question->type == 'checkbox') type="checkbox" @else type="radio" @endif disabled />
        <label class=" col-form-label">{{$choice->name}}</label>
        <br>
        @endforeach
        @endif
    </div>
    <hr>
        @endforeach
    </div>
    </div>

    </div>
  
  </div>
</div>

@endsection


@section('scripts')
    <script>
        function reIndex(){
            $('.question').each(function (k,v) {
                $(this).children('.row').children('.col-sm-6').children('#question').attr('name', 'question['+k+'][name]');
                $(this).children('.row').children('.col-sm-6').children('.question_type').attr({'name':'question['+k+'][type]','id':'question_type'+k+'','onChange':'toggle_choices('+k+')'});
                $(this).children('.row').children('.col-sm-2').children('button').attr({'id':'add_button'+k+'', 'onClick':'add_new_row('+k+')'});
                $(this).children('.row').children('.col-sm-4').children('.choice').attr({'name':'question['+k+'][choices][]'});
            });
        }

        var i = 0;
        function add_new_row(id){
            i += 1;

            $('#question-'+id).append('<div id="choice-'+i+'" class="form-group row"> <div class="col-sm-4"> <label class="row float-right col-form-label ">Choice :</label> </div> <div class="col-sm-4"> <input type="text" class="form-control choice" placeholder="Choice" name="question['+id+'][choices][]" required>  </div>  <div  class="col-sm-2"> <button onclick="remove_row(this.id)" id="'+i+'" type="button" style="height: 35px;" class="btn btn-outline-danger"> <i class="fas fa-minus"></i> </button> </div> </div>')
        }


        function remove_row(row_id){
            $('#choice-'+row_id).remove();
        }
        function toggle_choices(question_id){
            if($('#question_type'+question_id).val() == 'text' ){
                $('#choices_area div').remove();
                $('#add_button').hide();
                i = 0;

            }else{
                $('#add_button'+question_id).show();
            }
        }


        var question =  $('.question').length;

        function add_new_question() {
            question+=1;

            $('#question_area').append('<div id="question-'+question+'" class="question" > <div class="form-group row"> <div class="col-sm-3"><label for="question" class="row float-right col-form-label ">Question '+question+':</label></div>\n' +
                '<div class="col-sm-6"><input type="text" class="form-control" id="question" placeholder="Name" name="question['+question+'][name]" value="" required></div> ' +
                '<div  class="col-sm-2"> <button onclick="remove_question(this.id);" id="'+question+'" type="button" style="height: 35px;" class="btn btn-outline-danger"> <i class="fas fa-minus"></i> </button> </div> </div>'+
                '<div class="form-group row">\n' +
                '<div class="col-sm-3">\n' +
                '<label for="inputEmail1" class="row float-right col-form-label ">Type:</label>\n' +
                '</div>\n' +
                '<div class="col-sm-6">\n' +
                '     <select class="form-control question_type" id="question_type'+question+'" name="question['+question+'][type]" onchange="toggle_choices('+question+')" >\n' +
                '            <option value="text" selected >Text</option>\n' +
                '            <option value="checkbox">CheckBoxes</option>\n' +
                '             <option value="radiobutton">RadioButtons</option>\n' +
                '      </select>\n' +
                '</div>\n' +
                '<div class="col-sm-2">\n' +
                '     <button type="button"style="height: 35px; display:none; " id="add_button'+question+'"  onclick="add_new_row('+question+')" class="btn btn-outline-success"> <i class="fas fa-plus"></i> </button>\n' +
                ' </div>\n' +
                ' </div></div>\n' +
                '          ')


        }

        function remove_question(question_id) {

            $('#question-'+question_id).remove();
        }

        function beforeSubmit() {

            reIndex();
            $('#form-submit').submit();

        }


    </script>
@endsection