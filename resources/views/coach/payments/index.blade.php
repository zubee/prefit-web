@extends('layouts.dashboard')


@section('content')


<div class="row">

   @include('layouts.partials.swals')


   <div class="col-12 col-sm-12 col-lg-12" style="padding:20px">
    
    <div class="bg-white p-3 mb-3" style="border-radius:10px;">
  
      <form action="{{route('coach.financial-reports.filter')}}" class="form-horizontal">
    <div class="row">
      <div class="offset-1 col-2">
        <label class="col-form-label">Client Name</label>
        <input type="text" class="form-control" placeholder="Client Name" name="client_name" @if(isset($filters['client_name'])) value="{{$filters['client_name']}}" @endif />
      </div>
      
    
      <div class="col-2">
        <label class="col-form-label">Lower Date</label>
  
        <input type="text" class="form-control"  onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Lower Date" name="lower_date" @if(isset($filters['lower_date'])) value="{{$filters['lower_date']}}" @endif />
      </div>
      
     
      <div class="col-2">
        <label class="col-form-label">Upper Date</label>
  
        <input type="text" class="form-control"  onfocus="(this.type='date')" onblur="(this.type='text')" placeholder="Upper Date" name="upper_date" @if(isset($filters['upper_date'])) value="{{$filters['upper_date']}}" @endif />
      </div>
      <div class="col-2">
        <label class="col-form-label">Status</label>
  
        <select class="form-control" name="status" >
            <option value="">Select Payment Status</option>
            <option value="1"  @if(isset($filters['status']) && $filters['status'] == 1 ) selected @endif >Paid</option>
            <option value="0"  @if(isset($filters['status']) && $filters['status'] == 0 ) selected @endif  >UnPaid</option>
          </select>      
        </div>  
      
      <div class="col-2" style="margin-top:38px">
  
        <button type="submit" class="btn btn-outline-primary" > <i class="fa fa-search"></i> Filter</button>
        <a href="{{route('coach.financial-reports')}}" class="btn btn-outline-danger">X</a>
      </div>
    </div>
  </form>
    </div>
    </div>

    @if(!isset($filterd_payments))
    
    <div class="col-md-12" style="" >
    <div class="card card-info card-tabs p-0">
        <div class="card-header p-0 pt-1">
          <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
            <li class="nav-item text-center" style="width:50%;">
              <a  class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">New Payments</a>
            </li>
            <li class="nav-item text-center" style="width:50%;">
              <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Paid</a>
            </li>
         
          </ul>
        </div>
        <div class="card-body p-0">
          <div class="tab-content" id="custom-tabs-one-tabContent">
            <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
              <table class="table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Amount</th>
                    <th>Percentage</th>
                    <th>Payable Amount</th>
                    <th>Create Date</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($payments))
                @foreach($payments->where('status' , 0) as $k => $payment)
                <tr>
                    <td> {{$k+1}} </td>
                    <td> {{$payment->client->name}} </td>
                    <td>{{$payment->amount}} $</td>
                    <td>{{$payment->coach_percentage}} % </td>
                    <td>{{ ( $payment->amount / 100) * $payment->coach_percentage }} $ </td>
                    <td> {{$payment->created_at->format('m/d/Y') }} </td>
                   
                </tr>
                </tbody>
                @endforeach
                @endif
            </table>          
              </div>
            <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Amount</th>
                        <th>Percentage</th>
                        <th>Payable Amount</th>
                        <th>Create Date</th>
                        <th>Paid Date</th>

                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($payments))
                    @foreach($payments->where('status' , 1) as $k => $payment)
                    <tr>
                        <td> {{$k+1}} </td>
                        <td> {{$payment->client->name}} </td>
                        <td>{{$payment->amount}} $</td>
                        <td>{{$payment->coach_percentage}} % </td>
                        <td>{{ ( $payment->amount / 100) * $payment->coach_percentage }} $ </td>
                        <td> {{$payment->created_at->format('m/d/Y') }} </td>
                        <td> {{$payment->payment_clear_date }} </td>
                  </tr>
                    </tbody>
                    @endforeach
                    @endif
                </table>
              
              </div>
          </div>
        </div>
        <!-- /.card -->
      </div>
      </div>
      @endif








      @if(isset($filterd_payments))


      <div class="col-md-12" style="" >

        <div class="card card-warning card-tabs p-0">
            <div class="card-header p-0 pt-1">
            </div>
            <div class="card-body p-0">
              <div class="tab-content" id="custom-tabs-one-tabContent">
                <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
                    <table class="table">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Amount</th>
                            <th>Percentage</th>
                            <th>Payable Amount</th>
                            <th>Create Date</th>
                            <th>Paid Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($filterd_payments as $k => $payment)
                        <tr>
                          <td> {{$k+1}} </td>
                          <td> {{$payment->client->name}} </td>
                          <td>{{$payment->amount}} $</td>
                          <td>{{$payment->coach_percentage}} % </td>
                          <td>{{ ( $payment->amount / 100) * $payment->coach_percentage }} $ </td>
                          <td> {{$payment->created_at->format('m/d/Y') }} </td>
                          <td>  
                            
                              @if($payment->status == 1) <a href="#" class="btn btn-sm btn-success" >{{$payment->payment_clear_date }}</a>
                              @else 
                              <a href="#" class="btn btn-sm btn-danger" >Not Paid</a>
                              @endif
                          
                          </td>
                        </tr>
                        </tbody>
                        @endforeach
                    </table>          
                  </div>
                
              </div>
            </div>
            <!-- /.card -->
          </div>
          </div>
          @endif
      

      
</div>

@endsection

@section('scripts')
    <script>
        function AskBeforeDelete(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {

                    $('#delete-form-'+id).submit();
                }
            })
        }

        function change_user_status(id){
            $.ajax({
                url: '{{url("/")}}/change/user/status/'+id,
                success: function (res){
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'User Status Has been updated',
                        showConfirmButton: false,
                        timer: 1500
                      })
                }
            }).fail(function(){
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Some Problem occured',
                    showConfirmButton: false,
                    timer: 1500
                  })
            })
        }
    </script>
@endsection




