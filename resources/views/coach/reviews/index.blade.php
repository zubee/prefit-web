@extends('layouts.dashboard')


@section('styles')
    <style>
        .pagination{
            justify-content: center;
        }
    
    </style>
@endsection

@section('content')

<div class="row mt-4">
    <div class="col-12">
        <div id="accordion">
            <div class="card card-info">
              <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                    <div class="review-top">
                        <div>
                            All Reviews
                        </div>
                        <div class="review-count float-right">
                            {{$reviews->count()}}
                        </div> 
                    </div>
                </div>
                </h5>
          
                <div class="card-body">

                    @if($reviews && $reviews->count() > 0)
                    @foreach ($reviews as $review)
                        
                    <div class="review-head">
                        <div class="review-user-image-box">
                            @if($review->user && $review->user->photo)
                            <img class="review-user-image" src="{{asset('images/profile_images/'.$review->user->photo)}}" />
                            @else
                            <img class="review-user-image" src="{{asset('img/no-image-found.jpg')}}" />
                            @endif
                        </div>

                        <div class="rating">
                            <div class="review-user-name"> {{$review->user->name}} </div>
                            <div class="results">
                                <div class="results-content">
                                  <span class="stars">{{$review->rating}}</span> 
                                </div>
                              </div>
                        </div>
                        <div class="date">
                            {{$review->proper_date}}
                        </div>
                    </div>
                    <div class="review-body">
                        <div class="review-description"> {{$review->description}} </div>
                    </div>
                    <hr>
                    @endforeach
                    @else
                    <div class="alert alert-info">No Reviews </div>
                    @endif
                </div>
                {{$reviews->links()}}
              </div>            
          </div>
    </div>
</div>
  

@endsection

@section('scripts')
    
@endsection




