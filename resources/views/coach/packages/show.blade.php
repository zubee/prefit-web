@extends('layouts.dashboard')


@section('styles')
    <style>
        .card-info{
        }
    
    </style>
@endsection

@section('content')


<div class="row">


<div class="col-md-12" style="padding:30px" >
    <div class="card ">
      <div class="card-header">
        <h3 class="card-title">View Package</h3>
        <a class="btn btn-outline-primary float-right" href="#"> <i class="fas fa-calendar"></i> {{$package->created_at->format('m/d/Y') }}  </a>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
        
        
        <img class="mb-2" src="@if($package->image) {{asset('images/packages/'.$package->image)}} @else {{asset('img/no-image-found.jpg')}} @endif" style="width: auto; height: auto; max-width: 200px; max-height: 200px;" />
        <hr>
      
        <h4> {{$package->name}} </h4>

        <label class="col-form-label">Duration: </label> {{ $package->duration }} Days
            
        <br>
        <label class="col-form-label">Description</label>
        <div>
            {!! $package->description !!}
        </div>
        
        <label class="col-form-label">Highlights</label>
        <div>
            {!! $package->highlights !!}
        </div>

        <label class="col-form-label">@if($package->paid) Price: {{$package->price}}  @else Free @endif </label>
        
           
    </div>


</div>
<div class="row">
    <div class="col-12">
        <div id="accordion">
            <div class="card card-info">
              <div class="card-header" id="headingOne">
                <h5 class="mb-0">
                  <div class="toggle-btn" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    <div class="review-top">
                        <div>
                            {{ auth()->user()->name . ' Reviews '}} 
                        </div>
                        <div class="review-count">
                            {{$package->reviews->count()}}
                        </div> 
                    </div>
                </div>
                </h5>
              </div>
          
              <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">

                    @if($package && $package->reviews && $package->reviews->count() > 0)
                    @foreach ($package->reviews as $review)
                        
                    <div class="review-head">
                        <div class="review-user-image-box">
                            @if($review->user && $review->user->photo)
                            <img class="review-user-image" src="{{asset('images/profile_images/'.$review->user->photo)}}" />
                            @else
                            <img class="review-user-image" src="{{asset('img/no-image-found.jpg')}}" />
                            @endif
                        </div>

                        <div class="rating">
                            <div class="review-user-name"> {{$review->user->name}} </div>
                            <div class="results">
                                <div class="results-content">
                                  <span class="stars">{{$review->rating}}</span> 
                                </div>
                              </div>
                        </div>
                        <div class="date">
                            {{$review->proper_date}}
                        </div>
                    </div>
                    <div class="review-body">
                        <div class="review-description"> {{$review->description}} </div>
                    </div>
                    <hr>
                    @endforeach
                    @else
                    <div class="alert alert-info">No Reviews </div>
                    @endif
                </div>
              </div>
            </div>
            
            
          </div>
          
    </div>
</div>
  
  </div>
</div>

@endsection

@section('scripts')
    <script>
        function AskBeforeDelete(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {

                    $('#delete-form-'+id).submit();
                }
            })
        }

        function change_user_status(id){
            $.ajax({
                url: '{{url("/")}}/change/user/status/'+id,
                success: function (res){
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'User Status Has been updated',
                        showConfirmButton: false,
                        timer: 1500
                      })
                }
            }).fail(function(){
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Some Problem occured',
                    showConfirmButton: false,
                    timer: 1500
                  })
            })
        }
    </script>
@endsection




