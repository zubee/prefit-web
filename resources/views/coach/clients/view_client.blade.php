

@extends('layouts.dashboard')


@section('styles')
    <style>
     
    </style>
@endsection

@section('content')


<div class="row">


    {{-- @dd($client ,$client->dietitian)  --}}
  

<div class="col-md-12" style="padding:30px" >
    <div class="card card-info ">
      <div class="card-header">
        <h3 class="card-title">{{$client->name}} Profile</h3>
        {{--  <a href="{{route('trainer.profile.edit')}}" class="btn btn-success float-right" > <i class="fas fa-edit"></i> Edit </a>  --}}
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      
        
        <div class="ml-3">

            @if($client->photo)
                <img  src="{{asset('images/profile_images/'.$client->photo)}} " style="width: auto; height: auto; max-height: 100px; max-width: 100px; border-radius:100%;" />
            @else
                <img  src="{{asset('img/no-image-found.jpg')}} " style="width: auto; height: auto; max-height: 100px; max-width: 100px; border-radius:100%;" />

            @endif

              <a class="btn btn-outline-teal btn-sm float-right" href="{{route('coach.messages' , $client->id)}} " > <i class="fas fa-envelope f-20"></i></a>

            {{--  <div class="mt-3"></div>  --}}
            <hr>
            <div class="row">
                <div class="col-4 col-sm-12 col-md-4 col-lg-4">
                    <div class="">
                        Name :
                        <label> <b> {{$client->name}} </b> </label>
                    </div>
                    
                    <div class="">
                        Email :
                        <label> <b> {{$client->email}} </b> </label>
                    </div>
                    
                    <div class="">
                        Joined :
                        <label> <b> {{$client->created_at->toDateString()}} </b> </label>
                    </div>
                    
                    <div class="">
                        Status :
                        @if($client->status == 1)
                          <label><span class="badge badge-success">Active</span></label>
                          @else
                          <label><span class="badge badge-danger">Deactive</span></label>
                        @endif
                    </div>
                    <div class="">
                      Gender :
                      <label> <b> {{ ucwords($client->profile->gender) ?? ""}} </b> </label>
                    </div>
                </div>
                <div class="col-4 col-sm-12 col-md-4 col-lg-4">
                    
                    
                    <div class="">
                        Date Of Birth:
                        <label> <b> {{$client->profile->date_of_birth ?? "" }} </b> </label>
                    </div>
                    
                    <div class="">
                        Height :
                        <label> <b> {{$client->profile->height ?? "" }} </b> </label>
                    </div>
                    
                    <div class="">
                        Current Weight :
                        <label> <b> {{$client->profile->current_weight ?? "" }} </b> </label>
                    </div>
                    
                    <div class="">
                        Starting Weight :
                        <label> <b> {{$client->profile->starting_weight ?? "" }} </b> </label>
                    </div>

                    <div class="">
                      Current Weight :
                      <label> <b> {{$client->profile->current_weight ?? "" }} </b> </label>
                    </div>
                  </div>
                
                
                <div class="col-4 col-sm-12 col-md-4 col-lg-4">
                    <div class="">
                        Starting Weight :
                        <label> <b> {{$client->profile->starting_weight ?? "" }} </b> </label>
                    </div>
                    
                    <div class="">
                        Target Weight :
                        <label> <b> {{$client->profile->target_weight ?? "" }} </b> </label>
                    </div>
                    
                    <div class="">
                        Exercise Level :
                        <label> <b> {{ ucwords($client->profile->exercise_level) ?? "" }} </b> </label>
                    </div>   
                    
                    <div class="">
                        Goals :
                        @if($client->profile && $client->profile->goals && is_array($client->profile->goals) )
                        @foreach($client->profile->goals as $goal)
                            <label> <b> {{$goal}} </b> </label>
                        @endforeach
                        @endif
                    </div>
                    
                    <div class="">
                        Workouts:
                        @if($client->profile && $client->profile->workout && is_array($client->profile->workout))
                        @foreach($client->profile->workout as $workout)
                            <label> <b> {{$workout}} </b> </label>,
                        @endforeach
                        @endif
                    </div>
                    
                    
                </div>

                
                @if($client->profile && $client->profile->measurements)
                @php
                    $client->profile->measurements = json_decode($client->profile->measurements)
                @endphp
                <div class="col-12">
                    
                    <hr>
                    <h3>Measurements</h3>

                    <div>
                      <div class="row">

                        <div class="col-3">
                          Head
                        </div>
                        
                        <div class="col-3">
                          {{$client->profile->measurements->head ?? ''}}
                        </div>


                        <div class="col-3">
                          Chest
                        </div>
                        
                        <div class="col-3">
                          {{$client->profile->measurements->chest ?? ''}}
                        </div>

                      </div>

                      <div class="row">
                        <div class="col-3">
                          Left Bicep
                        </div>
                        
                        <div class="col-3">
                          {{$client->profile->measurements->left_bicep ?? ''}}
                        </div>
                        
                        <div class="col-3">
                          Right Bicep
                        </div>
                        
                        <div class="col-3">
                          {{$client->profile->measurements->right_bicep ?? ''}}
                        </div>
                      </div>
                           


                      <div class="row">
                        <div class="col-3">
                          Left Forearm
                        </div>
                        
                        <div class="col-3">
                          {{$client->profile->measurements->left_forearm ?? ''}}
                        </div>
                        
                        <div class="col-3">
                          Right Forearm
                        </div>
                        
                        <div class="col-3">
                          {{$client->profile->measurements->right_forearm ?? ''}}
                        </div>
                      </div>
                           

                      <div class="row">
                        <div class="col-3">
                          Left Calf
                        </div>
                        
                        <div class="col-3">
                          {{$client->profile->measurements->left_calf ?? ''}}
                        </div>
                        
                        <div class="col-3">
                          Right Calf
                        </div>
                        
                        <div class="col-3">
                          {{$client->profile->measurements->right_calf ?? ''}}
                        </div>
                      </div>
                           
                      <div class="row">
                        <div class="col-3">
                          Left Thigh
                        </div>
                        
                        <div class="col-3">
                          {{$client->profile->measurements->left_thigh ?? ''}}
                        </div>
                        
                        <div class="col-3">
                          Right Thigh
                        </div>
                        
                        <div class="col-3">
                          {{$client->profile->measurements->right_thigh ?? ''}}
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-3">
                          Hips
                        </div>
                        
                        <div class="col-3">
                          {{$client->profile->measurements->hips ?? ''}}
                        </div>

                        <div class="col-3">
                          Waist
                        </div>
                        
                        <div class="col-3">
                          {{$client->profile->measurements->waist ?? ''}}
                        </div>
                      </div>

                       
                    </div>
                </div>
                    
                @else
                <div class="col-12">
                    <hr>
                    <div class="alert alert-info">
                        <i class="fas fa-info-circle"></i> No Measurements 
                    </div>
                </div>
                @endif


            </div>
          
            <hr>
            <h4>Plans</h4>
            {{--  <div class="row">
                @if($client->plans)
                @foreach($client->plans->where('coach_id' , auth()->user()->coach->id ) as $plan)
                @if($plan->coach)
                <div class="col-4">
                    <div class="package-card">
                        <div class="package-card-body">
                            <div class="package-card-title-item"> 
                                <a href="{{route('coach.packages.view' , $plan->package->id)}}"> {{$plan->package->name}}</a>
                            </div>
                            <hr>

                            <div class="package-card-text-item">Status:
                                @if( $plan->start_status == 0) 
                                    <span class="badge badge-warning">Not Started</span>
                                @elseif($plan->is_completed == 0 && $plan->start_status == 1)
                                    <span class="badge badge-primary">Continue</span>
                                @elseif($plan->is_completed == 1 && $plan->start_status == 1 )
                                    <span class="badge badge-success">Completed</span>
                                @endif
                             </div>
                            <div class="package-card-text-item">Subscribed Date: {{$plan->created_at->toDateString()}}</div>

                            @if($plan->start_status == 1)
                            <div class="package-card-text-item">Progress: {{$plan->plan_progress}}%</div>
                            <div class="package-card-text-item">Start Date: {{$plan->start_date}}</div>
                            <div class="package-card-text-item">End Date: {{$plan->end_date}}</div>
                            @endif
                            <a class="btn btn-sm btn-outline-primary" href="{{route('coach.manage-client.package.create-plan' , [$plan->client->id , $plan->package->id ])}}">  View Plan</a>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                    @endif
                </div>  --}}


                <div class="row">
                    @if($client->plans && $client->plans->count() > 0)
                    @foreach($client->plans->where('coach_id' , auth()->user()->coach->id )  as $plan)
                    @if($plan->coach)
                    {{--  @dd($plan)  --}}
                    <div class="col-4">
                        <div class="package-card">
                            {{--  <img class="package-card-img" src="{{asset('images/packages/'.$plan->package->image)}}" alt="Card image cap">  --}}
                            <div class="package-card-body">
                                <div class="package-card-title-item"> 
                                    <a href="{{route('coach.packages.view' , $plan->package->id)}}"> {{$plan->package->name}}</a>
                                </div>
                                <hr>
                                <div class="package-card-text-area">
                                <div class="package-card-text-item">Status:
                                    @if( $plan->start_status == 0) 
                                        <span class="badge badge-warning">Not Started</span>
                                    @elseif($plan->is_completed == 0 && $plan->start_status == 1)
                                        <span class="badge badge-primary">In-Progress</span>
                                    @elseif($plan->is_completed == 1 && $plan->start_status == 1 )
                                        <span class="badge badge-success">Completed</span>
                                    @endif
                                 </div>
                                <div class="package-card-text-item">Subscribed Date: <b> {{$plan->created_at->toDateString()}} </b> </div>
    
                                @if($plan->start_status == 1)
                                <div class="package-card-text-item">Progress:  <b>{{$plan->plan_progress}}%</b></div>
                                <div class="package-card-text-item">Start Date: <b> {{$plan->start_date}}</b></div>
                                <div class="package-card-text-item">End Date: <b>{{$plan->end_date}}</b></div>
                                @endif
                                <a class="mt-2 btn btn-sm btn-outline-primary" href="{{route('coach.manage-client.package.create-plan' , [$plan->client->id , $plan->package->id ])}}">  View Plan</a>
                            </div>
                            </div>
                            </div>
                        </div>
                        @endif
                    @endforeach
                    @else
                    <span class="text-center alert alert-success w-100">No Plans Subscribed</span>
                    @endif
                
                {{--  @if($client->subscribed_packages)
                @foreach($client->subscribed_packages as $package)
                @dd($package)
                <div class="col-3">
                    <div class="card">
                        <img class="card-img-top" src="{{asset('images/packages/'.$package->image)}}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">{{$package->name}}</h5>
                            <h5 class="card-title">By: {{$package->name}}</h5>
                          <p class="card-text"> {{$}} </p>
                        </div>
                      </div>
                    </div>
                @endforeach
                @endif  --}}
            </div>
            
              
        
    </div>

    </div>
    </div>
  
  </div>
</div>

@endsection

@push('script')

    <script>
        var tagnames = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: '{{route('tagnames')}}'
        });

        tagnames.initialize();

        $('#tags').tagsinput({
            typeaheadjs: {
                name: 'tagnames',
                displayKey: 'name',
                valueKey: 'name',
                source: tagnames.ttAdapter()
            },
            freeInput : false
        });

    </script>

    @endpush