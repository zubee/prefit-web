
@extends('layouts.dashboard')

@section('styles')
  
  <link rel="stylesheet" href="{{ asset('admin-plugins/toastr/toastr.min.css')}}">


  <style>
    .StripeElement {
      box-sizing: border-box;
    
      height: 40px;
    
      padding: 10px 12px;
    
      border: 1px solid transparent;
      border-radius: 4px;
      background-color: white;
    
      box-shadow: 0 1px 3px 0 #e6ebf1;
      -webkit-transition: box-shadow 150ms ease;
      transition: box-shadow 150ms ease;
    }
    
    .StripeElement--focus {
      box-shadow: 0 1px 3px 0 #cfd7df;
    }
    
    .StripeElement--invalid {
      border-color: #fa755a;
    }
    
    .StripeElement--webkit-autofill {
      background-color: #fefde5 !important;
    }
    #card-element{
      width: 100%;
    }
    
button {
  border: none;
  border-radius: 4px;
  outline: none;
  text-decoration: none;
  color: #fff;
  background: #32325d;
  white-space: nowrap;
  display: inline-block;
  height: 40px;
  line-height: 40px;
  padding: 0 14px;
  box-shadow: 0 4px 6px rgba(50, 50, 93, .11), 0 1px 3px rgba(0, 0, 0, .08);
  border-radius: 4px;
  font-size: 15px;
  font-weight: 600;
  letter-spacing: 0.025em;
  text-decoration: none;
  -webkit-transition: all 150ms ease;
  transition: all 150ms ease;
  float: left;
  margin-left: 12px;
}

button:hover {
  transform: translateY(-1px);
  box-shadow: 0 7px 14px rgba(50, 50, 93, .10), 0 3px 6px rgba(0, 0, 0, .08);
  background-color: #43458b;
}







  
</style>
{{--  .results {
  font-size: 0;
  padding-bottom: 16px;
}
.results-content {
  font-size: 13px;
  display: inline-block;
  margin-left: 20px;
  vertical-align: top;
  background: url('https://i.stack.imgur.com/rwkqF.png') 0 0 repeat-x;
  width: 185px;
  height: 35px;
}
.results .results-content span.stars span {
  background: url('https://i.stack.imgur.com/rwkqF.png') 0 -36px repeat-x;
  display: inline-block;
  height: 35px;
}  --}}


  {{--  <script src="https://js.braintreegateway.com/web/3.64.2/js/client.min.js"></script>  --}}


@endsection

@section('content')


<div class="row">

   @include('layouts.partials.swals')



   
   <div class="col-md-12" style="padding:30px" >
    <div class="card ">
      <div class="card-header">
        <h3 class="card-title"> <b> {{$client->name}} </b> Packages</h3>
        {{-- <a class="btn btn-outline-primary float-right" href="{{route('admin.packages.create')}}"> <i class="fas fa-plus"></i> Create</a> --}}
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
        
        <div id="paypal-credit-button"></div>
        

      <table class="table">
          <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Price</th>
            <th>Duration</th>
            <th>Create Date</th>
            <th>Review</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Actions</th>
          </tr>
          </thead>
          <tbody>
            
            @foreach( $packages as $k => $package)

            @php
                $plan = $package->plans->where('coach_id' , auth()->user()->coach->id )->where('client_id' , $client->id )->first();
            @endphp
            <tr>
                <td> {{$k+1}} </td>
                <td> {{$package->name}} </td>
                @if($package->paid)
                    <td>{{$package->price}}$</td>
                @else
                    <td><span class="badge-success badge" >FREE</span></td>
                @endif
                <td> {{$package->duration}}-Days </td>
  
                <td> {{$package->pivot->created_at->format('m/d/Y') }} </td>
                  <td> 
                    
                    @if($plan && $plan->review)
                    <div class="results" onclick="showReview({{$plan->review}})">
                      <div class="results-content">
                        <span class="stars"> {{(float)$plan->review->rating}}</span> 
                      </div>
                    </div>
                    @else
                    <div class="results">
                      <div class="results-content">
                        <span class="stars">0</span> 
                      </div>
                    </div>
                    @endif

                  </td>
                <td> 
                  @if($plan)

                    @if($plan->start_status == 1)
                    {{ $plan->start_date }} 
                    @else
                    Not Started
                    @endif
                    
                    @endif
                    
                  </td>
                  <td>
                    @if($plan && $plan->start_status == 1)
                    {{ Carbon\Carbon::parse($plan->start_date)->addDays($package->duration)->toDateString() }}  @endif </td>
                 <td>

                    @if($plan)
                    <a class="btn btn-outline-success btn-sm" href="{{route('coach.manage-client.package.create-plan' , [ $client->id , $package->id] )}} " > <i class="fas fa-edit"></i> View</a>
                    @else    
                    <a class="btn btn-outline-success btn-sm" href="{{route('coach.manage-client.package.create-plan' , [ $client->id , $package->id] )}} " > <i class="fas fa-plus-circle"></i> Create</a>
                    @endif
            </tr>
            </tbody>
            @endforeach

      </table>
    </div>
    </div>
  
  </div>
</div>

  <!-- Modal -->
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Show Review</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <div class="head">

            <div class="results">
              <div class="results-content">
                <span class="stars" id="review-rating"> 0 </span> 
              </div>
            </div>
            
            <div id="review-date"></div>
          </div>

          <hr>

          <div id="review-description"></div>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  


  </script>
@endsection

@section('scripts')
    
  <script type="text/javascript">


 
    
    function showReview(review){

      $('#review-description').html('');
      $('#review-date').html('');
      $('#review-rating').html('');
      setRating(0);

      $('#review-description').html(review.description);
      $('#review-date').html(review.proper_date);
      //$('#review-rating').html(parseFloat(review.rating));
      setRating(parseFloat(review.rating));

      console.log(review)
      $('#exampleModalCenter').modal('show');

    }
    

    function setRating(val){

      
      var val = parseFloat(val) 
    // Make sure that the value is in 0 - 5 range, multiply to get width
    var size = Math.max(0, (Math.min(5, val))) * 24; 
    // Create stars holder
    var $span = $('<span> </span>').width(size); 
    // Replace the numerical value with stars
    $($('#review-rating')).empty().append($span);
  }

  

</script>
@endsection



{{--      $(function() {
      console.log("Calling stars()");
      $('.results-content span.stars').stars();
    });


    const ratings = {
      hotel_a : 2.8,
      hotel_b : 3.3,
      hotel_c : 1.9,
      hotel_d : 4.3,
      hotel_e : 4.74
    };
    
    // total number of stars
    const starTotal = 5;
    
    for(const rating in ratings) {  
      const starPercentage = (ratings[rating] / starTotal) * 100;
      const starPercentageRounded = `${(Math.round(starPercentage / 10) * 10)}%`;
      document.querySelector(`.${rating} .stars-inner`).style.width = starPercentageRounded; 
    }
    


    $.fn.stars = function() {
      return $(this).each(function() {
          // Get the value
          var val = parseFloat($(this).html());
          // Make sure that the value is in 0 - 5 range, multiply to get width
          var size = Math.max(0, (Math.min(5, val))) * 16;
          // Create stars holder
          var $span = $('<span />').width(size);
          // Replace the numerical value with stars
          $(this).html($span);
      });
  }
  
    $(function() {
      $('span.stars').stars();
  });



  --}}



  {{-- <script>

  var stripe = Stripe('pk_test_51HHaPcDqMeCNJddktJ45UvTWkZtMV0kPuEacvFrLQdaj3jvjcvYupOztEWuIx5TYTDayqHbe4ADaj7TqUxLagIAF00ezB76Nsz');

  // Create an instance of Elements.
  var elements = stripe.elements();
  
  // Custom styling can be passed to options when creating an Element.
  // (Note that this demo uses a wider set of styles than the guide below.)
  var style = {
    base: {
      color: '#32325d',
      fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
      fontSmoothing: 'antialiased',
      fontSize: '16px',
      '::placeholder': {
        color: '#aab7c4'
      }
    },
    invalid: {
      color: '#fa755a',
      iconColor: '#fa755a'
    }
  };
  
  // Create an instance of the card Element.
  var card = elements.create('card', {style: style});
  // Add an instance of the card Element into the `card-element` <div>.
  card.mount('#card-element');
  
  // Handle real-time validation errors from the card Element.
  card.on('change', function(event) {
    var displayError = document.getElementById('card-errors');
    if (event.error) {
      displayError.textContent = event.error.message;
    } else {
      displayError.textContent = '';
    }
  });
  
  // Handle form submission.
  var form = document.getElementById('payment-form');
  form.addEventListener('submit', function(event) {
    event.preventDefault();
  
    stripe.createToken(card).then(function(result) {
      if (result.error) {
        // Inform the user if there was an error.
        var errorElement = document.getElementById('card-errors');
        errorElement.textContent = result.error.message;
      } else {
        // Send the token to your server.
        stripeTokenHandler(result.token);
      }
    });
  });
  
  // Submit the form with the token ID.
  function stripeTokenHandler(token) {
    // Insert the token ID into the form so it gets submitted to the server
    var form = document.getElementById('payment-form');
    var hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('type', 'hidden');
    hiddenInput.setAttribute('name', 'stripeToken');
    hiddenInput.setAttribute('value', token.id);
    form.appendChild(hiddenInput);
  
    // Submit the form
    form.submit();
  }
  // Create an instance of the card Element.















  braintree.client.create({
    authorization: CLIENT_AUTHORIZATION
  }, function (err, clientInstance) {
    braintree.paypalCheckout.create({
      client: clientInstance
    }, function (err, paypalCheckoutInstance) {
  
      paypalCheckoutInstance.loadPayPalSDK({
        vault: true
        // Other config options here
      }, function () {
        // Set up regular PayPal button, then
  
        // Set up PayPal Credit button
        paypal.Buttons({
          // Renders the button using the PayPal Credit UI
          fundingSource: paypal.FUNDING.CREDIT,
  
          // Or createOrder if using Checkout flow
          createBillingAgreement: function () {
            return paypalCheckoutInstance.createPayment({
              flow: 'vault',
              offerCredit: true,
              // Pass additional options as required
            });
          },
          onApprove: function (data) {
            return paypalCheckoutInstance.tokenizePayment(data, function (err, payload) {
              // Submit `payload.nonce` to your server
            });
          },
        }).render('#paypal-credit-button');
      });
    });
  });
  
  

</script> --}}



@section('scripts')




<script src="{{ asset('admin-plugins/toastr/toastr.min.js')}}"></script>

    <script>
    

      open_payment_modal = (pivot , plan_id) => {

        $('#user_id').val(pivot.user_id);
        $('#coach_id').val(pivot.coach_id);
        $('#package_id').val(pivot.package_id);
        $('#plan_id').val(plan_id);

        $('#exampleModalCenter').modal('show');
      }


    </script>
@endsection




