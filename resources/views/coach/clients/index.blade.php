
@extends('layouts.dashboard')

@section('styles')
  <style>
    form{
      display: inline;
    }
    .loading-button{
      min-width: 80px;
      min-height: 28px;
    }
    .loader{
      font-size: 18px !important;
      top: 4px !important;
      right: 25px !important;
    }
    .request-number{
      background-color:white;
      color:#17a2b8;
      border-radius: 200%;
      padding: 0px 5px;
    }
  </style>
@endsection

@section('content')





{{--  @dd('asd')  --}}
<div class="row">

   @include('layouts.partials.swals')


   <div class="col-12 col-sm-12 col-lg-12" style="padding:30px">
    
    <div class="card p-3 mb-3" style="border-radius:5px;">
  
      <form action="{{route('coach.manage-clients.filter')}}" class="form-horizontal">
    <div class="row">
      <div class="offset-2 col-3">
        <label class="col-form-label">Client Name</label>
        <input type="text" class="form-control" placeholder="Client Name" name="client_name" @if(isset($filters['client_name'])) value="{{$filters['client_name']}}" @endif />
      </div>
      
        
        <div class="col-3">
        <label class="col-form-label">Packages</label>
  
        <select class="form-control" name="package" >
            <option value="">Select Package</option>
            @foreach(auth()->user()->coach->packages as $package)
            <option value="{{$package->id}}"  @if(isset($filters['package']) && $filters['package'] == $package->id ) selected @endif >{{$package->name}}</option>
            @endforeach
          </select>      
        </div>  
      
      <div class="col-3" style="margin-top:38px">
  
        <button type="submit" class="btn btn-outline-primary" > <i class="fa fa-search"></i> Filter</button>
        <a href="{{route('coach.manage-clients')}}" class="btn btn-outline-danger">X</a>
      </div>
    </div>
  </form>
    </div>
    </div>


   <div class="col-md-12" style="padding:30px" >
    <div class="card card-info card-tabs p-0 ">
      <div class="card-header p-0 pt-1">

        <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
          <li class="nav-item text-center" style="width:50%;">
            <a  class="nav-link active" id="custom-tabs-one-clients-tab" data-toggle="pill" href="#custom-tabs-one-clients" role="tab" aria-controls="custom-tabs-one-clients" aria-selected="true">Clients</a>
          </li>
          <li class="nav-item text-center" style="width:50%;">
            <a class="nav-link" id="custom-tabs-two-new-clients-tab" data-toggle="pill" href="#custom-tabs-two-new-clients" role="tab" aria-controls="custom-tabs-two-new-clients" aria-selected="false">New Client Requests 
              @if($requesting_clients->count() > 0)
              <span class="request-number"> {{$requesting_clients->count()}} </span> 
              @endif
            </a>
          </li>
        </ul>
      </div>
       
      <!-- /.card-header -->
      <div style="padding:30px" >

        <div class="tab-content" id="custom-tabs-one-tabContent">
          <div class="tab-pane fade show active" id="custom-tabs-one-clients" role="tabpanel" aria-labelledby="custom-tabs-one-clients-tab">

        <a class="btn btn-outline-primary float-right mb-3" href="{{route('export.clients' , [json_encode($clients->pluck('id')) , auth()->user()->coach->id   ] )}}"> <i class="fas fa-upload"></i> Export</a>

      <table class="table">
          <thead>
          <tr class="text-center">
              <th>#</th>
              <th>Name</th>
              <th>Profile Image</th>
              <th>Email</th>
              <th>Packages</th>
              {{--  <th>Actions</th>  --}}
          </tr>
          </thead>
          <tbody>
            
          @foreach( $clients as $k => $client)
          <tr id="user-list" class="text-center" >
            <td> {{$k+1}} </td>
            <td> {{$client->name}} </td>
            <td> <img class="profile-picture" src="@if($client->photo) {{asset('images/profile_images/'.$client->photo)}} @else {{asset('img/no-image-found.jpg')}} @endif"> </td>
            <td>{{$client->email}}</td>
            <td>
            
            <a class="btn btn-outline-success btn-sm" href="{{route('coach.manage-client.details' , $client->id)}} " > <i class="fas fa-list f-20"></i></a>


            <a class="btn btn-outline-primary btn-sm"  href="{{route('coach.manage-client.packages' , $client->id)}} " > <i class="fas fa-eye"></i> {{ $client->subscribed_packages->where('pivot.coach_id' , auth()->user()->coach->id)->count() }}</a>
            </td>
            
            {{--  <td>
            <a class="btn btn-outline-primary btn-sm" href="{{route('coach.packages.view' , $client->id)}} " > <i class="fas fa-eye"></i> View</a>
            </td>  --}}
             
          </tr>
          </tbody>
          @endforeach
      </table>
    </div>


    <div class="tab-pane fade show" id="custom-tabs-two-new-clients" role="tabpanel" aria-labelledby="custom-tabs-two-new-clients-tab">

      <table class="table" id="request-table">
        <thead>
        <tr class="text-center">
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Status</th>
            <th>Package</th>
            <th>Date</th>
            <th style="width: 20% !important;">Actions</th>
        </tr>
        </thead>
        <tbody>
          
        {{--  @foreach( $requesting_clients as $k => $client)  --}}
        {{--  @dd($client->pivot->package)  --}}
        <tr id="user-list" v-for="(client , index) in requesting_clients" :key="index" class="text-center" >

          <td> @{{index+1}} </td>
          <td> <a title="View Profile" :href="'/coach/manage-client/'+ client.id + '/details'"> @{{client.name}} </a></td>
          <td>@{{client.email}}</td>
          <td>
            <span class="badge badge-danger">
              @{{capitalize(client.pivot.status)}}
            </span>
          </td>
          
          <td>
            {{--  {{$packages->where( 'id' , $client->pivot->package_id)->first()->name}}  --}}
            @{{ client.pivot.package.name}}
          </td>
          <td>
            @{{dated(client.pivot.created_at)}}
          </td>
         
          <td>



              <button @click="acceptRequsest(client)"  class="btn btn-outline-success btn-sm position-relative loading-button" type="submit" > 
                <spinner v-if="success_loading == client"></spinner>
                <span v-else><i class="fas fa-check-circle mr-2"></i> Accept </span>
                </button>
            
              <button @click="cancelRequsest(client)" class="btn btn-outline-danger btn-sm position-relative loading-button" > 
                <spinner v-if="cancel_loading == client"></spinner>
                <span v-else><i class="fas fa-times-circle mr-2"></i>  Deny </span>
                </button>
            
              

          </td>

            
          {{--  <a class="btn btn-outline-success btn-sm" href="{{route('coach.manage-client.details' , $client->id)}} " > <i class="fas fa-list f-20"></i></a>

          <a class="btn btn-outline-primary btn-sm" href="{{route('coach.manage-client.packages' , $client->id)}} " > <i class="fas fa-eye"></i> {{ $client->subscribed_packages->where('pivot.coach_id' , auth()->user()->coach->id)->count() }}</a>  --}}
          
          {{--  <td>
          <a class="btn btn-outline-primary btn-sm" href="{{route('coach.packages.view' , $client->id)}} " > <i class="fas fa-eye"></i> View</a>
          </td>  --}}
           
        </tr>
        </tbody>
        {{--  @endforeach  --}}
    </table>
      
    </div>


    </div>


    
    </div>
    </div>
  
  </div>
</div>

@endsection

@section('scripts')
    <script>
        function AskBeforeDelete(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {

                    $('#delete-form-'+id).submit();
                }
            })
        }

        function change_user_status(id){
            $.ajax({
                url: '{{url("/")}}/change/user/status/'+id,
                success: function (res){
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'User Status Has been updated',
                        showConfirmButton: false,
                        timer: 1500
                      })
                }
            }).fail(function(){
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Some Problem occured',
                    showConfirmButton: false,
                    timer: 1500
                  })
            })
        }


        const handleRequest = new Vue({
          el: '#request-table',
          data: {
              coach : {!! auth()->user()->coach ?? '' !!},
              success_loading : '',
              cancel_loading : '',
              requesting_clients : {!! $requesting_clients ?? '[]' !!},
          },
          methods:{
            showInConsole(data){
              console.log(data , 'asd')
            },
            acceptRequsest(client){
              //console.log(client , 'acceptClientRequsest')
              this.success_loading = client
              this.cancel_loading = ''
              
              
              axios.post('/api/handle-client-request/'+client.id+'/coach/'+this.coach.id+'/package/' + client.pivot.package_id , {
                status : 'accepted'
              } )
              .then ( res => {

                if(res.data.success == true){
                  toastr.success('Request Processed Successfully.')
                  window.location.reload();
                }else{
                  toastr.error(res.data.error)
                  this.success_loading = ''
                }
                //console.log(res.data)

              }).catch( error => {
                console.log(error.response.data)
              })
            },
            cancelRequsest(client){
              //console.log(client , 'acceptCancelRequsest')
              this.cancel_loading = client
              this.success_loading = ''

              axios.post('/api/handle-client-request/'+client.id+'/coach/'+this.coach.id+'/package/' + client.pivot.package_id , {
                status : 'cancelled'
              } )
              .then ( res => {
              
                toastr.success('Request Processed Successfully.')
                window.location.reload();


              }).catch( error => {
                toastr.error('Request Failed.')
                console.log(error.response.data)
              })

            },
            dated(date){
              var date = new Date(date);
              return date.toLocaleDateString();
            },
            getPackage(id){
              
              const package = this.packages.find( package => package.id ==  id);
              return package.name;

            },
            capitalize(str) {
              return str.charAt(0).toUpperCase() + str.slice(1);
            }
            
          },
          created(){
            console.log('Create handleRequest Component')
            //toastr.success('Request .')

          }
        });
    </script>
@endsection




