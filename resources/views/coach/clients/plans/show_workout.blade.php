@extends('layouts.dashboard')


@section('styles')
<link rel="stylesheet" href="{{asset('assets/lightslider-master/dist/css/lightslider.css') }}">

    <style>
        .image-container{
            display: inline;
            position: relative;
        }
        .image-container .btn{
            position: absolute ;
            top: -50;
            right: 0;
            border-radius: 50%;
        }
        .video-container {
            position: relative;
            display: inline;

        }
         .video-container .btn{
            position: absolute ;
            top: -150;
            right: 0;
            border-radius: 50%;
            background-color: #c8c8c8;

        }
        
        .image-container .btn:hover{
            background-color: #c8c8c8;
            color: #fff;

        }
        
        .video-container .btn:hover{
            background-color: #c8c8c8;
            color: #fff;

        }
        .workout-image{
            height: auto;
            width: auto;
            max-width: 200px;
            max-height: 200px;
            border: 1px solid grey;
            border-radius: 5px;
        }
        .lSPrev{

        }
    </style>
@endsection




@section('content')


<div class="row">


<div class="col-md-12" style="padding:30px" >
    <div class="card ">
      <div class="card-header">
        <h3 class="card-title">View Workout Details</h3>
        <a class="btn btn-outline-danger float-right ml-3" href="{{url()->previous()}}"> <i class="fas fa-arrow-alt-circle-left"></i> Back  </a>

        <a class="btn btn-outline-primary float-right" href="#"> <i class="fas fa-calendar"></i> {{$workout->created_at->format('m/d/Y') }}  </a>

      
    </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
        
      
        <h4> {{$workout->name}} </h4>
            
        <br>
        <label class="col-form-label">Description</label>
        <div>
            {!! $workout->description !!}
        </div>
        
        <br>

        <div class="text-left">
            <label>Tags</label>
            <div>
                @foreach ($workout->tag as $tag)
                {{$tag->name}} , 
                @endforeach
            </div>
        </div>


        <br>



				<hr>
                @if( $workout->media && is_array(json_decode($workout->media)))

				<h5 class="font-family"><i class="far fa-images mr-2"></i>Image Gallery </h5>
                <hr>
				<ul id="lightSlider">
                    @foreach(json_decode($workout->media) as $k => $image )
                    <li style=" height:250px; width:250px;">
					<a href="{{asset('workouts/images/'.$image)}}" data-lightbox="roadtrip"  class="lightbox-image-anchor">
						<img src="{{asset('workouts/images/'.$image)}} " alt=""/ class="lightbox-image-list" style=" max-height: 240px; max-width: 240px; height:auto; width:auto;" >
						</a>
					</li>
                @endforeach
				</ul>
			@endif



        <br>
        @if( $workout->media && is_array(json_decode($workout->media)))
        {{-- <h4> Images </h4> --}}

    
        {{-- <div class="gallery"  id="image-preview" >
            @foreach(json_decode($workout->media) as $k => $image )
            <div class="image-container" id="image-{{$k}}" >
            <a href="{{asset('workouts/images/'.$image)}} " data-lightbox="roadtrip"  class="lightbox-image-anchor">
            <img src="{{asset('workouts/images/'.$image)}} "  class="m-2 workout-image " >
            </a> 
            </div>
            @endforeach
        </div> --}}
        @endif
        
        <br>
	    <hr>
        @if( $workout->videos && is_array(json_decode($workout->videos)))
        <h4> <i class="fas fa-video"></i> Videos </h4>
        <hr>

        <div id="video_area">
            @foreach(json_decode($workout->videos) as $k => $video )
            <div class="video-container" id="video-{{$k}}" >
                <video width="300" height="180" class="m-2 workout-videos" controls  >
                    <source src="{{asset('workouts/videos/'.$video)}}" id="video_display" >
                    Your browser does not support HTML5 video.
                </video>
            </div>

            @endforeach
        </div>
        @endif
        
           
    </div>
    </div>
  
  </div>
</div>

@endsection

@section('scripts')

<script src="{{asset('assets/lightslider-master/dist/js/lightslider.js') }}" defer></script>

<script type="text/javascript">

	$(document).ready(function(){
		console.log(screen.width);
		if(screen.width < 760 ){
			$('#youtube_video').css('width' , screen.width - 50);
			$('#youtube_video').css('height' , 'auto');
		}
	})

	$(document).ready(function() {
		$("#lightSlider").lightSlider({
			item: 3,
			autoWidth:true,
			loop:true,
			slideMove: 1, // slidemove will be 1 if loop is true
			slideMargin: 10,
	 
			addClass: 'image_slider',
			mode: "slide",
			useCSS: true,
			cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
			easing: 'linear', //'for jquery animation',////
	 
			speed: 400, //ms'
			auto: false,
			loop: false,
			slideEndAnimation: true,
			pause: 2000,
	 
			keyPress: false,
			controls: true,
			prevHtml: '',
			nextHtml: '',
	 
			@if(session('locale') == 'ar')
            rtl:true,
            @else
            rtl:false,
			@endif
			adaptiveHeight:false,
	 
			vertical:false,
			verticalHeight:500,
			vThumbWidth:100,
	 
			thumbItem:10,
			pager: true,
			gallery: false,
			galleryMargin: 5,
			thumbMargin: 5,
			currentPagerPosition: 'middle',
	 
			enableTouch:true,
			enableDrag:true,
			freeMove:true,
			swipeThreshold: 40,
	 
			responsive : [],
	 
			onBeforeStart: function (el) {},
			onSliderLoad: function (el) {},
			onBeforeSlide: function (el) {},
			onAfterSlide: function (el) {},
			onBeforeNextSlide: function (el) {},
			onBeforePrevSlide: function (el) {}
		});
	});
	</script>



    <script>
        function AskBeforeDelete(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {

                    $('#delete-form-'+id).submit();
                }
            })
        }

        function change_user_status(id){
            $.ajax({
                url: '{{url("/")}}/change/user/status/'+id,
                success: function (res){
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'User Status Has been updated',
                        showConfirmButton: false,
                        timer: 1500
                      })
                }
            }).fail(function(){
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Some Problem occured',
                    showConfirmButton: false,
                    timer: 1500
                  })
            })
        }
    </script>
@endsection




