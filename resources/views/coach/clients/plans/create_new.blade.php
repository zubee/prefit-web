@extends('layouts.dashboard')


@section('styles')

<!-- Toastr -->
{{--  <link rel="stylesheet" href="{{ asset('admin-plugins/toastr/toastr.min.css')}}">  --}}

<style>

   /* width */
   ::-webkit-scrollbar {
    width: 5px;
  }
  
  /* Track */
  ::-webkit-scrollbar-track {
    background: #f1f1f1; 

  }
   
  /* Handle */
  ::-webkit-scrollbar-thumb {
    border-radius:90px;
    background: #888; 
  }
  
  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: #555; 
  }


  .multiselect-container{
    width: 95% !important;
    padding: 10px !important;
  }

  .caret{
    float: right !important;
    margin-top: 8px;
  }

  .multiselect {
    height: 35px;
    margin-top: 8px;
    border: 1px solid #337ab7;
  } 

  .multiselect:hover {
    background-color:#eee !important ;
    color: black !important;
  }

  .multiselect-container > .active{
    background-color:#337ab7;
    color:white;
    text-decoration: none;
  }


  .multiselect-container > li {
    padding-top: 5px !important;
    border-radius: 4px;
  }
  
  .multiselect-container > li > a {
    padding-left: 10px !important;
    border-radius: 2px;
    border-radius: 3px;
  }

  .multiselect-clear-filter{
    display: none;
  }
  .multiselect-search{
    margin-bottom: 10px !important;
  }

  .action_buttons{
    position: absolute;
    top: 150px;
    right: 50px;
    z-index: 10;
  }

  .plan-start-button{
    position: absolute;
    top: -40px;
    right: 10px;
  }
  .main-action-buttons{
    font-size: 25px;
  }
  .lable-class{
      
  }
  .workouts-area{
    margin-top: 20px;
    height: auto;
    max-height: 200px;
    overflow: hidden auto;
    padding-right: 20px;
  }
  .sticky-title{
    position: sticky;
    top: 0px;
  }

  .delete-search-button {
    border-top-right-radius: 5px !important;
    border-bottom-right-radius: 5px !important;
    background-color: #ff5858 !important;
    color: #f1f1f1 !important;
    cursor: pointer;
  }


  .exercise-gif-image{
    border-radius: 5px;
    width: auto;
    height: auto;
    max-width: 160px;
  }

  .loading-for-workouts{
    position: relative;
    text-align: center;
  }

  .workout-label{
    margin-top: -5px;
    margin-left: 6px;
    font-weight:500;
    width: 100%;
  }
  .workouts-status-area{
    font-size: 25px;
    text-align: center;
    width: 30%;
  }
  .workout-status-area{
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .get-current-workouts{
    position: absolute;
    right: 0;
    top: 0;
    font-size: 25px;
  }
</style>

@endsection


@section('content')

@include('layouts.partials.swals')

<div class="row" id="vue-app">
{{--  <div class="row" id="plan">  --}}
  <vue-progress-bar></vue-progress-bar>

@php
    $count = 0;
    $count1 = 0;
@endphp


<div class="action_buttons">

  @php
  if($plan->start_status == 1){
    $day_prefix = '';
  }else{
    $day_prefix = 'day';
  }
  @endphp
  {{--  <a v-if="plan && plan.start_date" href="javascript:;" class="btn btn-outline-success  btn-sm mr-2 mb-2" > <i class="fas fa-calendar"></i> Start Plan Date: @{{plan.start_date}} </a>
  @click="start_plan_now"

  <a v-if="plan && !plan.start_date"  href="{{route('coach.manage-client.package.start-plan' , [$client->id , $package->id ])}}" class="btn btn-outline-success btn-sm  mr-2 mb-2" > <i class="fas fa-play-circle"></i> Start Plan Now</a>

  <a v-if="current_day_number && current_day" href="javascript:;" class="text-success main-action-buttons" @click="update_data_in_modal(current_day)"><i class="fas fa-edit"></i></a>
  
  <a v-if="current_day_number && !current_day" href="javascript:;" class="main-action-buttons"  @click="day_data_modal({{$count1}})"><i class="fas fa-plus-circle"></i></a>
  
  
  <a @click="refresh_data" href="javascript:;" class="text-danger ml-2 main-action-buttons" > <i class="fas fa-history "></i> </a>  --}}

  {{--  <a @click="init_toaster" href="javascript:;" class="text-warning ml-2 main-action-buttons" > <i class="fas fa-arrow-alt-circle-up "></i> </a>  --}}



</div>

      <!-- /.card-header -->

@php
    foreach($total_weeks as $k => $week){
        //dump($week);
        $total_weeks[$k] = array_values($week);
        //dump($week);
    }
    $total_weeks = array_values($total_weeks);

    @endphp
    {{--  @dd($total_weeks[0])  --}}

    <client-plan

        :total_weeks="{{json_encode(array_values($total_weeks))}}"
        :plan="{{json_encode($plan)}}"
        :local_workouts="{{json_encode($workouts)}}"
        :supplements="{{json_encode($supplements)}}"
        ></client-plan>


      
    
    










    </div>
  




    {{--  <div class="card card-warning card-outline">
      <div class="card-header">
        <h3 class="card-title">
          <i class="fas fa-edit"></i>
          Toastr Examples
        </h3>
      </div>
      <div class="card-body">
        <button type="button" class="btn btn-success toastrDefaultSuccess">
          Launch Success Toast
        </button>
        <button type="button" class="btn btn-info toastrDefaultInfo">
          Launch Info Toast
        </button>
        <button type="button" class="btn btn-danger toastrDefaultError">
          Launch Error Toast
        </button>
        <button type="button" class="btn btn-warning toastrDefaultWarning">
          Launch Warning Toast
        </button>
        <div class="text-muted mt-3">
          For more examples look at <a href="https://codeseven.github.io/toastr/">https://codeseven.github.io/toastr/</a>
        </div>
      </div>
      <!-- /.card -->
    </div>  --}}



@endsection

@push('script')
<!-- Toastr -->
{{--  <script src="{{ asset('admin-plugins/toastr/toastr.min.js')}}"></script>  --}}

<script src="{{asset('assets/select-checkbox1/select.js')}}" ></script>

<script>

</script>


    @endpush

