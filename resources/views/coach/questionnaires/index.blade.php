@extends('layouts.dashboard')


@section('content')


<div class="row">

   @include('layouts.partials.swals')

<div class="col-md-12" style="padding:30px" >
    <div class="card ">
      <div class="card-header">
        <h3 class="card-title">Manage Questionnaire</h3>
        {{-- <a class="btn btn-outline-primary float-right" href="{{route('admin.packages.create')}}"> <i class="fas fa-plus"></i> Create</a> --}}
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <table class="table">
          <thead>
          <tr>
              <th>#</th>
              <th>Name</th>
              <th>Create Date</th>
              <th>Actions</th>
          </tr>
          </thead>
          <tbody>
          {{-- @dd(auth()->user()->questionnaires) --}}
          @foreach( auth()->user()->questionnaires as $k => $questionnaire)
          <tr>
              <td> {{$k+1}} </td>
              <td> {{$questionnaire->name}} </td>

              <td> {{$questionnaire->created_at->format('m/d/Y') }} </td>
              {{-- <td><a href="{{route('admin.package.assigned', $package->id)}}"> {{$package->users()->count()}} </a></td> --}}
               <td>
                <a class="btn btn-sm btn-outline-success" href="{{route('coach.questionnaires.view' , $questionnaire->id)}} " > <i class="fas fa-eye"></i> View</a>

          </tr>
          </tbody>
          @endforeach
      </table>
    </div>
    </div>
  
  </div>
</div>

@endsection

@section('scripts')
    <script>
        function AskBeforeDelete(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {

                    $('#delete-form-'+id).submit();
                }
            })
        }

        function change_user_status(id){
            $.ajax({
                url: '{{url("/")}}/change/user/status/'+id,
                success: function (res){
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'User Status Has been updated',
                        showConfirmButton: false,
                        timer: 1500
                      })
                }
            }).fail(function(){
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Some Problem occured',
                    showConfirmButton: false,
                    timer: 1500
                  })
            })
        }
    </script>
@endsection




