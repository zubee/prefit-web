@extends('layouts.dashboard')


@section('content')


<div class="row">

   @include('layouts.partials.swals')


<div class="col-md-12" style="padding:30px" >
    <div class="card card-info ">
      <div class="card-header">
        <h3 class="card-title">View Questions</h3>
      </div>
      <!-- /.card-header -->
    <form action="{{route('coach.questionnaires.store-answer' , $questionnaire->id )}}" method="POST" >
        @csrf
        <div style="padding:30px" >
      <h4>{{$questionnaire->name}}</h4>
        <div>
        @foreach($questionnaire->questions as $k => $question)
        @php
        $answer = $question->answer(auth()->user()->coach->id);
        @endphp
        {{-- dump($question->id , $question->answers); --}}

        {{-- @dump($answer) --}}
        <input type="hidden" name="questions[]" value="{{$question->id}}" >

        <div class="ml-3">
        <label class="col-form-label">Q {{$k+1}} : {{$question->question}} </label>
        <p class="float-right">{{ isset($answer) ? $answer->updated_at->toDateString() : "-"}} </p>
        
        <div>
            {{-- @dd($question->answer(auth()->user()->coach->id)) --}}
            @if($question->type == 'text')
            <textarea name="question-{{$question->id}}" class="form-control" placeholder="{{$question->question}}" > {{ isset($answer) && !is_array(json_decode($answer->answer)) ? $answer->answer : "" }}</textarea>
            @endif
        </div>
        @if($question->type == 'checkbox')
        <div>
            @foreach($question->choices as $k => $choice)
            {{--  @dump($answer->answer)  --}}
            <input name="question-{{$question->id}}[]" id="question-{{$question->id}}-{{$choice->id}}" class="ml-5" value="{{$choice->name}}"  type="checkbox" 
            {{-- @dd(json_decode($question->answer(auth()->user()->coach->id)->answer)[$k] == $choice->name) --}}
            @if($answer && is_array(json_decode($answer->answer)) && in_array( $choice->name , json_decode($answer->answer) ) )
            checked
            @endif
            />
            <label  for="question-{{$question->id}}-{{$choice->id}}" class=" col-form-label">{{$choice->name}}</label>
            <br>
            @endforeach
    
        </div>
        @endif
        @if($question->type == 'radiobutton')
        <div>
            @foreach($question->choices as $choice)

            <input name="question-{{$question->id}}" id="question-{{$question->id}}-{{$choice->id}}" class="ml-5" value="{{$choice->name}}" type="radio" 
            @if($answer && $answer->answer == $choice->name)
            checked
            @endif
            />
            <label for="question-{{$question->id}}-{{$choice->id}}" class=" col-form-label">{{$choice->name}}</label>
            <br>
            @endforeach
    
        </div>
        @endif
    </div>
    <hr>
        @endforeach
    </div>
    <button type="submit" class="btn btn-sm btn-primary float-right"><i class="fas fa-save"></i> Save</button>
    </div>
    </form>
    </div>
  
  </div>
</div>

@endsection


@section('scripts')
    <script>
        function reIndex(){
            $('.question').each(function (k,v) {
                $(this).children('.row').children('.col-sm-6').children('#question').attr('name', 'question['+k+'][name]');
                $(this).children('.row').children('.col-sm-6').children('.question_type').attr({'name':'question['+k+'][type]','id':'question_type'+k+'','onChange':'toggle_choices('+k+')'});
                $(this).children('.row').children('.col-sm-2').children('button').attr({'id':'add_button'+k+'', 'onClick':'add_new_row('+k+')'});
                $(this).children('.row').children('.col-sm-4').children('.choice').attr({'name':'question['+k+'][choices][]'});
            });
        }

        var i = 0;
        function add_new_row(id){
            i += 1;

            $('#question-'+id).append('<div id="choice-'+i+'" class="form-group row"> <div class="col-sm-4"> <label class="row float-right col-form-label ">Choice :</label> </div> <div class="col-sm-4"> <input type="text" class="form-control choice" placeholder="Choice" name="question['+id+'][choices][]" required>  </div>  <div  class="col-sm-2"> <button onclick="remove_row(this.id)" id="'+i+'" type="button" style="height: 35px;" class="btn btn-outline-danger"> <i class="fas fa-minus"></i> </button> </div> </div>')
        }


        function remove_row(row_id){
            $('#choice-'+row_id).remove();
        }
        function toggle_choices(question_id){
            if($('#question_type'+question_id).val() == 'text' ){
                $('#choices_area div').remove();
                $('#add_button').hide();
                i = 0;

            }else{
                $('#add_button'+question_id).show();
            }
        }


        var question =  $('.question').length;

        function add_new_question() {
            question+=1;

            $('#question_area').append('<div id="question-'+question+'" class="question" > <div class="form-group row"> <div class="col-sm-3"><label for="question" class="row float-right col-form-label ">Question '+question+':</label></div>\n' +
                '<div class="col-sm-6"><input type="text" class="form-control" id="question" placeholder="Name" name="question['+question+'][name]" value="" required></div> ' +
                '<div  class="col-sm-2"> <button onclick="remove_question(this.id);" id="'+question+'" type="button" style="height: 35px;" class="btn btn-outline-danger"> <i class="fas fa-minus"></i> </button> </div> </div>'+
                '<div class="form-group row">\n' +
                '<div class="col-sm-3">\n' +
                '<label for="inputEmail1" class="row float-right col-form-label ">Type:</label>\n' +
                '</div>\n' +
                '<div class="col-sm-6">\n' +
                '     <select class="form-control question_type" id="question_type'+question+'" name="question['+question+'][type]" onchange="toggle_choices('+question+')" >\n' +
                '            <option value="text" selected >Text</option>\n' +
                '            <option value="checkbox">CheckBoxes</option>\n' +
                '             <option value="radiobutton">RadioButtons</option>\n' +
                '      </select>\n' +
                '</div>\n' +
                '<div class="col-sm-2">\n' +
                '     <button type="button"style="height: 35px; display:none; " id="add_button'+question+'"  onclick="add_new_row('+question+')" class="btn btn-outline-success"> <i class="fas fa-plus"></i> </button>\n' +
                ' </div>\n' +
                ' </div></div>\n' +
                '          ')


        }

        function remove_question(question_id) {

            $('#question-'+question_id).remove();
        }

        function beforeSubmit() {

            reIndex();
            $('#form-submit').submit();

        }


    </script>
@endsection



{{-- 
    <form action="{{route('admin.questionnaires.update' , $questionnaire->id)}}" method="POST">
      @include('layouts.partials.form_errors')

        @csrf
      @method('PATCH')
        <div class="form-group row">
          <div class="col-sm-2">
          <label for="questionnaire" class="row float-right col-form-label ">Questionnaire:</label>
        </div>
          <div class="col-sm-8">
            <input type="text" class="form-control" id="questionnaire" placeholder="Name" name="questionnaire" value="{{$questionnaire->name}}" required disabled>
          </div>
            <div class="col-sm-2">
                <button type="button"style="height: 35px;  " id="add_question"  onclick="add_new_question()" class="btn btn-outline-success"> <i class="fas fa-plus"></i> </button>

            </div>

        </div>


        <div id="question_area">

            <div id="choices_area">

            </div>


            @foreach($questionnaire->questions as $key=> $q)

            <div id="question-{{$key}}" class="question"> <div class="form-group row"> <div class="col-sm-3"><label for="question" class="row float-right col-form-label ">Question {{$key}}:</label></div>
                    <div class="col-sm-6"><input type="text" class="form-control" id="question" placeholder="Name" name="question[{{$key}}][name]" value="{{$q->question}}" required="" disabled></div> <div class="col-sm-2">
                        
                      <button onclick="remove_question(this.id);" id="{{$key}}" type="button" style="height: 35px;" class="btn btn-outline-danger"> <i class="fas fa-minus"></i> </button>
                  
                  </div> </div><div class="form-group row">
                    <div class="col-sm-3">
                        <label for="inputEmail1" class="row float-right col-form-label ">Type:</label>
                    </div>
                    <div class="col-sm-6">
                        <select class="form-control question_type" id="question_type{{$key}}" name="question[{{$key}}][type]" onchange="toggle_choices({{$key}})" disabled >
                            <option value="text" {{$q->type == "text"? 'selected' : ''}} >Text</option>
                            <option value="checkbox" {{$q->type == "checkbox" ? 'selected' : ''}}>CheckBoxes</option>
                            <option value="radiobutton" {{$q->type == "radiobutton" ? 'selected' : ''}}>RadioButtons</option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <button type="button" style="height: 35px;" id="add_button{{$key}}" onclick="add_new_row({{$key}})" class="btn btn-outline-success"> <i class="fas fa-plus"></i> </button>
                    </div>
                </div>
                    @foreach($q->choices as $ckey=> $choice)
                      <div id="choice-{{$ckey}}"  class="form-group row">
                    <div class="col-sm-4"> <label class="row float-right col-form-label ">Choice :</label> </div> <div class="col-sm-4"> <input type="text" disabled class="form-control choice" placeholder="Choice" name="question[{{$key}}][choices][]" value="{{$choice->name}}" required="">  </div>  <div class="col-sm-2">
                        
                      <button onclick="remove_row(this.id)" id="{{$ckey}}" type="button" style="height: 35px;" class="btn btn-outline-danger"> <i class="fas fa-minus"></i> </button> 
                  
                  </div> </div>
                  @endforeach
            </div>
                @endforeach
            </div>




        
        
       @php
           $user_ids = $questionnaire->users->pluck('id')->toArray();
       @endphp
      <div class="form-group row">
          <div class="col-sm-2">
          <label for="inputEmail1" class="row float-right col-form-label ">Assign To:</label>
        </div>
          <div class="col-sm-8">
              <select class="select2bs4" multiple style="width: 100%;height: 40px;" name="assigned[]" >
                  @foreach($users as $user)
                  <option value="{{$user->id}}" @if( in_array( $user->id , $user_ids ) ) selected @endif >{{$user->name}}</option>
                  @endforeach
              </select>
              </div>
      
        </div>

      
      <div>
          <button class="btn btn-primary float-right" onclick="beforeSubmit();">Update</button>
      </div>
    </form>--}}
  