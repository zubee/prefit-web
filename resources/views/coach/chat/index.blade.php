


@extends('layouts.dashboard')
@section('styles')

<link rel="stylesheet" href="{{ asset('admin-plugins/toastr/toastr.min.css')}}">

<style>
    .sticky-message-input{
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate3d(-50%, -50%, 100);
        transition: background .3s;
                
    }
    .overlay-input {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate3d(-50%, -50%, 100);
        white-space: nowrap;
        pointer-events: auto;
        transition: background .3s;
      }

      .user-names{
          color:black;
      }
      .user-names:hover{
        text-decoration: none;
        color:black;

      }
      .scroller {
        scrollbar-color: #f1f1f1 #888;
        scrollbar-width: thin;
      }

        /* width */
        ::-webkit-scrollbar {
            width: 5px;
        }
        
        
        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1; 

        }
        
        /* Handle */
        ::-webkit-scrollbar-thumb {
            border-radius:90px;
            background: #888; 
        }
        
        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #555; 
        }
        
        
        .list-group{
            overflow-y: auto;
            overflow-x: hidden;
            height: 100%;
            vertical-align: bottom;
            display: inline-block;

        }
        {{-- position: absolute; --}}
        .messages-content{
            bottom: 0;
            {{-- height: 88px; --}}
            height: auto;
            margin-bottom:18px;
            {{-- margin-top:18px; --}}
        }
        .message-input{
            overflow: scroll;
        }
        .simple-li-tags:{
            list-style: none;
            padding:10px;
        }
        .user-list{
            background-color: white;
        }
        .message-input{
            width: 100%;
            border: none;
        }
        .sent-message{
            border-radius: 8px;
            padding:20px;
            background-color: white;
            border:1px solid black;

        }
        {{--  background-color: #e0ebff;  --}}

        .recieved-message{
            border-radius: 8px;
            background-color: #6a9bd0;
            color: #fff;
        {{-- width:70%; --}}
            padding:20px;

        }
        .sent-time{
            position: absolute;
            left: 20px;
            top:30px;

        }
        .recieve-time{
            top:30px;
            right: 20px;
            position: absolute;
        }
        .profile-image{
            width:50px; 
            height: 50px; 
            border-radius: 100%
        }
        .message-number{
            top:30px;
            position: absolute;
            right: 10px;
            background-color: #6a9bd0;
            color: #fff;
            width: 25px;
            height: 25px;
            text-align: center;
            border-radius:100%;
        }
        
        .message-number-support{
            top:30px;
            position: absolute;
            right: 10px;
            background-color: #178457;
            color: #fff;
            width: 25px;
            height: 25px;
            text-align: center;
            border-radius:100%;
        }
        .contact-list{
            height:560px; 
            overflow: hidden scroll;
            border-radius:3px;
            border:1px solid #d2d4d7;
        }
        .active-chat{
            background-color: #6a9bd0 !important;
            color: #fff;
        }
        
        .active-chat .meesage-search{
            color: white;
        }


        .attach-file-icon{
            padding:13px;
            margin-top: -10px;
        }
        .download-icon{
            position: absolute;
            {{-- margin-top:2%; --}}
            {{-- color: #38c172 !important; --}}
            font-size: 18px;
            right:20;
            bottom:3;
            border: 1px solid #dfdfdf;
            border-radius:100%;
            padding:5px;
            background-color:#dfdfdf;
            {{-- margin-right: 10px; --}}
            
        }
        .file-icon{
            margin-top:20px;
            font-size: 18px;
            margin-right: 10px;
        }
        .user-list-item{
            {{--  margin-bottom: 20px;  --}}
        }
        .support-user{ 
            background-color: #2dd451;
            {{--  background-color: #31eb59;  --}}
            color: #fff !important;
        }   
        
        .support-user-search{
            color: #fff !important;   
        }
        .video-box{
        }
        .video-player{
            width: 270px;
            height: auto;
            border-radius: 10px;
        }
        {{--  6a9bd0  --}}
        .teal{
            background-color: #009688;
            color: white;
        }
        .notifiy-badge{
            font-size:15px;
        }
        .image-download-button{
            background-color: #00ba6d;
            padding: 5px 8px;
            border-radius: 50%;
            font-size: 15px;
            color: white;
            z-index: 10;
        }
        .image-button-right-link{
            position: absolute;
            left: auto;
            right: 30px;
            bottom: 4px;   
        }
        .image-button-left-link{
            position: absolute;
            left: 30px;
            right: auto;
            bottom: 4px;
        }

        .questionnaire-box{
            background-color: teal;
            color: white;
            border: 1px solid teal;
        }
</style>
@endsection

@section('content')

<div class="row" id="chat" style="padding-top: 10px" >












    <div class="offset-md-1 offset-lg-1 col-md-4 col-lg-4 col-sm-10">
        <div class="scroller bg-white  contact-list" style="">

            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                  <a class="nav-item nav-link @if($reciever_type != 'new request') active @endif w-50" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Clients 
                    <span class="notifiy-badge badge badge-success float-right" v-show="user_count > 0">@{{user_count}}</span>
                </a>

                <a class="nav-item nav-link w-50  @if($reciever_type == 'new request') active @endif" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">New Requests 
                    <span class="notifiy-badge badge badge-success float-right" v-show="request_user_count > 0">@{{request_user_count}}</span> 
                </a>
                </div>
              </nav>
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade  @if($reciever_type != 'new request') show active @endif" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <ul  id="myUL" class="simple-li-tags user-list" style="list-style: none;margin-left:-45px" >
                        {{-- @if( isset($reciever) && $reciever->id == $user->id  ) active-chat @endif @if($user->role_id == 6) support-user @endif  --}}
        
                        {{--  <li v-for="user in users" :class="'user-list-item user-item ' + ( (user.role_id == 6) ? 'support-user  ' : null) + ( (pre_reciever && pre_reciever.id == user.id ) ? 'active-chat' : null)" >  --}}

                        <li v-for="user in users" class="user-list-item user-item" :class="{'active-chat': user.id == reciever.id , 'support-user': user.role_id == 6  }" >
                        <a class="user-names" @click.prevent="get_this_chat(user)" href="jacascript:;">
                        <div class="meesage-search d-flex p-3 " style="position: relative">
                            
                            <img v-if="user.photo" :src="'{{asset('images/profile_images/')}}/'+user.photo" alt=""  class="profile-image" >
                            
                            <img v-else src="{{asset('img/no-image-found.jpg')}}" alt=""  class="profile-image" >
                            
                            <div>

                                <p :class="'search-name mt-1 mb-0 ml-2   '+ ( (user.role_id == 6) ? 'support-user-search  ' : null)">@{{user.name}}</p>
                                <p :class="'search-name mt-0 ml-2  '+ ( (user.role_id == 6) ? 'support-user-search  ' : null)" v-if="user.last_message_time" > <i class="far fa-clock"></i> @{{user.last_message_time}}</p>
                                <p class="search-name mt-0 ml-2" v-else> <i class="far fa-clock"></i> No Conversation </p>
        
                            </div>

                            {{-- @if($user->role_id == 6) message-number-support @else message-number @endif --}}
                             
                            <p :id="'user-message-count-'+user.id" v-if="user.unread_count > 0" :class="(user.role_id == 6) ? 'message-number-support' : 'message-number' " > @{{user.unread_count}}  </p>
        
                             {{-- @if($user->chat->where('reciever_id' , auth()->user()->id)->where('read_status' , 0)->count() == 0) style="display:none" @endif > {{$user->chat->where('reciever_id' , auth()->user()->id)->where('read_status' , 0)->count()}} --}}
                        </div>
                    </a></li>
                    </ul>

                </div>
                <div class="tab-pane fade  @if($reciever_type == 'new request') show active @endif" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <ul  id="myUL" class="simple-li-tags user-list" style="list-style: none;margin-left:-45px" >        
                        {{--  <li v-for="user in user_requests" :class="'user-list-item user-item ' + ( (user.role_id == 6) ? 'support-user  ' : null) + ( (pre_reciever && pre_reciever.id == user.id ) ? '  active-chat  ' : null)" >  --}}
                        
                        <li v-for="user in user_requests" class="user-list-item user-item" :class="{'active-chat': user.id == reciever.id , 'support-user': user.role_id == 6  }" >

                            
                        <a class="user-names" @click.prevent="get_this_chat(user)" href="jacascript:;">
                        <div class="meesage-search d-flex p-3 " style="position: relative">
                            
                            <img v-if="user.photo" :src="'{{asset('images/profile_images/')}}/'+user.photo" alt=""  class="profile-image" >
                            
                            <img v-else src="{{asset('img/no-image-found.jpg')}}" alt=""  class="profile-image" >
                            
                            <div>
                                <p :class="'search-name mt-1 mb-0 ml-2   '+ ( (user.role_id == 6) ? 'support-user-search  ' : null)">@{{user.name}}</p>
                                <p class="search-name mt-0 ml-2" v-if="user.last_message_time" > <i class="far fa-clock"></i> @{{user.last_message_time}}</p>
                                <p class="search-name mt-0 ml-2" v-else> <i class="far fa-clock"></i> No Conversation </p>
                            </div>
        
                             
                            <p :id="'user-message-count-'+user.id" v-if="user.unread_count > 0" :class="(user.role_id == 6) ? 'message-number-support' : 'message-number' " > @{{user.unread_count}}  </p>
        
                        </div>
                    </a></li>
                    </ul>    
                </div>
               
              </div>
              
              

            

    


      </div>
      </div>







    <div class=" col-md-6 col-lg-6 col-sm-10">
        <div class="card p-0" style="height:560px;" id="messages-div">
            <div class="form-group  d-flex mt-2 " style="position: relative">
                <div style="height: 40px; display: inline-block">
                <i v-if="active_status == 1" style="cursor: pointer;" @click="close_chat" class="fas fa-times-circle ml-3 mt-3 text-danger" ></i>
                <p style="position: absolute;right: 9px; top: 20%;" v-html="reciever.name" ></p>
            </div>
            </div>
    
            <hr class="mt-0">
    
            <ul class="scroller list-group" v-chat-scroll="{always: false, smooth: true}">
                
                <li v-for="message in messages" class="messages-content p-3">
                
                    <div class="row align-content-center"  v-if="message.sender_id == {{auth()->user()->id}}">
                        <div class="col-sm-12 col-12 col-md-3  text-center pl-4" style="align-self: center;">
                            @{{message_time(message.created_at)}}
                        </div>

                        <div class="d-sm-none d-md-none d-block ml-5"></div>
                        <div class="col-sm-8 col-8 col-md-7">
                        
                            

                          
                            
                            
                            <div v-if="message.file == 1 && !(message.mime.includes('jpg') || message.mime.includes('png') || message.mime.includes('jpeg') || message.mime.includes('mp4') || message.mime.includes('avi') || message.mime.includes('mkv') )" class="sent-message ">
                                <i class="fas fa-file"></i>
                                @{{message.realname}}
                                <a :href="'{{asset('/api/chat/download/file/')}}/'+message.id" > <i class="fas fa-download download-icon"></i> </a> 

                            </div>
                            
                            <div v-if="message.file == 1 && (message.mime.includes('jpg') || message.mime.includes('png') || message.mime.includes('jpeg'))" class="p-3">
                                <a :href="'{{asset('messages/files/')}}' +'/' + message.filename"  data-lightbox="roadtrip"  class="lightbox-image-anchor">
                                    
                                    <a class="image-button-right-link" :href="'{{asset('/api/chat/download/file/')}}/'+message.id" ><span class="image-download-button "> <i class="fas fa-download mt-2"></i> </span></a>


                                    <img  :src="'{{asset('messages/files/')}}' +'/' + message.filename"  style="width: auto; height: auto; max-width: auto; height: 100px;" class="lightbox-image-list float-right"/>   
                                </a>

                            </div>

                            <div v-if="message.file == 1 && (message.mime.includes('mp4') || message.mime.includes('mkv') || message.mime.includes('avi') )" class="video-box">

                                <video controls class="video-player" >
                                    <source :src="'/messages/files/' + message.filename" type="video/mp4">
                                  Your browser does not support the video tag.
                                  </video>
                            </div>


                            

                            <div v-if="message.file == 0 && message.is_questionnaire == 0" class="sent-message" >
                                @{{message.message}}

                            </div>

                            <div v-if="message.file == 0 && message.is_questionnaire == 1" class="sent-message questionnaire-box" >

                                @{{message.questionnaire.name}}
                                <br>
                                <a  target="_blank" class="btn btn-sm btn-success" :href="'/coach/client/'+  message.reciever_id  +'/questionnaires/'+message.questionnaire.id+'/show'" >view</a>

                            </div>

                            
                        </div>
                        <div class="col-sm-2 col-2 col-md-2 " style="align-self: center;">

                            <img  v-if="(sending_user.photo)" v-bind:src="'{{asset('images/profile_images')}}/'+sending_user.photo"  alt="" width="50" class="float-left ml-3" style="border-radius: 100%; width: 50px; height:50px;object-fit: cover;" >   

                            <img v-else src="{{asset('img/no-image-found.jpg')}}"  alt="" width="50" class="float-left ml-3" style="border-radius: 100%" >   


                        </div>
                    </div>
              
    

                <div class="row align-content-center"  v-else>

                    <div class=" d-block d-md-none d-lg-none  col-sm-12 col-12 col-md-3  text-center pr-4" style="align-self: center;">
                        @{{message_time(message.created_at)}}
                    </div>

                    <div class="col-sm-2 col-2 col-md-2 " style="align-self: center;">
                        
                    <img  v-if="(reciever.photo)" v-bind:src="'{{asset('images/profile_images')}}/'+reciever.photo"  alt="" width="50" class="float-left ml-3" style="border-radius: 100%; width: 50px; height:50px;object-fit: cover;">   
                                        
                    <img v-else src="{{asset('img/no-image-found.jpg')}}"  alt="" width="50" class="float-left ml-3" style="border-radius: 100%">   



                    </div>
                
                    <div class="col-sm-8 col-8 col-md-7 ">

                        <div v-if="message.file == 1 && !(message.mime.includes('jpg') || message.mime.includes('png') || message.mime.includes('jpeg') || message.mime.includes('mp4') || message.mime.includes('avi') || message.mime.includes('mkv'))" class="recieved-message ">
                            <i class="fas fa-file"></i>
                            @{{message.realname}}
                            <a :href="'{{asset('/api/chat/download/file/')}}/'+message.id" > <i class="fas fa-download  download-icon"></i> </a> 
                        </div>

                        
                        <div v-if="message.file == 1 && (message.mime.includes('jpg') || message.mime.includes('png') || message.mime.includes('jpeg'))" class="p-3">
                            <a :href="'{{asset('messages/files/')}}' +'/' + message.filename"  data-lightbox="roadtrip"  class="lightbox-image-anchor">

                                <a class="image-button-left-link" :href="'{{asset('/api/chat/download/file/')}}/'+message.id" ><span class="image-download-button "> <i class="fas fa-download mt-2"></i> </span></a>

                                
                                <img  :src="'{{asset('messages/files/')}}' +'/' + message.filename"  style="width: auto; height: auto; max-width: auto; height: 100px;" class="lightbox-image-list float-left"/>   
                            </a>
                        </div>

                        <div v-if="message.file == 1 && (message.mime.includes('mp4') || message.mime.includes('mkv') || message.mime.includes('avi') )" class="video-box">

                            <video controls class="video-player" >
                                <source :src="'/messages/files/' + message.filename" type="video/mp4">
                              Your browser does not support the video tag.
                              </video>
                        </div>

                        <div v-if="message.file == 0" class="recieved-message" >
                            @{{message.message}}

                        </div>

                        
                    </div>

                    <div class="d-sm-none  ml-5"></div>

                    <div class="d-none d-sm-none d-md-block d-lg-block col-sm-12 col-12 col-md-3  text-center pr-4" style="align-self: center;">
                        @{{message_time(message.created_at)}}
                    </div>

                </div>

              
            </li>
            
        </ul>
            
       
            
            <div class="bg-white row p-1 mb-2 mt-2" style="position: relative;bottom:0;margin:0" class="" >
                <hr style="width:100%;margin-left:1%">
                <div class="col-10" v-if="active_status == 1 ? active_action = 'display:block' : active_action = 'display:none'" :style="active_action">
    
                    <input type="text" placeholder="Type a message" v-model="new_message" class="message-input pl-2" @keyup.enter="store_new_message" style="black; height:40px; outline:none; "  >
    
                </div>
                <div class="col-2" v-if="active_status == 1 ? active_action = 'display:block' : active_action = 'display:none'" :style="active_action">
                    
                    <a href="javascript:;" style="position: absolute;right:15" @click="store_new_message" class="text-primary mt-2" ><i class="far fa-paper-plane fa-lg send-message search-bottom-icon-setting"></i></a>
                    
                    <input type="file" ref="file" style="display: none" @change="handleFiles" >


                    <a href="javascript:;" style="position: absolute;right:50%" @click="$refs.file.click()" class="mt-2 pr-2 " >
                        <i class="fas fa-paperclip attach-file-icon "></i></a>

                </div>
            </div>
        </div>
    
    
    </div>

    </div>

@endsection


@section('scripts')


<script src="{{ asset('admin-plugins/toastr/toastr.min.js')}}"></script>

<link rel="manifest" href="{{request()->root()}}/public/manifest.json">
{{--  importScripts('https://www.gstatic.com/firebasejs/7.14.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.14.2/firebase-messaging.js');  --}}

<script>

    {{--  $(document).ready(function() {
        $( ".user-list .user-item" ).bind( "click", function(event) {
            event.preventDefault();
            var clickedItem = $( this );
            $( ".user-list .user-item" ).each( function() {
                $( this ).removeClass( "active-chat" );
            });
            clickedItem.addClass( "active-chat" );
        });
    });  --}}

    @if(isset($reciever))
        $(document).ready(function() {
            //var $container = $('#myUL-3'),
            //$scrollTo = $('#user-message-count-{{$reciever->id}}');

            //$("#myUL-3").animate({ scrollTop: $('#myUL-3').height()}, 10);

            $(document).ready(function () {
                $('.contact-list ').animate({
                    scrollTop: $('.contact-list .active-chat').position().top
                }, 'slow');
            });
            //$('#myUL-3').scrollTop($('#user-message-count-{{$reciever->id}}'));


            // $('#myUL-3').animate({
             //   scrollTop: $("#user-message-count-{{$reciever->id}}").offset().top
            //}, 2000);
});
@endif


    </script>

  <script>

    const app = new Vue({
      el: '#chat',
      data: {
        users : {!! $contacts !!},
        user_requests : {!! $user_requests !!},
        messages: [],
        unread_messages: [],
        commentBox: '',
        new_message:'',
        new_message_sent: null,
        sending_message:null,
        sender: {{auth()->user()->id}} ,
        sending_user: {!! auth()->user() !!} ,
        reciever: '' ,
        reciever_id: '' ,
        profile_image: null ,
        active_status : 0,
        active_action : null,
        pre_reciever : {!! isset($reciever) ? json_encode($reciever) : 'null' !!},
        user_count: 0,
        request_user_count: 0,
        files: [],
      },
      mounted() { 
        //this.load_chat();
        //this.listener_for_chat();
        if(this.pre_reciever != null){ 
            this.get_this_chat(this.pre_reciever);
        }
        this.check_for_unread_messages();
        this.checkUnreadCount();
    },
      methods: {


        
        store_new_message(){

            
            if(this.new_message.length > 0){
            
            this.new_message_sent = { id : null , message: this.new_message , sender_id: this.sender , reciever_id : this.reciever.id , created_at : new Date , update_at : new Date , file:0 }

            this.messages.push(this.new_message_sent);
        
            this.sending_message = this.new_message;
            this.new_message = '';
            this.new_message_sent = '';
                
            axios.post('/api/chat/store', {
                message : this.sending_message,
                sender : this.sender,
                reciever : this.reciever.id,
            })
                .then( res => {
                    //console.log(res.data);
                    //this.messages.push(res.data);
                    //this.new_message = '';
                    //this.new_message_sent = '';
                })
                .catch( error => {
                console.log(error);
                });
            }
        },

        handleFiles(){

            this.$Progress.start();
            let loader = this.$loading.show();

  
  
            let uploadedFiles = this.$refs.file.files;

            for(var i = 0; i < uploadedFiles.length; i++) {
                this.files.push(uploadedFiles[i]);
            }
            
            for( let i = 0; i < this.files.length; i++ ){
                    if(this.files[i].id) {
                        continue;
                    }
                    let formData = new FormData();
                    formData.append('sender', this.sender);
                    formData.append('reciever', this.reciever.id);

                    formData.append('file', this.files[i]);    
                    axios.post('/api/chat/store/file',
                        formData,
                        {
                            headers: {
                                'Content-Type': 'multipart/form-data'
                            }
                        }
                    ).then(function(data) {
                        
                        loader.hide();
                        this.$Progress.finish();

                        //console.log(data.data.message);
                        //this.files[i].id = data['data']['id'];
                        //this.files.splice(i, 1, this.files[i]);

                        this.messages.push(data.data.message);

                    }.bind(this)).catch(function(data) {
                        //console.log(data);
                        /*Toast.fire({
                            icon: 'error',
                            title: 'File upload Failed'
                          }); */
                        toastr.error('File upload Failed.')

                        
                        loader.hide();
                        this.$Progress.fail();

                    });
                }
            this.files = [];
        },
        load_chat(){
            axios.get('/api/chat/'+ {{auth()->user()->id}} +'/get/'+ 1)
            .then( res => {
                this.messages = res.data.chats;
            }).catch( errors => {
                console.log(errors)
            });
        },
        get_this_chat(reciever){
            {{-- $('#messages-div').loading({
                stoppable: false
              });   --}}

            let loader = this.$loading.show();
            this.active_status = 1;
            this.reciever = reciever;

            $('#user-message-count-'+reciever.id).hide();

            axios.get('/api/chat/'+ {{auth()->user()->id}} +'/get/'+ reciever.id)
            .then( res => {

                this.messages = res.data.chats;
                this.updateUnreadCount(reciever.id);
                loader.hide();

            }).catch( errors => {
                console.log(errors)
            });

            setInterval(() => { this.update_current_chat() }, 8000); 

        },
         
        check_for_unread_messages(){
            
            setInterval(() => {
            
            axios.get('{{url("/")}}/api/get/new-messages/{{auth()->user()->id}}')
            .then( res => {
                this.users = res.data.users;
                this.user_requests = res.data.user_requests;
                this.checkUnreadCount();
            } )
            .catch(error => { console.log(error) });
            } , 15000);
        },
        update_current_chat(){
            //console.log('update is called')
            if(this.reciever){

                axios.get('/api/chat/'+ {{auth()->user()->id}} +'/get/'+ this.reciever.id)
                .then( res => {
                    this.messages = res.data.chats;
                    
                }).catch( errors => {
                    //console.log(errors)
                });
            }
        },
        checkUnreadCount(){
            user_count = 0;
            request_user_count = 0;

            this.users.map(user => {
                //console.log(user)
                if(user.unread_count > 0){
                    user_count ++;
                }
            })
            
            this.user_requests.map(user => {
                //console.log(user)
                if(user.unread_count > 0){
                    request_user_count ++;
                }
            })
            this.user_count = user_count
            this.request_user_count = request_user_count
            //console.log(this.users)
            //console.log(this.user_requests)
        },

        updateUnreadCount(id){
            user_count = 0;
            request_user_count = 0;
            console.log('updateUnreadCount')
            this.users.map(user => {
                //console.log(user)
                if( user.id != id && user.unread_count > 0){
                    user_count ++;
                }
            })
            
            this.user_requests.map(user => {
                //console.log(user)
                if(user.id != id && user.unread_count > 0){
                    request_user_count ++;
                }
            })
            this.user_count = user_count
            this.request_user_count = request_user_count
        },
        message_time(message_time){
            var time = new Date(message_time);
            //console.log( message_time , ' -- ' ,  time);

            var hours = time.getHours();
            var minutes = time.getMinutes();
            var ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0'+minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;

        },
        downloadFile(id){
        
        axios.get('/api/chat/download/file/'+id).then( () => {  } ).catch( () => {  } );

        
        },
        listener_for_chat(){
                Echo.channel("chat-{{auth()->user()->id}}").listen("ChatMessageEvent", e => {
                    //console.log(e);
                    //this.chat.messages.push(e.message);
                    this.messages.push(e.chat);

                //this.load_chat();
                });
                if(this.reciever){
                }
            } ,

            close_chat(){
                this.messages = [];
                this.reciever = ''
                this.active_status = 0

                $( ".user-list .user-item" ).each( function() {
                    $( this ).removeClass( "active-chat" );
                });
          

            },



            search_function() {
                var input, filter, ul, li, a, i, txtValue;
                input = document.getElementById("myInput");
                filter = input.value.toUpperCase();
                ul = document.getElementById("myUL");
                li = ul.getElementsByTagName("li");
                for (i = 0; i < li.length; i++) {
                    a = li[i].getElementsByTagName("a")[0];
                    txtValue = a.textContent || a.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        li[i].style.display = "";
                    } else {
                        li[i].style.display = "none";
                    }
                }
            }

            
      }
    });


 
  </script>
@endsection
{{-- <script src="https://www.gstatic.com/firebasejs/7.14.2/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.2/firebase-messaging.js"></script>
 --}}
{{--    var firebaseConfig = {
        apiKey: "AIzaSyDeMJQXeW71BD1XNd7O09hbIJ9jvpK9CHs",
        authDomain: "perfit-74ecd.firebaseapp.com",
        databaseURL: "https://perfit-74ecd.firebaseio.com",
        projectId: "perfit-74ecd",
        storageBucket: "perfit-74ecd.appspot.com",
        messagingSenderId: "934366178639",
        appId: "1:934366178639:web:ccc982ba696595d311693a",
        measurementId: "G-CZ79NKC623"
      };
      // Initialize Firebase
      firebase.initializeApp(firebaseConfig);
      //firebase.analytics();
      const messaging = firebase.messaging();
        
      
        messaging.getToken().then((currentToken) => {
            if (currentToken) {
                console.log('token recieved' , currentToken);
              sendTokenToServer(currentToken);
              //updateUIForPushEnabled(currentToken);
            } else {
              // Show permission request.
              console.log('No Instance ID token available. Request permission to generate one.');
              // Show permission UI.
              //updateUIForPushPermissionRequired();
              //setTokenSentToServer(false);
            }
          }).catch((err) => {
            console.log('An error occurred while retrieving token. ', err);
            //showToken('Error retrieving Instance ID token. ', err);
            //setTokenSentToServer(false);
          });
    

      // Retrieve an instance of Firebase Messaging so that it can handle background
      // messages.
      // Retrieve Firebase Messaging object.
      messaging.requestPermission().then(function() {
        console.log('Notification permission granted.');
        // TODO(developer): Retrieve an Instance ID token for use with FCM.
        // ...
      }).catch(function(err) {
        console.log('Unable to get permission to notify.', err);
      }); 
      messaging.onMessage( (payload) => {
        //alert('Firebase Message');
        console.log(payload)
      }); --}}



 {{-- <script src="https://www.gstatic.com/firebasejs/7.14.2/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.2/firebase-messaging.js"></script>
    <script>
     

        $(document).ready(function(){
            setInterval(() => { get_new_message() }, 8000); 
          })
    
          function get_new_message( ){
              $.ajax({
                    url : '{{url("/")}}/get/new-messages/'+{{auth()->user()->id}} ,
                    success : function(res){
    
                        res.forEach( function (message){
    
                            if(message[1] > 0){
                                $('#user-message-count-'+message[0]).html(message[1]);
                                $('#user-message-count-'+message[0]).show();
                            }
                            if(message[1] == 0)
                            $('#user-message-count-'+message[0]).hide();
    
                        })
                        
    
                        //console.log(res)
                    }
                }).fail( err => {
                console.log(err);
              })
          }



// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
firebase.initializeApp({
  apiKey: 'AIzaSyC2gmhm0X64V__GM_8NUO3LqL3wU4gZ9OE',
  authDomain: 'dawamitesting.firebaseapp.com',
  databaseURL: 'https://dawamitesting.firebaseio.com',
  projectId: 'dawamitesting',
  storageBucket: 'dawamitesting.appspot.com',
  messagingSenderId: '687996428523',
  appId: '1:687996428523:web:09faeffaf0f5eee4f2deae',
  measurementId: 'G-X0EGPMTBHP',
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
// Retrieve Firebase Messaging object.
const messaging = firebase.messaging();
messaging.requestPermission().then(function() {
  console.log('Notification permission granted.');
  // TODO(developer): Retrieve an Instance ID token for use with FCM.
  // ...
}).catch(function(err) {
  console.log('Unable to get permission to notify.', err);
});  --}}