

@extends('layouts.dashboard')

@section('content')


<div class="row mt-5">
  <div class="col-3">
  <div class="info-box mb-3 bg-warning">
      <span class="info-box-icon"><i class="fas fa-users"></i></span>
      
      <div class="info-box-content">
          <span class="info-box-text">Clients</span>
          <span class="info-box-number"> {{auth()->user()->coach->statistics['total_clients']}} </span>
      </div>
      
      <!-- /.info-box-content -->
  </div>
  </div>


  <div class="col-3">
  <div class="info-box mb-3 bg-danger">
      <span class="info-box-icon"><i class="fas fa-running"></i></span>
      
      <div class="info-box-content">
          <span class="info-box-text">Transformed</span>
          <span class="info-box-number"> {{auth()->user()->coach->statistics['completed_plans']}} </span>
      </div>
      <!-- /.info-box-content -->
  </div>
  </div>


  <div class="col-3">
  <div class="info-box mb-3 bg-success">
      <span class="info-box-icon"><i class="fas fa-spinner"></i></span>
      
      <div class="info-box-content">
          <span class="info-box-text">In-Progress</span>
          <span class="info-box-number"> {{auth()->user()->coach->statistics['new_plans']}} </span>
      </div>
      <!-- /.info-box-content -->
  </div>
  </div>

  <div class="col-3">
  <div class="info-box mb-3 bg-primary">
      <span class="info-box-icon"><i class="fas fa-star-half-alt"></i></span>
      
      <div class="info-box-content">
          <span class="info-box-text">Average Rating</span>
          <span class="info-box-number">{{auth()->user()->coach->statistics['average_ratings']}} ({{auth()->user()->coach->statistics['total_reviews']}}) </span>
      </div>
      <!-- /.info-box-content -->
  </div>
  </div>
</div>


<div class="row mt-4">
  <div class="col-12">
      <div id="accordion">
          <div class="card card-info">
            <div class="card-header" id="headingOne">
              <h5 class="mb-0">
                <div class="toggle-btn" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                  <div class="review-top">
                      <div>
                          {{ auth()->user()->name . ' Reviews '}} 
                      </div>
                      <div class="review-count">
                          {{auth()->user()->coach->reviews->count()}}
                      </div> 

                      <a class="ml-auto btn btn-success btn-sm" href="{{route('coach.reviews')}}"><i class="fas fa-list mr-2"></i> View All</a>
                  </div>
              </div>
              </h5>
            </div>
        
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
              <div class="card-body">

                  @if(auth()->user()->coach && auth()->user()->coach->reviews && auth()->user()->coach->reviews->count() > 0)
                  @foreach (auth()->user()->coach->reviews->take(5) as $review)
                      
                  <div class="review-head">
                      <div class="review-user-image-box">
                          @if($review->user && $review->user->photo)
                          <img class="review-user-image" src="{{asset('images/profile_images/'.$review->user->photo)}}" />
                          @else
                          <img class="review-user-image" src="{{asset('img/no-image-found.jpg')}}" />
                          @endif
                      </div>

                      <div class="rating">
                          <div class="review-user-name"> @if($review->user) {{$review->user->name}} @endif </div>
                          <div class="results">
                              <div class="results-content">
                                <span class="stars">{{$review->rating}}</span> 
                              </div>
                            </div>
                      </div>
                      <div class="date">
                          {{$review->proper_date}}
                      </div>
                  </div>
                  <div class="review-body">
                      <div class="review-description"> {{$review->description}} </div>
                  </div>
                  <hr>
                  @endforeach
                  @else
                  <div class="alert alert-info">No Reviews </div>
                  @endif
              </div>
            </div>
          </div>
          
          
        </div>
        
  </div>
</div>



{{--  <div class="card">

    <div id="chartContainer" style="height: 300px; width: 100%;"></div>

</div>  --}}


{{--  <div class="card">
    <div class="card-header d-flex p-0">
      <h3 class="card-title p-3">
        <i class="fas fa-chart-pie mr-1"></i>
        Sales
      </h3>
      <ul class="nav nav-pills ml-auto p-2">
        <li class="nav-item">
          <a class="nav-link active" href="#revenue-chart" data-toggle="tab">Area</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#sales-chart" data-toggle="tab">Donut</a>
        </li>
      </ul>
    </div><!-- /.card-header -->
    <div class="card-body">
      <div class="tab-content p-0">
        <!-- Morris chart - Sales -->
        <div class="chart tab-pane active" id="revenue-chart"
             style="position: relative; height: 300px;">
            <canvas id="revenue-chart-canvas" height="300" style="height: 300px;"></canvas>                         
         </div>
        <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;">
          <canvas id="sales-chart-canvas" height="300" style="height: 300px;"></canvas>                         
        </div>  
      </div>
    </div><!-- /.card-body -->
  </div>


    <div class="card-body">
        <div class="tab-content p-0">
        <!-- Morris chart - Sales -->
        <div class="chart tab-pane active" id="revenue-chart"
            style="position: relative; height: 300px;">
            <canvas id="revenue-chart-canvas" height="300" style="height: 300px;"></canvas>                         
        </div>
        <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;">
            <canvas id="sales-chart-canvas" height="300" style="height: 300px;"></canvas>                         
        </div>  
        </div>
    </div>
    
    <div class="card-body">
        <div class="tab-content p-0">
          <!-- Morris chart - Sales -->
          <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
              <canvas id="revenue-chart-canvas" height="300" style="height: 300px; display: block; width: 577px;" width="577" class="chartjs-render-monitor"></canvas>                         
           </div>
          <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;">
            <canvas id="sales-chart-canvas" height="0" style="height: 0px; display: block; width: 0px;" class="chartjs-render-monitor" width="0"></canvas>                         
          </div>  
        </div>
      </div>


        --}}




@endsection