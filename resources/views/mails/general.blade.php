<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FTBLE Email</title>
    {{--  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">  --}}
    
    {{--  background-color: #fab14952;  --}}
    {{--  font-weight: 600;  --}}
    <style>
        .header{
            width: 100%;
            background-color: #fab149;
            height: 60px;
            font-size: 45px;
            font-family: sans-serif;
            border-radius: 5px;
            text-align: center;
        }
        .body{
            padding: 10px 20px;
            font-family: sans-serif;
            line-height: 25px;        
        }
        .footer-logo{
            width: 80px;
            height: 80px;
        }
        .hr{
            height: 1px;
            width: 99%;
            border-width: 0;
            box-shadow: 0 0 5px 0px #afafaf;
            background-color: #c2c2c2;
        }
        .follow-strip{
            text-align: center;
        }
        .social-logo{
            width: 30px;
            height: 30px;
        }
        .banner-image-box{
            text-align: center;
        }
        .banner-image{
            max-width: 100%;
        }

        @media screen and (max-width:600px){
            .footer{
                text-align: center;
            }
            .footer-logo{
                width: 65px;
                height: 65px;
            }
            .social-logo{
                width: 25px;
                height: 25px;
            }
            .header {
                height: 50px;
                font-size: 45px;
                text-align: center;
                justify-content: center;
            }
        }
    </style>
</head>
<body>
    

    <div class="header">
        FTBLE
    </div>
    <div class="body">

        <div class="email-body">
            {!! $msg !!}
        </div>
        
        
        <hr class="hr">

        <div class="footer">
            <div>
                FTBLE Team,
            </div>
            <a href="http://ftble.com">
                <img  class="footer-logo" width="65" height="65" src="http://portal.ftble.com/img/ftble-logo.jpeg"/>
                {{--  <img  class="footer-logo" width="65" height="65" src="http://portal.ftble.com/img/logo.png"/>  --}}
                {{-- <img  class="footer-logo" src="{{asset('/img/logo.png')}}"/> --}}
            </a>
        </div>
        
        <div class="follow-strip">
            <div>
                Follow Us On
            </div>
            <div>
                <a href="https://www.facebook.com/ftble">
                    <img width="30" height="30" class="social-logo" src="http://portal.ftble.com/img/facebook-1.png"/>
                </a>
                <a href="https://www.instagram.com/ftble/">
                    <img width="30" height="30" class="social-logo" src="http://portal.ftble.com/img/instagram.png"/>
                </a>
                {{-- <img  class="social-logo" src="{{asset('/img/facebook.png')}}"/>
                <img  class="social-logo" src="{{asset('/img/instagram.png')}}"/> --}}
            </div>

        </div>
    </div>

</body>
</html>