@extends('layouts.dashboard')

@push('style')


@endpush

@section('content')


<div class="row">

@include('layouts.partials.swals')

<div class="col-md-12" style="padding:30px" >
    <div class="card card-info ">
        <div class="card-header">
            <h3 class="card-title">Update Supplement</h3>
        </div>
        <!-- /.card-header -->
        <div style="padding:30px" >
            <form action="{{route('admin.supplements.update', $supplement->id)}}" method="POST" enctype="multipart/form-data" >
                @include('layouts.partials.form_errors')

                @csrf
                @method('PATCH')

                <div class="form-group row">
                    <div class="col-sm-2">
                    </div>
                    <div class="col-sm-8">
            
                        <img id="image" src="@if($supplement->image) {{asset('images/supplements/'.$supplement->image)}} @else {{asset('img/no-image-found.jpg')}} @endif" class="profile-image" />
            
                        <button type="button" class="btn btn-outline-success" id="upload-button"> <i class="fas fa-image"></i> Upload Image </button>
                        
                        <input id="" type="file" class="file-upload" name="image" style="display:none" >
            
                    </div>
                    </div>


                <div class="form-group row">
                    <div class="col-sm-2">
                        <label for="name" class="row float-right col-form-label ">Name:</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" placeholder="Supplement Name" name="name" value="{{$supplement->name}}" required>
                    </div>
                </div>




                <div class="form-group row">
                    <div class="col-sm-2">
                        <label for="description" class="row float-right col-form-label ">Description:</label>
                    </div>
                    <div class="col-sm-8">
                        <textarea class="textarea" name="description" id="description" placeholder="Description" rows="4" >{!! $supplement->description !!}</textarea>
                    </div>
                </div>
                    
                <div class="form-group row">
                    <div class="col-sm-2">
                    <label for="inputEmail1" class="row float-right col-form-label ">Tags:</label>
                </div>

                <div class="col-sm-8">
                        <select  class=" select2bs4" multiple style="width: 100%" id="" placeholder="Tags" name="tags[]" required>
                        @foreach($tags as $tag)
                        <option value="{{$tag->id}}" @if(in_array($tag->id , $supplement->tags->pluck('id')->toArray())) selected @endif  > {{$tag->name}} </option>
                        @endforeach
                        </select>

                    </div>
                </div>
                {{-- <div class="form-group row">
                    <div class="col-sm-2">
                        <label for="image" class="row float-right col-form-label ">Image:</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="file" class="form-control" name="image" id="image">
                    </div>
                </div> --}}


                <div>
                    <button id="signup" class="btn btn-primary float-right">Update</button>
                </div>
            </form>
        </div>
    </div>

</div>
</div>

@endsection

@push('script')
<script>
    var tagnames = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '{{route('tagnames')}}'
    });

    tagnames.initialize();

    $('#tag').tagsinput({
        typeaheadjs: {
            name: 'tagnames',
            displayKey: 'name',
            valueKey: 'name',
            source: tagnames.ttAdapter()
        },
        freeInput : false

    });

</script>
@endpush