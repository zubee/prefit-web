@extends('layouts.dashboard')


@section('content')


    <div class="row">

        @include('layouts.partials.swals')

        <div class="col-md-12" style="padding:30px" >
            <div class="card ">
                <div class="card-header">
                    <h3 class="card-title">Manage Supplements</h3>
                    <a class="btn btn-outline-primary float-right" href="{{route('admin.supplements.create')}}"> <i class="fas fa-plus"></i> Create</a>
                </div>
                <!-- /.card-header -->
                <div style="padding:30px" >
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Supplement</th>
                            <th>Create Date</th>
                            <th>Actions</th>
                        </tr>
                        @if(isset($supplements))
                            @foreach($supplements as $k => $supplement)
                                <tr>
                                    <td> {{$k+1}} </td>
                                    <td> {{$supplement->name}} </td>

                                    <td> {{$supplement->created_at->format('m/d/Y') }} </td>
                                    <td>


                                        <a href="javascript:;" onclick="AskBeforeDelete({{$supplement->id}})" class="text-danger"  > <i class="fas fa-trash"></i> </a>

                                        <a href="{{route('admin.supplements.edit' , $supplement->id)}}" > <i class="fas fa-edit"></i> </a>
                                    </td>

                                    <form id="delete-form-{{$supplement->id}}" action="{{route('admin.supplements.destroy' , $supplement->id)}}" method="POST" >
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
    <script>
        function AskBeforeDelete(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {

                    $('#delete-form-'+id).submit();
                }
            })
        }

    </script>
@endsection




