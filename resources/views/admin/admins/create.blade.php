@extends('layouts.dashboard')


@section('content')


<div class="row">

   @include('layouts.partials.swals')

<div class="col-md-12" style="padding:30px" >
    <div class="card card-info ">
      <div class="card-header">
        <h3 class="card-title">Create New Admin</h3>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <form action="{{route('admin.admins.store')}}" method="POST" enctype="multipart/form-data">
          @include('layouts.partials.form_errors')
          @csrf

          <div class="form-group row">
            <div class="col-sm-2">
          </div>
            <div class="col-sm-8">
  
              <img id="image" src="{{asset('img/no-image-found.jpg')}}" class="profile-image" />
  
              <button type="button" class="btn btn-outline-success" id="upload-button"> <i class="fas fa-image"></i> Upload Image </button>
              
              <input id="" type="file" class="file-upload" name="image" style="display:none" >
  
            </div>
          </div>
  

          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Name:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputEmail1" placeholder="Name" name="name" value="{{old('name')}}" required>
            </div>
          </div>

          <div class="form-group row">
              <div class="col-sm-2">
                  <label for="username" class="row float-right col-form-label ">User Name:</label>
              </div>
              <div class="col-sm-8">
                  <input type="text" class="form-control" id="username" placeholder="User Name" name="username" value="{{old('username')}}" required >
                  <div >
                      <p id="username-error" class="text-danger" style="display:none; height: 10px" >&#x2717 username not available</p>
                      <p id="username-success" class="text-success" style="display:none; height: 10px" >&#x2713 username is available</p>
                  </div>

              </div>

          </div>

          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail2" class="row float-right col-form-label ">Email:</label>
          </div>
            <div class="col-sm-8">
              <input type="email" class="form-control" id="email" placeholder="Email" name="email" value="{{old('email')}}" required>
                <div>
                    <p id="email-error" class="text-danger" style="display:none; height: 10px" >&#x2717 email not available</p>
                    <p id="email-success" class="text-success" style="display:none; height: 10px" >&#x2713 email is available</p>
                </div>

            </div>
          </div>
          
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail3" class="row float-right col-form-label ">Password:</label>
          </div>
            <div class="col-sm-8">
              <input type="password" class="form-control" id="inputEmail3" placeholder="Password" name="password" autocomplete="off" required>
            </div>
          </div>
          
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail4" class="row float-right col-form-label ">Confirm Password:</label>
          </div>
            <div class="col-sm-8">
              <input type="password" class="form-control" id="inputEmail4" placeholder="Confirm Password" name="conf_password" autocomplete="off" required>
            </div>
          </div>
          

        
        <div>
            <button id="signup" class="btn btn-primary float-right">Create</button>
        </div>
      </form>
    </div>
    </div>
  
  </div>
</div>

@endsection

@push('script')

<script>

    $(document).ready(function () {
        var isValid = true;
        var usernameerror = false;
        var emailerror = false;



        jQuery('#username').keyup(function (e) {


            e.preventDefault();
            var username = $('#username').val();
            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            jQuery.ajax({
                url: "{{route('verify.username')}}",
                method: 'POST',
                data:{
                    username: username,
                },
                success: function (result) {
                    console.log(result)
                    if($('#username').val().length>0){
                        if(result == 1){
                            $('#username-success').hide();
                            $('#username-error').show();
                            usernameerror = true;

                            if(!isValid || ( usernameerror ||  emailerror ) )
                            {
                                $('#signup').prop('disabled', true)
                            }
                        }
                        else {
                            $('#username-success').show();
                            $('#username-error').hide();
                            usernameerror = false;

                            if(isValid && (!usernameerror && !emailerror)){
                                $('#signup').prop('disabled', false);
                            }
                        }
                    }

                    else {
                        $('#username-success').hide();
                        $('#username-error').hide();
                        usernameerror = false;
                    }


                }


            });


        })


        jQuery('#email').keyup(function (e) {


            e.preventDefault();
            var email = $('#email').val();
            $.ajaxSetup({
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            jQuery.ajax({
                url: "{{route('verify.email')}}",
                method: 'POST',
                data:{
                    email: email,
                },
                success: function (result) {
                    console.log(result)
                    if($('#email').val().length>0){
                        if(result == 1){
                            $('#email-success').hide();
                            $('#email-error').show();
                            emailerror = true;

                            if(!isValid || ( usernameerror ||  emailerror ) )
                            {
                                $('#signup').prop('disabled', true)
                            }
                        }
                        else {
                            $('#email-success').show();
                            $('#email-error').hide();
                            emailerror = false;

                            if(isValid && (!usernameerror && !emailerror)){
                                $('#signup').prop('disabled', false);
                            }
                        }
                    }

                    else {
                        $('#email-success').hide();
                        $('#email-error').hide();
                        emailerror = false;
                    }


                }


            });


        })


    });




</script>
    @endpush