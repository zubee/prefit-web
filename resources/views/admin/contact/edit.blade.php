@extends('layouts.dashboard')


@section('content')


<div class="row">


<div class="col-md-12" style="padding:30px" >
    <div class="card card-info ">
      <div class="card-header">
        <h3 class="card-title">Update User</h3>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <form action="{{route('admin.contact.update')}}" method="POST" enctype="multipart/form-data">
        @include('layouts.partials.form_errors')
        @csrf

        <div class="form-group row">
          <div class="col-sm-2">
        </div>
          <div class="col-sm-8">

            <img id="image" src="@if($support->photo) {{asset('images/profile_images/'.$support->photo)}} @else {{asset('img/no-image-found.jpg')}} @endif" class="profile-image" />

            <button type="button" class="btn btn-outline-success" id="upload-button"> <i class="fas fa-image"></i> Upload Image </button>
            
            <input id="" type="file" class="file-upload" name="image" style="display:none" >

          </div>
        </div>



          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Name:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputEmail1" placeholder="Name" name="name" value="{{old('name') ?? $support->name }}" required>
            </div>
          </div>

          <div class="form-group row">
              <div class="col-sm-2">
                  <label for="username" class="row float-right col-form-label ">User Name:</label>
              </div>
              <div class="col-sm-8">
                  <input type="text" class="form-control" id="username" placeholder="User Name" name="username" value="{{old('username') ?? $support->username}}" required disabled>
              </div>
          </div>


          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail2" class="row float-right col-form-label ">Email:</label>
          </div>
            <div class="col-sm-8">
              <input type="email" class="form-control" id="inputEmail2" placeholder="Email" name="email" value="{{old('email') ?? $support->email }}" required>
            </div>
          </div>
          

        
        <div>
            <button class="btn btn-primary float-right">Update</button>
        </div>
      </form>
    </div>
    </div>
  
  </div>
</div>

@endsection