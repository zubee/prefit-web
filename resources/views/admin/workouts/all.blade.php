
@extends('layouts.dashboard')


@section('styles')
    <style>
        .exercise-gif-image{
            border-radius: 5px;
            width: auto;
            height: auto;
            max-width: 160px;
          }
            
    </style>
@endsection

@section('content')


    <div class="row">

        @include('layouts.partials.swals')

        <div id="workouts-vue" class="w-100">
        <div class="col-md-12" style="padding:30px" >
            <div class="card ">
                <div class="card-header">
                    <h3 class="card-title"> Workouts</h3>
                        <exercise-search
                            @fetch-result="get_workouts"
                            @searching-workouts="searching_workouts"
                            @workouts-update="workouts_updated"
                            @response-error="api_error"
                        ></exercise-search>
                </div>
                <!-- /.card-header -->
                <div style="padding:30px" >
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                        <tr v-for="(workout , index) in workouts">
                            <td> @{{index + 1}} </td>
                            <td> <img class="exercise-gif-image" :src="workout.GIF_Img" />  </td>
                            <td> @{{workout.Exercise_Name}} </td>
                            <td> <a class="btn btn-sm btn-primary" :href="workout.URL" target="_blank" >View</a> </td>
                        </tr>
                    </table>
                    <div v-if="!fetching_workouts && workout_error" class="alert alert-info">@{{workout_error}}</div>
                </div>
            </div>

        </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        const vue = new Vue({
            el: '#workouts-vue',
            data:{
                fetching_workouts:false,
                workout_error:'',
                workouts :[],
            },
            methods:{
                get_workouts($workouts){
                    console.log($workouts)
                    this.fetching_workouts = false
                    this.workouts = $workouts
                  },            
            
                  searching_workouts(){
                    this.workout_error = '';
                    this.fetching_workouts = true;
                    //this.workouts = [];
                  },
            
                  workouts_updated($workouts){
                    this.workout_error = '';
                    this.fetching_workouts = false;
                    this.workouts = $workouts;
                  },
            
                  api_error($error){
                    this.fetching_workouts = false;
                    if($error.error.length > 0){
                        console.log('array has values');
                        this.workout_error = $error.error[0];
                    }else{
                        this.workout_error = 'No such exercise exist';
                        console.log('array does not values');
            
                    }
                    this.workouts = [];
                  },
            
            
            
            }
          });
          
    </script>
@endsection