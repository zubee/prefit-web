@extends('layouts.dashboard')

@section('styles')
    <style>
        .image-container{
            display: inline;
            position: relative;
        }
        .image-container .btn{
            position: absolute ;
            top: -50;
            right: 0;
            border-radius: 50%;
        }
        .video-container {
            position: relative;
            display: inline;

        }
         .video-container .btn{
            position: absolute ;
            top: -150;
            right: 0;
            border-radius: 50%;
            background-color: #c8c8c8;

        }
        
        .image-container .btn:hover{
            background-color: #c8c8c8;
            color: #fff;

        }
        
        .video-container .btn:hover{
            background-color: #c8c8c8;
            color: #fff;

        }
    </style>
@endsection


@section('content')


    <div class="row">

        @include('layouts.partials.swals')

        <div class="col-md-12" style="padding:30px" >
            <div class="card card-info ">
                <div class="card-header">
                    <h3 class="card-title">Update Workout</h3>
                </div>
                <!-- /.card-header -->
                <div style="padding:30px" >
                    <form action="{{route('admin.workouts.update',$workout->id)}}" method="POST" enctype="multipart/form-data" >
                        @include('layouts.partials.form_errors')

                        @csrf
                        @method('PATCH')


                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label for="name" class="row float-right col-form-label ">Name:</label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{{$workout->name}}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label for="description" class="row float-right col-form-label ">Description:</label>
                            </div>
                            <div class="col-sm-8">
                                <textarea class="textarea" id="description" name="description" placeholder="Description" rows="4" >{!! $workout->description !!}</textarea>
                            </div>
                        </div>


       
                        <div class="form-group row">
                            <div class="col-sm-2">
                            <label for="inputEmail1" class="row float-right col-form-label ">Tags:</label>
                        </div>
        
                        <div class="col-sm-8">
                                <select  class=" select2bs4" multiple style="width: 100%" id="" placeholder="Tags" name="tags[]" required>
                                @foreach($tags as $tag)
                                <option value="{{$tag->id}}" @if(in_array($tag->id , $workout->tag->pluck('id')->toArray())) selected @endif  > {{$tag->name}} </option>
                                @endforeach
                                </select>
        
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-sm-2">
                            <label for="inputEmail1" class="row float-right col-form-label ">Image Gallery:</label>
                        </div>
        
                        <div id="deleted_images">
                        </div>

                        <div class="col-sm-8">
                            <h2><button style="font-size: 20px" class="btn btn-success" type="button" onclick="open_images()" > <i class="fas fa-plus"></i> </button></h2>
                            <input type="file" name="images[]" style="display: none" multiple id="gallery-photo-add">

                            <div class="gallery"  id="image-preview" >
                                @if( $workout->media && is_array(json_decode($workout->media)))
                                @foreach(json_decode($workout->media) as $k => $image )
                                <div class="image-container" id="image-{{$k}}" >
                                <img src="{{asset('workouts/images/'.$image)}} "  class="m-2" width="100" height="100" > 
                                <button type="button" class="btn" onclick="remove_image({{$k}})" >X</button>
                                </div>
                                @endforeach
                                @endif
                            </div>
        
                            </div>
                        </div>

                     
                        <div id="deleted_videos">
                        </div>


                        <div class="form-group row">
                            <div class="col-sm-2">
                            <label for="inputEmail1" class="row float-right col-form-label ">Video:</label>
                        </div>
        
                        <div class="col-sm-8 ">
                            <h2><button style="font-size: 20px" class="btn btn-success" type="button" onclick="open_video()" > <i class="fas fa-video"></i> </button></h2>
                            
                            <input type="file" name="videos[]" multiple accept="video/*" style="display: none" id="video">

                            
                            <div id="video_area">
                                @if( $workout->videos && is_array(json_decode($workout->videos)))
                                @foreach(json_decode($workout->videos) as $k => $video )
                                <div class="video-container" id="video-{{$k}}" >
                                    <video width="250" height="150" class="m-2" controls >
                                        <source src="{{asset('workouts/videos/'.$video)}}" id="video_display" >
                                        Your browser does not support HTML5 video.
                                    </video>

                                    <button type="button" class="btn" onclick="remove_video({{$k}})" >X</button>
                                </div>

                                @endforeach
                                @endif
                            </div>
        
                            </div>
                        </div>

                        {{--  <div class="form-group row">
                            <div class="col-sm-2">
                                <label for="media" class="row float-right col-form-label ">Image/Video:</label>
                            </div>
                            <div class="col-sm-8">
                                <input type="file" class="form-control" name="media" id="media" >
                            </div>
                        </div>
  --}}

                        <div>
                            <button id="signup" class="btn btn-primary float-right">Update</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection

@push('script')

    <script>
        function remove_image(index){
            //console.log(index);
            //$('#deleted_images').val(index);
            $('#image-'+index).remove();
            $('#deleted_images').append('<input type="hidden" name="deleted_images[]" value="'+index+'" />')
            
        }

        function remove_video(index){
            //console.log(index);
            //$('#deleted_images').val(index);
            $('#video-'+index).remove();
            $('#deleted_videos').append('<input type="hidden" name="deleted_videos[]" value="'+index+'" />')
            
        }


        var tagnames = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: '{{route('tagnames')}}'
        });

        tagnames.initialize();

        $('#tags').tagsinput({
            typeaheadjs: {
                name: 'tagnames',
                displayKey: 'name',
                valueKey: 'name',
                source: tagnames.ttAdapter()
            },
            freeInput : false

        });
     
        
        function open_images(){
            $('#gallery-photo-add').click()
        }


        function open_video(){
            $('#video').click()
        }


        $(function() {
            // Multiple images preview in browser
            var imagesPreview = function(input, placeToInsertImagePreview) {
        
                if (input.files) {
                    var filesAmount = input.files.length;
        
                    for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();
        
                        reader.onload = function(event) {
                            $('#image-preview').append('<img src="'+event.target.result+'"  class="m-2" width="100" height="100" > ')
                            //$($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                        }
        
                        reader.readAsDataURL(input.files[i]);
                    }
                }
        
            };
        
            $('#gallery-photo-add').on('change', function() {
                imagesPreview(this, 'div.gallery');
            });
        });

        document.querySelector("#video")
        .onchange = function(event) {
            var files = event.target.files;
            for (var i = 0; i < files.length; i++) {
            var f = files[i];
            // Only process video files.
            if (!f.type.match('video.*')) {
                continue;
            }

            var source = document.createElement('video'); //added now

            $(source).addClass('m-2');

            source.width = 250;

            source.height = 150;

            source.controls = true;

            source.src = URL.createObjectURL(files[i]);

            //document.body.appendChild(source); // append `<video>` element
            $('#video_area').append(source); // append `<video>` element

            }
        }




        $(document).on("change", "#video", function(evt) {
            $('#video_area').show();
            var $source = $('#video_display');
            $source[0].src = URL.createObjectURL(this.files[0]);
            $source.parent()[0].load();
          });



    </script>
@endpush