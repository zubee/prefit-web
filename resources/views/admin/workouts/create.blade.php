@extends('layouts.dashboard')



@section('style')
    <style>
        input[type="file"] {
            display: block;
          }
          .imageThumb {
            max-height: 75px;
            border: 2px solid;
            padding: 1px;
            cursor: pointer;
          }
          .pip {
            display: inline-block;
            margin: 10px 10px 0 0;
          }
          .remove {
            display: block;
            background: #444;
            border: 1px solid black;
            color: white;
            text-align: center;
            cursor: pointer;
          }
          .remove:hover {
            background: white;
            color: black;
          }
    </style>
@endsection

@section('content')


    <div class="row">

        @include('layouts.partials.swals')

        <div class="col-md-12" style="padding:30px" >
            <div class="card card-info ">
                <div class="card-header">
                    <h3 class="card-title">Create Workout</h3>
                </div>
                <!-- /.card-header -->
                <div style="padding:30px" >
                    <form action="{{route('admin.workouts.store')}}" method="POST" enctype="multipart/form-data" >
                        @include('layouts.partials.form_errors')

                        @csrf

                        {{-- <div class="form-group row">
                            <div class="col-sm-2">
                            </div>
                            <div class="col-sm-8">
                    
                                <img id="image" src="{{asset('img/no-image-found.jpg')}}" class="profile-image" />
                    
                                <button type="button" class="btn btn-outline-success" id="upload-button"> <i class="fas fa-image"></i> Upload Image </button>
                                
                                <input id="" type="file" class="file-upload" name="media" style="display:none" >
                    
                            </div>
                            </div> --}}
        



                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label for="name" class="row float-right col-form-label ">Name:</label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{{old('name')}}" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label for="description" class="row float-right col-form-label ">Description:</label>
                            </div>
                            <div class="col-sm-8">
                                <textarea class="textarea" id="description" name="description" placeholder="Description" rows="4" >{{old('description')}}</textarea>
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-sm-2">
                            <label for="inputEmail1" class="row float-right col-form-label ">Tags:</label>
                        </div>
        
                        <div class="col-sm-8">
                                <select  class=" select2bs4" multiple style="width: 100%" id="" placeholder="Tags" name="tags[]" required>
                                @foreach($tags as $tag)
                                <option value="{{$tag->id}}" > {{$tag->name}} </option>
                                @endforeach
                                </select>
        
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-2">
                            <label for="inputEmail1" class="row float-right col-form-label ">Image Gallery:</label>
                        </div>
        
                        <div class="col-sm-8">
                            <h2><button style="font-size: 20px" class="btn btn-success" type="button" onclick="open_images()" > <i class="fas fa-plus"></i> </button></h2>
                            
                            <input type="file" name="images[]" style="display: none" multiple id="gallery-photo-add">

                            <div class="gallery" id="image-preview" >

                            </div>
        
                            </div>
                        </div>
        
        
                        <div class="form-group row">
                            <div class="col-sm-2">
                            <label for="inputEmail1" class="row float-right col-form-label ">Video:</label>
                        </div>
        
                        <div class="col-sm-8 ">
                            <h2><button style="font-size: 20px" class="btn btn-success" type="button" onclick="open_video()" > <i class="fas fa-video"></i> </button></h2>
                            
                            <input type="file" name="videos[]" multiple accept="video/*" style="display: none" id="video">

                            
                            <div id="video_area">
                                {{-- <video width="250" height="150" controls >
                                    <source src="mov_bbb.mp4" id="video_display" >
                                    Your browser does not support HTML5 video.
                                </video> --}}
                            </div>
        
                            </div>
                        </div>
                        {{-- <div class="form-group row">
                            <div class="col-sm-2">
                            <label for="inputEmail1" class="row float-right col-form-label ">Tags:</label>
                        </div>
        
                        <div class="col-sm-8">
                            <div class="field" align="left">
                                <h3>Upload your images</h3>
                                <input type="file" id="files" name="files[]" multiple />
                              </div>
                                      
                            </div>
                        </div> --}}


                       
                        
                        {{--  <div class="form-group row">
                            <div class="col-sm-2">
                                <label for="media" class="row float-right col-form-label ">Image/Video:</label>
                            </div>
                            <div class="col-sm-8">
                                <input type="file" class="form-control" name="media" id="media" >
                            </div>
                        </div>  --}}


                        <div>
                            <button id="signup" class="btn btn-primary float-right">Create</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection

@push('script')

    <script>

        var tagnames = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: '{{route('tagnames')}}'
        });

        tagnames.initialize();

        $('#tags').tagsinput({
            typeaheadjs: {
                name: 'tagnames',
                displayKey: 'name',
                valueKey: 'name',
                source: tagnames.ttAdapter()
            },
            freeInput : false

        });


        function open_images(){
            $('#gallery-photo-add').click()
        }
        function open_video(){
            $('#video').click()
        }


 
    
          


        $(function() {
            // Multiple images preview in browser
            var imagesPreview = function(input, placeToInsertImagePreview) {
        
                if (input.files) {
                    var filesAmount = input.files.length;
        
                    for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();
        
                        reader.onload = function(event) {
                            $('#image-preview').append('<img src="'+event.target.result+'"  class="m-2" width="100" height="100" > ')
                            //$($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                        }
        
                        reader.readAsDataURL(input.files[i]);
                    }
                }
        
            };
            
           
            $('#gallery-photo-add').on('change', function() {
                imagesPreview(this, 'div.gallery');
            });
            
           
        });


        document.querySelector("#video")
            .onchange = function(event) {
                var files = event.target.files;
                for (var i = 0; i < files.length; i++) {
                var f = files[i];
                // Only process video files.
                if (!f.type.match('video.*')) {
                    continue;
                }

                var source = document.createElement('video'); //added now

                $(source).addClass('m-2');

                source.width = 250;

                source.height = 150;

                source.controls = true;

                source.src = URL.createObjectURL(files[i]);

                //document.body.appendChild(source); // append `<video>` element
                $('#video_area').append(source); // append `<video>` element

                }
            }

 

    </script>
@endpush