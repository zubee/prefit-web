
@extends('layouts.dashboard')


@section('styles')

<style>
    .same-height{
        height:78px !important;
    }
</style>
@endsection

@section('content')


    <div class="row">

        @include('layouts.partials.swals')

        <div class="col-md-12 mt-4">
            
        <div class="card card-info card-tabs mb-0 p-0">
            <div class="card-header p-0 pt-1">
                <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                  <li class="nav-item text-center" style="width:50%;">
                    <a  class="nav-link active" id="custom-tabs-local-wokrouts-home-tab" data-toggle="pill" href="#custom-tabs-one-local-wokrouts" role="tab" aria-controls="custom-tabs-one-local-wokrouts" aria-selected="true">Local Workouts</a>
                  </li>
                  <li class="nav-item text-center" style="width:50%;">
                    <a class="nav-link" id="custom-tabs-one-exrx-workouts-tab" data-toggle="pill" href="#custom-tabs-one-exrx-workouts" role="tab" aria-controls="custom-tabs-one-exrx-workouts" aria-selected="false">EXRX Workouts</a>
                  </li>
               
                </ul>
              </div>     
              </div>     
              
              <div class="tab-content" id="custom-tabs-one-tabContent">
                <div class="tab-pane fade show active" id="custom-tabs-one-local-wokrouts" role="tabpanel" aria-labelledby="custom-tabs-one-local-wokrouts-tab">

                
                <div class="card">
                    <div class="card-header same-height">

                        <h3 class="card-title">Workouts</h3>
                        <a class="btn btn-outline-primary float-right" href="{{route('admin.workouts.create')}}"> <i class="fas fa-plus"></i> Create</a>

                    </div>
                    <table class="table">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Create Date</th>
                            <th>Actions</th>
                        </tr>
                        @if(isset($workouts))
                            @foreach($workouts as $k => $workout)
                                <tr>
                                    <td> {{$k+1}} </td>
                                    <td> {{$workout->name}} </td>
                                    <td> {{$workout->created_at->format('m/d/Y') }} </td>
                                    <td>


                                        <a href="javascript:;" onclick="AskBeforeDelete({{$workout->id}})" class="text-danger"  > <i class="fas fa-trash"></i> </a>

                                        <a href="{{route('admin.workouts.edit' , $workout->id)}}" > <i class="fas fa-edit"></i> </a>
                                    </td>

                                    <form id="delete-form-{{$workout->id}}" action="{{route('admin.workouts.destroy' , $workout->id)}}" method="POST" >
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
                </div>


                    <div class="tab-pane fade" id="custom-tabs-one-exrx-workouts" role="tabpanel" aria-labelledby="custom-tabs-one-exrx-workouts-tab">
                        <div id="workouts-vue" class="w-100">
                                <div class="card ">
                                    <div class="card-header">
                                        <h3 class="card-title"> Workouts</h3>
                                            <exercise-search
                                                @fetch-result="get_workouts"
                                                @searching-workouts="searching_workouts"
                                                @workouts-update="workouts_updated"
                                                @response-error="api_error"
                                            ></exercise-search>
                                    </div>
                                    <!-- /.card-header -->
                                    <div>
                                        <table class="table">
                                            <tr>
                                                <th>#</th>
                                                <th>Image</th>
                                                <th>Name</th>
                                                <th>Actions</th>
                                            </tr>
                                            <tr v-for="(workout , index) in workouts">
                                                <td> @{{index + 1}} </td>
                                                <td> <img class="exercise-gif-image" :src="workout.GIF_Img" />  </td>
                                                <td> @{{workout.Exercise_Name}} </td>
                                                <td> <a class="btn btn-sm btn-primary" :href="workout.URL" target="_blank" >View</a> </td>
                                            </tr>
                                        </table>
                                        <div v-if="!fetching_workouts && workout_error" class="alert alert-info">@{{workout_error}}</div>
                                    </div>
                                </div>
                    
                            </div>
                </div>
                </div>

        </div>
    </div>

@endsection

@section('scripts')
    <script>
        function AskBeforeDelete(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {

                    $('#delete-form-'+id).submit();
                }
            })
        }

    </script>


    <script>
        const vue = new Vue({
            el: '#workouts-vue',
            data:{
                fetching_workouts:false,
                workout_error:'',
                workouts :[],
            },
            methods:{
                get_workouts($workouts){
                    console.log($workouts)
                    this.fetching_workouts = false
                    this.workouts = $workouts
                  },            
            
                  searching_workouts(){
                    this.workout_error = '';
                    this.fetching_workouts = true;
                    //this.workouts = [];
                  },
            
                  workouts_updated($workouts){
                    this.workout_error = '';
                    this.fetching_workouts = false;
                    this.workouts = $workouts;
                  },
            
                  api_error($error){
                    this.fetching_workouts = false;
                    if($error.error.length > 0){
                        console.log('array has values');
                        this.workout_error = $error.error[0];
                    }else{
                        this.workout_error = 'No such exercise exist';
                        console.log('array does not values');
            
                    }
                    this.workouts = [];
                  },
            
            
            
            }
          });
          
    </script>
@endsection