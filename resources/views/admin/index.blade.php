

@extends('layouts.dashboard')



@section('styles')
    
{{--  <!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Tempusdominus Bbootstrap 4 -->
<link rel="stylesheet" href="{asset{(admin-'plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
<!-- iCheck -->
<link rel="stylesheet" href="{{asset('admin-plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
<!-- JQVMap -->
<link rel="stylesheet" href="{{asset('admin-plugins/jqvmap/jqvmap.min.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('admin-dist/css/adminlte.min.css')}}">
<!-- overlayScrollbars -->
<link rel="stylesheet" href="{{asset('admin-plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
<!-- Daterange picker -->
<link rel="stylesheet" href="{{asset('admin-plugins/daterangepicker/daterangepicker.css')}}">
<!-- summernote -->
<link rel="stylesheet" href="{{asset('admin-plugins/summernote/summernote-bs4.css')}}">  --}}

@endsection



@section('content')

<div class="p-4">

<div class="row">

    <div class="col-3">
    <div class="info-box mb-3 bg-warning">
        <span class="info-box-icon"><i class="fas fa-users"></i></span>
        
        <div class="info-box-content">
            <span class="info-box-text">Users</span>
            <span class="info-box-number">{{$counter['users']}}</span>
        </div>
        <!-- /.info-box-content -->
    </div>
    </div>


    <div class="col-3">
    <div class="info-box mb-3 bg-danger">
        <span class="info-box-icon"><i class="fas fa-running"></i></span>
        
        <div class="info-box-content">
            <span class="info-box-text">Personnel Trainers</span>
            <span class="info-box-number">{{$counter['coaches']}}</span>
        </div>
        <!-- /.info-box-content -->
    </div>
    </div>


    <div class="col-3">
    <div class="info-box mb-3 bg-success">
        <span class="info-box-icon"><i class="fas fa-cubes"></i></span>
        
        <div class="info-box-content">
            <span class="info-box-text">Packages</span>
            <span class="info-box-number">{{$counter['packages']}}</span>
        </div>
        <!-- /.info-box-content -->
    </div>
    </div>

    <div class="col-3">
    <div class="info-box mb-3 bg-primary">
        <span class="info-box-icon"><i class="fas fa-skating"></i></span>
        
        <div class="info-box-content">
            <span class="info-box-text">Workouts</span>
            <span class="info-box-number">{{$counter['workouts']}}</span>
        </div>
        <!-- /.info-box-content -->
    </div>
    </div>




    
</div>


{{--  <canvas id="myChart"></canvas>
<canvas class="ct-chart"></canvas>
<div id="curve_chart" style="width: 900px; height: 500px"></div>  --}}

{{--  <div class="ct-chart"></div>  --}}
{{--  
<div class="card p-3">

    <div id="chartContainer1" style="height: 300px; width: 100%;"></div>

  </div>  --}}


  {{--  <div class="card p-3">

    <canvas id="myChart" width="400" height="400"></canvas>

  </div>


  <div class="card p-3">

    <div id="chartContainer" style="height: 300px; width: 100%;"></div>

  </div>

  <div class="card p-3">

    <div id="chartContainer1" style="height: 300px; width: 100%;"></div>

  </div>
          --}}
        
        
        

</div>
@endsection

{{--  
      var ctx = document.getElementById('myChart').getContext('2d');
      var chart = new Chart(ctx, {
          // The type of chart we want to create
          type: 'line',
      
          // The data for our dataset
          data: {
              labels: {!! json_encode(array_reverse($all_dates)) !!},
              //labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
              datasets: [{
                  label: 'No of Payments',
                  backgroundColor: '#359aff',
                  borderColor: '#076acc',
                  data: {!! json_encode(array_reverse($all_payments)) !!}
                  //data: [0, 10, 5, 2, 20, 30, 45]
              }]
          },
      
          // Configuration options go here
          options: {}
      });
        --}}

@section('scripts')

  
<script>
    window.onload = function () {




























      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);


      function drawChart() {

      
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
        ]);

        var options = {
          title: 'Company Performance',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }

   
    
     
      var dataPoints = [];

      var options =  {
        animationEnabled: true,
        theme: "light2",
        title: {
          text: "Daily Sales Data"
        },
        axisX: {
          valueFormatString: "DD MMM YYYY",
        },
        axisY: {
          title: "USD",
          titleFontSize: 24
        },
        data: [{
          type: "spline", 
          yValueFormatString: "$#,###.##",
          dataPoints: dataPoints
        }]
      };
      
      function addData(data) {
        for (var i = 0; i < data.length; i++) {
          dataPoints.push({
            x: new Date(data[i].date),
            y: data[i].units
          });
        }
        $("#chartContainer1").CanvasJSChart(options);
      
      }


      addData({!! json_encode($data) !!});
      

     

    }
    
    
    </script>

    
 
@endsection