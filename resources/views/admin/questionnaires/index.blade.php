
@extends('layouts.dashboard')


@section('content')


<div class="row">

   @include('layouts.partials.swals')

<div class="col-md-12" style="padding:30px" >
    <div class="card ">
      <div class="card-header">
        <h3 class="card-title">Manage Questionnaire</h3>
        {{--  <h3 class="card-title">Manage Questionnaires</h3>  --}}
        <a class="btn btn-outline-primary float-right" href="{{route('admin.questionnaires.create')}}"> <i class="fas fa-plus"></i> Create</a>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <table class="table">
          <tr>
              <th>#</th>
              <th>Name</th>
              <th>Assign To</th>
              <th>Create Date</th>
              <th>Actions</th>
          </tr>
          @if(isset($questions))
          @foreach($questions as $k => $question)
          <tr>
              <td> {{$k+1}} </td>
              <td> {{$question->name}} </td>
              <td><a class="btn btn-sm btn-success" href="{{route('admin.question.assigned', $question->id)}}"> View ({{$question->users()->count()}}) </a></td>
              <td> {{$question->created_at->format('m/d/Y') }} </td>
              <td>
                
                {{-- <div class="icheck-success d-inline">
                    <input onchange="change_user_status({{$question->user->id}})" type="checkbox" @if($question->user->status == 1) checked @endif id="checkboxSuccess{{$question->user->id}}">
                    <label for="checkboxSuccess{{$question->user->id}}">
                    </label>
                  </div> --}}

                  <a href="javascript:;" onclick="AskBeforeDelete({{$question->id}})" class="text-danger"  > <i class="fas fa-trash"></i> </a>

                  <a href="{{route('admin.questionnaires.edit' , $question->id)}}" > <i class="fas fa-edit"></i> </a>

                  <a href="{{route('admin.questionnaires.show' , $question->id)}}" class="btn btn-sm btn-primary" > <i class="fas fa-eye"></i> View</a>


              </td>

              <form id="delete-form-{{$question->id}}" action="{{route('admin.questionnaires.destroy' , $question->id)}}" method="POST" >
                  @csrf
                  @method('DELETE')
              </form>
          </tr>
          @endforeach
          @endif
      </table>
    </div>
    </div>
  
  </div>
</div>

@endsection

@section('scripts')
    <script>
        function AskBeforeDelete(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
              }).then((result) => {
                if (result.value) {
                  
                    $('#delete-form-'+id).submit();
                }
              })
        }

        function change_user_status(id){
            $.ajax({
                url: '{{url("/")}}/change/user/status/'+id,
                success: function (res){
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'User Status Has been updated',
                        showConfirmButton: false,
                        timer: 1500
                      })
                }
            }).fail(function(){
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Some Problem occured',
                    showConfirmButton: false,
                    timer: 1500
                  })
            })
        }
    </script>
@endsection