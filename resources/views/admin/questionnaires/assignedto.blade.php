@extends('layouts.dashboard')


@section('content')


    <div class="row">

        @include('layouts.partials.swals')

        <div class="col-md-12" style="padding:30px" >
            <div class="card ">
                <div class="card-header">
                    <h3 class="card-title"><b>{{$question->question}}</b> Questionnaire Assigne To</h3>
                </div>
                <!-- /.card-header -->
                <div style="padding:30px" >
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>User Name</th>
                            <th>Email</th>
                            <th>Type</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($question))
                            @foreach($question->users as $k=> $user)
                                <tr>
                                    <td>{{$k+1}}</td>
                                    <td> {{$user->name}} </td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->role->name}}</td>
                                    <td>
                                        <a href="{{route('admin.question.coach.show' , [$question->id , $user->coach->id])}}" class="btn btn-sm btn-primary">View</a>
                                    </td>
                                </tr>
                        </tbody>
                        @endforeach
                        @endif
                    </table>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
    <script>

    </script>
@endsection




