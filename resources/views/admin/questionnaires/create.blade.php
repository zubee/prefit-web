@extends('layouts.dashboard')



@section('content')


<div class="row">

   @include('layouts.partials.swals')

<div class="col-md-12" style="padding:30px" >
    <div class="card card-info ">
      <div class="card-header">
        <h3 class="card-title">Create Questionnaire</h3>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <form action="{{route('admin.questionnaires.store')}}" method="POST">
        @include('layouts.partials.form_errors')

          @csrf

          <div class="form-group row">
            <div class="col-sm-2">
            <label for="questionnaire" class="row float-right col-form-label ">Questionnaire:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="questionnaire" placeholder="Name" name="questionnaire" value="{{old('questionnaire')}}" required>
            </div>
              <div class="col-sm-2">
                  <button type="button"style="height: 35px;  " id="add_question"  onclick="add_new_question()" class="btn btn-outline-success"> <i class="fas fa-plus"></i> </button>

              </div>
          </div>

          <div id="question_area">
              <div id="choices_area">


                  {{-- <div id="choice" class="form-group row">
                    <div class="col-sm-3">
                    <label class="row float-right col-form-label ">Choice:</label>
                  </div>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" placeholder="Choice" name="choice[]" value="" required>

                    </div>

                    <div  class="col-sm-2">
                        <button onclick="remove_row()" style="height: 35px;" class="btn btn-outline-danger" type="button" > <i class="fas fa-minus"></i> </button>
                    </div>

                  </div> --}}

              </div>


          </div>
          
       <!--   <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Type:</label>
          </div>
            <div class="col-sm-8">
              <select class="form-control" id="question_type" name="type" onchange="toggle_choices()" >
                  <option value="text" selected >Text</option>
                  <option value="checkbox">CheckBoxes</option>
                  <option value="radiobutton">RadioButtons</option>
              </select>
            </div>
            <div class="col-sm-2">
                <button type="button"style="height: 35px; display:none; " id="add_button"  onclick="add_new_row()" class="btn btn-outline-success"> <i class="fas fa-plus"></i> </button>
            </div>
          </div>-->
          
          


        <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Assign To:</label>
          </div>
            <div class="col-sm-8">
                <select class="select2bs4" multiple style="width: 100%;height: 40px;" name="assigned[]" >
                    @foreach($users as $user)
                    <option value="{{$user->id}}" >{{$user->name}}</option>
                    @endforeach
                </select>
                </div>
        
          </div>

        
        <div>
            <button class="btn btn-primary float-right" onclick="beforeSubmit();">Create</button>
        </div>
      </form>
    </div>
    </div>
  
  </div>
</div>

@endsection



@section('scripts')
    <script>

        function reIndex(){
            $('.question').each(function (k,v) {
               $(this).children('.row').children('.col-sm-6').children('#question').attr('name', 'question['+k+'][name]');
               $(this).children('.row').children('.col-sm-6').children('.question_type').attr({'name':'question['+k+'][type]','id':'question_type'+k+'','onChange':'toggle_choices('+k+')'});
                $(this).children('.row').children('.col-sm-2').children('button').attr({'id':'add_button'+k+'', 'onClick':'add_new_row('+k+')'});
                $(this).children('.row').children('.col-sm-4').children('.choice').attr({'name':'question['+k+'][choices][]'});
            });
        }
        var i = 0;
        function add_new_row(id){
            i += 1;

            $('#question-'+id).append('<div id="choice-'+i+'" class="form-group row "> <div class="col-sm-4"> <label class="row float-right col-form-label ">Choice :</label> </div> <div class="col-sm-4"> <input type="text" class="form-control choice" placeholder="Choice" name="question['+id+'][choices][]" required>  </div>  <div  class="col-sm-2"> <button onclick="remove_row(this.id)" id="'+i+'" type="button" style="height: 35px;" class="btn btn-outline-danger"> <i class="fas fa-minus"></i> </button> </div> </div>')
        }
        
        function remove_row(row_id){
            $('#choice-'+row_id).remove();
        }
        function toggle_choices(question_id){
            if($('#question_type'+question_id).val() == 'text' ){
                $('#choices_area div').remove();
                $('#add_button').hide();
                i = 0;

            }else{
                $('#add_button'+question_id).show();
            }
        }


        var question = 0;
        function add_new_question() {
            question+=1;

            $('#question_area').append('<div id="question-'+question+'" class="question" > <div class="form-group row"> <div class="col-sm-3"><label for="question" class="row float-right col-form-label ">Question '+question+':</label></div>\n' +
                '<div class="col-sm-6"><input type="text" class="form-control" id="question" placeholder="Name" name="question['+question+'][name]" value="" required></div> ' +
                    '<div  class="col-sm-2"> <button onclick="remove_question(this.id);" id="'+question+'" type="button" style="height: 35px;" class="btn btn-outline-danger"> <i class="fas fa-minus"></i> </button> </div> </div>'+
                '<div class="form-group row">\n' +
                '<div class="col-sm-3">\n' +
                '<label for="inputEmail1" class="row float-right col-form-label ">Type:</label>\n' +
                '</div>\n' +
                '<div class="col-sm-6">\n' +
                '     <select class="form-control question_type" id="question_type'+question+'" name="question['+question+'][type]" onchange="toggle_choices('+question+')" >\n' +
                '            <option value="text" selected >Text</option>\n' +
                '            <option value="checkbox">CheckBoxes</option>\n' +
                '             <option value="radiobutton">RadioButtons</option>\n' +
                '      </select>\n' +
                '</div>\n' +
                '<div class="col-sm-2">\n' +
                '     <button type="button"style="height: 35px; display:none; " id="add_button'+question+'"  onclick="add_new_row('+question+')" class="btn btn-outline-success"> <i class="fas fa-plus"></i> </button>\n' +
                ' </div>\n' +
                ' </div></div>\n' +
                '          ')

            
        }

        function remove_question(question_id) {

            $('#question-'+question_id).remove();
        }

        
        function beforeSubmit() {

            reIndex();
            $('#form-submit').submit();
            
        }


    </script>
@endsection