@extends('layouts.dashboard')



@section('content')


<div class="row">

   @include('layouts.partials.swals')

<div class="col-md-12" style="padding:30px" id="vue" >
    <div class="card card-info ">
      <div class="card-header">
        <h3 class="card-title">Create Questionnaire</h3>
      </div>
      <!-- /.card-header -->
        <div style="padding:30px" >
          {{--  @dd($questionnaire->users)  --}}
            <question-element 
                @if(isset($questionnaire)) :current_questionnaire="{{$questionnaire}}" @endif
                @if(isset($questionnaire->questions)) :current_questions="{{$questionnaire->questions}}" @endif
                @if(isset($users)) :users="{{$users}}" @endif
                @if(isset($questionnaire->users)) :current_users="{{$questionnaire->users}}" @endif
              
              ></question-element>
        </div>
    </div>
  
  </div>
</div>

@endsection



@section('scripts')
    <script>
    </script>
@endsection