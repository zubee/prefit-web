@extends('layouts.dashboard')


@section('content')


<div class="row">

   @include('layouts.partials.swals')


   <div class="col-md-12" style="padding:30px" >
    <div class="card card-info ">
      <div class="card-header">
        <h3 class="card-title">Answered By {{$coach->user->name}}</h3>
      </div>
      <!-- /.card-header -->
        <div style="padding:30px" >
      <h4>{{$questionnaire->name}}</h4>
        <div>
        @foreach($questionnaire->questions as $k => $question)
        @php
        $answer = $question->answer($coach->id);
        @endphp
        {{-- dump($question->id , $question->answers); --}}

        {{-- @dump($answer) --}}
        <input type="hidden" name="questions[]" value="{{$question->id}}" >

        <div class="ml-3">
        <label class="col-form-label">Q {{$k+1}} : {{$question->question}} </label>
        <p class="float-right">{{ isset($answer) ? $answer->updated_at->toDateString() : "-"}} </p>
        
        <div>
            {{-- @dd($question->answer(auth()->user()->coach->id)) --}}
            @if($question->type == 'text')
            <textarea class="form-control" disabled >{{ isset($answer) && !is_array(json_decode($answer->answer)) ? $answer->answer : ""}}</textarea>
            @endif
        </div>
        @if($question->type == 'checkbox')
        <div>
            @foreach($question->choices as $k => $choice)
            
            <input name="question-{{$question->id}}[]" id="question-{{$question->id}}-{{$choice->id}}" class="ml-5" value="{{$choice->name}}"  type="checkbox" 
            {{-- @dd(json_decode($question->answer(auth()->user()->coach->id)->answer)[$k] == $choice->name) --}}
            @if($answer && is_array(json_decode($answer->answer)) && in_array( $choice->name , json_decode($answer->answer) ) )
            checked
            @endif
            disabled
            />
            <label  for="question-{{$question->id}}-{{$choice->id}}" class=" col-form-label">{{$choice->name}}</label>
            <br>
            @endforeach
    
        </div>
        @endif
        @if($question->type == 'radiobutton')
        <div>
            @foreach($question->choices as $choice)

            <input name="question-{{$question->id}}" id="question-{{$question->id}}-{{$choice->id}}" class="ml-5" value="{{$choice->name}}" type="radio" 
            @if($answer && $answer->answer == $choice->name)
            checked
            @endif
            disabled
            />
            <label for="question-{{$question->id}}-{{$choice->id}}" class=" col-form-label">{{$choice->name}}</label>
            <br>
            @endforeach
    
        </div>
        @endif
    </div>
    <hr>
        @endforeach
    </div>
    </div>
    </div>
  
  </div>
</div>

@endsection
