@extends('layouts.dashboard')



@section('content')


<div class="row">

   @include('layouts.partials.swals')

<div class="col-md-12" style="padding:30px" >
    <div class="card card-info ">
      <div class="card-header">
        <h3 class="card-title">Create Trainer</h3>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <form action="{{route('admin.trainer.store')}}" method="POST" enctype="multipart/form-data" >
        @include('layouts.partials.form_errors')

          @csrf

          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Name:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputEmail1" placeholder="Name" name="name" value="{{old('name')}}" required>
            </div>
          </div>

          <div class="form-group row">
              <div class="col-sm-2">
                  <label for="username" class="row float-right col-form-label ">User Name:</label>
              </div>
              <div class="col-sm-8">
                  <input type="text" class="form-control" id="username" placeholder="User Name" name="username" value="{{old('username')}}" required>
                  <div >
                      <p id="username-error" class="text-danger" style="display:none; height: 10px" >&#x2717 username not available</p>
                      <p id="username-success" class="text-success" style="display:none; height: 10px" >&#x2713 username is available</p>
                  </div>

              </div>
          </div>


          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Email:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="email" placeholder="Email" name="email" value="{{old('email')}}" required>
                <div>
                    <p id="email-error" class="text-danger" style="display:none; height: 10px" >&#x2717 email not available</p>
                    <p id="email-success" class="text-success" style="display:none; height: 10px" >&#x2713 email is available</p>
                </div>

            </div>
          </div>

          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Password:</label>
          </div>
            <div class="col-sm-4">
              <input type="password" class="form-control" id="inputEmail1" placeholder="Password" name="password" required>
            </div>

            <div class="col-sm-4">
              <input type="password" class="form-control" id="inputEmail1" placeholder="Confirm Password" name="conf_password" required>
            </div>
          </div>
        
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Phone Number:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputEmail1" placeholder="Phone Number" name="phone" value="{{old('phone')}}" required>
            </div>
          </div>
          

{{-- 
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="tags" class="row float-right col-form-label ">Tags:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="tags" placeholder="Tags" name="tags" value="{{old('tags')}}" data-role="tagsinput" required>
            </div>
          </div> --}}

          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Tags:</label>
          </div>
            <div class="col-sm-8">
                <select class=" select2bs4" multiple style="width: 100%" id="" placeholder="Tags" name="tags[]" required>
                  @foreach($tags as $tag)
                  <option value="{{$tag->id}}" > {{$tag->name}} </option>
                  @endforeach
                </select>

            </div>
          </div>

          
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Percentage:</label>
          </div>
            <div class="col-sm-4 input-group">
              <input type="number" class="form-control" id="inputEmail1" placeholder="Percentage" name="percentage"  value="{{old('percentage')}}" required min="1" max="100" onkeyup="check_percentage()" >
                <div class="input-group-append">
                    <div class="input-group-text"><i class="fa fa-percent" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
          </div>
          <p id="percentage-error" class="text-danger" style="display:none; height: 10px; margin-left:17%" >&#x2717 Percentage Should be under 100</p>


          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Portfolio:</label>
          </div>
            <div class="col-sm-8">
              <textarea class="textarea" name="portfolio" placeholder="Portfolio" rows="4" >{{old('portfolio')}}</textarea>
            </div>
          </div>
          
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Image:</label>
          </div>
            <div class="col-sm-8">
              <input type="file" class="form-control" name="image" required >
            </div>
          </div>

        
        <div>
            <button id="signup" class="btn btn-primary float-right">Create</button>
        </div>
      </form>
    </div>
    </div>
  
  </div>
</div>

@endsection

@push('script')

    <script>

      function check_percentage(){
        console.log($('#percentage').val())
        if($('#percentage').val() > 100 || $('#percentage').val() < 1 ){
            $('#percentage').css('background-color' , '#ffc7c7');
            $('#signup').prop('disabled' , true);
            $('#percentage-error').show();
        }else{
          $('#percentage').css('background-color' , 'white');
          $('#signup').prop('disabled' , false);
          $('#percentage-error').hide();


        }
      }


        var tagnames = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: '{{route('tagnames')}}'
        });

        tagnames.initialize();

        $('#tags').tagsinput({
            typeaheadjs: {
                name: 'tagnames',
                displayKey: 'name',
                valueKey: 'name',
                source: tagnames.ttAdapter()
            },
            freeInput : false

        });


        $(document).ready(function () {
            var isValid = true;
            var usernameerror = false;
            var emailerror = false;



            jQuery('#username').keyup(function (e) {


                e.preventDefault();
                var username = $('#username').val();
                $.ajaxSetup({
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "{{route('verify.username')}}",
                    method: 'POST',
                    data:{
                        username: username,
                    },
                    success: function (result) {
                        console.log(result)
                        if($('#username').val().length>0){
                            if(result == 1){
                                $('#username-success').hide();
                                $('#username-error').show();
                                usernameerror = true;

                                if(!isValid || ( usernameerror ||  emailerror ) )
                                {
                                    $('#signup').prop('disabled', true)
                                }
                            }
                            else {
                                $('#username-success').show();
                                $('#username-error').hide();
                                usernameerror = false;

                                if(isValid && (!usernameerror && !emailerror)){
                                    $('#signup').prop('disabled', false);
                                }
                            }
                        }

                        else {
                            $('#username-success').hide();
                            $('#username-error').hide();
                            usernameerror = false;
                        }


                    }


                });


            })


            jQuery('#email').keyup(function (e) {


                e.preventDefault();
                var email = $('#email').val();
                $.ajaxSetup({
                    headers:{
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "{{route('verify.email')}}",
                    method: 'POST',
                    data:{
                        email: email,
                    },
                    success: function (result) {
                        console.log(result)
                        if($('#email').val().length>0){
                            if(result == 1){
                                $('#email-success').hide();
                                $('#email-error').show();
                                emailerror = true;

                                if(!isValid || ( usernameerror ||  emailerror ) )
                                {
                                    $('#signup').prop('disabled', true)
                                }
                            }
                            else {
                                $('#email-success').show();
                                $('#email-error').hide();
                                emailerror = false;

                                if(isValid && (!usernameerror && !emailerror)){
                                    $('#signup').prop('disabled', false);
                                }
                            }
                        }

                        else {
                            $('#email-success').hide();
                            $('#email-error').hide();
                            emailerror = false;
                        }


                    }


                });


            })


        });




    </script>
@endpush