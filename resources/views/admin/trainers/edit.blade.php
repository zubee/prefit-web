

@extends('layouts.dashboard')


@section('content')


<div class="row">

   @include('layouts.partials.swals')


   <div class="col-md-12" style="padding:30px" >
    <div class="card card-info ">
      <div class="card-header">
        <h3 class="card-title">Profile</h3>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      
    <form action="{{route('trainer.profile.update' , $trainer->id)}}" method="POST" enctype="multipart/form-data" >
        @include('layouts.partials.form_errors')
          @csrf


          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Name:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputEmail1" placeholder="Name" name="name" value="{{old('name') ?? $trainer->user->name }}" required>
            </div>
          </div>

          <div class="form-group row">
              <div class="col-sm-2">
                  <label for="username" class="row float-right col-form-label ">User Name:</label>
              </div>
              <div class="col-sm-8">
                  <input type="text" class="form-control" id="username" placeholder="User Name" name="username" value="{{old('username') ?? $trainer->user->username}}" required readonly>
              </div>
          </div>


          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Email:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputEmail1" placeholder="Email" name="email" value="{{old('email') ?? $trainer->user->email }}" required>
            </div>
          </div>

        
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Phone Number:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputEmail1" placeholder="Phone Number" name="phone" value="{{old('phone') ?? $trainer->phone }}" required>
            </div>
          </div>
          

          {{-- <div class="form-group row">
            <div class="col-sm-2">
            <label for="tags" class="row float-right col-form-label ">Tags:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="tags" placeholder="Tags" name="tags" value="{{$dietitian->tags}}" required data-role="tagsinput">
            </div>
          </div> --}}
           

          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Tags:</label>
          </div>
          @php
              $current_tags = $trainer->user->tags->pluck('id')->toArray();
          @endphp
            <div class="col-sm-8">
                <select  class=" select2bs4" multiple style="width: 100%" id="" placeholder="Tags" name="tags[]" required>
                  @foreach($tags as $tag)
                  <option value="{{$tag->id}}" @if(in_array( $tag->id , $current_tags)) selected @endif > {{$tag->name}} </option>
                  @endforeach
                </select>

            </div>
          </div>



          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Percentage:</label>
          </div>
            <div class="col-sm-4 input-group">
                <input type="number"  class="form-control" id="inputEmail1" placeholder="Percentage" name="percentage" value="{{$trainer->percentage}}" required>
                <div class="input-group-append" min="1" max="100" onkeyup="check_percentage()" >
                    <div class="input-group-text"><i class="fa fa-percent" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
          </div>
          <p id="percentage-error" class="text-danger" style="display:none; height: 10px; margin-left:17%" >&#x2717 Percentage Should be under 100</p>

          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Portfolio:</label>
          </div>
            <div class="col-sm-8">
              <textarea class="textarea" name="portfolio" placeholder="Portfolio" rows="4" >{{old('portfolio') ?? $trainer->portfolio}}</textarea>
            </div>
          </div>
          
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Image:</label>
          </div>
            <div class="col-sm-8">
              <input type="file" class="form-control" name="image" >
            </div>
          </div>

          <hr>

          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Password:</label>
          </div>
            <div class="col-sm-4">
              <input type="password" class="form-control" id="inputEmail1" placeholder="Password" name="password" >
            </div>

            <div class="col-sm-4">
              <input type="password" class="form-control" id="inputEmail1" placeholder="Confirm Password" name="conf_password">
            </div>
          </div>
        
        
        <div>
            <button class="btn btn-primary float-right">Update</button>
        </div>
      </form>
    </div>
    </div>
  
  </div>
</div>

@endsection

@push('script')

    <script>

      function check_percentage(){
        console.log($('#percentage').val())
        if($('#percentage').val() > 100 || $('#percentage').val() < 1 ){
            $('#percentage').css('background-color' , '#ffc7c7');
            $('#signup').prop('disabled' , true);
            $('#percentage-error').show();
        }else{
          $('#percentage').css('background-color' , 'white');
          $('#signup').prop('disabled' , false);
          $('#percentage-error').hide();


        }
      }

      
        var tagnames = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: '{{route('tagnames')}}'
        });

        tagnames.initialize();

        $('#tags').tagsinput({
            typeaheadjs: {
                name: 'tagnames',
                displayKey: 'name',
                valueKey: 'name',
                source: tagnames.ttAdapter()
            },
            freeInput : false
        });

    </script>

    @endpush
    
    
    