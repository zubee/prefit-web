@extends('layouts.dashboard')


@section('content')


<div class="row">

   @include('layouts.partials.swals')

<div class="col-md-12" style="padding:30px" >
    <div class="card card-info ">
      <div class="card-header">
        <h3 class="card-title">Update Package</h3>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <form action="{{route('admin.packages.update', $package->id)}}" method="POST" enctype="multipart/form-data">
        @include('layouts.partials.form_errors')
        @method('PATCH')
          @csrf
        {{--  @dd($package)  --}}

          <div class="form-group row">
            <div class="col-sm-2">
          </div>
            <div class="col-sm-8">
  
              <img id="image" src="@if($package->image) {{asset('images/packages/'.$package->image)}} @else {{asset('img/no-image-found.jpg')}} @endif" class="profile-image" />
  
              <button type="button" class="btn btn-outline-success" id="upload-button"> <i class="fas fa-image"></i> Upload Image </button>
              
              <input id="" type="file" class="file-upload" name="image" style="display:none" >
  
            </div>
          </div>


          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Name:</label>
          </div>
            <div class="col-sm-4">
              <input type="text" class="form-control" id="inputEmail1" placeholder="Name" name="name" value="{{$package->name}}" required>
            </div>

            <div class="col-sm-4">
              <input type="text" class="form-control" id="inputEmail8" placeholder="Package Duration (No. Of days) " name="duration" value="{{$package->duration}}" required>
            </div>


          </div>
          

          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Description:</label>
          </div>
            <div class="col-sm-8">
              <textarea class="textarea" name="description" >{{$package->description}} </textarea>
            </div>
          </div>
        
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Hightlights:</label>
          </div>
            <div class="col-sm-8">
              <textarea class="specific-textarea" name="highlights" >{{$package->highlights}}</textarea>
            </div>
          </div>

          
          {{--  <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail8" class="row float-right col-form-label ">Package Duration:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputEmail8" placeholder="Package Duration" name="duration" value="{{$package->duration}}" required>
            </div>
          </div>  --}}
       
          
          <div class="form-group row">
              <div class="col-md-2">
                  <label for="assigned" class="row float-right col-form-label">Assign To:</label>
              </div>
              @php
              $coaches_ids = $package->coaches->pluck('id')->toArray();
              @endphp
              <div class="col-md-8">
                  <select class="select2bs4 form-control" multiple id="assigned" name="assigned[]">
                      @foreach($users as $user)
                      @if($user->coach)
                          <option value="{{$user->coach->id}}" @if(in_array($user->coach->id, $coaches_ids))selected @endif>{{$user->name}}</option>
                          @endif
                      @endforeach
                  </select>
              </div>
          </div>

{{--  
          <div class="form-group row">
            <div class="col-md-2">
                <label for="assigned" class="row float-right col-form-label">Tags:</label>
            </div>
            <div class="col-md-8">
                <select class="select2bs4 form-control" multiple name="tags[]">
                    @foreach($tags as $tag)
                        <option value="{{$tag->id}}" @if(in_array($tag->id , $package->tags->pluck('id')->toArray())) selected @endif >{{$tag->name}}</option>
                        @endforeach
                </select>
            </div>
        </div>  --}}



          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Price:</label>
          </div>
            <div class="col-sm-1 mt-2">
              <input type="radio" id="paid" name="paid" value="1" @if($package->paid == 1) checked @endif onclick="javascript:paidfree();"  required />
              <label for="paid">Paid</label>
            </div>
            
            <div class="col-sm-1 mt-2">
              <input type="radio" id="Free" name="paid" value="0" @if($package->paid == 0) checked @endif onclick="javascript:paidfree();"  required />
              <label for="Free">Free</label>
            </div>
          </div>



          <div class="form-group row" id="pricebox">
              <div class="col-sm-2">
                  <label for="inputEmail1" class="row float-right col-form-label ">Price:</label>
              </div>
              <div class="col-sm-4 input-group">
                  <div class="input-group-prepend">
                      <div class="input-group-text">
                          <i class="fas fa-dollar-sign"></i>
                      </div>
                  </div>
                  <input type="text" class="form-control" id="price" placeholder="Price" name="price" value="{{$package->price}}" required>
              </div>
          </div>



          <div>
            <button class="btn btn-primary float-right">Update</button>
        </div>
      </form>
    </div>
    </div>
  
  </div>
</div>

@endsection

@push('script')


    <script>

        $(document).ready(function () {

            if($('#paid').is(':checked')){
                $('#pricebox').show();
            }
            else{
                $('#pricebox').hide();
                $('#price').val(0);
            }
        });
        function paidfree() {

            if($('#paid').is(':checked')){
                $('#pricebox').show();
            }
            else{
                $('#pricebox').hide();
                $('#price').val(0);
            }

        }



    </script>

@endpush