@extends('layouts.dashboard')


@section('content')


<div class="row">

   @include('layouts.partials.swals')

<div class="col-md-12" style="padding:30px" >
    <div class="card ">
      <div class="card-header">
        <h3 class="card-title">Manage Packages</h3>
        <a class="btn btn-outline-primary float-right" href="{{route('admin.packages.create')}}"> <i class="fas fa-plus"></i> Create</a>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <table class="table">
          <thead>
          <tr>
              <th>#</th>
              <th>Name</th>
              <th>Price</th>
              <th>Assigned To</th>
              <th>Create Date</th>
              <th>Actions</th>
          </tr>
          </thead>
          <tbody>
          @if(isset($packages))
          @foreach($packages as $k => $package)
          <tr>
              <td> {{$k+1}} </td>
              <td> {{$package->name}} </td>
              @if($package->paid)
                  <td>{{$package->price}}</td>
              @else
                  <td>Free</td>
              @endif
              <td><a href="{{route('admin.package.assigned', $package->id)}}"> {{$package->coaches()->count()}} </a></td>
              <td> {{$package->created_at->format('m/d/Y') }} </td>
              <td>
                
                {{-- <div class="icheck-success d-inline">
                    <input onchange="change_user_status({{$dietitian->user->id}})" type="checkbox" @if($dietitian->user->status == 1) checked @endif id="checkboxSuccess{{$dietitian->user->id}}">
                    <label for="checkboxSuccess{{$dietitian->user->id}}">
                    </label>
                  </div> --}}

                  <a href="javascript:;" onclick="AskBeforeDelete({{$package->id}})" class="text-danger"  > <i class="fas fa-trash"></i> </a>

                  <a href="{{route('admin.packages.edit' , $package->id)}}" > <i class="fas fa-edit"></i> </a>

                  <a href="{{route('admin.packages.show' , $package->id)}}" class="btn btn-sm btn-primary" > <i class="fas fa-eye"></i> View</a>
              </td>

              <form id="delete-form-{{$package->id}}" action="{{route('admin.packages.destroy' , $package->id)}}" method="POST" >
                  @csrf
                  @method('DELETE')
              </form>
          </tr>
          </tbody>
          @endforeach
          @endif
      </table>
    </div>
    </div>
  
  </div>
</div>

@endsection

@section('scripts')
    <script>
        function AskBeforeDelete(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {

                    $('#delete-form-'+id).submit();
                }
            })
        }

        function change_user_status(id){
            $.ajax({
                url: '{{url("/")}}/change/user/status/'+id,
                success: function (res){
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'User Status Has been updated',
                        showConfirmButton: false,
                        timer: 1500
                      })
                }
            }).fail(function(){
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Some Problem occured',
                    showConfirmButton: false,
                    timer: 1500
                  })
            })
        }
    </script>
@endsection




