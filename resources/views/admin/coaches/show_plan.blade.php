@extends('layouts.dashboard')


@section('styles')

<!-- Toastr -->
<link rel="stylesheet" href="{{ asset('admin-plugins/toastr/toastr.min.css')}}">


{{--  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.1/dist/css/bootstrap.min.css"/>  --}}
{{--  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/summernote@0.8.10/dist/summernote-bs4.css">  --}}

{{--  <script src="https://cdn.jsdelivr.net/npm/babel-polyfill@latest/dist/polyfill.min.js"></script>              --}}
{{--  <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.slim.min.js"></script>  --}}

{{--  <script src="https://cdn.jsdelivr.net/npm/popper.js@latest/dist/umd/popper.min.js"></script>  --}}
{{--  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.1/dist/js/bootstrap.min.js"></script>  --}}
    
{{--  <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.10/dist/summernote-bs4.min.js"></script>    --}}
{{--  <script src="https://kotarov.github.io/vue-summernote/vue-summernote.es5.js"></script>  --}}



{{--  <link href="{{asset('assets/select-checkbox1/select.css')}}">  --}}


{{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"> --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script> --}}




<style>

   /* width */
   ::-webkit-scrollbar {
    width: 5px;
  }
  
  /* Track */
  ::-webkit-scrollbar-track {
    background: #f1f1f1; 

  }
   
  /* Handle */
  ::-webkit-scrollbar-thumb {
    border-radius:90px;
    background: #888; 
  }
  
  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: #555; 
  }


  .multiselect-container{
    width: 95% !important;
    padding: 10px !important;
  }

  .caret{
    float: right !important;
    margin-top: 8px;
  }

  .multiselect {
    height: 35px;
    margin-top: 8px;
    border: 1px solid #337ab7;
  } 

  .multiselect:hover {
    background-color:#eee !important ;
    color: black !important;
  }

  .multiselect-container > .active{
    background-color:#337ab7;
    color:white;
    text-decoration: none;
  }


  .multiselect-container > li {
    padding-top: 5px !important;
    border-radius: 4px;
  }
  
  .multiselect-container > li > a {
    padding-left: 10px !important;
    border-radius: 2px;
    border-radius: 3px;
  }

  .multiselect-clear-filter{
    display: none;
  }
  .multiselect-search{
    margin-bottom: 10px !important;
  }

  .action_buttons{
    position: absolute;
    top: 150px;
    right: 50px;
    z-index: 10;
  }

  .plan-start-button{
    position: absolute;
    top: -40px;
    right: 10px;
  }
  .main-action-buttons{
    font-size: 25px;
  }
  .lable-class{
      
  }
  .workouts-area{
    height: auto;
    max-height: 200px;
    overflow: hidden auto;
    padding-right: 20px;
  }
  .sticky-title{
    position: sticky;
    top: 0px;
  }

  .delete-search-button {
    border-top-right-radius: 5px !important;
    border-bottom-right-radius: 5px !important;
    background-color: #ff5858 !important;
    color: #f1f1f1 !important;
    cursor: pointer;
  }
</style>

@endsection


@section('content')

@include('layouts.partials.swals')

<div class="row" id="plan">
  <vue-progress-bar></vue-progress-bar>

@php
    $count = 0;
    $count1 = 0;
@endphp



<div class="action_buttons">

  {{--  @dd($total_weeks)  --}}

  {{--  @php
      
  foreach(json_decode($plan->weeks) as $week){
    foreach($week as $day){
      dump($day);
    }
    dump($week);
  }


  dd('stop');
  @endphp  --}}

  @php
  if($plan->start_status == 1){
    $day_prefix = '';
  }else{
    $day_prefix = 'Day';
  }
  @endphp
  <a v-if="plan && plan.start_date" href="javascript:;" class="btn btn-outline-success  btn-sm mr-2 mb-2" > <i class="fas fa-calendar"></i> Start Plan Date: @{{plan.start_date}} </a>
  {{--  @click="start_plan_now"  --}}

  <a v-if="plan && !plan.start_date"  href="#" class="btn btn-outline-primary btn-sm  mr-2 mb-2" > <i class="fas fa-info-circle"></i> Not Started Yet</a>

  {{-- <a v-if="current_day_number && current_day" href="javascript:;" class="text-success main-action-buttons" @click="update_data_in_modal(current_day)"><i class="fas fa-edit"></i></a> --}}
  
  {{-- <a v-if="current_day_number && !current_day" href="javascript:;" class="main-action-buttons"  @click="day_data_modal({{$count1}})"><i class="fas fa-plus-circle"></i></a> --}}
  
  
  {{-- <a @click="refresh_data" href="javascript:;" class="text-danger ml-2 main-action-buttons" > <i class="fas fa-history "></i> </a> --}}

  {{--  <a @click="init_toaster" href="javascript:;" class="text-warning ml-2 main-action-buttons" > <i class="fas fa-arrow-alt-circle-up "></i> </a>  --}}



</div>

      <!-- /.card-header -->
      <div class="col-12 col-sm-12 col-lg-12" style="padding:30px" >
        {{--  <div class="card-header">
            <h3 class="card-title">Create Plan</h3>
          </div>  --}}

          <div class="card card-info card-tabs">
          <div class="card-header p-0 pt-1">
            <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                

                {{--  @for($i = 0; $i < $package->weeks; $i++ )  --}}
                @php
                $total_weeks_keys = [];
                @endphp

                @foreach($total_weeks as $i => $week)

                @php
                $keys = array_keys($week);
                $total_weeks_keys[] = $keys;
                @endphp

                
                <li class="nav-item" style="width: 15%" >
                <a class="nav-link @if($i == 1) active @endif" id="week-tab-{{$i}}-tab" data-toggle="pill" href="#week-tab-{{$i}}" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true" @click="bullet_click( $event , {{($i * 7 - 7) + 1 }} )" >Week {{$i}}</a>
                </li>

              
                @endforeach
        
                
            </ul>
          </div>
          <div class="card-body">
            <div class="tab-content" id="custom-tabs-one-tabContent">
              
                @foreach($total_weeks as $i => $week)
                  {{--  @for($i = 0; $i < $package->weeks; $i++ )  --}}
              

                <div class="tab-pane fade @if($i == 1) show @endif @if($i == 1) active @endif" id="week-tab-{{$i}}" role="tabpanel" aria-labelledby="week-tab-tab-{{$i}}">
                









                          <div class="row">
                            <div class="col-5 col-sm-3">
                              <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                
                                {{--  @for($j = 0; $j< 7; $j++ )  --}}
                                
                                {{--  @dump($keys , $week , array_search( 7 , $keys) , array_search( $week[$i] , $keys)  )  --}}

                                @foreach($week as $j => $day)
                                
                                @php
                                    $keys = array_keys($week);
                                    //dump($keys , $week , array_search( $j , $keys) , array_search( $week[$j] , $keys) , $day );


                                    $count = $count+1;

                                    if($count > $package->duration){
                                        break;
                                    }
                                @endphp
                                {{-- id="day-tab-{{$i}}-{{$j}}-tab" --}}
                                    {{--  @dump($j)  --}}
                                <a class="nav-link @if($j == 1) active @endif day-tab-{{$count}} day-tab day-tab-key-{{$j}}" id="{{$count}}" data-toggle="pill" href="#day-tab-{{$i}}-{{$j}}" role="tab" aria-controls="vert-tabs-home-{{$i}}-{{$j}}" aria-selected="true" @click="bullet_click($event , {{$count}})" >
                                  
                                  {{$day_prefix}} {{ $plan->start_status == 1 ? $day['day']. ',' : null }} {{$day['date'] ?? ""}} 
                                  {{ $plan->start_status == 0 ?  $count : ''}}                                
                                </a>

                                {{--  @endfor  --}}
                                @endforeach
                                {{--  @dd('asd')  --}}

                                {{--  <a class="nav-link" id="vert-tabs-profile-tab" data-toggle="pill" href="#vert-tabs-profile" role="tab" aria-controls="vert-tabs-profile" aria-selected="false">Profile</a>  --}}
                       
                              </div>
                            </div>
                            <div class="col-7 col-sm-9">
                              <div class="tab-content" id="vert-tabs-tabContent" >

                                {{--  @for($j = 0; $j < 7; $j++ )  --}}
                                
                                @foreach($week as $j => $day)
                                @php
                                  $keys = array_keys($week);

                                  $count1 = $count1+1;

                                  if($count1 > $package->duration){
                                      break;
                                  }
                                @endphp


                                <div class="tab-pane text-left fade @if($j == 1 && $i == 1) show active @endif day-tab day-tab-{{$count1}} day-tab-key-{{$j}} " day-counter="{{$count1}}"  id="day-tab-{{$i}}-{{$j}}"  role="tabpanel" ref="data_tabs" aria-labelledby="vert-tabs-home-tab-{{$i}}-{{$j}}">

                                  
                                  


                                <div v-for="(day , index) in planned_days" v-show="for_execution(day.id , index)" >

                                 
                                  
                                  <div v-if="day.day_number == {{$count1}}" >

                               
                                  <h4>Exercises</h4>
                                  <hr>

                                  <div class="row mt-3" v-for="(workout , sub_index) in day.workouts" >
                                    <div class="col-8">
                                      <div class="row">
  
                                        <div class="col-3">
                                            Name
                                        </div>
                                        
                                        <div class="col-9">
                                          <span v-if="workout.exercise"> @{{workout.exercise.Exercise_Name}}  </span>
                                          <span v-if="!workout.exercise"> @{{workout.name}}  </span>
                                          
                                        </div>
                                      </div>
                                      
                                      <div class="row">
                                        
                                        <div class="col-3">
                                            Steps
                                        </div>
                                        
                                        <div class="col-9">
                                          @{{day.workout_steps[workout.Exercise_Id]}}
                                          {{--  @{{showInConsole(day.workout_steps[workout.Exercise_Id.toString()])}}  --}}
                                          
                                        </div>
                                      </div>
                                      
                                      <div class="row">
                                        
                                        <div class="col-3">
                                            Reps
                                        </div>
                                        
                                        <div class="col-9">
                                          @{{day.workout_reps[workout.Exercise_Id]}}
                                          {{--  @{{showInConsole(day.workout_reps[workout.exercise_id.toString()])}}  --}}
                                        </div>
                                      </div>
                                      
                                      <div class="row">
                                        
                                        <div class="col-3">
                                            <a  v-if="workout.exercise" target="_blank" :href="workout.exercise.URL">View <i class="ml-1 fas fa-eye"></i></a>
                                            <a  v-if="!workout.exercise" target="_blank" :href="'/coach/view/workout/'+workout.id">View <i class="ml-1 fas fa-eye"></i></a>
                                        </div>
                                        
                                        <div class="col-9">
                                        </div>
                                      </div>
                                    </div>
  
                                    <div class="col-4">
                                      <img v-if="workout.exercise" class="exercise-gif-image" :src="workout.exercise.GIF_Img" />
                                      <img v-if="!workout.exercise && workout.media && workout.media.length" class="exercise-gif-image" :src="'/workouts/images/' + workout.media[0]" />
                                      
                                      <img v-if="!workout.exercise && workout.media && workout.media.length == 0" class="exercise-gif-image" src="/img/no-image-found.jpg" />
                                    </div>
                                    <div  v-if="!(sub_index == (day.workouts.length - 1)) "  class="col-12">
                                      <hr >
                                    </div>
                                  </div>
                                  
                                  <div v-if="(!day.workouts || day.workouts.length == 0)" class="alert alert-info" >
                                      <i class="fas fa-info-circle"></i> No Exercises
                                  </div>



                                  <hr>
                                  <h4>Measurements</h4>
                                  <hr>
                                  <div v-if="day.measurements" >
                                    
                                    <div class="row">
                                      <div class="col-2">
                                        Chest
                                      </div>
                                      
                                      <div class="col-2">
                                        @{{day.measurements.chest}}
                                      </div>

                                      <div class="col-2">
                                        Waist
                                      </div>
                                      
                                      <div class="col-2">
                                        @{{day.measurements.waist}}
                                      </div>
                                    </div>

                                    
                                    <div class="row">
                                      <div class="col-2">
                                        Hips
                                      </div>
                                      
                                      <div class="col-9">
                                        @{{day.measurements.hips}}
                                      </div>
                                    </div>
                                   

                                    <div class="row">
                                      <div class="col-2">
                                        Left Bicep
                                      </div>
                                      
                                      <div class="col-2">
                                        @{{day.measurements.left_bicep}}
                                      </div>
                                      
                                      <div class="col-2">
                                        Right Bicep
                                      </div>
                                      
                                      <div class="col-2">
                                        @{{day.measurements.right_bicep}}
                                      </div>
                                    </div>
                                         


                                    <div class="row">
                                      <div class="col-2">
                                        Left Forearm
                                      </div>
                                      
                                      <div class="col-2">
                                        @{{day.measurements.left_forearm}}
                                      </div>
                                      
                                      <div class="col-2">
                                        Right Forearm
                                      </div>
                                      
                                      <div class="col-2">
                                        @{{day.measurements.right_forearm}}
                                      </div>
                                    </div>
                                         

                                    <div class="row">
                                      <div class="col-2">
                                        Left Calf
                                      </div>
                                      
                                      <div class="col-2">
                                        @{{day.measurements.left_calf}}
                                      </div>
                                      
                                      <div class="col-2">
                                        Right Calf
                                      </div>
                                      
                                      <div class="col-2">
                                        @{{day.measurements.right_calf}}
                                      </div>
                                    </div>
                                         
                                    <div class="row">
                                      <div class="col-2">
                                        Left Thigh
                                      </div>
                                      
                                      <div class="col-2">
                                        @{{day.measurements.left_thigh}}
                                      </div>
                                      
                                      <div class="col-2">
                                        Right Thigh
                                      </div>
                                      
                                      <div class="col-2">
                                        @{{day.measurements.right_thigh}}
                                      </div>
                                    </div>
                                         

                                     
                                  </div>
                                  <hr v-if="day.measurements">

                                  <div v-if="!day.measurements" class="alert alert-info" >
                                    <i class="fas fa-info-circle"></i> No Measurements 
                                  </div>





                                  <h4>Meals</h4>
                                  <hr>
                                  <div v-for="( meal , sub_index ) in day.meals">
                                    <div class="row">

                                      <div class="col-9">

                                        <img v-if="(day && day.meal_images && day.meal_images[sub_index])" :src="'{{asset('images/plan_images')}}/'+ day.meal_images[sub_index]" class="profile-image " />

                                        {{-- <img v-if="day.meal_images[index]" :src="'{{asset('images/plan_images')}}/'+ day.meal_images[index]" class="profile-image " /> --}}
                                        {{-- @{{sub_index}} --}}
                                        {{-- @{{return_proper_image( day , sub_index)}} --}}
                                        <img v-else src="{{asset('img/no-image-found.jpg')}}" class="profile-image" />
                      
                                      </div>
                                    </div>
                                    
                                    <div class="row">

                                      <div class="col-9 mt-3">
                                        <h5>
                                          @{{meal}}
                                        </h5>
                                      </div>
                                      
                                      {{-- <div class="col-9">
                                        @{{meal}}
                                      </div> --}}
                                    </div>
                                    
                                    <div class="row">
                                      
                                      <div class="col-3">
                                          Title
                                      </div>
                                      
                                      <div class="col-9">
                                        @{{day.meal_title[sub_index]}}
                                      </div>
                                    </div>
                                     
                                    
                                    <div class="row">
                                      
                                      <div class="col-3">
                                          Summary
                                      </div>
                                      
                                      <div class="col-9">
                                        @{{day.meal_summary[sub_index]}}
                                      </div>
                                    </div>
                                    
                                     
                                    <div class="row">
                                      
                                      <div class="col-3">
                                          What You Need to do
                                      </div>
                                      
                                      <div class="col-9" v-html="day.meal_needs[sub_index]" >

                                      </div>
                                    </div>
                                    
                                    <div class="row">
                                      
                                      <div class="col-3">
                                          How to do
                                      </div>
                                      
                                      <div class="col-9" v-html="day.meal_how_to_do[sub_index]">
                                      </div> 
                                    </div>
                                    
                                    <hr v-if="!(sub_index == (day.meals.length - 1)) " >
                                  </div>

                                  <div v-if="(!day.meals || day.meals.length == 0)" class="alert alert-info" >
                                    <i class="fas fa-info-circle"></i> No Meals 
                                  </div>




                                  <hr>
                                  <h4>Supplements</h4>
                                  <hr>
                                  <div v-for="(supplement_time , index) in day.supplement_timings">

                                    <div >
                                      <h5> @{{supplement_time}}  </h5>
                                    </div>

                                    <div v-for="(supplement , sub_index) in day.supplements[index]">

                                    <div class="row" >

                                      <div class="col-3">
                                          Name
                                      </div>
                                      
                                      <div class="col-9">
                                        @{{supplement.name}}
                                      </div>
                                    </div>
                                  
                                  
                                    <div class="row">

                                      <div class="col-3">
                                          Prescription
                                      </div>
                                      
                                      <div class="col-9">
                                        @{{day.supplement_prescription[index][sub_index] }}
                                      </div>
                                    </div>


                                  </div>
                                  <hr v-if="!(index == (day.supplements.length - 1)) " >


                                  

                                </div>

                                <div v-if="(!day.supplement_timings || day.supplement_timings.length == 0)" class="alert alert-info" >
                                  <i class="fas fa-info-circle"></i> No Supplements 
                                </div>



                                     
                                </div>

                            </div>


                                </div>



                                @endforeach
                                {{--  @endfor  --}}

                                {{--  <div class="tab-pane fade" id="vert-tabs-profile" role="tabpanel" aria-labelledby="vert-tabs-profile-tab">

                                </div>  --}}
                                
                                
                              </div>
                            </div>
                          </div>
                          












                </div>

                {{--  @endfor  --}}
                @endforeach

              
              
            </div>
          </div>
          <!-- /.card -->
          <div class="row">
              <div class="col-12 text-center mb-2">
                  {{--  <a class="mb-3 btn btn-success" href="#" > <i class="fas fa-save"></i> Draft</a>  --}}
                  {{--  <a class="mb-3 btn btn-primary" href="#" > <i class="fas fa-sticky-note"></i> Save</a>  --}}


              </div>
          </div>
        </div>



      </div>




      
    <div class="modal fade" id="day_data_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">

            <h5 v-if="!edit_mode" class="modal-title" id="exampleModalLongTitle">Add a Day</h5>
            
            <h5 v-if="edit_mode" class="modal-title" id="exampleModalLongTitle">Update Day</h5>
            
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form @submit.prevent="save_day_data">


          <div class="modal-body" id="bodyModal">

            {{--  <div class="form-group row">
              <label class="col-3 col-form-label ">Exercises</label>
              <div class="col-9">
                <select class="multiselect-ui form-control d-none mt-2" id="workouts" multiple   v-model="selected_workouts" >
                  <option v-for="workout in workouts" :value="workout.id" > @{{ workout.name }} </option>
              </select>
            
            </div>
          </div>  --}}

          {{-- myInput
           --}}

          <div class="workouts-area"  >
            
            <div class="row">
              <div class="col-12">
                <div class="float-right w-50">
                  {{-- <input type="text" id="myInput" name="search_workouts" class="form-control mt-2 float-right" placeholder="Search Workouts.... " /> --}}

                  <div class="input-group mb-2 mr-sm-2 mt-2">
                    <input type="text" id="myInput" class="form-control " id="inlineFormInputGroupUsername2" placeholder="Search Workouts.... ">
                      <div class="input-group-prepend">
                        <div class="input-group-text delete-search-button" onclick="empty_search_bar()" >X</div>
                      </div>
                  </div>

                </div>
              </div>
            </div>
            <hr>



            <ul id="myUL" >
            <li class="form-group row" v-for="(workout,index) in workouts" >
            <div class="col-3 " >
              <label class="col-form-label position-sticky" v-if="index == 0" >Exercises</label>
            </div>

              <div class="col-9"  >
                
                <div class="row">
                  <div class="col-4">
                    <input type="checkbox" :id="'workout'+workout.id" :value="workout.id" v-model="selected_workouts" @change="workout_click( $event , index , workout)" />
                    <label :for="'workout'+workout.id" >@{{ workout.name }}</label> <a :href="'/coach/view/workout/'+ workout.id" target="_blank" ><i class="fas fa-eye"></i> </a>
                  </div> 

                  <div class="col-4" v-if="selected_workouts.includes(workout.id)" >
                    <input type="text" class="form-control" v-model="selected_workouts_steps[workout.id]" placeholder="Steps" />

                </div>
                  <div class="col-4" v-if="selected_workouts.includes(workout.id)">
                    <input type="text" class="form-control" v-model="selected_workouts_reps[workout.id]" placeholder="Repititions" />

                </div>
                </div>
                
              </div>
              
            </li>
          </ul>

          </div>
         
            <hr>
            <div class="row">

            <div class="col-3" >
              <label class="col-form-label">Meals</label>
            </div>
            
            <div class="col-9" >
              <v-select
                placeholder="Select Meals"
                v-model="selected_meals_current"
                multiple
                label="meals_array"
                :options="meals_array"
                @input="meals_select_triggered"
              />
            </div>
          </div>
          <hr>



          
            <div id="meal-collapse" class="meal-collapse">
            <div class="row" v-for="(meal,sub_index) in selected_meals" :key="sub_index" >
              <div class="col-3"></div>
              <div class="col-9">


              <a class="btn btn-outline-primary w-100 m-1" data-toggle="collapse" :href="'#meal_details'+sub_index" role="button" aria-expanded="false" aria-controls="collapseExample"  >
                {{-- onclick="check_for_editors()" --}}
                {{--  @click="toggle_area_for_meal(sub_index)"  --}}
                @{{meal}}
              </a>

              <div class="collapse" :id="'meal_details'+sub_index" data-parent="#meal-collapse">
                <div class="card card-body">
                  <div class="form-group">


                    <img :id="'image'+sub_index" v-if="meal_description_images[sub_index]" :src="'{{asset('images/plan_images')}}/'+ meal_description_images[sub_index]" :class="'profile-image meal-profile-image-'+sub_index" />

                    <img :id="'image'+sub_index" v-if="!meal_description_images[sub_index]" src="{{asset('img/no-image-found.jpg')}}" :class="'profile-image meal-profile-image-'+sub_index" />
        
                    <button type="button" class="btn btn-outline-success" @click="click_the_upload_icon(sub_index)"> <i class="fas fa-image"></i> Upload Image </button>
                    
                    <input :id="'file-upload-'+sub_index" type="file" @change="update_meal_image($event  , sub_index)"  class="file-upload" name="image" style="display:none" >
        
                  </div>
                  <div class="form-group">

                    <label class="lable-class">Title:</label>

                    <input type="text" class="form-control" placeholder="Title" v-model="meal_description_title[sub_index]" required />
                  </div>

                  <div class="form-group">
                    <label class="lable-class">Summary:</label>
                    <textarea class="form-control" rows="4" placeholder="Summary" v-model="meal_description_summary[sub_index]" required ></textarea>
                  </div>
                  
                  <div class="form-group">
                    <label class="lable-class">What you will need:</label>
                    {{--  <summernote v-model="meal_description_needs[sub_index]"></summernote>  --}}
                    <wysiwyg v-model="meal_description_needs[sub_index]" />

                  </div>
  
                  <div class="form-group">
                    <label class="lable-class">How to do it:</label>
                    <wysiwyg v-model="meal_description_how_to_do[sub_index]" />
                    {{--  <summernote v-model="meal_description_how_to_do[sub_index]"></summernote>  --}}

                  </div>

  
                </div>
              </div>
              

            </div>
          </div>
        </div>
        <hr v-if="selected_meals_current.length > 0" >



        <div class="row">

          <div class="col-3" >
            <label class="col-form-label">Supplement Timing</label>
          </div>
          
          <div class="col-9" >
            <v-select
              placeholder="Select Supplement Timings"
              v-model="selected_supplements_timings_current"
              {{--  v-model="selected_supplements_timings"  --}}
              multiple
              :options="supplement_timing"
              @input="supplement_time_select_change_handle"

            />
          </div>
        </div>
        <hr>


        <div v-for="(time,index) in selected_supplements_timings">
        <div class="row" >

          <div class="col-3" >
            <label class="col-form-label"> @{{time}} Supplements</label>
          </div>
          
          <div class="col-9" >
            <v-select
              placeholder="Select Supplements"
              v-model="selected_supplements_current[index]"
              {{--  :value="selected_supplements[index]"  --}}
              {{--  v-model="selected_supplements[index]"  --}}
              multiple
              :options="supplements"
              label="name"
              required
              @input="selected_supplement_select_change_handle($event , index)"
            />
          </div>
        </div>

        <div class="row" v-for="(supplement , sub_index) in selected_supplements[index]" >
          <div class="col-3"></div>
          <div class="col-9">

            <div class="form-group">

              <label> @{{supplement.name}} Prescription</label>

              <input type="text" class="form-control" v-model="supplement_prescription[index][sub_index]" :placeholder="supplement.name+' Prescription'" required />
            </div>


          {{--  <a class="btn btn-outline-primary w-100 m-1" data-toggle="collapse" :href="'#supplement'+index" role="button" aria-expanded="false" aria-controls="collapseExample">
            @{{supplement.name}}
          </a>  --}}

          {{--  <div class="collapse" :id="'supplement'+index">
            <div class="card card-body">
              
              <div class="form-group">

                <label>Perscription</label>

                <input type="text" class="form-control" placeholder="Title" />
              </div>


            </div>
          </div>  --}}
          

        </div>
      </div>
      <hr>
    </div>


        
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button v-if="!edit_mode" type="submit" class="btn btn-primary">Save changes</button>
            
            {{--  <VueLoadingButton aria-label='Save Changes' />  --}}

            <button v-if="edit_mode" type="submit" class="btn btn-success">Update changes</button>
          </div>
        </form>
        </div>
      </div>
    </div>
    










    </div>
  




    {{--  <div class="card card-warning card-outline">
      <div class="card-header">
        <h3 class="card-title">
          <i class="fas fa-edit"></i>
          Toastr Examples
        </h3>
      </div>
      <div class="card-body">
        <button type="button" class="btn btn-success toastrDefaultSuccess">
          Launch Success Toast
        </button>
        <button type="button" class="btn btn-info toastrDefaultInfo">
          Launch Info Toast
        </button>
        <button type="button" class="btn btn-danger toastrDefaultError">
          Launch Error Toast
        </button>
        <button type="button" class="btn btn-warning toastrDefaultWarning">
          Launch Warning Toast
        </button>
        <div class="text-muted mt-3">
          For more examples look at <a href="https://codeseven.github.io/toastr/">https://codeseven.github.io/toastr/</a>
        </div>
      </div>
      <!-- /.card -->
    </div>  --}}



@endsection

@push('script')
<!-- Toastr -->
<script src="{{ asset('admin-plugins/toastr/toastr.min.js')}}"></script>

<script src="{{asset('assets/select-checkbox1/select.js')}}" ></script>


<script>
  


  $(function() {
    //$('#mySelect').multiselect();
     $('.multiselect-ui').multiselect({
      nonSelectedText: 'Select Workouts',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
    });
  });

  $(document).ready(function(){
    $("#myInput").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#myUL li").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
  });

  

  function empty_search_bar(){
    $('#myInput').val('').keyup()
  }
  

    $(document).ready(function(){
  


      $('.toastrDefaultSuccess').click(function() {
        toastr.success('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
      });
      $('.toastrDefaultInfo').click(function() {
        toastr.info('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
      });
      $('.toastrDefaultError').click(function() {
        toastr.error('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
      });
      $('.toastrDefaultWarning').click(function() {
        toastr.warning('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
      });
    });

  const plan = new Vue({
    el: '#plan',
   
    data: {
    
      
      client_id : {{$client->id}},
      package_id : {{$package->id}},
      package : {!! $package !!},
      coach_id : {{$coach->id}},
      plan_id : null,

      edit_mode : false,

      planned_days: null , 
      plan : null,

    	content: 'hello world',
    	meal_image: null,


      day: null,
      temp_day: null,
      workouts: '',

      selected_workouts:[],
      removed_workouts:[],
      
      selected_workouts_steps:[],
      selected_workouts_reps:[],
      meals_array : [
        'Breakfast',
        'Lunch',
        'Dinner',
        'Snacks1',
        'Snacks2',
        'Snacks3',
      ],

      selected_meals:[],
      selected_meals_current:[],
      
      meal_description_images : [],
      
      meal_images_upload_indication : [],
      
      meal_description_title : [],
      
      meal_description_summary : [],
      meal_description_needs : [],
      meal_description_how_to_do : [],

      supplement_timing:[
        'Morning',
        'Evening',
        'Night',
      ],
      selected_supplements_timings: [],
      selected_supplements_timings_current: [],

      selected_supplements: [
         [],[],[],
      ],
      selected_supplements_current: null,

      supplements:[],
      supplement_prescription: [
        [],
        [],
        [],
      ],
      
      morning_supplement_prescription:[],
      evening_supplement_prescription:[],
      night_supplement_prescription:[],



      plan_days: [],


      editor: null,



      current_day : null,
      current_day_number: 1,
      current_day_to_update: '',

      created_actions_buttons: [],

      counted_days: [],

      current_images: [],

      total_weeks : {!! json_encode($total_weeks) !!},
      total_weeks_keys : {!! json_encode($total_weeks_keys) !!},

      {{--  new_form : new Form({
        selected_supplements:[],
      }),  --}}

      /*new_form : new Form({
        profile_id: null,
        category_id: null,
        industry_id: null,
        level: null,
        type: null,
        title: [''],
        role: null,
        country: ['null'],
        visa: ['null'],
        target_salary: null,
        target_currency: null,
        summary: null,
        notice_period: null,
    }), */
    
    },

    methods: {
      get_all_data(){
        axios.get('/api/get_all_data')
        .then( response => {
          //console.log(response);
          this.workouts = response.data.workouts;
          this.supplements = response.data.supplements;
        } )
        .catch(error => {
          toastr.error(error.response.data.message)

        });
        
      },

      get_plan_data(){

        
        this.$Progress.start();

        let loader = this.$loading.show();
  
        axios.get('/api/get_plan/client/'+ this.client_id +'/coach/'+ this.coach_id +'/package/'+ this.package_id)
        .then( response => {
          //this.plan_days : days.
          
          this.planned_days = response.data.plan.days;

          this.plan = response.data.plan;

          this.created_actions_buttons = response.data.plan_days_ids;
          
          this.current_day = null;
  
          for(i = 0; i < this.planned_days.length; i ++ ){
            if(this.planned_days[i].day_number == this.current_day_number){
  
              this.current_day = this.planned_days[i];
  
            }
          }

          
          loader.hide();
          this.$Progress.finish();
          
          //console.log(response)
        } )
        .catch(error  => {
  
          loader.hide();
          this.$Progress.fail();
          toastr.error(error.response.data.message)
          console.log(error.response);
        } )
      },

      save_day_data(){


        let loader = this.$loading.show();
        
        this.$Progress.start({
          // Optional parameters
            container: this.fullPage ? null : this.$refs.formContainer,
            canCancel: false,
            onCancel: this.onCancel,
            });


            /* for(i = 0; i < this.removed_workouts.length; i++){
              var remvoe_index = this.removed_workouts[i];
              //console.log(remvoe_index);
              //this.selected_workouts.filter(val=>val)
              //this.selected_workouts_steps.filter(val=>val)
              //this.selected_workouts_reps.filter(val=>val)
              
              //this.selected_workouts.splice(remvoe_index , (remvoe_index + 1));            
              //this.selected_workouts_steps.splice(remvoe_index , (remvoe_index + 1));            
              //this.selected_workouts_reps.splice(remvoe_index , (remvoe_index + 1));              
            } */
              
                            //this.selected_workouts.filter(val=>val)
            //this.selected_workouts_steps.filter(val=>val)
            //this.selected_workouts_reps.filter(val=>val)

            //this.selected_workouts = this.selected_workouts.filter(val=>val);            
            //this.selected_workouts_steps = this.selected_workouts_steps.filter(val=>val);            
            //this.selected_workouts_reps = this.selected_workouts_reps.filter(val=>val);             

        axios.post('/api/store_day_data',{

          client_id :  this.client_id,
          package_id : this.package_id,
          coach_id :  this.coach_id,
          
          day_number : this.current_day_number,
          
          selected_workouts : this.selected_workouts,
          selected_workouts_steps : this.selected_workouts_steps,
          selected_workouts_reps : this.selected_workouts_reps,


          selected_meals : this.selected_meals,
          meal_description_title : this.meal_description_title,

          //meal_description_images : this.meal_description_images,
          meal_description_images : this.current_images,
          current_images : this.meal_description_images,

          meal_images_upload_indication : this.meal_images_upload_indication,
          meal_description_summary : this.meal_description_summary,
          meal_description_needs : this.meal_description_needs,
          meal_description_how_to_do : this.meal_description_how_to_do,



          selected_supplements_timings : this.selected_supplements_timings,
          selected_supplements : this.selected_supplements,
          supplement_prescription : this.supplement_prescription,


        }).then(reposne => {
            this.refresh_data();

            

            $('#day_data_modal').modal('hide');
            this.reset_current_data();
            loader.hide();
            this.$Progress.finish();
            //Sweel("Successful!", "Data Saved successfully!", "success");
            toastr.success('Data Saved successfully.')

        })
        .catch(error => {

          loader.hide();
          this.$Progress.fail();
          toastr.error(error.response.data.message)

          //toastr.error('Request Failed. Try Later.')

          //Sweel("Oops!", 'Request Failed. Try Later' , "error");
        });

        {{--  this.form.get('/api/store_day_data')
        .then( response => {
          console.log(response)
            
        } )
        .catch( error => { 
          console.log(error)

        } );  --}}

      },

      day_data_modal(day_count){
        //console.log(day_count)
        this.edit_mode = false;
        this.reset_current_data();
        this.day = day_count;
        $('#day_data_modal').modal('show');
      
      },

      refresh_data(){
        this.reset_current_data();
        this.get_all_data();
        this.get_plan_data();
  
      },

      update_data_in_modal(day){
        let temp_day = day;
        //let loader = this.$loading.show();

        this.reset_current_data();

        this.edit_mode = true;

        //console.log(day);
        this.day = temp_day.day_number;

        this.selected_workouts = temp_day.workout_ids;
        this.selected_workouts_steps = temp_day.workout_steps;
        this.selected_workouts_reps = temp_day.workout_reps;


        this.selected_meals = temp_day.meals;
        this.selected_meals_current = temp_day.meals;
        this.meal_description_title = temp_day.meal_title;
        this.meal_description_images = temp_day.meal_images ? temp_day.meal_images : [];
        
        //this.current_images = day.meal_images ? day.meal_images : [];
        
        //this.meal_images_upload_indication = day.meal_images ? day.meal_images : [];

        this.meal_description_summary = temp_day.meal_summary;
        this.meal_description_needs = temp_day.meal_needs;
        this.meal_description_how_to_do = temp_day.meal_how_to_do;



        this.selected_supplements_timings = (temp_day.supplement_timings && temp_day.supplement_timings.length > 0) ? temp_day.supplement_timings : [] ;
        
        this.selected_supplements_timings_current = (temp_day.supplement_timings && temp_day.supplement_timings.length > 0) ? temp_day.supplement_timings : [] ;
        
        
        this.selected_supplements = (temp_day.supplements && temp_day.supplements.length > 0) ? temp_day.supplements : [ [],[],[],]  ;
        
        

        this.selected_supplements_current =  (temp_day.supplements && temp_day.supplements.length > 0) ?Object.assign({}, temp_day.supplements)  : [ [],[],[],];


        this.supplement_prescription = (temp_day.supplement_prescription && temp_day.supplement_prescription.length > 0) ? temp_day.supplement_prescription : [ [],[],[], ];

        //loader.hide();
        $('#day_data_modal').modal('show');


        //selected_supplements_timings : day.selected_supplements_timings,
        //selected_supplements : day.selected_supplements,
        //supplement_prescription : day.supplement_prescription,

      },

      reset_current_data(){

        $('#myInput').val('').keyup()

        this.day = null;

        this.selected_workouts = [];
        this.selected_workouts_steps = [];
        this.selected_workouts_reps = [];


        this.selected_meals = [];
        this.selected_meals_current = [];
        this.meal_description_title = [];
        this.meal_description_images = [];
        this.meal_images_upload_indication = [];
        this.meal_description_summary = [];
        this.meal_description_needs = [];
        this.meal_description_how_to_do = [];


        this.selected_supplements_timings_current = [];
        this.selected_supplements_timings = [];
        this.selected_supplements = [
          [],[],[],
        ];
        this.selected_supplements_current = [];
        this.supplement_prescription = [
          [],
          [],
          [],
        ];

      }, 

      start_plan_now(){
        axios.get('/api/start_plan_now/plan/'+this.plan.id)
        .then( response => {

            Sweel("Successful!", "Plan is Now Started!", "success");

            this.refresh_data();

        } ).catch( (errors) => {

          console.log(errors);

          Sweel("Oops!", 'Plan Does not exists or already started' , "error");

        });
      },

      update_texteditor(index){
        
        //console.log('1' , index , $('#text-editor1').val() )
        
        //this.meal_description_needs[index] = $('#text-editor1').val();
        //this.meal_description_how_to_do[index] = $('#text-editor2').val();
        
        //console.log('2' , $('#text-editor2').val() )
      },
      check_if_day_is_present(index , count){
        //console.log('index is ' , index , count );
        let status = false;
        if(!(this.created_actions_buttons.includes(count))){
          status = true;
          console.log(status);
        }
        return false;


      },
      for_execution(day_id , index){
        //console.log(day_id , index);
        return true;
      },

      bullet_click(e , count){
        
        //console.log(e.target.id)

        var num = e.target.id.replace(/\D/g,'');

        if(e.target.id.includes('week-tab') ){
          

         // console.log( num, this.total_weeks_keys[num-1][0] );
          
          $('.day-tab' ).removeClass('active');
          $('.day-tab' ).removeClass('show');
          $('.day-tab-key-'+ this.total_weeks_keys[num-1][0] ).addClass('active');
          $('.day-tab-key-'+ this.total_weeks_keys[num-1][0] ).addClass('show');

          /* old logic for making tabs active
          for(i = 1; i <= this.package.duration; i ++){
  
              $('.day-tab-'+i).removeClass('active');
              $('.day-tab-'+i).removeClass('show');
              //$('#'+e.target.id).removeClass('active');

          }
          //console.log('day-tab-'+ (num * 7 +1))
          
            $('.day-tab-'+ ((num * 7 - 7) +1)).addClass('active');
            $('.day-tab-'+ ((num * 7 - 7) +1)).addClass('show');
            //$('#day-tab'+num+'1').addClass('active');
            //console.log(num)
            */

                
            var current_week = num - 1;
            var day_counter = 0;

            for(i = 0; i < num-1 ; i++){

              day_counter = day_counter + this.total_weeks_keys[i].length;

              //console.log( i ,  this.total_weeks_keys[num-1 ].length );

            }
            count = day_counter + 1;

          }

        //$('#'+count).addClass('active')
        //$('#'+count).addClass('show')
        //console.log(count);

        const children = this.$el.querySelectorAll('.active .show');
        
        //console.log(children[0])
        //const day_count = children[1
        
        
        //console.log(num, this.total_weeks_keys[num-1] , this.total_weeks_keys[num-1].length)

        //this.total_weeks_keys.forEach((week)=> {
          //})
     
        

          this.current_day_number = count;
          this.current_day = null;

        
        for(i = 0; i < this.planned_days.length; i ++ ){
          
          if(this.planned_days[i].day_number == count){

            this.current_day = this.planned_days[i];

          }
        }
        
      },

      update_meal_image(e , index){

        let file = e.target.files[0];
        
        //console.log(file);

        let reader = new FileReader();
        reader.onloadend = (file) => {
          //console.log('RESULT' , reader.result);
          //console.log(this.meal_description_images);
          //this.meal_description_images[index] =  reader.result; 

          this.current_images[index] =  reader.result; 

        }
        this.meal_images_upload_indication[index] =  1; 

        reader.onload = function (e) {
          $('.meal-profile-image-'+index).attr('src', e.target.result);
      }
        reader.readAsDataURL(file);

      },

      click_the_upload_icon(index){

        $('#file-upload-'+index).click();
      
      },

      toggle_area_for_meal(index){

        $('#meal_details'+index).toggle()

      },

      init_toaster(){
    
        this.new_form.post('/api/store_day_data');

        toastr.info('Are you the 6 fingered man?')

      },
      meals_select_triggered(value){
        //console.log(value);
        //console.log(this.selected_meals)
        //this.selected_meals = value;
        //console.log(this.meal_description_title)
        let index = null;
        //console.log(value.length < this.selected_meals.length)
        if(value.length < this.selected_meals.length ){
          for(i = 0; i < this.selected_meals.length ; i ++){
            //console.log('execution ' , i);

            if(this.selected_meals[i] != value[i]){
              index = i;
              //console.log('found index is: ' + i , this.selected_meals[i] , value[i] );
              this.selected_meals = value;

              //this.selected_meals_current.splice(index , (index+1)) 
              this.meal_description_images.splice(index , (index+1)) 
              this.meal_images_upload_indication.splice(index , (index+1)) 
              this.meal_description_title.splice(index , (index+1)) 
              this.meal_description_summary.splice(index , (index+1)) 
              this.meal_description_needs.splice(index , (index+1)) 
              this.meal_description_how_to_do.splice(index , (index+1)) 

              break;
            }
            //console.log(value[i]  , this.selected_meals[i] );
          }
        }else{
          this.selected_meals = value;

        } 
      },

      workout_click(event , index, workout){
        
        //console.log( this.selected_workouts  , index,   workout.name , !event.target.checked);

        if(!event.target.checked){
              //this.selected_workouts.splice(workout.id , (workout.id + 1));            
              //this.selected_workouts_steps.splice((workout.id) , (workout.id + 1));            
              //this.selected_workouts_reps.splice(workout.id , (workout.id + 1));              

              //delete this.selected_workouts [workout.id] ;            
              delete this.selected_workouts_steps [workout.id] ;            
              delete this.selected_workouts_reps [workout.id] ;              
              //this.removed_workouts.push(index);              
        }else{
          //this.removed_workouts.pop(index);              

        }

      },

      return_workout_for_id(id){
        var found_workout = null;
        for(i = 0; i < this.workouts.length; i++ ){
          if(this.workouts[i].id == id ){
            found_workout = this.workouts[i];
            break;
          }
        }
        return found_workout;
      },

      supplement_time_select_change_handle(value){
        
        //console.log(value);

        let index = null;
        
        if(value.length < this.selected_supplements_timings.length ){

          for(i = 0; i < this.selected_supplements_timings.length ; i ++){

            if(this.selected_supplements_timings[i] != value[i]){
              index = i;
              //console.log(i)
              //this.selected_supplements_timings = value;

              if(i == 2){
                this.selected_supplements[index] = [] ;
                this.supplement_prescription[index] = [] ;
                //this.supplement_prescription.splice(index , (index+1)) ;
                //console.log(i , ' assign empty')
                
              }else{
                for(j = i; j < 3; j ++){
                  if(this.selected_supplements[j]){

                    this.selected_supplements[j] = this.selected_supplements[j+1] ;
                    this.supplement_prescription[j] = this.supplement_prescription[j+1] ;

                    //console.log(j , this.supplement_prescription[j+1])
                    //if(this.selected_supplements[j] === undefined){
                    
                      //console.log(j , this.supplement_prescription[j+1])

                      //this.selected_supplements_current[j] = []; 
                      //this.selected_supplements[j] = []; 
                      //this.supplement_prescription_current[j] = []; 
                      //this.supplement_prescription[j] = []; 
                      
                      //this.supplement_prescription.push([]); 
                      //this.supplement_prescription_current.push([]);   
                    
                    //}
                    //this.selected_supplements[j+1] = [] 
                    //this.supplement_prescription.splice(index , (index+1)) ;
                  }
              }
  
              }
              //this.selected_supplements[index] = this.selected_supplements[index+1] ;
              //this.supplement_prescription[index] = this.supplement_prescription[index+1] ;
              //this.supplement_prescription.splice(index , (index+1)) ;

              break;
            }
          }

          this.selected_supplements_timings = value;

        }else{
          this.selected_supplements_timings = value;
        }


      },
      
      selected_supplement_select_change_handle( event , index  ){
        
        
          //console.log( event , index , this.selected_supplements[index] )
        
          
            //console.log(this.selected_supplements[i].length ,  values.length)
            

            if(this.selected_supplements[index].length > event.length){

            //console.log(this.selected_supplements[i].length  , values)
           
            for(j = 0; j < this.selected_supplements[index].length ; j ++){
             
              console.log( 'length is more',  this.selected_supplements[index][j] , event[j] );


              if(this.selected_supplements[index][j] != event[j] || this.selected_supplements[index][j] && !(event[j])){

              //console.log('splicing')

              this.selected_supplements[index].splice(j , (j+1)); 
              this.supplement_prescription[index].splice(j , (j+1)); 
            }  
           
          }

          //this.selected_supplements[index] = event;
          }
                  
          if( this.selected_supplements[index].length < event.length ){
          console.log( 'changed index  ' + index  , this.selected_supplements[index] , event , event[event.length-1] )

          this.selected_supplements[index] = event;

          }

      },

    
    },
    mounted() { 


    

      let loader = this.$loading.show();

      this.get_all_data();
      this.get_plan_data();
      loader.hide();

      //console.log('vue is here!');
    },


    
 });



</script>
    @endpush

