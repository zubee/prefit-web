

@extends('layouts.dashboard')


@section('styles')
<style>
</style>
    
@endsection
@section('content')


<div class="row">


    {{--  @dd($coach->statistics)  --}}
  

<div class="col-md-12" style="padding:30px" >
    <div class="card card-info ">
      <div class="card-header">
        <h3 class="card-title">Profile</h3>
        {{--  <a href="{{route('trainer.profile.edit')}}" class="btn btn-success float-right" > <i class="fas fa-edit"></i> Edit </a>  --}}
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      
        
        <div class="ml-3">
            <div class="row">
                <div class="col-3">
                    @if($coach->user->photo)
                    <img  src="{{asset('images/profile_images/'.$coach->user->photo)}} " class="profile-image" />
                    @else
                        <img  src="{{asset('img/no-image-found.jpg')}} " class="profile-image" />
                    @endif
                </div>

                
                
            <div class="col-9 mt-auto">
            <div class="row">

                <div class="col-3">
                <div class="info-box mb-3 bg-warning">
                    <span class="info-box-icon"><i class="fas fa-users"></i></span>
                    
                    <div class="info-box-content">
                        <span class="info-box-text">Clients</span>
                        <span class="info-box-number"> {{$coach->statistics['total_clients']}} </span>
                    </div>
                    
                    <!-- /.info-box-content -->
                </div>
                </div>
            
            
                <div class="col-3">
                <div class="info-box mb-3 bg-danger">
                    <span class="info-box-icon"><i class="fas fa-running"></i></span>
                    
                    <div class="info-box-content">
                        <span class="info-box-text">Transformed</span>
                        <span class="info-box-number"> {{$coach->statistics['completed_plans']}} </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                </div>
            
            
                <div class="col-3">
                <div class="info-box mb-3 bg-success">
                    <span class="info-box-icon"><i class="fas fa-spinner"></i></span>
                    
                    <div class="info-box-content">
                        <span class="info-box-text">In-Progress</span>
                        <span class="info-box-number"> {{$coach->statistics['new_plans']}} </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                </div>
            
                <div class="col-3">
                <div class="info-box mb-3 bg-primary">
                    <span class="info-box-icon"><i class="fas fa-star-half-alt"></i></span>
                    
                    <div class="info-box-content">
                        <span class="info-box-text">Average Rating</span>
                        <span class="info-box-number">{{$coach->statistics['average_ratings']}} ({{$coach->statistics['total_reviews']}}) </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                </div>
            </div>
            
        </div>
            
            
            
                
            </div>


            {{--  <div class="mt-3"></div>  --}}
            <hr>
            <br>
        Name :
        <label> <b> {{$coach->user->name}} </b> </label>
            <br>
        Username : 
        <label> <b> {{$coach->user->username}} </b> </label>
            <br>
        Email :
        <label> <b> {{$coach->user->email}} </b> </label>
            <br>
        Phone Number :
        <label> <b> {{$coach->phone}} </b> </label>
            <br>
            Percentage :
        <label> <b> {{ $coach->percentage}} % </b> </label>
            <br>
            Portfolio :
       {!! $coach->portfolio !!}
            <br>
        Tags :
        <label> <b> @foreach($coach->tags as $tag ) {{$tag->name}} @if(!$loop->last) , @endif  @endforeach </b> </label>
            <br>
    </div>

    </div>
    </div>




    <div class="row">
        <div class="col-12">
            <div id="accordion">
                <div class="card card-info">
                  <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                      <div class="toggle-btn" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        <div class="review-top">
                            <div>
                                {{ $coach->user->name . ' Reviews '}} 
                            </div>
                            <div class="review-count">
                                {{$coach->reviews->count()}}
                            </div> 
                        </div>
                      </div>
                    </h5>
                  </div>
              
                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                      <div class="card-body">
                          
                    @if($coach && $coach->reviews && $coach->reviews->count() > 0)
                        @foreach ($coach->reviews as $review)
                            
                        <div class="review-head">
                            <div class="review-user-image-box">
                                @if($review->user && $review->user->photo)
                                <img class="review-user-image" src="{{asset('images/profile_images/'.$review->user->photo)}}" />
                                @else
                                <img class="review-user-image" src="{{asset('img/no-image-found.jpg')}}" />
                                @endif
                            </div>

                            <div class="rating">
                                <div class="review-user-name"> {{$review->user->name}} </div>
                                <div class="results">
                                    <div class="results-content">
                                      <span class="stars">{{$review->rating}}</span> 
                                    </div>
                                  </div>
                            </div>
                            <div class="date">
                                {{$review->proper_date}}
                            </div>
                        </div>
                        <div class="review-body">
                            <div class="review-description"> {{$review->description}} </div>
                        </div>
                        <hr>
                        @endforeach
                        @else
                        <div class="alert alert-info">No Reviews </div>
                        @endif
    
                    </div>
                  </div>
                </div>
                
                
              </div>
              
        </div>
    </div>



  
  </div>
</div>

@endsection

@push('script')

    <script>
        var tagnames = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: '{{route('tagnames')}}'
        });

        tagnames.initialize();

        $('#tags').tagsinput({
            typeaheadjs: {
                name: 'tagnames',
                displayKey: 'name',
                valueKey: 'name',
                source: tagnames.ttAdapter()
            },
            freeInput : false
        });

    </script>

    @endpush