@extends('layouts.dashboard')


@section('content')

@php
    $trainers = $coaches->where('role' , 3);
    $dietitians = $coaches->where('role' , 4);
@endphp

<div class="row">

   @include('layouts.partials.swals')

   <div class="col-12 col-sm-12 col-lg-12" style="padding:30px">
    <div class="card-header">
        <h3 class="card-title">Manage Coaches</h3>
        <a class="btn btn-outline-primary float-right" href="{{route('admin.coach.create')}}"> <i class="fas fa-user-cog"></i> Create</a>
      </div>

    <div class="card card-info card-tabs">
      <div class="card-header p-0 pt-1">
        <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
          <li class="nav-item" style="width:50%;">
            <a class="nav-link active" id="custom-tabs-one-home-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Trainers</a>
          </li>
          <li class="nav-item" style="width:50%;">
            <a class="nav-link" id="custom-tabs-one-profile-tab" data-toggle="pill" href="#custom-tabs-one-profile" role="tab" aria-controls="custom-tabs-one-profile" aria-selected="false">Dietitians</a>
          </li>
    
        </ul>
      </div>
      <div class="card-body">
        <div class="tab-content" id="custom-tabs-one-tabContent">
          <div class="tab-pane fade show active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
            <table class="table">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Image</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Create Date</th>
                    <th>Actions</th>
                </tr>
                @if(isset($trainers))
                @foreach($trainers as $k => $coach)
                <tr id="coach-list">
                    <td> {{$k+1}} </td>
                    <td> {{$coach->user->name}} </td>

                    <td> <img class="profile-picture" src="@if($coach->user->photo) {{asset('images/profile_images/'.$coach->user->photo)}} @else {{asset('img/no-image-found.jpg')}} @endif"> </td>

                    <td> {{$coach->user->username}} </td>

                    <td> {{$coach->user->email}} </td>
                    
                    <td> {{$coach->created_at->format('m/d/Y') }} </td>
                    <td>
                      
                      <div class="icheck-success d-inline">
                          <input onchange="change_user_status({{$coach->user->id}})" type="checkbox" @if($coach->user->status == 1) checked @endif id="checkboxSuccess{{$coach->user->id}}">
                          <label for="checkboxSuccess{{$coach->user->id}}">
                          </label>
                        </div>
      
                        <a href="{{route('admin.coach.edit' , $coach->id)}}" > <i class="fas fa-edit"></i> </a>

                        <a href="{{route('admin.coach.show' , $coach->id)}}" class="text-info" > <i class="fas fa-eye" title="View Profile" ></i> </a>

                        <a href="javascript:;" onclick="AskBeforeDelete({{$coach->id}})" class="text-danger"  > <i class="fas fa-trash"></i> </a>
      

                        <a href="{{route('admin.coach.clients' , $coach->id)}}" class="text-success" title="View Clients" > <i class="fas fa-list"></i> </a>
                    </td>
      
                    <form id="delete-form-{{$coach->id}}" action="{{route('admin.coach.destroy' , $coach->id)}}" method="POST" >
                        @csrf
                        @method('DELETE')
                    </form>
                </tr>
                @endforeach
                @endif
            </table>          </div>
          <div class="tab-pane fade" id="custom-tabs-one-profile" role="tabpanel" aria-labelledby="custom-tabs-one-profile-tab">
            <table class="table">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Profile Image</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Create Date</th>
                    <th>Actions</th>
                </tr>
                @if(isset($dietitians))
                @foreach($dietitians as $k => $coach)
                <tr id="coach-list">
                    <td> {{$k+1}} </td>
                    <td> {{$coach->user->name}} </td>

                    <td> <img class="profile-picture" src="@if($coach->user->photo) {{asset('images/profile_images/'.$coach->user->photo)}} @else {{asset('img/no-image-found.jpg')}} @endif"> </td>

                    <td> {{$coach->user->username}} </td>

                    
                    <td> {{$coach->user->email}} </td>
                    
                    <td> {{$coach->created_at->format('m/d/Y') }} </td>
                    <td>
                      
                      <div class="icheck-success d-inline">
                          <input onchange="change_user_status({{$coach->user->id}})" type="checkbox" @if($coach->user->status == 1) checked @endif id="checkboxSuccess{{$coach->user->id}}">
                          <label for="checkboxSuccess{{$coach->user->id}}">
                          </label>
                        </div>
                        <a href="{{route('admin.coach.edit' , $coach->id)}}" > <i class="fas fa-edit"></i> </a>

                        <a href="{{route('admin.coach.show' , $coach->id)}}" class="text-info" > <i class="fas fa-eye" title="View Profile" ></i> </a>

                        <a href="javascript:;" onclick="AskBeforeDelete({{$coach->id}})" class="text-danger"  > <i class="fas fa-trash"></i> </a>
      

                        <a href="{{route('admin.coach.clients' , $coach->id)}}" class="text-success" title="View Clients" > <i class="fas fa-list"></i> </a>
                
                    </td>
      
                    <form id="delete-form-{{$coach->id}}" action="{{route('admin.coach.destroy' , $coach->id)}}" method="POST" >
                        @csrf
                        @method('DELETE')
                    </form>
                </tr>
                @endforeach
                @endif
            </table>
        </div>
          
        </div>
      </div>
      <!-- /.card -->
    </div>
  </div>

{{-- <div class="col-md-12" style="padding:30px" >
    <div class="card ">
      <div class="card-header">
        <h3 class="card-title">Manage Coaches</h3>
        <a class="btn btn-outline-primary float-right" href="{{route('admin.coach.create')}}"> <i class="fas fa-user-cog"></i> Create</a>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >

        
      <table class="table">
          <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>Create Date</th>
              <th>Actions</th>
          </tr>
          @if(isset($coaches))
          @foreach($coaches as $k => $coach)
          <tr>
              <td> {{$k+1}} </td>
              <td> {{$coach->user->name}} </td>
              
              <td> {{$coach->user->email}} </td>
              
              <td> {{$coach->created_at->format('m/d/Y') }} </td>
              <td>
                
                <div class="icheck-success d-inline">
                    <input onchange="change_user_status({{$coach->user->id}})" type="checkbox" @if($coach->user->status == 1) checked @endif id="checkboxSuccess{{$coach->user->id}}">
                    <label for="checkboxSuccess{{$coach->user->id}}">
                    </label>
                  </div>

                  <a href="javascript:;" onclick="AskBeforeDelete({{$coach->id}})" class="text-danger"  > <i class="fas fa-trash"></i> </a>

                  <a href="{{route('admin.coach.edit' , $coach->id)}}" > <i class="fas fa-edit"></i> </a>
              </td>

              <form id="delete-form-{{$coach->id}}" action="{{route('admin.coach.destroy' , $coach->id)}}" method="POST" >
                  @csrf
                  @method('DELETE')
              </form>
          </tr>
          @endforeach
          @endif
      </table>
    </div>
    </div>
  
  </div> --}}
</div>

@endsection

@section('scripts')
    <script>
        function AskBeforeDelete(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
              }).then((result) => {
                if (result.value) {
                  
                    $('#delete-form-'+id).submit();
                }
              })
        }

        function change_user_status(id){
            $.ajax({
                url: '{{url("/")}}/change/user/status/'+id,
                success: function (res){
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'User Status Has been updated',
                        showConfirmButton: false,
                        timer: 1500
                      })
                }
            }).fail(function(){
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Some Problem occured',
                    showConfirmButton: false,
                    timer: 1500
                  })
            })
        }
    </script>
@endsection




