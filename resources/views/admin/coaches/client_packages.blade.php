
@extends('layouts.dashboard')

@section('styles')
  <link rel="stylesheet" href="{{ asset('admin-plugins/toastr/toastr.min.css')}}">

@endsection

@section('content')

<div class="row">

   @include('layouts.partials.swals')

   <div class="col-md-12" style="padding:30px" >
    <div class="card ">
      <div class="card-header">
        <h3 class="card-title"> <b> {{$client->name}} </b> Packages</h3>
        {{-- <a class="btn btn-outline-primary float-right" href="{{route('admin.packages.create')}}"> <i class="fas fa-plus"></i> Create</a> --}}
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <table class="table">
          <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Price</th>
            <th>Duration</th>
            <th>Create Date</th>
            <th>Start Date</th>
            <th>Actions</th>
          </tr>
          </thead>
          <tbody>
            
            @foreach( $packages as $k => $package)
            <tr>
                <td> {{$k+1}} </td>
                <td> {{$package->name}} </td>
                @if($package->paid)
                    <td>{{$package->price}}$</td>
                @else
                    <td>Free</td>
                @endif
                <td> {{$package->duration}}-Days </td>
  
                {{-- <td><a href="{{route('admin.package.assigned', $package->id)}}"> {{$package->users()->count()}} </a></td> --}}
                <td> {{$package->pivot->created_at->format('m/d/Y') }} </td>
                <td> 

                  @if($package->plans->where('coach_id' , $coach->id )->where('client_id' , $client->id )->first())

                    @if($package->plans->where('coach_id' , $coach->id )->where('client_id' , $client->id )->first()->start_status)
                    {{ $package->plans->where('coach_id' , $coach->id )->where('client_id' , $client->id )->first()->start_date }} 
                    @else
                    Not Started
                    @endif

                    @endif

                </td>
                 <td>

                    @if($package->plans->where('coach_id' , $coach->id )->where('client_id' , $client->id )->first())

                    <a class="btn btn-outline-success btn-sm" href="{{route('admin.coach.client.package.plan.view' , [ $coach->id , $client->id , $package->id] )}} " > <i class="fas fa-info-circle"></i> View</a>
                    @else
                    <a class="btn btn-outline-primary btn-sm" href="#" > <i class="fas fa-info-circle"></i> No Plan</a>
                    @endif
                  </tr>
            </tbody>
            @endforeach

      </table>
    </div>
    </div>
  
  </div>
</div>

  <!-- Modal -->
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
           {{--  <a id="paypal"></a>   --}}


          <form action="{{url('api/payment_via_paypal')}}" method="POST">
          <input class="form-control" name="card" >
          
            <button type="submit" class="btn btn-success" >PAY</button>
          </form>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script src="{{ asset('admin-plugins/toastr/toastr.min.js')}}"></script>

<script src="https://www.paypal.com/sdk/js?client-id={{env('PAYPAL_CLIENT_ID')}}&currency=USD" data-namespace="paypal_sdk" ></script>

<script>

  paypal_sdk.Buttons({
    createOrder: function(data, actions) {
      // This function sets up the details of the transaction, including the amount and line item details.
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: '110.01'
          }
        }]
      });
    },
    onApprove: function(data, actions) {
      // This function captures the funds from the transaction.
      return actions.order.capture().then(function(details) {
        // This function shows a transaction success message to your buyer.
        console.log(details)
        //alert('Transaction completed by ' + details.payer.name.given_name);
        axios.post('/api/payment_via_paypal' , {
            all_details: details ,
        }).then( res => {
          
          console.log(res)

          toastr.success('Data Saved successfully.')

        } )
        .catch(errors => { console.log(errors) 
        
          toastr.error(error.response.data.message)
        });
      });
    }
  }).render('#paypal');
  //This function displays Smart Payment Buttons on your web page.

  //paypal_sdk.Buttons().render('#paypal');
</script>


    <script>
        function AskBeforeDelete(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {

                    $('#delete-form-'+id).submit();
                }
            })
        }

        function change_user_status(id){
            $.ajax({
                url: '{{url("/")}}/change/user/status/'+id,
                success: function (res){
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'User Status Has been updated',
                        showConfirmButton: false,
                        timer: 1500
                      })
                }
            }).fail(function(){
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Some Problem occured',
                    showConfirmButton: false,
                    timer: 1500
                  })
            })
        }
    </script>
@endsection




