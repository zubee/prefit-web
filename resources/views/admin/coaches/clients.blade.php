
@extends('layouts.dashboard')


@section('content')


<div class="row">

   @include('layouts.partials.swals')

   <div class="col-md-12" style="padding:30px" >
    <div class="card ">
      <div class="card-header">
        <h3 class="card-title">{{$coach->user->name}} Clients</h3>
        @if($clients->count() > 0)
        <a class="btn btn-outline-primary float-right" href="{{route('export.clients' , [json_encode($clients->pluck('id')) , $coach->id   ] )}}"> <i class="fas fa-upload"></i> Export</a>
        @endif
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <table class="table">
          <thead>
          <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>Packages</th>
              {{--  <th>Actions</th>  --}}
          </tr>
          </thead>
          <tbody>
            
          @foreach( $clients as $k => $client)
          <tr>
            <td> {{$k+1}} </td>
            <td> {{$client->name}} </td>
            <td>{{$client->email}}</td>
              
            <td>
            
            <a class="btn btn-outline-primary btn-sm" href="{{route('admin.coach.client.packages' , [ $coach->id ,  $client->id])}} " > <i class="fas fa-eye"></i> {{ $client->subscribed_packages->where('pivot.coach_id' , $coach->id)->count() }}</a>
            </td>
            
            {{--  <td>
            <a class="btn btn-outline-primary btn-sm" href="{{route('coach.packages.view' , $client->id)}} " > <i class="fas fa-eye"></i> View</a>
            </td>  --}}
             
          </tr>
          </tbody>
          @endforeach
      </table>
    </div>
    </div>
  
  </div>
</div>

@endsection

@section('scripts')
    <script>
        function AskBeforeDelete(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {

                    $('#delete-form-'+id).submit();
                }
            })
        }

        function change_user_status(id){
            $.ajax({
                url: '{{url("/")}}/change/user/status/'+id,
                success: function (res){
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'User Status Has been updated',
                        showConfirmButton: false,
                        timer: 1500
                      })
                }
            }).fail(function(){
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Some Problem occured',
                    showConfirmButton: false,
                    timer: 1500
                  })
            })
        }
    </script>
@endsection




