@extends('layouts.dashboard')


@section('content')


    <div class="row">

        @include('layouts.partials.swals')

        <div class="col-md-12" style="padding:30px" >
            <div class="card ">
                <div class="card-header">
                    <h3 class="card-title"><b>{{$package->name}}</b> Package Assigne To</h3>
                </div>
                <!-- /.card-header -->
                <div style="padding:30px" >
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Type</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($package))
                            @foreach($package->coaches as $k => $coach)
                                <tr>
                                    <td>{{$k+1}}</td>
                                    <td> {{$coach->user->name}} </td>
                                        <td>{{$coach->user->email}}</td>
                                         <td>{{$coach->user->role->name}}</td>
                                   </tr>
                        </tbody>
                        @endforeach
                        @endif
                    </table>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
    <script>

    </script>
@endsection




