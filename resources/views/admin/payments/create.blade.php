@extends('layouts.dashboard')


@section('content')


<div class="row">

   @include('layouts.partials.swals')

<div class="col-md-12" style="padding:30px" >
    <div class="card card-info ">
      <div class="card-header">
        <h3 class="card-title">Create Package</h3>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <form action="{{route('admin.packages.store')}}" method="POST"  enctype="multipart/form-data" >
        @include('layouts.partials.form_errors')

          @csrf

          <div class="form-group row">
            <div class="col-sm-2">
          </div>
            <div class="col-sm-8">
  
              <img id="image" src="{{asset('img/no-image-found.jpg')}}" class="profile-image" />
  
              <button type="button" class="btn btn-outline-success" id="upload-button"> <i class="fas fa-image"></i> Upload Image </button>
              
              <input id="" type="file" class="file-upload" name="image" style="display:none" >
  
            </div>
          </div>


          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Name:</label>
          </div>
            <div class="col-sm-4">
              <input type="text" class="form-control" id="inputEmail1" placeholder="Name" name="name" value="{{old('name')}}" required>
            </div>

            <div class="col-sm-4">
              <input type="text" class="form-control" id="inputEmail8" placeholder="Package Duration" name="duration" value="{{old('duration')}}" required>
            </div>

          </div>
          

          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Description:</label>
          </div>
            <div class="col-sm-8">
              <textarea class="textarea" name="description" ></textarea>
            </div>
          </div>
        
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Hightlights:</label>
          </div>
            <div class="col-sm-8">
              <textarea class="specific-textarea" name="highlights" ></textarea>
            </div>
          </div>

          {{--  <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail8" class="row float-right col-form-label ">Package Duration:</label>
          </div>
            <div class="col-sm-4">
              <input type="text" class="form-control" id="inputEmail8" placeholder="Package Duration" name="duration" value="{{old('duration')}}" required>

          </div>
          </div>  --}}
        
          <div class="form-group row">
              <div class="col-md-2">
                  <label for="assigned" class="row float-right col-form-label">Assign To:</label>
              </div>
              <div class="col-md-8">
                  <select class="select2bs4 form-control" multiple id="assigned" name="assigned[]">
                      @foreach($users as $user)
                          @if($user->coach)
                          <option value="{{$user->coach->id}}">{{$user->name}}</option>
                          @endif
                          @endforeach
                  </select>
              </div>
          </div>
          
         {{--  <div class="form-group row">
              <div class="col-md-2">
                  <label for="assigned" class="row float-right col-form-label">Tags:</label>
              </div>
              <div class="col-md-8">
                  <select class="select2bs4 form-control" multiple name="tags[]">
                      @foreach($tags as $tag)
                          <option value="{{$tag->id}}">{{$tag->name}}</option>
                          @endforeach
                  </select>
              </div>
          </div>  --}}

          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Price:</label>
          </div>

            <div class="col-sm-1 mt-2">
              <input type="radio" id="paid" name="paid" value="1" onclick="javascript:paidfree()" required/>
              <label for="paid">Paid</label>
            </div>
            
            <div class="col-sm-1 mt-2">
              <input type="radio" id="free" name="paid" value="0" onclick="javascript:paidfree()" required/>
              <label for="Free">Free</label>
            </div>
          </div>

          <div class="form-group row"  id="pricebox" style="display: none">
              <div class="col-sm-2">
                  <label for="inputEmail1" class="row float-right col-form-label ">Price:</label>
              </div>
              <div class="col-sm-4 input-group">
                  <div class="input-group-prepend">
                      <div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
                  </div>
                  <input type="text" class="form-control" id="price" placeholder="Price" name="price" value="{{old('price')}}" >
              </div>
          </div>



          <div>
            <button class="btn btn-primary float-right">Create</button>
        </div>
      </form>
    </div>
    </div>
  
  </div>
</div>

@endsection

@push('script')


    <script>

        $(document).ready(function () {


        });
        function paidfree() {

            if($('#paid').is(':checked')){
                $('#pricebox').show();
            }
            else{
                $('#pricebox').hide();
                $('#price').val(0);
            }

        }



    </script>

    @endpush