@extends('layouts.dashboard')


@section('content')


    <div class="row">

        @include('layouts.partials.swals')

        <div class="col-md-12" style="padding:30px" >
            <div class="card ">
                <div class="card-header">
                    <h3 class="card-title">Manage Tags</h3>
                    <a class="btn btn-outline-primary float-right" href="{{route('admin.tags.create')}}"> <i class="fas fa-plus"></i> Create</a>
                </div>
                <!-- /.card-header -->
                <div style="padding:30px" >
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($tags))
                            @foreach($tags as $k => $tag)
                                <tr>
                                    <td> {{$k+1}} </td>
                                    <td> {{$tag->name}} </td>
                                    <td>

                                        {{-- <div class="icheck-success d-inline">
                                            <input onchange="change_user_status({{$dietitian->user->id}})" type="checkbox" @if($dietitian->user->status == 1) checked @endif id="checkboxSuccess{{$dietitian->user->id}}">
                                            <label for="checkboxSuccess{{$dietitian->user->id}}">
                                            </label>
                                          </div> --}}

                                        <a href="javascript:;" onclick="AskBeforeDelete({{$tag->id}})" class="text-danger"  > <i class="fas fa-trash"></i> </a>

                                        <a href="{{route('admin.tags.edit' , $tag->id)}}" > <i class="fas fa-edit"></i> </a>
                                    </td>

                                    <form id="delete-form-{{$tag->id}}" action="{{route('admin.tags.destroy' , $tag->id)}}" method="POST" >
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>

@endsection

@section('scripts')
    <script>
        function AskBeforeDelete(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {

                    $('#delete-form-'+id).submit();
                }
            })
        }

    </script>
@endsection




