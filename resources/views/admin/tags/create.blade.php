@extends('layouts.dashboard')


@section('content')


    <div class="row">

        @include('layouts.partials.swals')

        <div class="col-md-12" style="padding:30px" >
            <div class="card card-info ">
                <div class="card-header">
                    <h3 class="card-title">Create Tag</h3>
                </div>
                <!-- /.card-header -->
                <div style="padding:30px" >
                    <form action="{{route('admin.tags.store')}}" method="POST"  enctype="multipart/form-data" >
                        @include('layouts.partials.form_errors')

                        @csrf

                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label for="tag" class="row float-right col-form-label ">Tag Name:</label>
                            </div>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="tag" placeholder="Tag Name" name="tag" value="{{old('tag')}}" required>
                            </div>
                        </div>





                        <div>
                            <button class="btn btn-primary float-right">Create</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection

@push('script')


    <script>

        $(document).ready(function () {


        });



    </script>

@endpush