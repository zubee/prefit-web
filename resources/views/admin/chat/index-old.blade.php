


@extends('layouts.dashboard')
@section('styles')
<style>
    .sticky-message-input{
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate3d(-50%, -50%, 100);
        transition: background .3s;
                
    }
    .overlay-input {
        position: absolute;
        left: 50%;
        top: 50%;
        transform: translate3d(-50%, -50%, 100);
        white-space: nowrap;
        pointer-events: auto;
        transition: background .3s;
      }

      .user-names{
          color:black;
      }
      .user-names:hover{
        text-decoration: none;
        color:black;

      }
      .scroller {
        scrollbar-color: #f1f1f1 #888;
        scrollbar-width: thin;
      }

        /* width */
        ::-webkit-scrollbar {
            width: 5px;
        }
        
        
        /* Track */
        ::-webkit-scrollbar-track {
            background: #f1f1f1; 

        }
        
        /* Handle */
        ::-webkit-scrollbar-thumb {
            border-radius:90px;
            background: #888; 
        }
        
        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #555; 
        }
        
        
        .list-group{
            overflow-y: auto;
            overflow-x: hidden;
            height: 100%;
            vertical-align: bottom;
            display: inline-block;

        }
        {{-- position: absolute; --}}
        .messages-content{
            bottom: 0;
            {{-- height: 88px; --}}
            height: auto;
            margin-bottom:18px;
            {{-- margin-top:18px; --}}
        }
        .message-input{
            overflow: scroll;
        }
        .simple-li-tags:{
            list-style: none;
            padding:10px;
        }
        .user-list{
            background-color: white;
        }
        .message-input{
            width: 100%;
            border: none;
        }
        .sent-message{
            border-radius: 8px;
            padding:20px;
            background-color: white;
            border:1px solid black;

        }
        {{--  background-color: #e0ebff;  --}}

        .recieved-message{
            border-radius: 8px;
            background-color: #4599f3;
            color: #fff;
        {{-- width:70%; --}}
            padding:20px;

        }
        .sent-time{
            position: absolute;
            left: 20px;
            top:30px;

        }
        .recieve-time{
            top:30px;
            right: 20px;
            position: absolute;
        }
        .profile-image{
            max-width:50px; 
            max-height: 50px; 
            width:auto; 
            height: auto; 
            border-radius: 100%
        }
        .message-number{
            top:30px;
            position: absolute;
            right: 10px;
            background-color: #4599f3;
            color: #fff;
            width: 25px;
            height: 25px;
            text-align: center;
            border-radius:100%;
        }
        .contact-list{
            height:560px; 
            overflow: hidden scroll;
            border-radius:3px;
            border:1px solid #d2d4d7;
        }
        .active-chat{
            background-color: #4599f3;
            color: #fff;
        }
        
        .active-chat .meesage-search{
            color: white;
        }


        .attach-file-icon{
            padding:13px;
            margin-top: -10px;
        }
        .download-icon{
            position: absolute;
            {{-- margin-top:2%; --}}
            {{-- color: #38c172 !important; --}}
            font-size: 18px;
            right:20;
            bottom:3;
            border: 1px solid #dfdfdf;
            border-radius:100%;
            padding:5px;
            background-color:#dfdfdf;
            {{-- margin-right: 10px; --}}
            
        }
        .file-icon{
            margin-top:20px;
            font-size: 18px;
            margin-right: 10px;
        }
        .user-list-item{
            {{-- margin:-20px; --}}
            margin-bottom: 20px;
        }
     
</style>
@endsection

@section('content')

<div class="row" id="chat" style="padding-top: 10px" >












    <div class="offset-md-1 offset-lg-1 col-md-4 col-lg-4 col-sm-10">
        <div class="scroller bg-white  contact-list" style="">

            <ul  id="myUL" class="simple-li-tags user-list" style="list-style: none;margin-left:-45px" >
                @foreach($users as $user)
                <li class="user-list-item user-item  @if( isset($reciever) && $reciever->id == $user->id  ) active-chat @endif ">
                <a class="user-names" @click.prevent="get_this_chat({{$user}})" href="jacascript:;">
                <div class="meesage-search d-flex p-3 " style="position: relative">
                    <img src="@if($user->photo) {{asset('images/profile_images/'.$user->photo)}} @else {{asset('img/no-image-found.jpg')}} @endif" alt=""  class="profile-image" >
                    <p class="search-name mt-3 ml-2 ">{{$user->name}}</p>
                     <p id="user-message-count-{{$user->id}}" class="message-number"@if($user->chat->where('reciever_id' , $support->id)->where('read_status' , 0)->count() == 0) style="display:none" @endif > {{$user->chat->where('reciever_id' , $support->id)->where('read_status' , 0)->count()}}  </p>
                     {{-- @if($user->chat->where('reciever_id' , $support->id)->where('read_status' , 0)->count() == 0) style="display:none" @endif > {{$user->chat->where('reciever_id' , $support->id)->where('read_status' , 0)->count()}}  --}}
                </div>
            </a></li>
                @endforeach
            </ul>

    


      </div>
      </div>







    <div class=" col-md-6 col-lg-6 col-sm-10">
        <div class="card p-0" style="height:560px;" id="messages-div">
            <div class="form-group  d-flex mt-2 " style="position: relative">
                <div style="height: 40px; display: inline-block">
                <i v-if="active_status == 1" style="cursor: pointer;" @click="close_chat" class="fas fa-times-circle ml-3 mt-3 text-danger" ></i>
                <p style="position: absolute;right: 9px; top: 20%;" v-html="reciever.name" ></p>
            </div>
            </div>
    
            <hr class="mt-0">
    
            <ul class="scroller list-group" v-chat-scroll="{always: false, smooth: true}">
                
                <li v-for="message in messages" class="messages-content p-3">
                
                    <div class="row align-content-center"  v-if="message.sender_id == {{$support->id}}">
                        <div class="col-sm-12 col-12 col-md-3  text-center pl-4" style="align-self: center;">
                            @{{message_time(message.created_at)}}
                        </div>

                        <div class="d-sm-none d-md-none d-block ml-5"></div>
                        <div class="col-sm-8 col-8 col-md-7">
                            <div v-if="message.file == 1 && !(message.mime.includes('jpg') || message.mime.includes('png') || message.mime.includes('jpeg'))" class="sent-message ">
                                <i class="fas fa-file"></i>
                                @{{message.realname}}
                                <a :href="'{{asset('/api/chat/download/file/')}}/'+message.id" > <i class="fas fa-download  download-icon"></i> </a> 

                            </div>
                            
                            <div v-if="message.file == 1 && (message.mime.includes('jpg') || message.mime.includes('png') || message.mime.includes('jpeg'))" class="p-3">
                                <a :href="'{{asset('messages/files/')}}' +'/' + message.filename"  data-lightbox="roadtrip"  class="lightbox-image-anchor">
                                    <img  :src="'{{asset('messages/files/')}}' +'/' + message.filename"  style="width: auto; height: auto; max-width: auto; height: 100px;" class="lightbox-image-list float-right"/>   
                                </a>

                            </div>
                            <div v-if="message.file == 0" class="sent-message" >
                                @{{message.message}}

                            </div>

                            
                        </div>
                        <div class="col-sm-2 col-2 col-md-2 " style="align-self: center;">

                            <img  v-if="(sending_user.photo)" v-bind:src="'{{asset('images/profile_images')}}/'+sending_user.photo"  alt="" width="50" class="float-left ml-3" style="border-radius: 100%" >   

                            <img v-else src="{{asset('img/no-image-found.jpg')}}"  alt="" width="50" class="float-left ml-3" style="border-radius: 100%" >   


                        </div>
                    </div>
              
    

                <div class="row align-content-center"  v-else>

                    <div class=" d-block d-md-none d-lg-none  col-sm-12 col-12 col-md-3  text-center pr-4" style="align-self: center;">
                        @{{message_time(message.created_at)}}
                    </div>

                    <div class="col-sm-2 col-2 col-md-2 " style="align-self: center;">
                        
                    <img  v-if="(reciever.photo)" v-bind:src="'{{asset('images/profile_images')}}/'+reciever.photo"  alt="" width="50" class="float-left ml-3" style="border-radius: 100%">   
                                        
                    <img v-else src="{{asset('img/no-image-found.jpg')}}"  alt="" width="50" class="float-left ml-3" style="border-radius: 100%">   



                    </div>
                
                    <div class="col-sm-8 col-8 col-md-7 ">
                        <div v-if="message.file == 1 && !(message.mime.includes('jpg') || message.mime.includes('png') || message.mime.includes('jpeg'))" class="recieved-message ">
                            <i class="fas fa-file"></i>
                            @{{message.realname}}
                            <a :href="'{{asset('/api/chat/download/file/')}}/'+message.id" > <i class="fas fa-download  download-icon"></i> </a> 

                        </div>
                        
                        <div v-if="message.file == 1 && (message.mime.includes('jpg') || message.mime.includes('png') || message.mime.includes('jpeg'))" class="p-3">
                            <a :href="'{{asset('messages/files/')}}' +'/' + message.filename"  data-lightbox="roadtrip"  class="lightbox-image-anchor">
                                <img  :src="'{{asset('messages/files/')}}' +'/' + message.filename"  style="width: auto; height: auto; max-width: auto; height: 100px;" class="lightbox-image-list float-left"/>   
                            </a>

                        </div>
                        <div v-if="message.file == 0" class="recieved-message" >
                            @{{message.message}}

                        </div>

                        
                    </div>

                    <div class="d-sm-none  ml-5"></div>

                    <div class="d-none d-sm-none d-md-block d-lg-block col-sm-12 col-12 col-md-3  text-center pr-4" style="align-self: center;">
                        @{{message_time(message.created_at)}}
                    </div>

                </div>

              
            </li>
            
        </ul>
            
       
            
            <div class="bg-white row p-1 mb-2 mt-2" style="position: relative;bottom:0;margin:0" class="" >
                <hr style="width:100%;margin-left:1%">
                <div class="col-10" v-if="active_status == 1 ? active_action = 'display:block' : active_action = 'display:none'" :style="active_action">
    
                    <input type="text" placeholder="Type a message" v-model="new_message" class="message-input pl-2" @keyup.enter="store_new_message" style="black; height:40px; outline:none; "  >
    
                </div>
                <div class="col-2" v-if="active_status == 1 ? active_action = 'display:block' : active_action = 'display:none'" :style="active_action">
                    
                    <a href="javascript:;" style="position: absolute;right:15" @click="store_new_message" class="text-primary mt-2" ><i class="far fa-paper-plane fa-lg send-message search-bottom-icon-setting"></i></a>
                    
                    <input type="file" ref="file" style="display: none" @change="handleFiles" >


                    <a href="javascript:;" style="position: absolute;right:50%" @click="$refs.file.click()" class="mt-2 pr-2 " >
                        <i class="fas fa-paperclip attach-file-icon "></i></a>

                </div>
            </div>
        </div>
    
    
    </div>

    </div>

@endsection


@section('scripts')
<link rel="manifest" href="{{request()->root()}}/public/manifest.json">
{{--  importScripts('https://www.gstatic.com/firebasejs/7.14.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.14.2/firebase-messaging.js');  --}}

<script>

    $(document).ready(function() {
        $( ".user-list .user-item" ).bind( "click", function(event) {
            event.preventDefault();
            var clickedItem = $( this );
            $( ".user-list .user-item" ).each( function() {
                $( this ).removeClass( "active-chat" );
            });
            clickedItem.addClass( "active-chat" );
        });
    });

    @if(isset($reciever))
        $(document).ready(function() {
            //var $container = $('#myUL-3'),
            //$scrollTo = $('#user-message-count-{{$reciever->id}}');

            //$("#myUL-3").animate({ scrollTop: $('#myUL-3').height()}, 10);

            $(document).ready(function () {
                $('.contact-list ').animate({
                    scrollTop: $('.contact-list .active-chat').position().top
                }, 'slow');
            });
            //$('#myUL-3').scrollTop($('#user-message-count-{{$reciever->id}}'));


            // $('#myUL-3').animate({
             //   scrollTop: $("#user-message-count-{{$reciever->id}}").offset().top
            //}, 2000);
});
@endif


    </script>

  <script>

    const app = new Vue({
      el: '#chat',
      data: {
        messages: [],
        commentBox: '',
        new_message:'',
        new_message_sent: null,
        sending_message:null,
        sender: {{$support->id}} ,
        sending_user: {!! $support !!} ,
        reciever: '' ,
        reciever_id: '' ,
        profile_image: null ,
        active_status : 0,
        active_action : null,
        pre_reciever : {!! isset($reciever) ? json_encode($reciever) : 'null' !!},

        files: [],
      },
      mounted() { 
        //this.load_chat();
        //this.listener_for_chat();
        if(this.pre_reciever != null){ 
            this.get_this_chat(this.pre_reciever);
        }
    },
      methods: {
        
        store_new_message(){

            
            if(this.new_message.length > 0){
            
            this.new_message_sent = { id : null , message: this.new_message , sender_id: this.sender , reciever_id : this.reciever.id , created_at : new Date , update_at : new Date , file:0 }

            this.messages.push(this.new_message_sent);
        
            this.sending_message = this.new_message;
            this.new_message = '';
            this.new_message_sent = '';
                
            axios.post('/api/chat/store', {
                message : this.sending_message,
                sender : this.sender,
                reciever : this.reciever.id,
            })
                .then( res => {
                    //console.log(res.data);
                    //this.messages.push(res.data);
                    //this.new_message = '';
                    //this.new_message_sent = '';
                })
                .catch( error => {
                console.log(error);
                });
            }
        },

        handleFiles(){

            this.$Progress.start();

  
            let uploadedFiles = this.$refs.file.files;

            for(var i = 0; i < uploadedFiles.length; i++) {
                this.files.push(uploadedFiles[i]);
            }
            
            for( let i = 0; i < this.files.length; i++ ){
                    if(this.files[i].id) {
                        continue;
                    }
                    let formData = new FormData();
                    formData.append('sender', this.sender);
                    formData.append('reciever', this.reciever.id);

                    formData.append('file', this.files[i]);    
                    axios.post('/api/chat/store/file',
                        formData,
                        {
                            headers: {
                                'Content-Type': 'multipart/form-data'
                            }
                        }
                    ).then(function(data) {
            
                        this.$Progress.finish();

                        //console.log(data.data.message);
                        //this.files[i].id = data['data']['id'];
                        //this.files.splice(i, 1, this.files[i]);

                        this.messages.push(data.data.message);

                    }.bind(this)).catch(function(data) {
                        //console.log(data);
                        Toast.fire({
                            icon: 'error',
                            title: 'File upload Failed'
                          })
                          this.$Progress.fail();

                    });
                }
            this.files = [];
        },
        load_chat(){
 
           
            axios.get('/api/chat/'+ {{$support->id}} +'/get/'+ 1)
            .then( res => {
                this.messages = res.data.chats;

            }).catch( errors => {
                console.log(errors)
            });
        },
        get_this_chat(reciever){
            {{-- $('#messages-div').loading({
                stoppable: false
              });   --}}

              let loader = this.$loading.show();

    
            this.active_status = 1;

            
            this.reciever = reciever;
            $('#user-message-count-'+reciever.id).hide();

            axios.get('/api/chat/'+ {{$support->id}} +'/get/'+ reciever.id)
            .then( res => {
                this.messages = res.data.chats;

                loader.hide();

                //$('#messages-div').loading('stop');  

            }).catch( errors => {
                console.log(errors)
            });

            setInterval(() => { this.update_current_chat() }, 8000); 

        },
        update_current_chat(){
            //console.log('update is called')
            axios.get('/api/chat/'+ {{$support->id}} +'/get/'+ this.reciever.id)
            .then( res => {
                this.messages = res.data.chats;

            }).catch( errors => {
                console.log(errors)
            });


        },

        message_time(message_time){
            var time = new Date(message_time);
            //console.log( message_time , ' -- ' ,  time);

            var hours = time.getHours();
            var minutes = time.getMinutes();
            var ampm = hours >= 12 ? 'pm' : 'am';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0'+minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;

        },
        downloadFile(id){
        
        axios.get('/api/chat/download/file/'+id).then( () => {  } ).catch( () => {  } );

        
        },
        listener_for_chat(){
                Echo.channel("chat-{{$support->id}}").listen("ChatMessageEvent", e => {
                    //console.log(e);
                    //this.chat.messages.push(e.message);
                    this.messages.push(e.chat);

                //this.load_chat();
                });
                if(this.reciever){
                }
            } ,

            close_chat(){
                this.messages = [];
                this.reciever = ''
                this.active_status = 0

                $( ".user-list .user-item" ).each( function() {
                    $( this ).removeClass( "active-chat" );
                });
          

            },



            search_function() {
                var input, filter, ul, li, a, i, txtValue;
                input = document.getElementById("myInput");
                filter = input.value.toUpperCase();
                ul = document.getElementById("myUL");
                li = ul.getElementsByTagName("li");
                for (i = 0; i < li.length; i++) {
                    a = li[i].getElementsByTagName("a")[0];
                    txtValue = a.textContent || a.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        li[i].style.display = "";
                    } else {
                        li[i].style.display = "none";
                    }
                }
            }

            
      }
    });
  </script>
@endsection





{{--  <script src="https://www.gstatic.com/firebasejs/7.14.2/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.2/firebase-messaging.js"></script>
    <script>
     

        $(document).ready(function(){
            setInterval(() => { get_new_message() }, 8000); 
          })
    
          function get_new_message( ){
              $.ajax({
                    url : '{{url("/")}}/get/new-messages/'+{{auth()->user()->id}} ,
                    success : function(res){
    
                        res.forEach( function (message){
    
                            if(message[1] > 0){
                                $('#user-message-count-'+message[0]).html(message[1]);
                                $('#user-message-count-'+message[0]).show();
                            }
                            if(message[1] == 0)
                            $('#user-message-count-'+message[0]).hide();
    
                        })
                        
    
                        //console.log(res)
                    }
                }).fail( err => {
                console.log(err);
              })
          }



// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
firebase.initializeApp({
  apiKey: 'AIzaSyC2gmhm0X64V__GM_8NUO3LqL3wU4gZ9OE',
  authDomain: 'dawamitesting.firebaseapp.com',
  databaseURL: 'https://dawamitesting.firebaseio.com',
  projectId: 'dawamitesting',
  storageBucket: 'dawamitesting.appspot.com',
  messagingSenderId: '687996428523',
  appId: '1:687996428523:web:09faeffaf0f5eee4f2deae',
  measurementId: 'G-X0EGPMTBHP',
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
// Retrieve Firebase Messaging object.
const messaging = firebase.messaging();
messaging.requestPermission().then(function() {
  console.log('Notification permission granted.');
  // TODO(developer): Retrieve an Instance ID token for use with FCM.
  // ...
}).catch(function(err) {
  console.log('Unable to get permission to notify.', err);
});  --}}