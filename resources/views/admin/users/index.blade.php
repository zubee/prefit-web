@extends('layouts.dashboard')


@section('styles')
    <style>
        #user-list td{
            vertical-align: middle;
        }
        .pagination{
            justify-content: center;
        }
    </style>
@endsection

@section('content')


<div class="row">

   @include('layouts.partials.swals')

<div class="col-md-12" style="padding:30px" >
    <div class="card ">
      <div class="card-header">
        <h3 class="card-title">Users List</h3>
       <!-- <a class="btn btn-outline-primary float-right" href="{{route('admin.users.create')}}"> <i class="fas fa-user-plus"></i> Create</a>-->
          <a class="btn btn-outline-primary float-right" href="{{route('admin.export.user')}}">
              <i class="fas fa-file-excel"></i> Export
          </a>

      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <table class="table">
          <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>Profile Image</th>
              <th>Joined Date</th>
              <th>Actions</th>
          </tr>

          @foreach($users as $k => $user)
          <tr id="user-list">
              <td> {{$k+1}} </td>
              <td> {{$user->name}} </td>
              <td> {{$user->email}} </td>
              <td> <img class="profile-picture" src="@if($user->photo) {{asset('images/profile_images/'.$user->photo)}} @else {{asset('img/no-image-found.jpg')}} @endif"> </td>
              <td> {{$user->created_at->toDateString()}} </td>
              <td>
                
                <div class="icheck-success d-inline">
                    <input onchange="change_user_status({{$user->id}})" type="checkbox" @if($user->status == 1) checked @endif id="checkboxSuccess{{$user->id}}">
                    <label for="checkboxSuccess{{$user->id}}">
                    </label>
                  </div>

                  <a href="{{route('admin.users.show' , $user->id)}}" class="text-success"  > <i class="fas fa-eye"></i> </a>

                  <a href="{{route('admin.users.edit' , $user->id)}}" > <i class="fas fa-edit"></i> </a>

                  <a href="javascript:;" onclick="AskBeforeDelete({{$user->id}})" class="text-danger"  > <i class="fas fa-trash"></i> </a>

              </td>

              <form id="delete-form-{{$user->id}}" action="{{route('admin.users.destroy' , $user->id)}}" method="POST" >
                  @csrf
                  @method('DELETE')
              </form>
          </tr>
          @endforeach
      </table>

    {{ $users->links() }}
    </div>
    </div>
  
  </div>
</div>

@endsection

@section('scripts')
    <script>
        function AskBeforeDelete(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
              }).then((result) => {
                if (result.value) {
                  
                    $('#delete-form-'+id).submit();
                }
              })
        }

        function change_user_status(id){
            $.ajax({
                url: '{{url("/")}}/change/user/status/'+id,
                success: function (res){
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        title: 'User Status Has been updated',
                        showConfirmButton: false,
                        timer: 1500
                      })
                }
            }).fail(function(){
                Swal.fire({
                    position: 'top-end',
                    icon: 'error',
                    title: 'Some Problem occured',
                    showConfirmButton: false,
                    timer: 1500
                  })
            })
        }
    </script>
@endsection