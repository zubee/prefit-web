

@extends('layouts.dashboard')


@section('styles')
    <style>
    </style>
@endsection

@section('content')


<div class="row">


    {{-- @dd($user ,$user->dietitian)  --}}
  

<div class="col-md-12" style="padding:30px" >
    <div class="card card-info ">
      <div class="card-header">
        <h3 class="card-title">{{$user->name}} Profile</h3>
        {{--  <a href="{{route('trainer.profile.edit')}}" class="btn btn-success float-right" > <i class="fas fa-edit"></i> Edit </a>  --}}
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      
        
        <div class="ml-3">

            @if($user->photo)
                <img  src="{{asset('images/profile_images/'.$user->photo)}} " style="width: auto; height: auto; max-height: 100px; max-width: 100px; border-radius:100%;" />
            @else
                <img  src="{{asset('img/no-image-found.jpg')}} " style="width: auto; height: auto; max-height: 100px; max-width: 100px; border-radius:100%;" />

            @endif

            <a class="btn btn-outline-teal btn-sm float-right" href="{{route('admin.messages' , $user->id)}} " > <i class="fas fa-envelope f-20"></i></a>


            {{--  <div class="mt-3"></div>  --}}
            <hr>
            <div class="row">
              <div class="col-4 col-sm-12 col-md-4 col-lg-4">
                <div class="">
                    Name :
                    <label> <b> {{$user->name}} </b> </label>
                </div>
                
                <div class="">
                    Email :
                    <label> <b> {{$user->email}} </b> </label>
                </div>
                
                <div class="">
                    Joined :
                    <label> <b> {{$user->created_at->toDateString()}} </b> </label>
                </div>
                
                <div class="">
                    Status :
                    @if($user->status == 1)
                      <label><span class="badge badge-success">Active</span></label>
                      @else
                      <label><span class="badge badge-danger">Deactive</span></label>
                    @endif
                </div>
                <div class="">
                  Gender :
                  <label> <b> {{ ucwords($user->profile->gender) ?? ""}} </b> </label>
                </div>
            </div>
            <div class="col-4 col-sm-12 col-md-4 col-lg-4">
                
                
                <div class="">
                    Date Of Birth:
                    <label> <b> {{$user->profile->date_of_birth ?? "" }} </b> </label>
                </div>
                
                <div class="">
                    Height :
                    <label> <b> {{$user->profile->height ?? "" }} </b> </label>
                </div>
                
                <div class="">
                    Current Weight :
                    <label> <b> {{$user->profile->current_weight ?? "" }} </b> </label>
                </div>
                
                <div class="">
                    Starting Weight :
                    <label> <b> {{$user->profile->starting_weight ?? "" }} </b> </label>
                </div>

                <div class="">
                  Current Weight :
                  <label> <b> {{$user->profile->current_weight ?? "" }} </b> </label>
                </div>
              </div>
            
            
            <div class="col-4 col-sm-12 col-md-4 col-lg-4">
                <div class="">
                    Starting Weight :
                    <label> <b> {{$user->profile->starting_weight ?? "" }} </b> </label>
                </div>
                
                <div class="">
                    Target Weight :
                    <label> <b> {{$user->profile->target_weight ?? "" }} </b> </label>
                </div>
                
                <div class="">
                    Exercise Level :
                    <label> <b> {{ ucwords($user->profile->exercise_level) ?? "" }} </b> </label>
                </div>   
                
                <div class="">
                    Goals :
                    @if($user->profile && $user->profile->goals && is_array($user->profile->goals) )
                    @foreach($user->profile->goals as $goal)
                        <label> <b> {{$goal}} </b> </label>
                    @endforeach
                    @endif
                </div>
                
                <div class="">
                    Workouts:
                    @if($user->profile && $user->profile->workout && is_array($user->profile->workout))
                    @foreach($user->profile->workout as $workout)
                        <label> <b> {{$workout}} </b> </label>,
                    @endforeach
                    @endif
                </div>
                
                
            </div>

            {{--  @dd($user->profile->measurements)  --}}
                @if($user->profile && $user->profile->measurements)
                @php
                    $user->profile->measurements = json_decode($user->profile->measurements)
                @endphp
                <div class="col-12">
                    
                  <hr>
                  <h3>Measurements</h3>

                  <div>
                    <div class="row">

                      <div class="col-3">
                        Head
                      </div>
                      
                      <div class="col-3">
                        {{$user->profile->measurements->head}}
                      </div>


                      <div class="col-3">
                        Chest
                      </div>
                      
                      <div class="col-3">
                        {{$user->profile->measurements->chest}}
                      </div>

                    </div>

                    <div class="row">
                      <div class="col-3">
                        Left Bicep
                      </div>
                      
                      <div class="col-3">
                        {{$user->profile->measurements->left_bicep}}
                      </div>
                      
                      <div class="col-3">
                        Right Bicep
                      </div>
                      
                      <div class="col-3">
                        {{$user->profile->measurements->right_bicep}}
                      </div>
                    </div>
                         


                    <div class="row">
                      <div class="col-3">
                        Left Forearm
                      </div>
                      
                      <div class="col-3">
                        {{$user->profile->measurements->left_forearm}}
                      </div>
                      
                      <div class="col-3">
                        Right Forearm
                      </div>
                      
                      <div class="col-3">
                        {{$user->profile->measurements->right_forearm}}
                      </div>
                    </div>
                         

                    <div class="row">
                      <div class="col-3">
                        Left Calf
                      </div>
                      
                      <div class="col-3">
                        {{$user->profile->measurements->left_calf}}
                      </div>
                      
                      <div class="col-3">
                        Right Calf
                      </div>
                      
                      <div class="col-3">
                        {{$user->profile->measurements->right_calf}}
                      </div>
                    </div>
                         
                    <div class="row">
                      <div class="col-3">
                        Left Thigh
                      </div>
                      
                      <div class="col-3">
                        {{$user->profile->measurements->left_thigh}}
                      </div>
                      
                      <div class="col-3">
                        Right Thigh
                      </div>
                      
                      <div class="col-3">
                        {{$user->profile->measurements->right_thigh}}
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-3">
                        Hips
                      </div>
                      
                      <div class="col-3">
                        {{$user->profile->measurements->hips}}
                      </div>

                      <div class="col-3">
                        Waist
                      </div>
                      
                      <div class="col-3">
                        {{$user->profile->measurements->waist}}
                      </div>
                    </div>

                     
                  </div>
              </div>
                @else
                <div class="col-12">
                    <hr>
                    <div class="alert alert-info">
                        <i class="fas fa-info-circle"></i> No Measurements 
                    </div>
                </div>
                @endif



            </div>
          
            <hr>
            <h4>Plans</h4>
            <div class="row">
                @if($user->plans && $user->plans->count() > 0)
                @foreach($user->plans as $plan)
                @if($plan->coach)
                {{--  @dd($plan)  --}}
                <div class="col-4">
                    <div class="package-card">
                        {{--  <img class="package-card-img" src="{{asset('images/packages/'.$plan->package->image)}}" alt="Card image cap">  --}}
                        <div class="package-card-body">
                            <div class="package-card-title-item"> 
                                <a href="{{route('admin.packages.show' , $plan->package->id)}}"> {{$plan->package->name}}</a>
                            </div>
                            <div class="package-card-title-item">By: 
                                <a href="{{route('admin.coach.show' , $plan->coach->id)}}">{{$plan->coach->user->name}} </a>
                            </div>
                            <hr>
                            <div class="package-card-text-area">
                            <div class="package-card-text-item">Status:
                                @if( $plan->start_status == 0) 
                                    <span class="badge badge-warning">Not Started</span>
                                @elseif($plan->is_completed == 0 && $plan->start_status == 1)
                                    <span class="badge badge-primary">n-Progress</span>
                                @elseif($plan->is_completed == 1 && $plan->start_status == 1 )
                                    <span class="badge badge-success">Completed</span>
                                @endif
                             </div>
                            <div class="package-card-text-item">Subscribed Date: <b> {{$plan->created_at->toDateString()}} </b> </div>

                            @if($plan->start_status == 1)
                            <div class="package-card-text-item">Progress:  <b>{{$plan->plan_progress}}%</b></div>
                            <div class="package-card-text-item">Start Date: <b> {{$plan->start_date}}</b></div>
                            <div class="package-card-text-item">End Date: <b>{{$plan->end_date}}</b></div>
                            @endif
                            <a class="mt-2 btn btn-sm btn-outline-primary" href="{{route('admin.coach.client.package.plan.view' , [$plan->coach->id , $plan->client->id , $plan->package->id ])}}">  View Plan</a>
                            </div>
                        </div>
                        </div>
                    </div>
                    @endif
                @endforeach
                @else
                <span class="text-center alert alert-success w-100">No Plans Subscribed</span>
                @endif
                
                {{--  @if($user->subscribed_packages)
                @foreach($user->subscribed_packages as $package)
                @dd($package)
                <div class="col-3">
                    <div class="card">
                        <img class="card-img-top" src="{{asset('images/packages/'.$package->image)}}" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">{{$package->name}}</h5>
                            <h5 class="card-title">By: {{$package->name}}</h5>
                          <p class="card-text"> {{$}} </p>
                        </div>
                      </div>
                    </div>
                @endforeach
                @endif  --}}
            </div>
            
              
        
    </div>

    </div>
    </div>
  
  </div>
</div>

@endsection

@push('script')

    <script>
        var tagnames = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: '{{route('tagnames')}}'
        });

        tagnames.initialize();

        $('#tags').tagsinput({
            typeaheadjs: {
                name: 'tagnames',
                displayKey: 'name',
                valueKey: 'name',
                source: tagnames.ttAdapter()
            },
            freeInput : false
        });

    </script>

    @endpush