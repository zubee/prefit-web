@extends('layouts.dashboard')


@section('content')


<div class="row">

   @include('layouts.partials.swals')

<div class="col-md-12" style="padding:30px" >
    <div class="card card-info ">
      <div class="card-header">
        <h3 class="card-title">Create New User</h3>
      </div>
      <!-- /.card-header -->
      <div style="padding:30px" >
      <form action="{{route('admin.users.store')}}" method="POST">
          @include('layouts.partials.form_errors')
          @csrf

          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail1" class="row float-right col-form-label ">Name:</label>
          </div>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="inputEmail1" placeholder="Name" name="name" value="{{old('name')}}" required>
            </div>
          </div>
          
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail2" class="row float-right col-form-label ">Email:</label>
          </div>
            <div class="col-sm-8">
              <input type="email" class="form-control" id="inputEmail2" placeholder="Email" name="email" value="{{old('email')}}" required>
            </div>
          </div>
          
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail3" class="row float-right col-form-label ">Password:</label>
          </div>
            <div class="col-sm-8">
              <input type="password" class="form-control" id="inputEmail3" placeholder="Password" name="password" autocomplete="off" required>
            </div>
          </div>
          
          <div class="form-group row">
            <div class="col-sm-2">
            <label for="inputEmail4" class="row float-right col-form-label ">Confirm Password:</label>
          </div>
            <div class="col-sm-8">
              <input type="password" class="form-control" id="inputEmail4" placeholder="Confirm Password" name="conf_password" autocomplete="off" required>
            </div>
          </div>
          

        
        <div>
            <button class="btn btn-primary float-right">Create</button>
        </div>
      </form>
    </div>
    </div>
  
  </div>
</div>

@endsection