@extends('layouts.dashboard')


@section('styles')

<!-- Toastr -->
<link rel="stylesheet" href="{{ asset('admin-plugins/toastr/toastr.min.css')}}">


{{--  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.1/dist/css/bootstrap.min.css"/>  --}}
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/summernote@0.8.10/dist/summernote-bs4.css">

<script src="https://cdn.jsdelivr.net/npm/babel-polyfill@latest/dist/polyfill.min.js"></script>            
{{--  <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.slim.min.js"></script>  --}}

{{--  <script src="https://cdn.jsdelivr.net/npm/popper.js@latest/dist/umd/popper.min.js"></script>  --}}
{{--  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.1/dist/js/bootstrap.min.js"></script>  --}}
    
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.10/dist/summernote-bs4.min.js"></script>  
<script src="https://kotarov.github.io/vue-summernote/vue-summernote.es5.js"></script>



<link href="{{asset('assets/select-checkbox1/select.css')}}">


{{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"> --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script> --}}




<style>
  .multiselect-container{
    width: 95% !important;
    padding: 10px !important;
  }

  .caret{
    float: right !important;
    margin-top: 8px;
  }

  .multiselect {
    height: 35px;
    margin-top: 8px;
    border: 1px solid #337ab7;
  } 

  .multiselect:hover {
    background-color:#eee !important ;
    color: black !important;
  }

  .multiselect-container > .active{
    background-color:#337ab7;
    color:white;
    text-decoration: none;
  }


  .multiselect-container > li {
    padding-top: 5px !important;
    border-radius: 4px;
  }
  
  .multiselect-container > li > a {
    padding-left: 10px !important;
    border-radius: 2px;
    border-radius: 3px;
  }

  .multiselect-clear-filter{
    display: none;
  }
  .multiselect-search{
    margin-bottom: 10px !important;
  }

  .action_buttons{
    position: absolute;
    top: 150px;
    right: 50px;
    z-index: 10;
  }

  .plan-start-button{
    position: absolute;
    top: -40px;
    right: 10px;
  }
  .main-action-buttons{
    font-size: 25px;
  }
</style>

@endsection


@section('content')

@include('layouts.partials.swals')

<div class="row" id="plan">
  <vue-progress-bar></vue-progress-bar>

@php
    $count = 0;
    $count1 = 0;
@endphp



<div class="action_buttons">

  


  <a v-if="plan && plan.start_date" href="javascript:;" class="btn btn-outline-success  btn-sm mr-2 mb-2" > <i class="fas fa-calendar"></i> Start Plan Date: @{{plan.start_date}} </a>

  <a v-if="plan && !plan.start_date" @click="start_plan_now" href="javascript:;" class="btn btn-outline-success btn-sm  mr-2 mb-2" > <i class="fas fa-play-circle"></i> Start Plan Now</a>

  <a v-if="current_day_number && current_day" href="javascript:;" class="text-success main-action-buttons" @click="update_data_in_modal(current_day)"><i class="fas fa-edit"></i></a>
  
  <a v-if="current_day_number && !current_day" href="javascript:;" class="main-action-buttons"  @click="day_data_modal({{$count1}})"><i class="fas fa-plus-circle"></i></a>
  
  
  <a @click="refresh_data" href="javascript:;" class="text-danger ml-2 main-action-buttons" > <i class="fas fa-history "></i> </a>

  <a @click="init_toaster" href="javascript:;" class="text-warning ml-2 main-action-buttons" > <i class="fas fa-arrow-alt-circle-up "></i> </a>



</div>

      <!-- /.card-header -->
      <div class="col-12 col-sm-12 col-lg-12" style="padding:30px" >
        {{--  <div class="card-header">
            <h3 class="card-title">Create Plan</h3>
          </div>  --}}

          <div class="card card-info card-tabs">
          <div class="card-header p-0 pt-1">
            <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                

                @for($i = 0; $i < $package->weeks; $i++ )
                <li class="nav-item" style="width: 15%" >
                <a class="nav-link @if($i == 0) active @endif" id="week-tab-{{$i}}-tab" data-toggle="pill" href="#week-tab-{{$i}}" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true" @click="bullet_click( $event , {{$i * 7 + 1 }} )" >Week {{$i+1}}</a>
                </li>
                @endfor
        
            </ul>
          </div>
          <div class="card-body">
            <div class="tab-content" id="custom-tabs-one-tabContent">
              
                @for($i = 0; $i < $package->weeks; $i++ )
              
                <div class="tab-pane fade @if($i == 0) show @endif @if($i == 0) active @endif" id="week-tab-{{$i}}" role="tabpanel" aria-labelledby="week-tab-tab-{{$i}}">
                









                          <h4>Week {{$i+1}} Days</h4>
                          <div class="row">
                            <div class="col-5 col-sm-2">
                              <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                
                                @for($j = 0; $j< 7; $j++ )
                                @php
                                    $count = $count+1;

                                    if($count > $package->duration){
                                        break;
                                    }
                                @endphp
                                {{-- id="day-tab-{{$i}}-{{$j}}-tab" --}}
                                <a class="nav-link @if($j == 0) active @endif day-tab-{{$count}} " id="{{$count}}" data-toggle="pill" href="#day-tab-{{$i}}-{{$j}}" role="tab" aria-controls="vert-tabs-home-{{$i}}-{{$j}}" aria-selected="true" @click="bullet_click($event , {{$count}})" >Day {{$j+1}} -- {{$count}} </a>

                                @endfor

                                {{--  <a class="nav-link" id="vert-tabs-profile-tab" data-toggle="pill" href="#vert-tabs-profile" role="tab" aria-controls="vert-tabs-profile" aria-selected="false">Profile</a>  --}}
                       
                              </div>
                            </div>
                            <div class="col-7 col-sm-9">
                              <div class="tab-content" id="vert-tabs-tabContent" >

                                @for($j = 0; $j < 7; $j++ )
                                
                                @php
                                  $count1 = $count1+1;

                                  if($count1 > $package->duration){
                                      break;
                                  }
                                @endphp


                                <div class="tab-pane text-left fade @if($j == 0 && $i == 0) show active @endif day-tab day-tab-{{$count1}} " day-counter="{{$count1}}"  id="day-tab-{{$i}}-{{$j}}"  role="tabpanel" ref="data_tabs" aria-labelledby="vert-tabs-home-tab-{{$i}}-{{$j}}">

                                  <div class="row">
                                    <div class="col-12">
                                      {{$i+1}} Week -- {{$j+1}} Day
    
                                    {{-- <a v-if="plan && !plan.start_date" @click="start_plan_now" href="javascript:;" class="btn btn-outline-success mr-2 float-right" > <i class="fas fa-play-circle"></i> Start Plan Now</a>

                                    <a v-if="plan && plan.start_date" href="javascript:;" class="btn btn-outline-success mr-2 float-right" > <i class="fas fa-calendar"></i> Start Plan Date: @{{plan.start_date}} </a> --}}

                                    </div>
                                  </div>

                                        
                                  {{--  <div class="float-right" v-for="day in planned_days">
                                    <a   v-if="day.day_number != {{$count1}}" href="javascript:;"  @click="day_data_modal({{$count1}})"><i class="fas fa-plus-circle fa-2x"></i></a>
                                    
                                    <a  v-if="day.day_number == {{$count1}}" href="javascript:;" class="text-success" @click="update_data_in_modal(day)"><i class="fas fa-edit fa-2x"></i></a>

                                    <a @click="refresh_data" class="text-danger ml-2" > <i class="fas fa-history fa-2x"></i> </a>

                                  </div>  --}}
                                  {{--  slice( {{$count1 - 1 }} ,  {{$count1}})  --}}
                                  <hr>
                                  {{--  <div class="row">
                                    <div class="col-12">
                                      <div class="float-right" v-for="slice( index , planned_days.length) (duration.duration - planned_days.length)" >
                                    
                                        <a href="javascript:;" class="text-success" @click="update_data_in_modal(day)"><i class="fas fa-edit fa-2x"></i></a>
    
                                        <a  href="javascript:;"  @click="day_data_modal({{$count1}})"><i class="fas fa-plus-circle fa-2x"></i></a>
                                        
    
                                        <a @click="refresh_data" class="text-danger ml-2" > <i class="fas fa-history fa-2x"></i> </a>
    
    
                                      </div>
                                    </div>
                                  </div>  --}}
                                  


                                <div v-for="(day , index) in planned_days" v-show="for_execution(day.id , index)" >

                                 
                                  
                                  <div v-if="day.day_number == {{$count1}}" >

                                    {{--  <div class="float-right" >
                                    
                                      <a href="javascript:;" class="text-success" @click="update_data_in_modal(day)"><i class="fas fa-edit fa-2x"></i></a>                                      
  
                                      <a @click="refresh_data" class="text-danger ml-2" > <i class="fas fa-history fa-2x"></i> </a>
  
  
                                    </div>  --}}

                                  {{--  <hr>  --}}
                                  <h4>Exercises</h4>
                                  <hr>

                                  <div v-for="(workout , sub_index) in day.workouts">
                                    <div class="row">

                                      <div class="col-3">
                                          Name
                                      </div>
                                      
                                      <div class="col-9">
                                        @{{workout.name}}
                                      </div>
                                    </div>
                                    
                                    <div class="row">
                                      
                                      <div class="col-3">
                                          Steps
                                      </div>
                                      
                                      <div class="col-9">
                                        @{{day.workout_steps[sub_index]}}
                                      </div>
                                    </div>
                                    
                                    <div class="row">
                                      
                                      <div class="col-3">
                                          Reps
                                      </div>
                                      
                                      <div class="col-9">
                                        @{{day.workout_reps[sub_index]}}
                                      </div>
                                    </div>
                                    <hr v-if="!(sub_index == (day.workouts.length - 1)) " >
                                  </div>



                                  <hr>
                                  <h4>Measurements</h4>
                                  <hr>
                                  <div >
                                    
                                  </div>




                                  <h4>Meals</h4>
                                  <hr>
                                  <div v-for="( meal , sub_index ) in day.meals">
                                    <div class="row">

                                      <div class="col-9">

                                        <img v-if="(day && day.meal_images && day.meal_images[sub_index])" :src="'{{asset('images/plan_images')}}/'+ day.meal_images[sub_index]" class="profile-image " />

                                        {{-- <img v-if="day.meal_images[index]" :src="'{{asset('images/plan_images')}}/'+ day.meal_images[index]" class="profile-image " /> --}}
                                        {{-- @{{sub_index}} --}}
                                        {{-- @{{return_proper_image( day , sub_index)}} --}}
                                        <img v-else src="{{asset('img/no-image-found.jpg')}}" class="profile-image" />
                      
                                      </div>
                                    </div>
                                    
                                    <div class="row">

                                      <div class="col-9">
                                        <h5>
                                          @{{meal}}
                                        </h5>
                                      </div>
                                      
                                      {{-- <div class="col-9">
                                        @{{meal}}
                                      </div> --}}
                                    </div>
                                    
                                    <div class="row">
                                      
                                      <div class="col-3">
                                          Title
                                      </div>
                                      
                                      <div class="col-9">
                                        @{{day.meal_title[sub_index]}}
                                      </div>
                                    </div>
                                     
                                    
                                    <div class="row">
                                      
                                      <div class="col-3">
                                          Summary
                                      </div>
                                      
                                      <div class="col-9">
                                        @{{day.meal_summary[sub_index]}}
                                      </div>
                                    </div>
                                    
                                     
                                    <div class="row">
                                      
                                      <div class="col-3">
                                          What You Need to do
                                      </div>
                                      
                                      <div class="col-9" v-html="day.meal_needs[sub_index]" >

                                      </div>
                                    </div>
                                    
                                    <div class="row">
                                      
                                      <div class="col-3">
                                          How to do
                                      </div>
                                      
                                      <div class="col-9" v-html="day.meal_how_to_do[sub_index]">
                                      </div> 
                                    </div>
                                    
                                    <hr v-if="!(sub_index == (day.meals.length - 1)) " >
                                  </div>



                                  <hr>
                                  <h4>Supplements</h4>
                                  <hr>
                                  <div v-for="(supplement_time , index) in day.supplement_timings">

                                    <div >
                                      <h5> @{{supplement_time}} </h5>
                                    </div>

                                    <div v-for="(supplement , sub_index) in day.supplements[index]">

                                    <div class="row" >

                                      <div class="col-3">
                                          Name
                                      </div>
                                      
                                      <div class="col-9">
                                        @{{supplement.name}}
                                      </div>
                                    </div>
                                  
                                  
                                    <div class="row">

                                      <div class="col-3">
                                          Prescription
                                      </div>
                                      
                                      <div class="col-9">
                                        @{{day.supplement_prescription[index][sub_index] }}
                                      </div>
                                    </div>


                                    </div>


                                  

                                </div>


                                     
                                </div>
                                


                                  {{--  <div class="row" >
                                    <div class="col-12">
                                      <div class="float-right">
                                    
                                        <a href="javascript:;" class="text-success" @click="update_data_in_modal(day)"><i class="fas fa-edit fa-2x"></i></a>

                                        <a  href="javascript:;"  @click="day_data_modal({{$count1}})"><i class="fas fa-plus-circle fa-2x"></i></a>
                                        

                                        <a @click="refresh_data" class="text-danger ml-2" > <i class="fas fa-history fa-2x"></i> </a>


                                      </div>
                                    </div>
                                  </div>  --}}





                            </div>


                                </div>



                                @endfor

                                {{--  <div class="tab-pane fade" id="vert-tabs-profile" role="tabpanel" aria-labelledby="vert-tabs-profile-tab">

                                </div>  --}}
                                
                                
                              </div>
                            </div>
                          </div>
                          












                </div>

                @endfor

              
              
            </div>
          </div>
          <!-- /.card -->
          <div class="row">
              <div class="col-12 text-center mb-2">
                  {{--  <a class="mb-3 btn btn-success" href="#" > <i class="fas fa-save"></i> Draft</a>  --}}
                  {{--  <a class="mb-3 btn btn-primary" href="#" > <i class="fas fa-sticky-note"></i> Save</a>  --}}


              </div>
          </div>
        </div>



      </div>




      
    <div class="modal fade" id="day_data_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">

            <h5 v-if="!edit_mode" class="modal-title" id="exampleModalLongTitle">Add a Day</h5>
            
            <h5 v-if="edit_mode" class="modal-title" id="exampleModalLongTitle">Update Day</h5>
            
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form @submit.prevent="save_day_data">


          <div class="modal-body" id="bodyModal">

            {{--  <div class="form-group row">
              <label class="col-3 col-form-label ">Exercises</label>
              <div class="col-9">
                <select class="multiselect-ui form-control d-none mt-2" id="workouts" multiple   v-model="selected_workouts" >
                  <option v-for="workout in workouts" :value="workout.id" > @{{ workout.name }} </option>
              </select>
            
            </div>
          </div>  --}}




          
          <div class="form-group row" v-for="(workout,index) in workouts" >
            <div class="col-3" >
              <label class="col-form-label" v-show="index == 0" >Exercises</label>
            </div>

              <div class="col-9"  >
                
                <div class="row">
                  <div class="col-4">
                    <input type="checkbox" :id="'workout'+workout.id" :value="workout.id" v-model="full_data_form.workout_ids" />
                    <label :for="'workout'+workout.id" >@{{ workout.name }}</label> <a :href="'/coach/view/workout/'+workout.id"><i class="fas fa-eye"></i> </a>
    
                  </div>
                  <div class="col-4" v-if="full_data_form.workout_ids.includes(workout.id)" >
                    <input type="text" class="form-control" v-model="full_data_form.workout_steps[index]" placeholder="Steps" required />

                </div>
                  <div class="col-4" v-if="full_data_form.workout_ids.includes(workout.id)">
                    <input type="text" class="form-control" v-model="full_data_form.workout_reps[index]" placeholder="Repititions" required />

                </div>
                </div>
                
              </div>
              
            </div>

            <hr>
            <div class="row">

            <div class="col-3" >
              <label class="col-form-label">Meals</label>
            </div>
            
            <div class="col-9" >
              <v-select
                placeholder="Select Meals"
                v-model="full_data_form.current_meals"
                {{--  v-model="full_data_form.current_meals"  --}}
                multiple
                label="meals_array"
                :options="meals_array"
                @input="meals_select_triggered"
              />
            </div>
          </div>
          <hr>

          <div class="row" v-for="(meal,index) in full_data_form.meals" >
            <div class="col-3"></div>
            <div class="col-9">
            <a class="btn btn-outline-primary w-100 m-1" data-toggle="collapse" :href="'#meal_details'+index" role="button" aria-expanded="false" aria-controls="collapseExample" @click="toggle_area_for_meal(index)" >
              {{-- onclick="check_for_editors()" --}}
              @{{meal}}
            </a>

            <div class="collapse" :id="'meal_details'+index">
              <div class="card card-body">
                <div class="form-group">

                  
                  
                  
                  
                  <img :id="'image'+index" v-if="full_data_form.meal_images[index]" :src="'{{asset('images/plan_images')}}/'+ full_data_form.meal_images[index]" :class="'profile-image meal-profile-image-'+index" />

                  <img :id="'image'+index" v-if="!full_data_form.meal_images[index]" src="{{asset('img/no-image-found.jpg')}}" :class="'profile-image meal-profile-image-'+index" />
      
                  <button type="button" class="btn btn-outline-success" @click="click_the_upload_icon(index)"> <i class="fas fa-image"></i> Upload Image </button>
                  
                  <input :id="'file-upload-'+index" type="file" @change="update_meal_image($event  , index)"  class="file-upload" name="image" style="display:none" >
      
                </div>
                <div class="form-group">

                  <label>Title</label>

                  <input type="text" class="form-control" placeholder="Title" v-model="full_data_form.meal_title[index]" required />
                </div>

                <div class="form-group">
                  <label>Summary</label>
                  <textarea class="form-control" rows="4" placeholder="Summary" v-model="full_data_form.meal_summary[index]" required ></textarea>
                </div>
                
                <div class="form-group">
                  <label>What you will need</label>
                  <summernote v-model="full_data_form.meal_needs[index]"></summernote>

                </div>
 
                <div class="form-group">
                  <label>How to do it</label>

                  <summernote v-model="full_data_form.meal_how_to_do[index]"></summernote>

                </div>

 
              </div>
            </div>
            

          </div>
        </div>

        <hr>



        <div class="row">

          <div class="col-3" >
            <label class="col-form-label">Supplement Timing</label>
          </div>
          
          <div class="col-9" >
            <v-select
              placeholder="Select Supplement Timings"
              v-model="full_data_form.supplement_timings"
              multiple
              :options="supplement_timing"
            />
          </div>
        </div>
        <hr>


        <div v-for="(time,index) in full_data_form.supplement_timings">
        <div class="row" >

          <div class="col-3" >
            <label class="col-form-label"> @{{time}} Supplements</label>
          </div>
          
          <div class="col-9" >
            <v-select
              placeholder="Select Supplements"
              v-model="full_data_form.supplements[index]"
              multiple
              :options="supplements"
              label="name"
            />
          </div>
        </div>

        <div class="row" v-for="(supplement , sub_index) in full_data_form.supplements[index]" >
          <div class="col-3"></div>
          <div class="col-9">

            <div class="form-group">

              <label> @{{supplement.name}} Prescription</label>

              <input type="text" class="form-control" v-model="full_data_form.supplement_prescription[index][sub_index]" :placeholder="supplement.name+' Prescription'" required />
            </div>


          {{--  <a class="btn btn-outline-primary w-100 m-1" data-toggle="collapse" :href="'#supplement'+index" role="button" aria-expanded="false" aria-controls="collapseExample">
            @{{supplement.name}}
          </a>  --}}

          {{--  <div class="collapse" :id="'supplement'+index">
            <div class="card card-body">
              
              <div class="form-group">

                <label>Perscription</label>

                <input type="text" class="form-control" placeholder="Title" />
              </div>


            </div>
          </div>  --}}
          

        </div>
      </div>
      <hr>
    </div>


        
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button v-if="!edit_mode" type="submit" class="btn btn-primary">Save changes</button>
            
            {{--  <VueLoadingButton aria-label='Save Changes' />  --}}

            <button v-if="edit_mode" type="submit" class="btn btn-success">Update changes</button>
          </div>
        </form>
        </div>
      </div>
    </div>
    










    </div>
  




    {{--  <div class="card card-warning card-outline">
      <div class="card-header">
        <h3 class="card-title">
          <i class="fas fa-edit"></i>
          Toastr Examples
        </h3>
      </div>
      <div class="card-body">
        <button type="button" class="btn btn-success toastrDefaultSuccess">
          Launch Success Toast
        </button>
        <button type="button" class="btn btn-info toastrDefaultInfo">
          Launch Info Toast
        </button>
        <button type="button" class="btn btn-danger toastrDefaultError">
          Launch Error Toast
        </button>
        <button type="button" class="btn btn-warning toastrDefaultWarning">
          Launch Warning Toast
        </button>
        <div class="text-muted mt-3">
          For more examples look at <a href="https://codeseven.github.io/toastr/">https://codeseven.github.io/toastr/</a>
        </div>
      </div>
      <!-- /.card -->
    </div>  --}}



@endsection

@push('script')
<!-- Toastr -->
<script src="{{ asset('admin-plugins/toastr/toastr.min.js')}}"></script>

<script src="{{asset('assets/select-checkbox1/select.js')}}" ></script>


<script>
  




  $(function() {
    //$('#mySelect').multiselect();
     $('.multiselect-ui').multiselect({
      nonSelectedText: 'Select Workouts',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
    });
  });

    $(document).ready(function(){

      $('.toastrDefaultSuccess').click(function() {
        toastr.success('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
      });
      $('.toastrDefaultInfo').click(function() {
        toastr.info('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
      });
      $('.toastrDefaultError').click(function() {
        toastr.error('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
      });
      $('.toastrDefaultWarning').click(function() {
        toastr.warning('Lorem ipsum dolor sit amet, consetetur sadipscing elitr.')
      });
    });

  const plan = new Vue({
    el: '#plan',
   
    data: {

      client_id : {{$client->id}},
      package_id : {{$package->id}},
      package : {!! $package !!},
      coach_id : {{auth()->user()->coach->id}},
      plan_id : null,

      edit_mode : false,

      planned_days: null , 
      plan : null,

    	content: 'hello world',
    	meal_image: null,


      day: null,
      temp_day: null,
      workouts: '',
      selected_workouts:[],
      selected_workouts_steps:[],
      selected_workouts_reps:[],
      meals_array : [
        'Breakfast',
        'Lunch',
        'Dinner',
        'Snacks1',
        'Snacks2',
        'Snacks3',
      ],

      selected_meals:[],
      selected_meals_current:[],
      
      meal_description_images : [],
      
      meal_images_upload_indication : [],
      
      meal_description_title : [],
      
      meal_description_summary : [],
      meal_description_needs : [],
      meal_description_how_to_do : [],

      supplement_timing:[
        'Morning',
        'Evening',
        'Night',
      ],
      selected_supplements_timings: [],

      selected_supplements: [],
      supplements:[],
      supplement_prescription: [
        [],
        [],
        [],
      ],
      
      morning_supplement_prescription:[],
      evening_supplement_prescription:[],
      night_supplement_prescription:[],



      plan_days: [],


      editor: null,



      current_day : null,
      current_day_number: 1,
      current_day_to_update: '',

      created_actions_buttons: [],

      counted_days: [],

      current_images: [],

      full_data_form : new Form({
        day_number : null ,
        workouts : [] ,
        workout_ids : [] ,
        workout_steps : [] ,
        workout_reps : [] ,
        
        meals : [] ,
        current_meals : [] ,
        meal_title : [] ,
        meal_images : [] ,
        meal_summary : [] ,
        meal_needs : [] ,
        meal_how_to_do : [] ,
        
        meal_images_upload_indication: [],

        supplements : [] ,
        supplement_timings : [] ,
        supplement_ids : [] ,
        supplement_prescription : [
          [],
          [],
          [],
        ] ,
    }),
    
    },

    methods: {
      get_all_data(){
        axios.get('/api/get_all_data')
        .then( response => {
          //console.log(response);
          this.workouts = response.data.workouts;
          this.supplements = response.data.supplements;
        } )
        .catch(error => {

        });
        
      },

      get_plan_data(){

        
        this.$Progress.start();

        let loader = this.$loading.show();
  
        axios.get('/api/get_plan/client/'+ this.client_id +'/coach/'+ this.coach_id +'/package/'+ this.package_id)
        .then( response => {
          //this.plan_days : days.
          
          this.planned_days = response.data.days;

          this.plan = response.data.plan;

          this.created_actions_buttons = response.data.plan_days_ids;
          
          this.current_day = null;
  
          for(i = 0; i < this.planned_days.length; i ++ ){
            if(this.planned_days[i].day_number == this.current_day_number){
  
              this.current_day = this.planned_days[i];
  
            }
          }

          
          loader.hide();
          this.$Progress.finish();
          
          //console.log(response)
        } )
        .catch(error  => {
  
          loader.hide();
          this.$Progress.fail();

          console.log('error');
        } )
      },

      save_day_data(){


        let loader = this.$loading.show();
        
        this.$Progress.start({
          // Optional parameters
            container: this.fullPage ? null : this.$refs.formContainer,
            canCancel: false,
            onCancel: this.onCancel,
            });

        this.full_data_form.post('/api/store_day_data')
        /*axios.post('/api/store_day_data',{

          client_id :  this.client_id,
          package_id : this.package_id,
          coach_id :  this.coach_id,
          
          day_number : this.current_day_number,
          
          selected_workouts : this.selected_workouts,
          selected_workouts_steps : this.selected_workouts_steps,
          selected_workouts_reps : this.selected_workouts_reps,


          selected_meals : this.selected_meals,
          meal_description_title : this.meal_description_title,

          //meal_description_images : this.meal_description_images,
          meal_description_images : this.current_images,
          current_images : this.meal_description_images,

          meal_images_upload_indication : this.meal_images_upload_indication,
          meal_description_summary : this.meal_description_summary,
          meal_description_needs : this.meal_description_needs,
          meal_description_how_to_do : this.meal_description_how_to_do,



          selected_supplements_timings : this.selected_supplements_timings,
          selected_supplements : this.selected_supplements,
          supplement_prescription : this.supplement_prescription,


        }) */
        .then(reposne => {
            this.refresh_data();

            

            $('#day_data_modal').modal('hide');
            this.reset_current_data();
            loader.hide();
            this.$Progress.finish();
            //Sweel("Successful!", "Data Saved successfully!", "success");
            toastr.success('Data Saved successfully.')

        })
        .catch(error => {

          loader.hide();
          this.$Progress.fail();

          toastr.error('Request Failed. Try Later.')

          //Sweel("Oops!", 'Request Failed. Try Later' , "error");
        });

        {{--  this.form.get('/api/store_day_data')
        .then( response => {
          console.log(response)
            
        } )
        .catch( error => { 
          console.log(error)

        } );  --}}

      },

      day_data_modal(day_count){
        //console.log(day_count)
        this.edit_mode = false;
        this.reset_current_data();
        this.day = day_count;
        $('#day_data_modal').modal('show');
      
      },

      refresh_data(){

        this.get_all_data();
        this.get_plan_data();
  
      },

      update_data_in_modal(day){
        let temp_day = day;
        //let loader = this.$loading.show();

        //this.reset_current_data();

        this.edit_mode = true;
        this.full_data_form.reset()
        this.full_data_form.fill(day)
        
        this.full_data_form.current_meals = day.meals;

        //console.log(day);
        /*this.day = temp_day.day_number;

        this.selected_workouts = temp_day.workout_ids;
        this.selected_workouts_steps = temp_day.workout_steps;
        this.selected_workouts_reps = temp_day.workout_reps;


        this.selected_meals = temp_day.meals;
        this.selected_meals_current = temp_day.meals;
        this.meal_description_title = temp_day.meal_title;
        this.meal_description_images = temp_day.meal_images ? temp_day.meal_images : [];
        
        //this.current_images = day.meal_images ? day.meal_images : [];
        
        //this.meal_images_upload_indication = day.meal_images ? day.meal_images : [];

        this.meal_description_summary = temp_day.meal_summary;
        this.meal_description_needs = temp_day.meal_needs;
        this.meal_description_how_to_do = temp_day.meal_how_to_do;



        this.selected_supplements_timings = (temp_day.supplement_timings && temp_day.supplement_timings.length > 0) ? temp_day.supplement_timings : [] ;
        this.selected_supplements = (temp_day.supplements && day.supplements.length > 0) ? temp_day.supplements : []  ;
        this.supplement_prescription = temp_day.supplement_prescription;*/

        //loader.hide();
        $('#day_data_modal').modal('show');


        //selected_supplements_timings : day.selected_supplements_timings,
        //selected_supplements : day.selected_supplements,
        //supplement_prescription : day.supplement_prescription,

      },

      reset_current_data(){

        this.day = null;

        this.selected_workouts = [];
        this.selected_workouts_steps = [];
        this.selected_workouts_reps = [];


        this.selected_meals = [];
        this.selected_meals_current = [];
        this.meal_description_title = [];
        this.meal_description_images = [];
        this.meal_images_upload_indication = [];
        this.meal_description_summary = [];
        this.meal_description_needs = [];
        this.meal_description_how_to_do = [];



        this.selected_supplements_timings = [];
        this.selected_supplements = [];
        this.supplement_prescription = [
          [],
          [],
          [],
        ];

      }, 

      start_plan_now(){
        axios.get('/api/start_plan_now/plan/'+this.plan.id)
        .then( response => {

            Sweel("Successful!", "Plan is Now Started!", "success");

            this.refresh_data();

        } ).catch( (errors) => {

          console.log(errors);

          Sweel("Oops!", 'Plan Does not exists or already started' , "error");

        });
      },

      update_texteditor(index){
        
        //console.log('1' , index , $('#text-editor1').val() )
        
        //this.meal_description_needs[index] = $('#text-editor1').val();
        //this.meal_description_how_to_do[index] = $('#text-editor2').val();
        
        //console.log('2' , $('#text-editor2').val() )
      },
      check_if_day_is_present(index , count){
        //console.log('index is ' , index , count );
        let status = false;
        if(!(this.created_actions_buttons.includes(count))){
          status = true;
          console.log(status);
        }
        return false;


      },
      for_execution(day_id , index){
        //console.log(day_id , index);
        return true;
      },

      bullet_click(e , count){
        
        console.log(e.target.id)
        if(e.target.id.includes('week-tab') ){
          
          var num = e.target.id.replace(/\D/g,'');

          for(i = 1; i <= this.package.duration; i ++){
  
              $('.day-tab-'+i).removeClass('active');
              $('.day-tab-'+i).removeClass('show');
              //$('#'+e.target.id).removeClass('active');

          }
          //console.log('day-tab-'+ (num * 7 +1))
          
            $('.day-tab-'+ (num * 7 +1)).addClass('active');
            $('.day-tab-'+ (num * 7 +1)).addClass('show');
            //$('#day-tab'+num+'1').addClass('active');
            

          console.log(num)
        //console.log('yes')
        }
        //$('#'+count).addClass('active')
        //$('#'+count).addClass('show')
        //console.log(count);

        const children = this.$el.querySelectorAll('.active .show');
        
        //console.log(children[0])
        //const day_count = children[1
        
        this.current_day_number = count;
        this.current_day = null;

        for(i = 0; i < this.planned_days.length; i ++ ){
          if(this.planned_days[i].day_number == count){

            this.current_day = this.planned_days[i];

          }
        }
        
      },

      update_meal_image(e , index){

        let file = e.target.files[0];
        
        //console.log(file);

        let reader = new FileReader();
        reader.onloadend = (file) => {
          //console.log('RESULT' , reader.result);
          //console.log(this.meal_description_images);
          //this.meal_description_images[index] =  reader.result; 

          this.current_images[index] =  reader.result; 

        }
        this.full_data_form.meal_images_upload_indication[index] =  1; 

        reader.onload = function (e) {
          $('.meal-profile-image-'+index).attr('src', e.target.result);
      }
        reader.readAsDataURL(file);

      },

      click_the_upload_icon(index){

        $('#file-upload-'+index).click();
      
      },

      toggle_area_for_meal(index){

        $('#meal_details'+index).toggle()

      },

      init_toaster(){
    
        this.full_data_form.post('/api/store_day_data');

        toastr.info('Are you the 6 fingered man?')

      },
      meals_select_triggered(value){

        let index = null;

        //full_data_form.current_meals
        
        if(value.length < this.full_data_form.meals.length ){
          for(i = 0; i < this.full_data_form.meals.length ; i ++){

            if(this.full_data_form.meals[i] != value[i]){
              index = i;
              
              this.full_data_form.meals = value;

              this.full_data_form.meal_images.splice(index , (index+1)) 
              this.full_data_form.meal_d_indication.splice(index , (index+1)) 
              this.full_data_form.meal_title.splice(index , (index+1)) 
              this.full_data_form.meal_summary.splice(index , (index+1)) 
              this.full_data_form.meal_needs.splice(index , (index+1)) 
              this.full_data_form.meal_how_to_do.splice(index , (index+1)) 

              break;
            }
            //console.log(value[i]  , this.selected_meals[i] );
          }
        }else{
          this.selected_meals = value;

        }
      },
  
    
    },
    mounted() { 


    

      let loader = this.$loading.show();

      this.get_all_data();
      this.get_plan_data();
      loader.hide();

      //console.log('vue is here!');
    },


    
 });



</script>
    @endpush

