@extends('layouts.dashboard')


@section('styles')
<link href="{{asset('assets/select-checkbox1/select.css')}}">


<style>
  .multiselect-container{
    width: 95% !important;
    padding: 10px !important;
  }

  .caret{
    float: right !important;
    margin-top: 8px;
  }

  .multiselect {
    height: 35px;
    margin-top: 8px;
    border: 1px solid #337ab7;
  } 

  .multiselect:hover {
    background-color:#eee !important ;
    color: black !important;
  }

  .multiselect-container > .active{
    background-color:#337ab7;
    color:white;
    text-decoration: none;
  }


  .multiselect-container > li {
    padding-top: 5px !important;
    border-radius: 4px;
  }
  
  .multiselect-container > li > a {
    padding-left: 10px !important;
    border-radius: 2px;
    border-radius: 3px;
  }

  .multiselect-clear-filter{
    display: none;
  }
  .multiselect-search{
    margin-bottom: 10px !important;
  }
</style>

@endsection


@section('content')

@include('layouts.partials.swals')

<div class="row" id="plan">

@php
    $count = 0;
    $count1 = 0;
@endphp

      <!-- /.card-header -->
      <div class="col-12 col-sm-12 col-lg-12" style="padding:30px" >
        {{--  <div class="card-header">
            <h3 class="card-title">Create Plan</h3>
          </div>  --}}
        <div class="card card-info card-tabs">
          <div class="card-header p-0 pt-1">
            <ul class="nav nav-tabs" id="custom-tabs-one-tab" role="tablist">
                
                @for($i = 0; $i < $package->weeks; $i++ )
                <li class="nav-item" style="width: 15%" >
                <a class="nav-link @if($i == 0) active @endif" id="week-tab-{{$i}}-tab" data-toggle="pill" href="#week-tab-{{$i}}" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true">Week {{$i+1}}</a>
                </li>
                @endfor
        
            </ul>
          </div>
          <div class="card-body">
            <div class="tab-content" id="custom-tabs-one-tabContent">
              
                @for($i = 0; $i < $package->weeks; $i++ )
              
                <div class="tab-pane fade @if($i == 0) show @endif @if($i == 0) active @endif" id="week-tab-{{$i}}" role="tabpanel" aria-labelledby="week-tab-tab-{{$i}}">
                









                          <h4>Week {{$i+1}} Days</h4>
                          <div class="row">
                            <div class="col-5 col-sm-2">
                              <div class="nav flex-column nav-tabs h-100" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
                
                                @for($j = 0; $j< 7; $j++ )
                                @php
                                    $count = $count+1;

                                    if($count > $package->duration){
                                        break;
                                    }
                                @endphp
                                <a class="nav-link @if($j == 0) active @endif" id="day-tab-{{$i}}-{{$j}}-tab" data-toggle="pill" href="#day-tab-{{$i}}-{{$j}}" role="tab" aria-controls="vert-tabs-home-{{$i}}-{{$j}}" aria-selected="true">Day {{$j+1}} -- {{$count}} </a>

                                @endfor

                                {{--  <a class="nav-link" id="vert-tabs-profile-tab" data-toggle="pill" href="#vert-tabs-profile" role="tab" aria-controls="vert-tabs-profile" aria-selected="false">Profile</a>  --}}
                       
                              </div>
                            </div>
                            <div class="col-7 col-sm-9">
                              <div class="tab-content" id="vert-tabs-tabContent">

                                @for($j = 0; $j < 7; $j++ )
                                
                                @php
                                  $count1 = $count1+1;

                                  if($count1 > $package->duration){
                                      break;
                                  }
                                @endphp

                                <div class="tab-pane text-left fade @if($j == 0) show @endif @if($j == 0) active @endif" id="day-tab-{{$i}}-{{$j}}" role="tabpanel" aria-labelledby="vert-tabs-home-tab-{{$i}}-{{$j}}">

                                        {{$i+1}} week -- {{$j+1}} day
                                        <a href="javascript:;" class="float-right" @click="day_data_modal({{$count1}})"><i class="fas fa-plus-circle fa-2x"></i></a>

                                     
                                </div>



                                @endfor

                                {{--  <div class="tab-pane fade" id="vert-tabs-profile" role="tabpanel" aria-labelledby="vert-tabs-profile-tab">

                                </div>  --}}
                                
                                
                              </div>
                            </div>
                          </div>
                          












                </div>

                @endfor

              
              
            </div>
          </div>
          <!-- /.card -->
          <div class="row">
              <div class="col-12 text-center">
                  <a class="mb-3 btn btn-success" href="#" > <i class="fas fa-save"></i> Draft</a>
                  <a class="mb-3 btn btn-primary" href="#" > <i class="fas fa-sticky-note"></i> Save</a>
              </div>
          </div>
        </div>



      </div>




      
    <div class="modal fade" id="day_data_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Add New Day</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form @submit.prevent="save_day_data">
          <div class="modal-body" id="bodyModal">

            {{--  <div class="form-group row">
              <label class="col-3 col-form-label ">Exercises</label>
              <div class="col-9">
                <select class="multiselect-ui form-control d-none mt-2" id="workouts" multiple   v-model="selected_workouts" >
                  <option v-for="workout in workouts" :value="workout.id" > @{{ workout.name }} </option>
              </select>
            
            </div>
          </div>  --}}
      
          
          <div class="form-group row" v-for="(workout,index) in workouts" >
            <div class="col-3" >
              <label class="col-form-label" v-show="index == 0" >Exercises</label>
            </div>

              <div class="col-9"  >
                
                <div class="row">
                  <div class="col-4">
                    <input type="checkbox" :id="'workout'+workout.id" :value="workout.id" v-model="selected_workouts" />
                    <label :for="'workout'+workout.id" >@{{ workout.name }}</label> <a href="#"><i class="fas fa-eye"></i> </a>
    
                  </div>
                  <div class="col-4" v-if="selected_workouts.includes(workout.id)" >
                    <input type="text" class="form-control" v-model="selected_workouts_steps[index]" placeholder="Steps" />

                </div>
                  <div class="col-4" v-if="selected_workouts.includes(workout.id)">
                    <input type="text" class="form-control" v-model="selected_workouts_reps[index]" placeholder="Repititions" />

                </div>
                </div>
                
              </div>
              
            </div>

            <hr>
            <div class="row">

            <div class="col-3" >
              <label class="col-form-label">Meals</label>
            </div>
            
            <div class="col-9" >
              <v-select
                v-model="selected_meals"
                taggable
                multiple
                label="meals_array"
                :options="meals_array"
                
              />
            </div>
          </div>
          <hr>

          <div class="row" v-for="(meal,index) in selected_meals" >
            <div class="col-3"></div>
            <div class="col-9">
            <a class="btn btn-outline-primary w-100 m-1" data-toggle="collapse" :href="'#meal_details'+index" role="button" aria-expanded="false" aria-controls="collapseExample" onclick="check_for_editors()">
              @{{meal}}
            </a>

            <div class="collapse" :id="'meal_details'+index">
              <div class="card card-body">
                <div class="form-group">


                  <img id="image" src="{{asset('img/no-image-found.jpg')}}" class="profile-image" />
      
                  <button type="button" class="btn btn-outline-success" id="upload-button"> <i class="fas fa-image"></i> Upload Image </button>
                  
                  <input id="" type="file" class="file-upload" name="image" style="display:none" >
      
                </div>
                <div class="form-group">

                  <label>Title</label>

                  <input type="text" class="form-control" placeholder="Title" />
                </div>

                <div class="form-group">
                  <label>Summary</label>
                  <textarea class="form-control" rows="4" placeholder="Summary"></textarea>
                </div>
                
                <div class="form-group">
                  <label>What you will need</label>
                  <textarea   class="textarea form-control" ></textarea>
                </div>
 
                <div class="form-group">
                  <label>How to do it</label>
                  <textarea  class="textarea form-control" ></textarea>
                </div>
         

 
              </div>
            </div>
            

          </div>
        </div>

        <hr>



        <div class="row">

          <div class="col-3" >
            <label class="col-form-label">Supplement Timing</label>
          </div>
          
          <div class="col-9" >
            <v-select
              v-model="selected_supplements_timings"
              taggable
              multiple
              :options="supplement_timing"
            />
          </div>
        </div>
        <hr>


        <div v-for="(time,index) in selected_supplements_timings">
        <div class="row" >

          <div class="col-3" >
            <label class="col-form-label"> @{{time}} Supplements</label>
          </div>
          
          <div class="col-9" >
            <v-select
              v-model="selected_supplements[index]"
              taggable
              multiple
              :options="supplements"
              label="name"
            />
          </div>
        </div>

        <div class="row" v-for="supplement in selected_supplements[index]" >
          <div class="col-3"></div>
          <div class="col-9">

            <div class="form-group">

              <label> @{{supplement.name}} Prescription</label>

              <input type="text" class="form-control" v-model="supplement_prescription[index]" :placeholder="supplement.name+' Prescription'" />
            </div>


          {{--  <a class="btn btn-outline-primary w-100 m-1" data-toggle="collapse" :href="'#supplement'+index" role="button" aria-expanded="false" aria-controls="collapseExample">
            @{{supplement.name}}
          </a>  --}}

          {{--  <div class="collapse" :id="'supplement'+index">
            <div class="card card-body">
              
              <div class="form-group">

                <label>Perscription</label>

                <input type="text" class="form-control" placeholder="Title" />
              </div>


            </div>
          </div>  --}}
          

        </div>
      </div>
      <hr>
    </div>


        
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
        </div>
      </div>
    </div>
    










    </div>
  








@endsection

@push('script')

<script src="{{asset('assets/select-checkbox1/select.js')}}" ></script>


<script>
  

  function check_for_editors(){
    $('.textarea').summernote({
      height:150,
    })
    
  }



  $(function() {
    //$('#mySelect').multiselect();
     $('.multiselect-ui').multiselect({
      nonSelectedText: 'Select Workouts',
      enableFiltering: true,
      enableCaseInsensitiveFiltering: true,
    });
  });

  

  const plan = new Vue({
    el: '#plan',
   
    data: {
      day: null,
      workouts: '',
      selected_workouts:[],
      selected_workouts_steps:[],
      selected_workouts_reps:[],
      meals_array : [
        'Breakfast',
        'Lunch',
        'Dinner',
        'Snacks1',
        'Snacks2',
        'Snacks3',
      ],
      selected_meals:[],

      supplement_timing:[
        'Morning',
        'Evening',
        'Night',
      ],
      selected_supplements_timings: [],

      selected_supplements: [],
      supplements:[],
      supplement_prescription:[],

      form : new Form({
          data : 'empty',
      }),


    },


    methods: {
      get_all_data(){
        axios.get('/api/get_all_data')
        .then( response => {
          //console.log(response);
          this.workouts = response.data.workouts;
          this.supplements = response.data.supplements;
        } )
        .catch(error => {

        });
      },

      save_day_data(){

        console.log('post' , this.form)
        
        axios.post('/api/store_day_data',{
          client_id : {{$client->id}},
          package_id : {{$package->id}},
          day_count : this.day,
          selected_workouts : this.selected_workouts,
          selected_workouts_steps : this.selected_workouts_steps,
          selected_workouts_reps : this.selected_workouts_reps,
          selected_meals : this.selected_meals,
          selected_supplements_timings : this.selected_supplements_timings,
          selected_supplements : this.selected_supplements,
          supplement_prescription : this.supplement_prescription,
        });

        {{--  this.form.get('/api/store_day_data')
        .then( response => {
          console.log(response)
        } )
        .catch( error => { 
          console.log(error)

        } );  --}}

      },

      day_data_modal(day_count){
        console.log(day_count)
        this.day = day_count;

        $('#day_data_modal').modal('show');
      
      },
    
    },
    mounted() { 
      this.get_all_data();
      
      //console.log('vue is here!');
    },


    
 });



</script>
    @endpush