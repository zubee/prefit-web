
<html lang="en" style="height: auto;"><head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Dashboard | FTBLE</title>
  
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <link rel="stylesheet" href="{{asset('admin-dist/css/adminlte.min.css')}}">

<script src="{{asset('admin-dist/js/adminlte.js')}}" defer></script>


  <link rel="stylesheet" href="{{asset('admin-plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href=" {{asset('admin-plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('admin-plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{asset('admin-plugins/jqvmap/jqvmap.min.css')}}">
  <!-- Theme style -->
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('admin-plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('admin-plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('admin-plugins/summernote/summernote-bs4.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
 
 
  <!-- Toastr -->
  <link rel="stylesheet" href="{{ asset('admin-plugins/toastr/toastr.min.css')}}">
  
  
    
    <link rel="stylesheet" href="{{asset('css/app.css')}}" >

   <link id="noteanywherecss" media="screen" type="text/css" rel="stylesheet" href="data:text/css,.note-anywhere%20.closebutton%7Bbackground-image%3A%20url%28chrome-extension%3A//bohahkiiknkelflnjjlipnaeapefmjbh/asset/deleteButton.png%29%3B%7D%0A"> 

   {{-- summer note --}}
   <link rel="stylesheet" href="{{asset('admin-plugins/summernote/summernote-bs4.css')}}">


   <!-- Select2 -->
   <link rel="stylesheet" href="{{asset('admin-plugins/select2/css/select2.min.css')}}">
   <link rel="stylesheet" href="{{asset('admin-plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

  <link rel="stylesheet" href="{{asset('css/bootstrap-tagsinput.css')}}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/typeahead.js-bootstrap-css/1.2.1/typeaheadjs.min.css">


  <link href="{{asset('assets/lightbox/dist/css/lightbox.css')}}" rel="stylesheet" />
  
  <link href="{{asset('admin-plugins/chartist-js-master/dist/chartist.css')}}" rel="stylesheet" />

  <script src="{{asset('assets/lightbox/dist/js/lightbox.js')}}"></script>



  <style>
    .navbar-badge {
      position: initial;
    }
    .dropdown-menu{
      min-width: 280px;
    }

    .profile-image{
      display: inherit; 
      width:135px; 
      height: 135px;
      margin-bottom:10px;
      border-radius:3px;
      object-fit: cover;
     }

     .notification-drop{
       height: auto;
       max-height: 300px;
       overflow: hidden auto;
     }
     .navbar-badge {
      position: initial;
    }

    .notification-icons{
      margin-top: 5px;
    }
    #myChart {
      height: 100%;
      width: 100%;
    }

  .notification_rows {
        overflow-wrap: break-word;
        word-wrap: break-word;
        hyphens: auto;
        white-space: normal !important;
      
      }
      .profile-picture{
        width: 50px;
        height: 50px;
        object-fit: cover;
        max-height: 50px;
        border-radius: 50%;

      }
      #user-list td{
        vertical-align: middle;
      }
      
      #coach-list td{
        vertical-align: middle;
      }
      .package-card{
        height: 100%;
        padding: 5px 10px;
        border: 1px solid #d8d8d8;
        border-radius: 5px;
        box-shadow: 0px 2px 6px #00000014;
        box-sizing: content-box;
        }
        .package-card-text-area{
          height: 55%;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }
        .package-card-body{
            padding: 10px 5px 5px 5px;
            text-align: center;
        }
        .package-card-img{
            width: 100%;
            height: 200px;
        }
        .package-card-text-item{
            font-size: 15px;
        }
        
        .package-card-title-item{
            font-size: 20px;
        }
        .f-20{
          font-size: 20px;
        }
        .btn-outline-teal{
          background-color: transparent;
          border-color: #17a2b8;
          color : #17a2b8;
        }
        .btn-outline-teal:hover{
          background-color: #17a2b8;
          color : #ffffff;

        }
      .exercise-gif-image{
        border-radius: 5px;
        width: auto;
        height: auto;
        max-width: 160px;
      }


    
          
      .results {
        font-size: 0;
        padding-bottom: 16px;
      }
      .results-content {
        font-size: 13px;
        display: inline-block;
        vertical-align: top;
        background: url(https://i.stack.imgur.com/rwkqF.png) 0 0 no-repeat;
        background-size: 75px;
        width: 75px;
        height: 15px;
        cursor: pointer;
      }  
      .results .results-content span.stars span {
        background: url(https://i.stack.imgur.com/rwkqF.png) 0 -15px no-repeat;
        display: inline-block;
        height: 35px;
        background-size: 75px;
      }

      .head{
        display: flex;
      }
      #review-date{
        float: right;
        width: 100%;
        text-align: right;
      }
      



      {{--  display reviews  --}}
      .toggle-btn{
        cursor: pointer;
        color: white !important;
        }

        .review-head{
            display: flex;
        }
        .date{
            width: 100%;
            text-align: right;
        }
        .review-body{
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .review-description{
            border-top: 1px solid #f5f5f5;
            width: 88%;
            padding-top: 5px;
        }
        .review-user-image-box{
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .review-user-image{
            width: 60px;
            height: 60px;
            border-radius: 200%;
            margin-right: 10px;
        }
        .rating{
          width: 50%;
        }
        .review-user-name{
            font-size: 25px;
        }
        .review-count{
            background-color: white;
            border-radius: 200%;
            padding: 5px;
            color: black;
            width: 30px;
            height: 30px;
            display: flex;
            justify-content: center;
            align-items: center;
            margin-left: 15px;        
          }
        .review-top{
          display: flex;
          align-items: center;

        }
        .coach-profile-image{
            width: auto; 
            height: auto; 
            max-height: 140px; 
            max-width: 140px; 
            object-fit: cover;
            border-radius:100%;
        }
  
  </style>


    @yield('styles')

  @stack('style')
</head>
  <body class="sidebar-mini fixed " style="height: auto;">
  <div class="wrapper" id="wrapper" >

    @include('layouts.partials.swals')

  
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
        {{--  <li class="nav-item d-none d-sm-inline-block">
          <a href="index3.html" class="nav-link">Home</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="#" class="nav-link">Contact</a>
        </li>  --}}
      </ul>
  
      <!-- SEARCH FORM -->
      {{--  <form class="form-inline ml-3">
        <div class="input-group input-group-sm">
          <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-navbar" type="submit">
              <i class="fas fa-search"></i>
            </button>
          </div>
        </div>
      </form>  --}}
  
      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto" id="vue-notification" >
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-comments notification-icons"></i>
            <span class="badge badge-danger navbar-badge">@{{messages.length}}</span>
          </a>
          
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <a v-for="message in messages" :href="'/'+base_url+'/messages/'+message.id" class="dropdown-item"  >
              <!-- Message Start -->
              <div class="media">

                <img v-if="message.photo" :src="'{{asset('images/profile_images')}}/'+message.photo" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                
                <img v-else src="{{asset('img/no-image-found.jpg')}}" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                
                
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    @{{message.name}}

                  </h3>
                  <p class="text-sm" v-if="message.last_message && message.last_message.message" >@{{message.last_message.message.substr(0, 20)}}</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> @{{ message.last_message.time }}</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            
            @if( in_array(auth()->user()->role_id , [3,4] ))
            <a href="{{route('coach.messages')}}" class="dropdown-item dropdown-footer">See All Messages</a>
            @else
            <a href="{{route('admin.messages')}}" class="dropdown-item dropdown-footer">See All Messages</a>
            @endif
          </div>
        </li>
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-bell"></i>
            <span class="badge badge-warning navbar-badge">@{{notifications.length}}</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right notification-drop ">
            <span class="dropdown-header">@{{notifications.length}} Notifications</span>
            <div class="dropdown-divider"></div>

            <div  v-for="notification in notifications">
            <a :href="'/'+base_url + notification.link"class="dropdown-item notification_rows">
              <i class="fas fa-bell mr-2"></i> @{{notification.title}}
              <span class="float-right text-muted text-sm">@{{notification.time}}</span>
            </a>
            <div class="dropdown-divider"></div>
          </div>
            {{--  <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-users mr-2"></i> 8 friend requests
              <span class="float-right text-muted text-sm">12 hours</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-file mr-2"></i> 3 new reports
              <span class="float-right text-muted text-sm">2 days</span>
            </a> 
            <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>  --}}
          </div>
        </li>
        {{--  <li class="nav-item">
          <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i class="fas fa-th-large"></i></a>
        </li>  --}}
      </ul>
    </nav>
    <!-- /.navbar -->
  
    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="{{url('/admin')}}" class="brand-link">

        <img src="/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">FTBLE</span>
      </a>
  
      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
             {{--  @if( auth()->user() && isset(auth()->user()->avatar) )
            <img src="{{asset('/user_avatar/'.auth()->user()->avatar)}}" class="img-circle elevation-2" alt="User Image">
            @else 
            @endif --}} 
            <img src="@if(auth()->user()->photo) {{asset('images/profile_images/'.auth()->user()->photo)}} @else {{asset('img/no-image-found.jpg')}} @endif"  class="img-circle elevation-2" alt="User Image" style="width: 50px;">

            {{-- <img src="/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image"> --}}

          </div>
          <div class="info" style="display: flex; align-items: center;">
            {{--  <a href="{{route('admin.user.profile' , auth()->user()->id )}}" class="d-block">{{auth()->user()->name ?? '' }}</a>  --}}
            <a href="@if((auth()->user()->role_id == 3 || auth()->user()->role_id == 4)) {{route('coach.profile')}} @else # @endif" class="d-block">{{auth()->user()->name}}</a>
          </div>
        </div>
        
        {{--  @if (strpos(url()->current() , '/dashboard')) active }} @endif  --}}

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
                 with font-awesome or any other icon font library -->
            @if( in_array(auth()->user()->role_id , [1,2] ))
            <li class="nav-item">
              <a href="{{route('admin.index')}}" class="nav-link {{ request()->is('admin') ? 'active' : '' }}">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                  Dashboard
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('admin.admins.index')}}" class="nav-link {{ strpos(url()->current() , '/admins') ? 'active' : '' }} ">
                <i class="nav-icon fas fa-users-cog"></i>
                <p>
                    Manage Admin
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('admin.users.index')}}" class="nav-link {{ strpos(url()->current() , '/users') ? 'active' : '' }} ">
                <i class="nav-icon fas fa-user-plus"></i>
                <p>
                    Manage Users
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('admin.coach.index')}}" class="nav-link {{ strpos(url()->current() , '/coach') ? 'active' : '' }} ">
                <i class="nav-icon fas fa-running"></i>
                <p>
                    Manage Coaches
                </p>
              </a>
            </li>
            
            {{-- <li class="nav-item">
              <a href="{{route('admin.dietitian.index')}}" class="nav-link {{ strpos(url()->current() , '/dietitian') ? 'active' : '' }} ">
                <i class="nav-icon fas fa-dumbbell"></i>
                <p>
                    Manage Dietitian
                </p>
              </a>
            </li>
           <li class="nav-item">
              <a href="{{route('admin.trainer.index')}}" class="nav-link {{ strpos(url()->current() , '/trainer') ? 'active' : '' }} ">
                <i class="nav-icon fas fa-running"></i>
                <p>
                    Manage Trainers
                </p>
              </a>
            </li> --}}
            <li class="nav-item">
              <a href="{{route('admin.messages')}}" class="nav-link {{ strpos(url()->current() , '/messages') ? 'active' : '' }} ">
                <i class="nav-icon fas fa-envelope"></i>
                <p>
                  Messages
                </p>
                <span id="new-messages-badge" class="badge badge-danger float-right">0</span>
              </a>
            </li>
            
            <li class="nav-item">
              <a href="{{route('admin.contact')}}" class="nav-link {{ strpos(url()->current() , '/contact') ? 'active' : '' }} ">
                <i class="nav-icon fas fa-phone-alt"></i>
                <p>
                  Contact Information
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('admin.questionnaires.index')}}" class="nav-link {{ strpos(url()->current() , '/questionnaires') ? 'active' : '' }} ">
                <i class="nav-icon fas fa-question"></i>
                <p>
                    Manage Questionnaire
                </p>
              </a>
            </li>
            {{--  <li class="nav-item">
              <a href="{{route('admin.financial-reports')}}" class="nav-link {{ strpos(url()->current() , '/financial-reports') ? 'active' : '' }} ">
                <i class="nav-icon fas fa-chart-line"></i>
                <p>
                  Financial Reports
                </p>
              </a>
            </li>  --}}
            
            <li class="nav-item">
              <a href="{{route('admin.packages.index')}}" class="nav-link {{ strpos(url()->current() , '/packages') ? 'active' : '' }} ">
                <i class="nav-icon fas fa-cubes"></i>
                <p>
                  Manage Packages
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('admin.tags.index')}}" class="nav-link {{(request()->is('admin/tags*')) ? 'active' : ''}}">
                <i class="nav-icon fas fa-tags"></i>
                <p>
                  Manage Tags
                </p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('admin.supplements.index')}}" class="nav-link {{(request()->is('admin/supplements*'))? 'active' : ''}}">
                <i class="nav-icon fas fa-table"></i>
                <p>Manage Supplements</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{route('admin.workouts.index')}}" class="nav-link {{(request()->is('admin/workouts*'))? 'active' : ''}}">
                <i class="nav-icon fas fa-skating"></i>
                <p>Workouts</p>
              </a>
            </li>
         
            @endif

            @if( in_array(auth()->user()->role_id , [3,4] ))
              
            <li class="nav-item">
                <a href="{{route('coach.index')}}" class="nav-link {{ request()->is('coach') || request()->is('coach/dashboard') ? 'active' : '' }} ">
                  <i class="nav-icon fas fa-tachometer-alt"></i>
                  <p>
                    Dashboard
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('coach.packages')}}" class="nav-link {{ strpos(url()->current() , '/packages') ? 'active' : '' }} ">
                  <i class="nav-icon fas fa-cubes"></i>
                  <p>
                    Packages
                  </p>
                </a>
              </li>
              {{--  <li class="nav-item">
                <a href="{{route('coach.financial-reports')}}" class="nav-link {{ strpos(url()->current() , '/financial-reports') ? 'active' : '' }} ">
                  <i class="nav-icon fas fa-chart-line"></i>
                  <p>
                    Financial Reports
                  </p>
                </a>
              </li>  --}}

              <li class="nav-item">
                <a href="{{route('coach.questionnaires')}}" class="nav-link {{ strpos(url()->current() , 'questionnaires') && !strpos(url()->current() , 'client') ? 'active' : '' }} ">
                  <i class="nav-icon fas fa-question"></i>
                  <p>
                    Questionnaire
                  </p>
                </a>
              </li>
              
              <li class="nav-item">
                <a href="{{route('coach.client-questionnaires.index')}}" class="nav-link {{ strpos(url()->current() , 'questionnaires') && strpos(url()->current() , 'client') ? 'active' : '' }} ">
                  <i class="nav-icon fas fa-question-circle"></i>
                  <p>
                    Client Questionnaire
                  </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{route('coach.manage-clients')}}" class="nav-link {{ strpos(url()->current() , '/manage-client') ? 'active' : '' }} ">
                  <i class="nav-icon fas fa-user-check"></i>
                  <p>
                    Manage Clients
                  </p>
                </a>
              </li>
            </li>
            <li class="nav-item">
              <a href="{{route('coach.messages')}}" class="nav-link {{ strpos(url()->current() , '/messages') ? 'active' : '' }} ">
                <i class="nav-icon fas fa-envelope"></i>
                <p>
                  Messages
                </p>
                <span id="new-messages-badge" class="badge badge-danger float-right">0</span>
              </a>
            </li>
              @endif

         
              
              <li class="nav-item">
                  <a href="{{route('logout')}}" class="nav-link {{(request()->is('admin/logout*'))? 'active' : ''}}"   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">

                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                      <i class="nav-icon fas fa-sign-out-alt"></i>
                      <p>Logout</p>
                  </a>
              </li>

              {{--  <li class="nav-item">
                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="nav-link">
                  <i class="nav-icon fas fa-power-off"></i>
                  <p>
                    Logout
                  </p>
                </a>
             </a>

             <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                 @csrf
             </form>

              </li>  --}}

          </ul>
        </nav>
      </div>
      <!-- /.sidebar -->
    </aside>
  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="min-height: 211px;">

      <section class="content" >
        <div class="container-fluid" >
      @yield('content')

        </div>
      </section>
    </div>
    <!-- /.content-wrapper -->
  
    <!-- Control Sidebar -->
    {{--  <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
      <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
      </div>
    </aside>  --}}
    <!-- /.control-sidebar -->
  
    <!-- Main Footer -->
    <footer class="main-footer">
      <!-- To the right -->
      <div class="float-right d-none d-sm-inline">
        Anything you want
      </div>
      <!-- Default to the left -->
      <strong>Copyright © 2020 <a href="#">FTBLE</a>.</strong>
    </footer>
  <div id="sidebar-overlay"></div></div>
  <!-- ./wrapper -->
  
  <!-- REQUIRED SCRIPTS -->
  
  <!-- jQuery -->
  {{--  <script src="plugins/jquery/jquery.min.js"></script>  --}}
  <!-- Bootstrap 4 -->
  {{--  <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>  --}}
  <!-- AdminLTE App -->
  {{--  <script src="dist/js/adminlte.min.js"></script>  --}}

  {{-- <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script> --}}

<script src="{{asset('js/app.js')}}"  ></script>

<script src="{{asset('admin-plugins/jquery-ui/jquery-ui.min.js')}}"></script>

<script src="{{asset('admin-plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Toastr -->
<script src="{{ asset('admin-plugins/toastr/toastr.min.js')}}"></script>

<!-- ChartJS -->
<script src="{{asset('admin-plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('admin-plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->
<script src="{{asset('admin-plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('admin-plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('admin-plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('admin-plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('admin-plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('admin-plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('admin-plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('admin-plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>

<script src="{{asset('admin-plugins/summernote/summernote-bs4.min.js')}}"></script>

<!-- Select2 -->
<script src="{{asset('admin-plugins/select2/js/select2.full.min.js')}}"></script>

<script src="{{asset('admin-plugins/chartist-js-master/dist/chartist.js')}}"></script>

  <script src="{{asset('js/typeahead.bundle.js')}}"></script>

  {{--  <script src="{{asset('js/bootstrap-tagsinput.js')}}"></script>
  <script src="{{asset('js/bootstrap-tagsinput-angular.js')}}"></script>  --}}


  
  
  
  <script src="{{asset('admin-dist/js/pages/dashboard.js')}}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{asset('admin-dist/js/demo.js')}}"></script> 
  
  {{--  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>  --}}

 

  <script>
  $(function () {
    // Summernote
    $('.textarea').summernote({
      height:250,
    })
    
    $('.specific-textarea').summernote({
      toolbar: [
        // [groupName, [list of button]]
        //['style', ['bold', 'italic', 'underline', 'clear']],
        //['font', ['strikethrough', 'superscript', 'subscript']],
        //['fontsize', ['fontsize']],
        //['color', ['color']],
        //['height', ['height']]
        ['para', ['ul', 'ol']],
      ]
    })

    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4',
      placeholder:'Select From Options',
    })


  });




  
  $(document).ready(function() {

    
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    

    $(".file-upload").on('change', function(){
        readURL(this);
    });
    
    $("#upload-button").on('click', function() {
       $(".file-upload").click();
    });
});



$.fn.stars = function() { 
  return this.each(function() {
    // Get the value
    var val = parseFloat($(this).html()); 
    // Make sure that the value is in 0 - 5 range, multiply to get width
    var size = Math.max(0, (Math.min(5, val))) * 24; 
    // Create stars holder
    var $span = $('<span> </span>').width(size); 
    // Replace the numerical value with stars
    $(this).empty().append($span);
  });
}

$(function() {
  console.log("Calling stars()");
  $('.results-content span.stars').stars();
});
  
</script>

<script>
  const vue_notification = new Vue({
    el: '#vue-notification',
    data: {
      @if( in_array(auth()->user()->role_id , [3,4] ))
      base_url : 'coach',
      @else
      base_url : 'admin',
      @endif
      current_url: "{{url()->current()}}",
      messages : [],
      notifications: [],
    },
    methods: {
      update_notifications(){
        
        axios.get('/get/notifications/{{auth()->user()->id}}')
        .then(res => { 
          //console.log(res) 
          this.messages = res.data.messages;
          this.notifications = res.data.notifications;
          this.showNewMessagesBadge();
        })
        .catch(err => { console.log(err) });
  
      },
      showNewMessagesBadge(){
        if(this.messages.length > 0 && !this.current_url.includes('messages')){
          //console.log('show badge')
          $('#new-messages-badge').html(this.messages.length);
          $('#new-messages-badge').show();
        }else{
          $('#new-messages-badge').hide();
          //console.log('hide badge')
        }
        //console.log(current_url.contains('messages'))
        //current_url.contains('messages')
      },
    },
    mounted(){
      //console.log('dashboard');
      
      this.update_notifications();
      setInterval(() => {
        this.update_notifications();
      } , 15000);
    },
  });
</script>


{{--  <script src="https://cdn.anychart.com/releases/8.7.1/js/anychart-base.min.js" type="text/javascript"></script>

<script src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>  --}}

@yield('scripts')
@stack('script')




{{--  <script src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>  --}}

{{--  <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha512-s+xg36jbIujB2S2VKfpGmlC3T5V2TF3lY48DX7u2r9XzGzgPsa6wTpOQA7J9iffvdeBN0q9tKzRxVxw1JviZPg==" crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css" integrity="sha512-/zs32ZEJh+/EO2N1b0PEdoA10JkdC3zJ8L5FTiQu82LR9S/rOQNfQN7U59U9BC12swNeRAz3HSzIL2vpp4fv3w==" crossorigin="anonymous" />
    --}}



  </body>
  </html>
