

<table>
    <tr>
        <th style="width: 10px;" >#</th>
        <th style="width: 20px;" >Name</th>
        <th style="width: 20px;" >Amount</th>
        <th style="width: 20px;" >Percentage</th>
        <th style="width: 20px;" >Payable Amount</th>
        <th style="width: 20px;" >Coach Name</th>
        <th style="width: 20px;" >Create Date</th>
        <th style="width: 20px;" >Paid Date</th>
    </tr>


    @foreach($data as $k => $payment)
    <tr>
        <td> {{$k+1}} </td>
        <td> {{$payment->client->name}} </td>
        <td>{{$payment->amount}} $</td>
        <td>{{$payment->coach_percentage}} % </td>
        <td>{{ ( $payment->amount / 100) * $payment->coach_percentage }} $ </td>
        <td>{{$payment->coach->user->name}}</td>
        <td> {{$payment->created_at->format('m/d/Y') }} </td>
        <td>
            @if($payment->status == 1) {{$payment->payment_clear_date }}
            @else 
            Not Paid
            @endif
        </td>
        </tr>
    @endforeach
</table>