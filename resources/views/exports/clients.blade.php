

<table>
    <tr>
        <th style="width: 10px;" >#</th>
        <th style="width: 20px;" >Name</th>
        <th style="width: 30px;" >Email</th>
        <th style="width: 20px;" >Package</th>
        <th style="width: 20px;" >Start Date</th>
        <th style="width: 20px;" >End Date</th>
        <th style="width: 20px;" >Payment</th>
    </tr>

    @foreach($data as $k => $client)

    @foreach($client->subscribed_packages->where('pivot.coach_id' , $coach->id) as $k => $package)

    @php
        $plan = $package->plans->where('coach_id' , $coach->id )->where('client_id' , $client->id )->first();
    @endphp

    <tr>
        <td> {{$k}} </td>
        <td> {{$client->name}} </td>
        <td> {{$client->email}} </td>
        <td> {{$package->name}} </td>
        <td> {{ isset($plan) && $plan->start_status ? $plan->start_date : '' }} </td>
        <td> {{ isset($plan) && $plan->start_status ? Carbon\Carbon::parse($plan->start_date)->addDays($package->duration) : '' }} </td>
        <td> {{isset($plan) && $plan->payment ? 'YES' : 'No' }} </td>
    </tr>
    @endforeach
    @endforeach
</table>