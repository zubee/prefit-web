@extends('layouts.auth.admin')

@push('style')


    @endpush


    @section('title')
    <a href="/"><b>FTBLE </b>COACH</a>
        
    @endsection


    @section('content')


    <p class="login-box-msg">Sign in</p>

    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div class="input-group mb-3">
            <input type="text" class="form-control  @error('username') is-invalid @enderror input-fields" name="username" value="{{ old('username') }}" required placeholder="Username">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-envelope"></span>
                </div>
            </div>
            @error('username')
            <span class="invalid-feedback" role="alert">
          <strong>{{ $message }}</strong>
        </span>
            @enderror
        </div>
        <div class="input-group mb-3">
            <input type="password" class="form-control @error('password') is-invalid @enderror input-fields" name="password" required  placeholder="Password">
            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock"></span>
                </div>
            </div>
            @error('password')
            <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
            @enderror
        </div>
        <div class="row">
            <div class="col-8">

            </div>
            <!-- /.col -->
            <input type="hidden" name="role" value="3" />

            <div class="col-4">
                <button type="submit" class="btn btn-primary btn-block">Sign In</button>
            </div>
            <!-- /.col -->
        </div>
    </form>

    <p class="mb-1">
        {{--  <a href="{{route('password.request')}}">I forgot my password</a>  --}}
    </p>

@endsection
