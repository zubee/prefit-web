/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


import Vue from 'vue'
window.Vue = require('vue');

import { Form, HasError, AlertError } from 'vform';
window.Form = Form;
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);


import Swal from 'sweetalert2';
window.Swal = Swal;

import Sweel from 'sweetalert';
window.Sweel = Sweel;

import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';

Vue.component('v-select', vSelect)

import Loading from 'vue-loading-overlay';
// Import stylesheet
import 'vue-loading-overlay/dist/vue-loading.css';
Vue.use(Loading, {
    // props
    color: 'red',
  })


import VueProgressBar from 'vue-progressbar';
window.VueProgressBar = VueProgressBar;

Vue.use(VueProgressBar, {
  color: 'rgb(143, 255, 199)',
  failedColor: 'red',
  height: '2px'
})


const options = {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '2px',
    thickness: '8px',
    // transition: {
    //   speed: '0.2s',
    //   opacity: '0.6s',
    //   termination: 300
    // },
  }

Vue.use(VueProgressBar, options)



import VueLoadingButton from 'vue-loading-button';
Vue.component('VueLoadingButton', VueLoadingButton)

// import CKEditor from '@ckeditor/ckeditor5-vue';
// import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
// Vue.use( CKEditor , ClassicEditor );


import wysiwyg from "vue-wysiwyg"; 
Vue.use(wysiwyg, {}); // config is optional. more below


// Vue.component('tiptap_texteditor', require('./components/text-editor.vue').default);
// Vue.component('tiptap-editor', require('./components/text-editor.vue').default);
// Vue.component('summernote-editor', require('./components/vue-summernote.vue').default);


import VueChatScroll from 'vue-chat-scroll'
Vue.use(VueChatScroll)


import loading from 'jquery-easy-loading';
window.loading = loading;


Vue.component('exercise-search', require('./components/exerciseSearch.vue').default);
// Vue.component('show-plan', require('./components/showPlan.vue').default);
// Vue.component('client-plan', require('./components/clientPlan.vue').default);
// Vue.component('plan-workouts', require('./components/planWorkouts.vue').default);
// Vue.component('plan-measurements', require('./components/planMeasurements.vue').default);
// Vue.component('plan-meals', require('./components/planMeals.vue').default);
// Vue.component('plan-supplements', require('./components/planSupplements.vue').default);
Vue.component('component-loader', require('./components/componentLoader.vue').default);
Vue.component('spinner', require('./components/spinner.vue').default);

Vue.component('question-element', require('./components/questionElement.vue').default);
Vue.component('client-questionnaire', require('./components/clientQuestionnaire.vue').default);
Vue.component('client-questionnaires', require('./components/questionnaires.vue').default);


import VueToastr2 from 'vue-toastr-2'
import 'vue-toastr-2/dist/vue-toastr-2.min.css'

window.toastr = require('toastr')

Vue.use(VueToastr2)


const vue = new Vue({
  el: '#vue',
});

const vue_app= new Vue({
  el: '#vue-app',
});


let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.post['X-CSRF-TOKEN'] = token.content;
    // window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}




// Vue.component(Editor);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
