"use strict";

var _vue = _interopRequireDefault(require("vue"));

var _vform = require("vform");

var _sweetalert = _interopRequireDefault(require("sweetalert2"));

var _sweetalert2 = _interopRequireDefault(require("sweetalert"));

var _vueSelect = _interopRequireDefault(require("vue-select"));

require("vue-select/dist/vue-select.css");

var _vueLoadingOverlay = _interopRequireDefault(require("vue-loading-overlay"));

require("vue-loading-overlay/dist/vue-loading.css");

var _vueProgressbar = _interopRequireDefault(require("vue-progressbar"));

var _vueLoadingButton = _interopRequireDefault(require("vue-loading-button"));

var _vueWysiwyg = _interopRequireDefault(require("vue-wysiwyg"));

var _vueChatScroll = _interopRequireDefault(require("vue-chat-scroll"));

var _jqueryEasyLoading = _interopRequireDefault(require("jquery-easy-loading"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

window.Vue = require('vue');
window.Form = _vform.Form;

_vue["default"].component(_vform.HasError.name, _vform.HasError);

_vue["default"].component(_vform.AlertError.name, _vform.AlertError);

window.Swal = _sweetalert["default"];
window.Sweel = _sweetalert2["default"];

_vue["default"].component('v-select', _vueSelect["default"]);

_vue["default"].use(_vueLoadingOverlay["default"], {
  // props
  color: 'red'
});

window.VueProgressBar = _vueProgressbar["default"];

_vue["default"].use(_vueProgressbar["default"], {
  color: 'rgb(143, 255, 199)',
  failedColor: 'red',
  height: '2px'
});

var options = {
  color: 'rgb(143, 255, 199)',
  failedColor: 'red',
  height: '2px',
  thickness: '8px' // transition: {
  //   speed: '0.2s',
  //   opacity: '0.6s',
  //   termination: 300
  // },

};

_vue["default"].use(_vueProgressbar["default"], options);

_vue["default"].component('VueLoadingButton', _vueLoadingButton["default"]); // import CKEditor from '@ckeditor/ckeditor5-vue';
// import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
// Vue.use( CKEditor , ClassicEditor );


_vue["default"].use(_vueWysiwyg["default"], {}); // config is optional. more below
// Vue.component('tiptap_texteditor', require('./components/text-editor.vue').default);
// Vue.component('tiptap-editor', require('./components/text-editor.vue').default);
// Vue.component('summernote-editor', require('./components/vue-summernote.vue').default);


_vue["default"].use(_vueChatScroll["default"]);

window.loading = _jqueryEasyLoading["default"];

_vue["default"].component('exercise-search', require('./components/exerciseSearch.vue')["default"]);

_vue["default"].component('component-loader', require('./components/componentLoader.vue')["default"]);

_vue["default"].component('spinner', require('./components/spinner.vue')["default"]);

var vue = new _vue["default"]({
  el: '#vue'
}); // Vue.component(Editor);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */