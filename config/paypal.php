<?php

return [ 
    'client_id' => env('PAYPAL_CLIENT_ID',''),
    'secret' => env('PAYPAL_SECRET',''),
    'settings' => array(
        'mode' => env('PAYPAL_MODE','sandbox'),
        'http.ConnectionTimeOut' => 30,
        'log.LogEnabled' => true,
        'log.FileName' => storage_path() . '/logs/paypal.log',
        'log.LogLevel' => 'ERROR'
    ),
];

// return [
//     'mode'    => 'sandbox', // Can only be 'sandbox' Or 'live'. If empty or invalid, 'live' will be used.
//     'sandbox' => [
//         'username'    => env('PAYPAL_SANDBOX_API_USERNAME_', ''),
//         'password'    => env('PAYPAL_SANDBOX_API_PASSWORD_', ''),
//         'secret'      => env('PAYPAL_SANDBOX_SECRET', ''),
//         'certificate' => "MIIEwTCCAqmgAwIBAgIIasMJN7PIYoIwDQYJKoZIhvcNAQELBQAwgYsxIjAgBgNVBAMMGVNhbmRib3hfTWVyY2hhbnRJc3N1aW5nQ0ExGjAYBgNVBAsMEVBsYXRmb3JtIFNlY3VyaXR5MRQwEgYDVQQKDAtQYXlQYWwgSW5jLjERMA8GA1UEBwwIU2FuIEpvc2UxEzARBgNVBAgMCkNhbGlmb3JuaWExCzAJBgNVBAYTAlVTMB4XDTIwMDgwNTEwNTYzNFoXDTIzMDgwNTEwNTYzNFowNDEyMDAGA1UEAwwpc2ItOHFmemIyNTc2NzE5X2FwaTEuYnVzaW5lc3MuZXhhbXBsZS5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCTxXPsj/JcKQahdxwdAUVwGtPBvJ8SeFST3OSovEyrUoX4bULSDvyHQ3QH/cX+CpCiOXz0XHRxwdc/p4mdYcuGvq7wuWBA//l4eJiiPNM2XQCfXZMR4tfF0bkgWAUk/5dVIL5z6kcGBPRl1tXL6BBsyMA4GA1UdDwEB/wQEAwIF4DAdBgNVHSUEFjAUBggrBgEFBQcDAgYIKwYBBQUHAwQwDQYJKoZIhvcNAQELBQADggIBAFUU0PGjWi8PO+2T6dWlcKlLScpXBKRCBhBSzXSpJm4eSWe/gp/QDNV/z+LsGmJVmaGSYoxDjYzI02rEAx0OecRrmu9Gp13JrdcfpgfHZxK0x2IkUuQSECK1K5imtWp3LekvyCWs3l61kHSAeUF6XnbeMT9Zk6Kyn0qHJtwAfNIkCbJCmPKED4NfKsABXEXfiG0sQ9RKvOXkvT8HcSi6xmapAkX/tAwTZSxIC1vs5+L6/SFAXJQDanQbdakyRZObJnDectQRoUtiwm7VXd7HCQ34x6ezji5k2tGleWSbnNt1xG7ngpsvFpLof6pMMsLJi6wg0OHyoNJujvoHWu+AtXCtK60jMEQ/IdnEY4Hp2IG5YdgDlPo9H8wD27/CIogkMC0tDwzBc6Dj9/5ZEPITSWXE5KTmmqlEfOObJtBvNiWYFyelMG2dLQl5ZOm9U38vb9KWPxMUmTWyo4KJeNfUX66QRA5Dc9ovTK5ViccMKJetRbEpWY7eNtQw543M9oO5NDjmPT94mQS8RG+iGvJ7LrocLoBtHtxem2w++3/uI6hvsLoqi5KvIXrAMwrQzM3HwllcRwl7hMry3MkLHCCS6mJOqYQAyqkgaZarX9wUU0/pvMiDZ6Wn7+eLjKjTM1t4fDLaov5M7TanZJKAjQ9susZ0oRBqcNg6Tc5/XPzTKkR",
//         'app_id'      => 'APP-80W284485P519543T', // Used for testing Adaptive Payments API in sandbox mode
//     ],
//     'live' => [
//         'username'    => env('PAYPAL_LIVE_API_USERNAME', ''),
//         'password'    => env('PAYPAL_LIVE_API_PASSWORD', ''),
//         'secret'      => env('PAYPAL_LIVE_API_SECRET', ''),
//         'certificate' => env('PAYPAL_LIVE_API_CERTIFICATE', ''),
//         'app_id'      => '', // Used for Adaptive Payments API
//     ],

//     'payment_action' => 'Sale', // Can only be 'Sale', 'Authorization' or 'Order'
//     'currency'       => 'USD',
//     'notify_url'     => '', // Change this accordingly for your application.
//     'locale'         => '', // force gateway language  i.e. it_IT, es_ES, en_US ... (for express checkout only)
//     'validate_ssl'   => false, // Validate SSL when creating api client.
//     // 'validate_ssl'   => true, // Validate SSL when creating api client.
// ];